package com.ober.libpng;

import android.graphics.Bitmap;
import androidx.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * 不要混淆它..
 * Created by ober on 2019/4/12.
 */
public class OPngNative {

    /* 同步PngBitmap.h中的常量宏定义*/
    /*----------------#defines---------------------*/
    @IntDef({PNG_FORM_RGBA, PNG_FORM_RGB,
            PNG_FORM_PALETTE, PNG_FORM_GRAY,
            PNG_FORM_GRAY_ALPHA})
    @Retention(RetentionPolicy.SOURCE)
    public @interface PNG_COLOR_TYPE {}

    public static final int PNG_FORM_RGBA = 6;
    public static final int PNG_FORM_RGB = 2;
    public static final int PNG_FORM_PALETTE = 3;
    public static final int PNG_FORM_GRAY = 0;
    public static final int PNG_FORM_GRAY_ALPHA = 4;


    @IntDef({OPNG_DATA_FORMAT_RGBA, OPNG_DATA_FORMAT_ARGB,
    OPNG_DATA_FORMAT_ABGR, OPNG_DATA_FORMAT_BGRA})
    @Retention(RetentionPolicy.SOURCE)
    public @interface OPNG_DATA_FORMAT {}

    public static final int OPNG_DATA_FORMAT_RGBA = 1;
    public static final int OPNG_DATA_FORMAT_ARGB = 2;
    public static final int OPNG_DATA_FORMAT_ABGR = 3;
    public static final int OPNG_DATA_FORMAT_BGRA = 4;


    @IntDef({OPNG_RESULT_FAIL, OPNG_RESULT_SUCCESS,
    OPNG_RESULT_BAD_FILE, OPNG_RESULT_BAD_FILE_CONTENT,
    OPNG_RESULT_BAD_FILE_HEAD, OPNG_RESULT_BAD_ARGUMENT})
    @Retention(RetentionPolicy.SOURCE)
    public @interface OPNG_RESULT_CODE{}

    public static final int OPNG_RESULT_FAIL = -1;
    public static final int OPNG_RESULT_SUCCESS = 0;
    public static final int OPNG_RESULT_BAD_FILE = 1;
    public static final int OPNG_RESULT_BAD_FILE_CONTENT = 2;
    public static final int OPNG_RESULT_BAD_FILE_HEAD = 3;
    public static final int OPNG_RESULT_BAD_ARGUMENT = 5;
    /*--------------#defines end---------------*/

    @OPNG_RESULT_CODE
    public static native int createPngBitmapFromFile(String jFilePath, long[] pngBitmapPtr);

    @OPNG_RESULT_CODE
    public static native int drawAndroidBitmap(long pngBitmapPtr,
                                                     Bitmap jbmp);
    @OPNG_RESULT_CODE
    public static native int recyclePngBitmap(long pngBitmapPtr);

    @OPNG_RESULT_CODE
    public static int getPngBitmapInfoStruct(long pngBitmapPtr,
                                             OPngInfoStruct infoStruct) {
        int[] infodata = new int[4];
        int r = getPngBitmapInfoData(pngBitmapPtr, infodata);
        if(r == OPNG_RESULT_SUCCESS) {
            infoStruct.setData(infodata);
            return r;
        } else {
            return r;
        }
    }

    @OPNG_RESULT_CODE
    private static native int getPngBitmapInfoData(long pngBitmapPtr,
                                                int[] infodata);

}
