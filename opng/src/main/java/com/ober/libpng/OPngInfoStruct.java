package com.ober.libpng;

/**
 * Created by ober on 2019/4/16.
 */
public final class OPngInfoStruct {

    private int[] infodata;
    //w,h,data_format,color_type

    public OPngInfoStruct() {

    }

    void setData(int[] infodata) {
        this.infodata = infodata;
    }

    public final boolean hasData() {
        return infodata != null;
    }

    public final int getWidth() {
        return infodata[0];
    }

    public final int getHeight() {
        return infodata[1];
    }

    @OPngNative.OPNG_DATA_FORMAT
    public final int getDataFormat() {
        return infodata[2];
    }

    @OPngNative.PNG_COLOR_TYPE
    public final int getColorType() {
        return infodata[3];
    }

}
