//
// Created by ober on 2019/4/12.
//

#include "opng_android_bridge.h"
#include "PngBitmap.h"
#include <android/bitmap.h>
#include <stdio.h>
#include <cstring>
#include <android/log.h>

#ifdef __cplusplus
#define EXTERN extern "C"
#else
#define EXTERN
#endif


EXTERN jint Java_com_ober_libpng_OPngNative_createPngBitmapFromFile(JNIEnv *env, jclass jtype,
                                                             jstring jFilePath,
                                                             jlongArray out) {

    if(out == NULL || jFilePath == NULL) {
        return OPNG_RESULT_BAD_ARGUMENT;
    }

    jsize len = env -> GetArrayLength(out);
    if(len == 0) {
        return OPNG_RESULT_BAD_ARGUMENT;
    }

    const char *filepath = env->GetStringUTFChars(jFilePath, NULL);
    FILE *fp = fopen(filepath, "r");
    env->ReleaseStringUTFChars(jFilePath, filepath);

    if(fp == NULL) {
        return OPNG_RESULT_BAD_ARGUMENT;
    }

    int result;
    PngBitmap *png = PngBitmap::new_from_file(fp, OPNG_DATA_FORMAT_ABGR, result);

    if(result == OPNG_RESULT_SUCCESS) {
        jlong* outarr = env -> GetLongArrayElements(out, NULL);
        outarr[0] = (long)png;
        env -> ReleaseLongArrayElements(out, outarr, 0);
        return OPNG_RESULT_SUCCESS;
    } else {
        return result;
    }

}

EXTERN jint Java_com_ober_libpng_OPngNative_drawAndroidBitmap(JNIEnv *env, jclass jtype, jlong pngBitmapPtr,
                                                      jobject jbmp) {

    if(pngBitmapPtr == NULL) {
        return OPNG_RESULT_BAD_ARGUMENT;
    }

    PngBitmap *pngBitmap = (PngBitmap *) pngBitmapPtr;
    uint8_t format;
    if (pngBitmap->getDataFormat(format) != OPNG_RESULT_SUCCESS) {
        return OPNG_RESULT_FAIL;
    }

    AndroidBitmapInfo info;
    AndroidBitmap_getInfo(env, jbmp, &info);
    int size[2];
    pngBitmap->getSize(size);

    if (!(info.width == size[0] && info.height == size[1])) {
        return OPNG_RESULT_BAD_ARGUMENT;
    }

    uint32_t *data;
    pngBitmap->getData(&data);
    uint32_t *pixels;
    AndroidBitmap_lockPixels(env, jbmp, (void **) &pixels);

    memcpy(pixels, data, info.width * info.height * sizeof(uint32_t));

    AndroidBitmap_unlockPixels(env, jbmp);

    return OPNG_RESULT_SUCCESS;
}

EXTERN jint Java_com_ober_libpng_OPngNative_recyclePngBitmap(JNIEnv *env, jclass jtype, jlong pngBitmapPtr) {

    if(pngBitmapPtr == NULL) {
        return OPNG_RESULT_FAIL;
    }

    PngBitmap *pngBitmap = (PngBitmap *) pngBitmapPtr;
    PngBitmap::recycle(pngBitmap);

    return OPNG_RESULT_SUCCESS;
}

EXTERN jint Java_com_ober_libpng_OPngNative_getPngBitmapInfoData(JNIEnv *env, jclass jtype, jlong pngBitmapPtr,
                                                     jintArray outJArray) {

    if(pngBitmapPtr == NULL) {
        return OPNG_RESULT_BAD_ARGUMENT;
    }

    PngBitmap *pngBitmap = (PngBitmap *) pngBitmapPtr;
    jint* outdata = env -> GetIntArrayElements(outJArray, NULL);

    int size[2];
    uint8_t data_format;
    int color_type;

    if(pngBitmap -> getDataFormat(data_format) == OPNG_RESULT_SUCCESS
    && pngBitmap -> getSize(size) == OPNG_RESULT_SUCCESS
    && pngBitmap -> getColorType(color_type) == OPNG_RESULT_SUCCESS) {
        outdata[0] = size[0];
        outdata[1] = size[1];
        outdata[2] = data_format;
        outdata[3] = color_type;
        env -> ReleaseIntArrayElements(outJArray, outdata, 0);
        return OPNG_RESULT_SUCCESS;
    }

    env -> ReleaseIntArrayElements(outJArray, outdata, 0);

    return OPNG_RESULT_FAIL;
}
