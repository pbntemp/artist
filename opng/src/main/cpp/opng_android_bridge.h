//
// Created by ober on 2019/4/15.
//

#ifndef TESTN_OPNG_ANDROID_BRIDGE_H
#define TESTN_OPNG_ANDROID_BRIDGE_H

#include <jni.h>

#ifdef __cplusplus
extern "C" {
#endif

JNIEXPORT jint JNICALL
Java_com_ober_libpng_OPngNative_createPngBitmapFromFile(JNIEnv *env,
                                                       jclass jtype,
                                                       jstring jFilePath,
                                                       jlongArray out);


JNIEXPORT jint JNICALL
Java_com_ober_libpng_OPngNative_drawAndroidBitmap(JNIEnv* env,
                                                       jclass jtype,
                                                       jlong pngBitmapPtr,
                                                       jobject jbmp);

JNIEXPORT jint JNICALL
Java_com_ober_libpng_OPngNative_recyclePngBitmap(JNIEnv* env,
                                                 jclass jtype,
                                                 jlong pngBitmapPtr);


JNIEXPORT jint JNICALL
Java_com_ober_libpng_OPngNative_getPngBitmapInfoData(JNIEnv* env,
                                                jclass jtype,
                                                jlong pngBitmapPtr,
                                                jintArray outJArray);



#ifdef __cplusplus
};
#endif

#endif //TESTN_OPNG_ANDROID_BRIDGE_H
