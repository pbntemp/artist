cmake_minimum_required(VERSION 3.4.1)

include_directories(png)

set(LIB_PNG_CFILES)
list(APPEND LIB_PNG_CFILES
        ./png/pngget.c
        ./png/pngread.c
        ./png/pngrutil.c
        ./png/pngtrans.c
        ./png/pngwtran.c
        ./png/png.c
        ./png/pngmem.c
        ./png/pngrio.c
        ./png/pngset.c
        ./png/pngwio.c
        ./png/pngwutil.c
        ./png/pngerror.c
        ./png/pngpread.c
        ./png/pngrtran.c
        ./png/pngwrite.c
        )

add_library( # Sets the name of the library.
        opng

        STATIC

        ${LIB_PNG_CFILES}
        PngBitmap.cpp
        opng_android_bridge.cpp)


find_library( # Sets the name of the path variable.
        log-lib

        # Specifies the name of the NDK library that
        # you want CMake to locate.
        log)


target_link_libraries( # Specifies the target library.
        opng

        # Links the target library to the log library
        # included in the NDK.
        ${log-lib}
        z
        jnigraphics)


