#ifndef OPNG_LOGDEF_H
#define OPNG_LOGDEF_H

#include "opng_config.h"

#ifdef OPNG_DEBUG
#include <android/log.h>
#define OPNG_LOG_TAG "OPNG"

#define LOGD(...) __android_log_print(ANDROID_LOG_DEBUG,OPNG_LOG_TAG ,__VA_ARGS__)
#define LOGI(...) __android_log_print(ANDROID_LOG_INFO,OPNG_LOG_TAG ,__VA_ARGS__)
#define LOGW(...) __android_log_print(ANDROID_LOG_WARN,OPNG_LOG_TAG ,__VA_ARGS__)
#define LOGE(...) __android_log_print(ANDROID_LOG_ERROR,OPNG_LOG_TAG ,__VA_ARGS__)
#define LOGF(...) __android_log_print(ANDROID_LOG_FATAL,OPNG_LOG_TAG ,__VA_ARGS__)

#else
#define OPNG_LOG_TAG
#define LOGD(...)
#define LOGI(...)
#define LOGW(...)
#define LOGE(...)
#define LOGF(...)
#endif



#endif