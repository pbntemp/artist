//
// Created by ober on 2019/4/12.
//

#include "PngBitmap.h"

#include <stdlib.h>
#include <string.h>
#include "png/png.h"
#include <android/bitmap.h>

#include "opng_logdef.h"

#define PNG_BYTES_TO_CHECK 4

static void inline init_row_buffer(png_bytepp row_points, int row_count) {
    for(int row = 0; row < row_count; row++) {
        row_points[row] = NULL;
    }
}

static void inline alloc_row_buffer(png_structp png_ptr,
        png_bytepp row_points,
        int row_count, int row_bytes) {
    for(int row = 0; row < row_count; row++) {
        row_points[row] = (png_bytep)png_malloc(png_ptr, row_bytes);
    }
}

static void inline free_row_buffer(png_structp png_ptr,
        png_bytepp row_points, int row_count) {
    for(int row = 0; row < row_count; row++) {
        png_free(png_ptr, row_points[row]);
        row_points[row] = NULL;
    }
}

static void inline read_RGBA_to_data(u_int32_t *data, png_bytepp row_points,
                                     int w, int h,
                                     int data_offset,
                                     int start_y, int to_read_row_count,
                                     int o1, int o2, int o3, int o4) {

    int x,y;
    int index = data_offset;
    int end = to_read_row_count + start_y;
    for(y = start_y; y < end; ++y) {
        for(x = 0; x < w * 4;) {
            data[index++] = ((row_points)[y][x++] << o1)
                    | ((row_points)[y][x++] << o2)
                    | ((row_points)[y][x++] << o3)
                    | ((row_points)[y][x++] << o4);
        }
    }
}

static void inline read_RGB_to_data(u_int32_t *data, png_bytepp row_points,
                                     int w, int h,
                                     int data_offset,
                                     int start_y, int to_read_row_count,
                                     int o1, int o2, int o3, int o4) {

    int x,y;
    int index = data_offset;
    int end = to_read_row_count + start_y;
    for(y = start_y; y < end; ++y) {
        for(x = 0; x < w * 3;) {
            data[index++] = ((row_points)[y][x++] << o1)
                            | ((row_points)[y][x++] << o2)
                            | ((row_points)[y][x++] << o3)
                            | (0xff << o4);
        }
    }
}

static bool inline readData(u_int32_t *data,
                            png_structp png_ptr,
                            png_bytepp row_points,
                            int w, int h,
                            int png_color_type,
                            int data_form,
                            int buf_row_count,
                            int row_bytes) {
    int o1, o2, o3, o4;
    switch (data_form) {
        case OPNG_DATA_FORMAT_ABGR:
            o1 = 0;
            o2 = 8;
            o3 = 16;
            o4 = 24;
            break;
        case OPNG_DATA_FORMAT_ARGB:
            o1 = 16;
            o2 = 8;
            o3 = 0;
            o4 = 24;
            break;
        case OPNG_DATA_FORMAT_BGRA:
            o1 = 8;
            o2 = 16;
            o3 = 24;
            o4 = 0;
            break;
        case OPNG_DATA_FORMAT_RGBA:
            o1 = 24;
            o2 = 16;
            o3 = 8;
            o4 = 0;
            break;
        default:
            LOGE("bad data_format %d", data_form);
            return false;

    }

    alloc_row_buffer(png_ptr, row_points, buf_row_count, row_bytes);

    LOGD("png_read start");

    int rowOffset = 0;
    int dataOffset = 0;
    const int maxRowOffset = h - 1;

    while (rowOffset < maxRowOffset) {
        int to_read_row_count;
        if(maxRowOffset - rowOffset < buf_row_count) {
            to_read_row_count = maxRowOffset - rowOffset;
        } else {
            to_read_row_count = buf_row_count;
        }

        png_read_rows(png_ptr, row_points, NULL, to_read_row_count);
        if(png_color_type == PNG_COLOR_TYPE_RGB) {
            read_RGB_to_data(data, row_points, w, h, dataOffset, 0, to_read_row_count, o1, o2, o3,
                              o4);
        } else {
            read_RGBA_to_data(data, row_points, w, h, dataOffset, 0, to_read_row_count, o1, o2, o3,
                              o4);
        }
        dataOffset = dataOffset + w * to_read_row_count;
        rowOffset = rowOffset + to_read_row_count;
    }

    LOGD("png_read end");

    free_row_buffer(png_ptr, row_points, buf_row_count);

    return true;
}

PngBitmap* PngBitmap::new_from_file(FILE *fp, uint8_t useFormat, int &result) {
    LOGD("new_from_file(FILE, uchar, int&)");

    if(fp == nullptr) {
        LOGE("null fp");
        result = OPNG_RESULT_BAD_FILE;
        return nullptr;
    }

    png_structp png_ptr;
    png_infop info_ptr;

    unsigned char buf[PNG_BYTES_TO_CHECK];
    int w, h, temp, color_type;
    uint32_t* data = nullptr;

    png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, 0, 0, 0);
    info_ptr = png_create_info_struct(png_ptr);

    if(info_ptr == nullptr) {
        png_destroy_read_struct(&png_ptr, NULL, NULL);
        result = OPNG_RESULT_BAD_FILE;
        return nullptr;
    }

    LOGD("read first %d bytes header", PNG_BYTES_TO_CHECK);
    temp = fread(buf, 1, PNG_BYTES_TO_CHECK, fp);

    if(temp != PNG_BYTES_TO_CHECK) {
        LOGE("bad file");
        fclose(fp);
        png_destroy_read_struct(&png_ptr, &info_ptr, 0);
        result = OPNG_RESULT_BAD_FILE_CONTENT;
        return nullptr;
    }

    temp = png_sig_cmp((png_bytep)buf, 0, PNG_BYTES_TO_CHECK);
    if(temp) {
        //not png
        LOGW("file head %d,%d,%d,%d", buf[0], buf[1], buf[2], buf[3]);
        LOGE("not png");
        fclose(fp);
        png_destroy_read_struct(&png_ptr, &info_ptr, 0);
        result = OPNG_RESULT_BAD_FILE_HEAD;
        return nullptr;
    }

    rewind(fp);

    LOGD("png_init_io");
    png_init_io(png_ptr, fp);

    LOGD("png_read_info");
    png_read_info(png_ptr, info_ptr);

    color_type = png_get_color_type(png_ptr, info_ptr);
    LOGD("got color_type=%d", color_type);

    unsigned char depth = png_get_bit_depth(png_ptr, info_ptr);
    LOGD("got bit_depth=%d", depth);

    w = png_get_image_width(png_ptr, info_ptr);
    h = png_get_image_height(png_ptr, info_ptr);
    LOGD("got size %d,%d", w, h);

    if(w == 0 || h == 0) {
        png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
        fclose(fp);
        result = OPNG_RESULT_BAD_FILE_CONTENT;
        return nullptr;
    }

    //calc buffer size
    int buffer_row_count;
    {
        if (w <= 512) {
            buffer_row_count = 8;
        } else if(w <= 1024) {
            buffer_row_count = 4;
        } else {
            buffer_row_count = 2;
        }
    }

    png_bytep row_points[buffer_row_count];

    init_row_buffer(row_points, buffer_row_count);

    if(setjmp(png_jmpbuf(png_ptr))) {
        LOGE("jpm ERR");
        free_row_buffer(png_ptr, row_points, buffer_row_count);
        png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
        fclose(fp);
        result = OPNG_RESULT_BAD_FILE_CONTENT;
        return nullptr;
    }

    bool apply_transform = false;

    if(color_type == PNG_COLOR_TYPE_PALETTE) {
        apply_transform = true;
        LOGD("transform png_set_palette_to_rgb");
        png_set_palette_to_rgb(png_ptr);
    }

    if(color_type == PNG_COLOR_TYPE_GRAY && depth < 8) {
        apply_transform = true;
        LOGD("transform png_set_expand_gray_1_2_4_to_8");
        png_set_expand_gray_1_2_4_to_8(png_ptr);
    }

    if(depth == 16) {
        apply_transform = true;
        LOGD("transform png_set_strip_16");
        png_set_strip_16(png_ptr);
    }

    if(png_get_valid(png_ptr, info_ptr, PNG_INFO_tRNS)) {
        apply_transform = true;
        LOGD("transform png_set_tRNS_to_alpha");
        png_set_tRNS_to_alpha(png_ptr);
    }

    if(color_type == PNG_COLOR_TYPE_GRAY || color_type == PNG_COLOR_TYPE_GRAY_ALPHA) {
        apply_transform = true;
        LOGD("transform png_set_gray_to_rgb");
        png_set_gray_to_rgb(png_ptr);
    }

    if(apply_transform) {
        png_read_update_info(png_ptr, info_ptr);
        LOGD("transform updated");
    }

    int row_bytes = png_get_rowbytes(png_ptr, info_ptr);
    LOGD("got row_bytes=%d", row_bytes);

    bool check_rowbytes = true;
    if(color_type == PNG_COLOR_TYPE_RGB_ALPHA || color_type == PNG_COLOR_TYPE_RGBA) {
        if(row_bytes != 4 * w) {
            check_rowbytes = false;
        }
    } else if(color_type == PNG_COLOR_TYPE_RGB) {
        if(row_bytes != 3 * w) {
            check_rowbytes = false;
        }
    }

    if(!check_rowbytes) {
        LOGE("program err, row bytes must be 4*W after transform!!");
        free_row_buffer(png_ptr, row_points, buffer_row_count);
        png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
        fclose(fp);
        result = OPNG_RESULT_FAIL;
        return nullptr;
    }

    data = (uint32_t*)malloc(sizeof(uint32_t) * w * h);

    if(!readData(data, png_ptr, row_points, w, h, color_type,
            useFormat, buffer_row_count, row_bytes)) {
        free_row_buffer(png_ptr, row_points, buffer_row_count);
        png_read_end(png_ptr, info_ptr);
        free(data);
        png_destroy_read_struct(&png_ptr, &info_ptr, 0);
        result = OPNG_RESULT_BAD_ARGUMENT;
        return nullptr;
    }

    png_read_end(png_ptr, info_ptr);
    png_destroy_read_struct(&png_ptr, &info_ptr, 0);

    PngBitmap* pngBitmap = new PngBitmap(data, w, h, useFormat, color_type);
    result = OPNG_RESULT_SUCCESS;
    LOGI("PNgBitmap Created %ld", ((long)pngBitmap));
    return pngBitmap;
}

void PngBitmap::recycle(PngBitmap *pngBitmap) {
    if(pngBitmap != nullptr) {

        free(pngBitmap -> size);
        pngBitmap -> size = nullptr;
        free(pngBitmap -> data);
        pngBitmap -> data = nullptr;

    }
}

int PngBitmap::getSize(int *out_size) {

    if(out_size == nullptr) {
        return OPNG_RESULT_BAD_ARGUMENT;
    }

    if(size == nullptr) {
        return OPNG_RESULT_FAIL;
    }
    out_size[0] = size[0];
    out_size[1] = size[1];

    return OPNG_RESULT_SUCCESS;
}

int PngBitmap::getDataFormat(uint8_t &out_dataFormat) {

    out_dataFormat = dataFormat;

    return OPNG_RESULT_SUCCESS;
}

int PngBitmap::getData(uint32_t **out_data) {

    if(out_data == nullptr) {
        return OPNG_RESULT_BAD_ARGUMENT;
    }

    *out_data = data;

    return OPNG_RESULT_SUCCESS;
}


int PngBitmap::getColorType(int &out_color_type) {
    out_color_type = colorType;
    return OPNG_RESULT_SUCCESS;
}

PngBitmap::~PngBitmap() {
    if(data != nullptr) {
        free(data);
        data = nullptr;
    }
    if(size != nullptr) {
        free(size);
        size = nullptr;
    }
}

PngBitmap::PngBitmap(uint32_t *data, int w, int h, uint8_t data_format, int color_type) {
    PngBitmap::data = data;

    size = (int*)malloc(sizeof(int) * 2);
    *size = w;
    *(size+1) = h;
    dataFormat = data_format;
    colorType = color_type;
}



