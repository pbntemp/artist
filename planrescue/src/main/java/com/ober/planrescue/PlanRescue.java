package com.ober.planrescue;

import java.io.*;
import java.util.*;

//try to rescue "plan" after region.png updated
public class PlanRescue {

    public static boolean repair(ImageStruct oldRegionImage, ImageStruct newRegionImage,
                                 String oldPlanFile, String outNewPlanFile, boolean debug) {

        Debug.ON = debug;

        Debug.println("--------PlanRescue repair start-------");
        Debug.println(">>>>[VERSION " + Debug.VERSION + "]");

        Debug.println(">>>>decode old plan file");
        File oldPlan = new File(oldPlanFile);
        if(!oldPlan.exists() || oldPlan.isDirectory()) {
            System.err.println("err: oldplan file not found : " + oldPlanFile);
            Debug.println("--------PlanRescue repair end--------");
            return false;
        }

        String planStr = readFromFile(oldPlan);
        if(planStr == null) {
            System.err.println("err: oldplan can not read : " + oldPlanFile);
            Debug.println("--------PlanRescue repair end--------");
            return false;
        }

        PreColorBlock[] preColorBlocks = decodeFromPlan(planStr);
        if(preColorBlocks == null) {
            System.err.println("err: oldplan file decode : " + oldPlanFile);
            Debug.println("--------PlanRescue repair end--------");
            return false;
        }

        Debug.println(">>>>run match algorithm");
        RegionMapAlgorithm.MatchResult matchResult = RegionMapAlgorithm.match(
                oldRegionImage, newRegionImage);

        if(matchResult.matchedResult == null || matchResult.matchedResult.isEmpty()) {
            Debug.println("nothing matched");
            Debug.println("--------PlanRescue repair end--------");
            return false;
        }

        Debug.println(">>>>generate new plan");

        List<PreColorBlock> newPreColorBlocks = new LinkedList<>();

        Map<Integer, Integer> old2newNumMap = matchResult.matchedResult;

        for(PreColorBlock block : preColorBlocks) {
            HashSet<Integer> newAreas = new HashSet<>();
            for(Integer num : block.areas) {
                Integer newNum = old2newNumMap.get(num);
                if(newNum == null) {
                    //no matched num in new region
                    //discard this num
                } else {
                    newAreas.add(newNum);
                }
            }
            if(newAreas.isEmpty()) {
                //this block not need anymore
                continue;
            }
            block.areas = newAreas;
            newPreColorBlocks.add(block);
        }

        String newPlanStr = encodeToPlan(newPreColorBlocks);

        if(Debug.ON) {
            Debug.println("newPlanStr generated");
            Debug.println(newPlanStr);
        }

        Debug.println(">>>>write new plan to file");

        File outNewPlan = new File(outNewPlanFile);

        writeToFile(outNewPlan, newPlanStr);

        final boolean success;
        //check newPlanFile length
        if(outNewPlan.length() == newPlanStr.getBytes().length) {
            Debug.println(">>>> success " );
            success = true;
        } else {
            System.err.println("err: outNewPlan not write correctly");
            success = false;
        }

        Debug.println("--------PlanRescue repair end--------");

        return success;
    }


    private static String encodeToPlan(List<PreColorBlock> preColors) {
        StringBuilder sb = new StringBuilder();
        int len = preColors.size();
        for(int i = 0; i < len; i++) {
            PreColorBlock block = preColors.get(i);
            sb.append(encodeIntArr(block.areas));
            sb.append("#");
            sb.append(block.color.replace("#FF", ""));
            if(i != len - 1) {
                sb.append("|");
            }
        }

        return sb.toString();
    }

    private static PreColorBlock[] decodeFromPlan(String s) {
        if(s == null || s.length() == 0) {
            return null;
        }
        String[] ss = s.split("\\|");
        int len = ss.length;
        PreColorBlock[] preColors = new PreColorBlock[len];
        for(int i = 0; i < len; i++) {
            String[] parts = ss[i].split("#");
            PreColorBlock preColor = new PreColorBlock();
            preColor.areas = decodeIntArr(parts[0]);
            preColor.color = "#FF" + parts[1];
            preColors[i] = preColor;
        }
        return preColors;
    }

    private static String encodeIntArr(HashSet<Integer> ints) {
        StringBuilder sb = new StringBuilder();
        int len = ints.size();
        int index = 0;
        for(Integer i : ints) {
            sb.append(i);
            if(index != len - 1) {
                sb.append(",");
            }
            index++;
        }
        return sb.toString();
    }

    private static HashSet<Integer> decodeIntArr(String s) {
        if(s == null || s.length() == 0) {
            return new HashSet<>();
        }
        String[] ss = s.split(",");
        int len = ss.length;
        HashSet<Integer> set = new HashSet<>();
        for(int i = 0; i < len; i++) {
            set.add(Integer.valueOf(ss[i]));
        }
        return set;
    }

    private static class PreColorBlock {
        HashSet<Integer> areas;
        String color;
    }

    private static String readFromFile(File f) {

        if (f == null || !f.exists() || f.isDirectory())
            return null;

        StringBuilder sb = new StringBuilder();
        try (
                FileReader fileReader = new FileReader(f);
                BufferedReader reader = new BufferedReader(fileReader);
        ){
            String s;
            while ((s = reader.readLine()) != null) {
                sb.append(s);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return sb.toString();
    }

    private static void writeToFile(File f, String content) {
        if(f.exists()) {
            f.delete();
        }

        try (FileOutputStream fos = new FileOutputStream(f)) {
            fos.write(content.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
