package com.ober.planrescue;

public class Debug {

    static final String VERSION = "1.0.0";

    public static boolean ON = true;

    static void println(String s) {
        if(ON) {
            System.out.println(s);
        }
    }
}
