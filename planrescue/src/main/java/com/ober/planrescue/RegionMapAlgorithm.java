package com.ober.planrescue;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;


public class RegionMapAlgorithm {

    public static class MatchResult {
        /**
         * The area Mapping, key=(area_num in old_region), value=(area_num in new_region)
         */
        public Map<Integer, Integer> matchedResult;

        /**
         * non matched area_nums in old_region
         */
        public Set<Integer> nonMatchResult;
    }

    /**
     * algorithm entry
     */
    public static MatchResult match(ImageStruct imageOld, ImageStruct imageNew) {

        if(imageOld.w != imageNew.w || imageOld.h != imageNew.h) {
            throw new RuntimeException("Confusion Region Image Size");
        }

        Map<Integer, RegionStruct> regionMap_old = analyzeRegion(imageOld);
        Map<Integer, RegionStruct> regionMap_new = analyzeRegion(imageNew);

        Map<Integer, Integer> matchedResult = new HashMap<>();
        Set<Integer> nonMatchResult = new HashSet<>();

        Debug.println("matching start");
        for(Map.Entry<Integer, RegionStruct> entry : regionMap_old.entrySet()) {
            SingleMatchResult rs = matchArea(imageOld, entry.getKey(), entry.getValue(), imageNew, regionMap_new);
            if(rs.matched) {
                matchedResult.put(rs.oldNum, rs.newNum);
            } else {
                nonMatchResult.add(rs.oldNum);
            }

        }
        Debug.println("matching end");

        if(Debug.ON) {
            Debug.println("matched : ");
            Debug.println(matchedResult.toString());
            Debug.println("non matched : ");
            Debug.println(nonMatchResult.toString());
        }

        MatchResult result = new MatchResult();
        result.matchedResult = matchedResult;
        result.nonMatchResult = nonMatchResult;

        return result;
    }


    private static class SingleMatchResult {
        int oldNum;
        boolean matched;
        int newNum;
    }

    /**
     * try to find an area in new_region that is matched to {num} in old_region
     */
    private static SingleMatchResult matchArea(ImageStruct imageOld,
                                               final int num,
                                               RegionStruct targetRegion,

                                               ImageStruct imageNew,
                                               Map<Integer, RegionStruct> newRegionMap) {
        final int originS = targetRegion.s;


        //the nums in new-region that cover by old-region
        //<num - count>
        final Map<Integer, Integer> oldCoverNums = new HashMap<>();

        final int oldW = imageOld.w;

        final int[] oldData = imageOld.data;
        final int[] newData = imageNew.data;

        {
            //calculate oldCoverNums
            for (int y = targetRegion.t; y <= targetRegion.b; y++) {
                final int y_plus_w = y * oldW;
                for (int x = targetRegion.l; x <= targetRegion.r; x++) {
                    if (oldData[y_plus_w + x] == num) {
                        int pixelInNewRegion = newData[y_plus_w + x];
                        Integer before = oldCoverNums.get(pixelInNewRegion);
                        //noinspection Java8MapApi
                        if(before == null) {
                            oldCoverNums.put(pixelInNewRegion, 1);
                        } else {
                            oldCoverNums.put(pixelInNewRegion, before + 1);
                        }
                    }
                }
            }
        }

        //remove white space
        oldCoverNums.remove(Integer.valueOf(0x00ffffff));

        if(oldCoverNums.isEmpty()) {
            //no match
            SingleMatchResult r = new SingleMatchResult();
            r.oldNum = num;
            r.matched = false;
            return r;
        }

        Map.Entry<Integer, Integer> maxCoverNumEntry = null;
        for(Map.Entry<Integer, Integer> entry : oldCoverNums.entrySet()) {
            if(maxCoverNumEntry == null) {
                maxCoverNumEntry = entry;
            } else {
                if(entry.getValue() > maxCoverNumEntry.getValue()) {
                    maxCoverNumEntry = entry;
                }
            }
        }

        if(maxCoverNumEntry == null) {
            System.err.println("num = " + num);
            throw new RuntimeException("logic err");
        }

        final int nonMatchCount_A = originS - maxCoverNumEntry.getValue();

        //use new-region to cover old-region for this area
        final int numNew = maxCoverNumEntry.getKey();

        final RegionStruct regionNew = newRegionMap.get(numNew);
        if(regionNew == null) {
            throw new RuntimeException("logic err");
        }

        final int nonMatchCount_B;
        {
            int newCoverCount = 0;
            //calculate newCoverNums
            for(int y = regionNew.t; y <= regionNew.b; y++) {
                final int y_plus_w = y * oldW;
                for(int x = regionNew.l; x <= regionNew.r; x++) {
                    if(newData[y_plus_w + x] == numNew) {
                        if(oldData[y_plus_w + x] == num) {
                            newCoverCount ++;
                        }
                    }
                }
            }
            nonMatchCount_B = regionNew.s - newCoverCount;
        }

        boolean match = realCheckMatch(originS, nonMatchCount_A, nonMatchCount_B);

        SingleMatchResult rs = new SingleMatchResult();
        if(match) {
            rs.matched = true;
            rs.oldNum = num;
            rs.newNum = numNew;
        } else {
            rs.oldNum = num;
            rs.matched = false;
        }

        System.out.println("single match rs[" + num + ", " + nonMatchCount_A + ", " + nonMatchCount_B + "]");

        return rs;
    }

    /**
     * determines whether Region_A is matched to Region_B
     * @param oldRegionArea the points count of Points(old_region)
     * @param nonMatchCount_A the number of points not match: Points(old_region) - Points(new_region)
     * @param nonMatchCount_B the number of points not match: Points(new_region) - Points(old_region)
     * @return true if think region match success.
     */
    private static boolean realCheckMatch(int oldRegionArea, int nonMatchCount_A, int nonMatchCount_B) {
        double d = Math.sqrt(oldRegionArea);
        return d >= nonMatchCount_A + nonMatchCount_B;
    }

    /**
     * calculate bound-rect for each region area
     * @return HashMap key=num for this area,value=RegionStruct for this area
     */
    private static HashMap<Integer, RegionStruct> analyzeRegion(ImageStruct image) {
        final int w = image.w;
        final int h = image.h;
        final int[] data = image.data;

        HashMap<Integer, RegionStruct> map = new HashMap<>();

        int lastNum = 0xff000000;
        RegionStruct lastRegion = null;

        for(int y = 0; y < h; y++) {
            for(int x = 0; x < w; x++) {
                final int num = (data[y * w + x] & 0x00ffffff);

                if(num == 0x00ffffff) {
                    //skip white pixels
                    continue;
                }

                if(lastNum == num && lastRegion != null) {
                    recordArea(lastRegion, x, y);
                } else {
                    RegionStruct region = map.get(num);
                    if(region == null) {
                        region = new RegionStruct();
                        map.put(num, region);
                    }

                    recordArea(region, x, y);
                    lastNum = num;
                    lastRegion = region;
                }
            }
        }

        return map;
    }

    private static void recordArea(RegionStruct region, int x, int y) {

        if(region.l < 0 && region.t < 0) {
            region.l = x;
            region.t = y;
            region.r = x;
            region.b = y;
            region.s = 1;
            return;
        }

        if(x < region.l) {
            region.l = x;
        }
        if(x > region.r) {
            region.r = x;
        }
        if(y < region.t) {
            region.t = y;
        }
        if(y > region.b) {
            region.b = y;
        }

        region.s++;
    }


    private static class RegionStruct {
        public int l;
        public int t;
        public int r;
        public int b;

        public int s; //point count

        RegionStruct() {
            l = -1;
            t = -1;
            r = -1;
            b = -1;
            s = -1;
        }
    }
}
