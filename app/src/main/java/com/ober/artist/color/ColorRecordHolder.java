package com.ober.artist.color;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.ober.artist.R;
import com.ober.artist.color.widget.ColorCircleView;

/**
 * Created by ober on 19-4-28.
 */
public class ColorRecordHolder extends RecyclerView.ViewHolder {


    ColorCircleView colorCircleView;
    TextView tvCount;

    public ColorRecordHolder(View itemView) {
        super(itemView);
        colorCircleView = itemView.findViewById(R.id.view_circle);
        tvCount = itemView.findViewById(R.id.tv_count);
    }
}
