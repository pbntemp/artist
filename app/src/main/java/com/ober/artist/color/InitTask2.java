package com.ober.artist.color;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;

import com.meevii.color.fill.model.core.FloodFillArea;
import com.meevii.color.fill.model.core.origin.ColorOriginImage;
import com.ober.artist.color.data.ColorDataProvider;
import com.ober.artist.color.data.ColorPreparedData2;
import com.ober.artist.color.data.ColorRecord;
import com.ober.artist.color.data.RegionData;
import com.ober.artist.color.localdata.AssetPaletteLoader;
import com.ober.artist.color.localdata.LocalData;
import com.ober.artist.db.PbnDb;
import com.ober.artist.db.PictureRecord;
import com.ober.artist.net.bean.Picture;
import com.ober.artist.utils.SToast;
import com.ober.palette.PaletteColorBean;

import java.io.File;
import java.lang.ref.SoftReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by ober on 2018/12/5.
 */
class InitTask2 extends AsyncTask<Void, Void, InitTask2.ResultSet> {

    public static class ResultSet {

        public Map<Integer, List<PaletteColorBean>> palettes;

        public Set<Integer> badAreasSet;
        public ColorOriginImage colorOriginImage;
        public List<ColorRecord> colorRecords;
        public int recordCount;
        public int areaCount;
    }

    private SoftReference<ColorActivity2> activityWeakReference;

    private final String uid;
    private final String id;
    private final boolean isLocal;
    private final int pbntype;

    private String errMsg;

    InitTask2(ColorActivity2 a, String uid, String id, boolean isLocal, int pbntype) {
        activityWeakReference = new SoftReference<>(a);
        this.uid = uid;
        this.id = id;
        this.isLocal = isLocal;
        this.pbntype = pbntype;
    }

    public String getErrMsg() {
        return errMsg;
    }

    @Override
    protected ResultSet doInBackground(Void... voids) {

        ColorActivity2 a = activityWeakReference.get();
        if (a == null) {
            errMsg = "程序错误";
            return null;
        }

        final int pngsize[] = new int[2];
        final int regionsize[] = new int[2];
        final int pdfsize[] = new int[2];

        File pngFile = LocalData.getTargetPngFile(uid, isLocal, id);
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(pngFile.getAbsolutePath(), options);

        pngsize[0] = options.outWidth;
        pngsize[1] = options.outHeight;

        boolean pdfExist = LocalData.getTargetPdfFile(uid, isLocal, id).exists();
        if(!pdfExist) {
            errMsg = "pdf不存在";
            return null;
        }

        ColorPreparedData2 preparedData = ColorDataProvider.prepareData2(uid, isLocal, id);

        HashMap<Integer, FloodFillArea> areaMap = null;
        if (preparedData.colorMap != null) {
            areaMap = new HashMap<>();
            for (Map.Entry<Integer, Integer> entry : preparedData.colorMap.entrySet()) {
                FloodFillArea area = new FloodFillArea();
                area.color = entry.getValue();
                areaMap.put(entry.getKey(), area);
            }
        }

        RegionData region = preparedData.region;

        if(region == null) {
            errMsg = "读取region出错";
            return null;
        }

        final int regionW = region.size[0];
        final int regionH = region.size[1];

        regionsize[0] = regionW;
        regionsize[1] = regionH;

        final Bitmap editBmp;
        if (preparedData.editBitmap == null) {
            editBmp = Bitmap.createBitmap(regionW, regionH,
                    Bitmap.Config.ARGB_8888);
            editBmp.eraseColor(Color.WHITE);
        } else {
            editBmp = preparedData.editBitmap;
        }

        if(editBmp.getWidth() != regionW) {
            new Handler(Looper.getMainLooper())
                    .post(() -> SToast.show("Size Not Match!!!"));
            return null;
        }

        if(pbntype == Picture.PBN_TYPE_NORMAL) {
            pdfsize[0] = 2048;
            pdfsize[1] = 2048;
        } else if(pbntype == Picture.PBN_TYPE_COLORED) {
            pdfsize[0] = 2048;
            pdfsize[1] = 2048;
        } else if(pbntype == Picture.PBN_TYPE_WALL_NORMAL) {
            pdfsize[0] = 750;
            pdfsize[1] = 1334;
        } else if(pbntype == Picture.PBN_TYPE_WALL_COLORED) {
            pdfsize[0] = 750*2;
            pdfsize[1] = 1334*2;
        } else if(pbntype == 0) {
            //以前的数据
            pdfsize[0] = pngsize[0];
            pdfsize[1] = pngsize[1];
        } else {
            errMsg = "数据错误";
            return null;
        }

        PictureRecord record;
        {
            List<PictureRecord> records = PbnDb.get().getPicRecordDao().getById(uid, id);
            if(!records.isEmpty()) {
                record = records.get(0);
            } else {
                record = null;
            }
        }

        try {
            a.mFillIV.initAndPreFill(editBmp, preparedData.region.file, areaMap, false, pdfsize);
        } catch (Exception e) {
            e.printStackTrace();
            errMsg = "图片读取失败,请重试";
            return null;
        }

        ResultSet resultSet = new ResultSet();

        HashMap<Integer, List<PaletteColorBean>> palettes = new HashMap<>();
        palettes.put(PaletteSelection.PALETTE_COMMON, loadPaletteColorBeans(a));
        palettes.put(PaletteSelection.PALETTE_ANCIENT, loadPaletteColorBeans2(a));
        if(record == null || record.getBuildVersion() > PictureRecord.VERSION_1) {
            palettes.put(PaletteSelection.PALETTE_SKIN, loadPaletteColorBeans3(a));
        }
        resultSet.palettes = palettes;

        HashMap<Integer, FloodFillArea> regionMap = a.mFillIV.getAreaMap();
        resultSet.badAreasSet = loadBadAreas(regionMap);
        resultSet.areaCount = regionMap.size();

        int[] sort = LocalData.readSort(uid, isLocal, id);
        int count = 0;
        if(sort != null) {
            List<ColorRecord> colorRecords = generateColorRecords(palettes, regionMap);
            sortColorRecords(colorRecords, sort);
            resultSet.colorRecords = colorRecords;
            {
                for(ColorRecord r : colorRecords) {
                    count += r.count;
                }
            }
        }

        resultSet.recordCount = count;
        resultSet.colorOriginImage = preparedData.colorOriginImage;

        return resultSet;
    }

    private void sortColorRecords(List<ColorRecord> colorRecords, int[] sort) {
        Collections.sort(colorRecords, new Comparator<ColorRecord>() {
            @Override
            public int compare(ColorRecord o1, ColorRecord o2) {
                return indexof(sort, o1.color)
                        - indexof(sort, o2.color);
            }
        });
    }

    private static int indexof(int[] ints, int a) {
        for(int i = 0; i < ints.length; i++) {
            if(a == ints[i]) {
                return i;
            }
        }
        return -1;
    }

    private List<ColorRecord> generateColorRecords(Map<Integer, List<PaletteColorBean>> palettes,
                                                   HashMap<Integer, FloodFillArea> areaMap) {

        final Map<Integer, Integer> colorPaletteIdMap = new HashMap<>();
        for(Map.Entry<Integer, List<PaletteColorBean>> entry : palettes.entrySet()) {
            final int paletteId = entry.getKey();
            for(PaletteColorBean bean : entry.getValue()) {
                int[] data = bean.data;
                for(int color : data) {
                    colorPaletteIdMap.put((color|0xff000000), paletteId);
                }
            }
        }

        Map<Integer, Integer> colorCountMap = new HashMap<>();

        for(Map.Entry<Integer, FloodFillArea> entry : areaMap.entrySet()) {

            final FloodFillArea area = entry.getValue();
            if(area.color == null) {
                continue;
            }

            Integer v = colorCountMap.get(area.color);
            if(v == null) {
                colorCountMap.put(area.color, 1);
            } else {
                colorCountMap.put(area.color, v + 1);
            }
        }

        List<ColorRecord> colorRecords = new ArrayList<>();
        for(Map.Entry<Integer, Integer> entry : colorCountMap.entrySet()) {
            int color = (entry.getKey() | 0xff000000);

            Integer paletteId = colorPaletteIdMap.get(color);

            ColorRecord record = new ColorRecord(paletteId == null ? -1: paletteId);
            record.color = color;
            record.count = entry.getValue();
            colorRecords.add(record);
        }

        return colorRecords;
    }

    private Set<Integer> loadBadAreas(HashMap<Integer, FloodFillArea> regionMap) {
        Set<Integer> mBadAreas = new HashSet<>();
        for(Map.Entry<Integer, FloodFillArea> entry : regionMap.entrySet()) {
            final FloodFillArea area = entry.getValue();
            if(area.getWidth() <= 2 && area.getHeight() <= 2) {
                mBadAreas.add(entry.getKey());
            }
        }
        return mBadAreas;
    }

    private List<PaletteColorBean> loadPaletteColorBeans(Context context) {
        AssetManager am = context.getAssets();
        return AssetPaletteLoader.loadFromAssets(am, "colors_v2.txt");
    }

    private List<PaletteColorBean> loadPaletteColorBeans2(Context context) {
        AssetManager am = context.getAssets();
        return AssetPaletteLoader.loadFromAssets(am, "colors_antiquity_v1.txt");
    }

    private List<PaletteColorBean> loadPaletteColorBeans3(Context context) {
        AssetManager am = context.getAssets();
        return AssetPaletteLoader.loadFromAssets(am, "colors_skin_v1.txt");
    }

    @Override
    protected void onPostExecute(ResultSet rs) {

        ColorActivity2 a = activityWeakReference.get();
        if (a == null) {
            return;
        }
        a.handleInitComplete(rs);
    }
}
