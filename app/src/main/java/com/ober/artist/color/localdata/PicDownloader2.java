package com.ober.artist.color.localdata;

import android.os.AsyncTask;

import androidx.core.util.Consumer;

import com.ober.artist.App;
import com.ober.artist.db.PbnDb;
import com.ober.artist.db.PictureRecord;
import com.ober.artist.pack.ZipUtil;
import com.ober.artist.utils.Files;
import com.ober.artist.utils.OkDownloadUtil;
import com.ober.artist.utils.ResourceSizeCheckUtil;
import com.socks.library.KLog;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

/**
 * Created by ober on 2019-10-28.
 */
public class PicDownloader2 extends AsyncTask<Void, Integer, PicDownloader2.Result> {

    private static final String TAG = "PicDownloader2";

    public static final int RESULT_OK = 0;

    public static class Result {
        public int code;
        public String errMsg;

        public String url;
        public String generatedId;
        public int pbnType;
    }

    private Consumer<Result> mCallback;

    private final String generatedId;
    private final String url;
    private final int pbnType;
    private final String uid;
    private final boolean isLocal;

    public PicDownloader2(String uid,
                          boolean isLocal,
                          String url,
                          int pbnType,
                          Consumer<Result> callback) {
        this.uid = uid;
        this.isLocal = isLocal;
        this.generatedId = UUID.randomUUID().toString().replace("-", "");
        this.url = url;
        this.pbnType = pbnType;
        this.mCallback = callback;
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
        mCallback = null;
    }

    @Override
    protected void onPostExecute(Result result) {
        super.onPostExecute(result);
        Consumer<Result> cb = mCallback;
        if(cb != null) {
            cb.accept(result);
        }
    }

    @Override
    protected Result doInBackground(Void... voids) {

        File cacheDir = App.getInstance().getCacheDir();
        if(!cacheDir.exists()) {
            cacheDir.mkdirs();
        }
        File tempFile = new File(cacheDir, generatedId);
        if(tempFile.exists()) {
            tempFile.delete();
        }

        KLog.i(TAG, "start download", "file = " + tempFile, "url = " + url);

        try {
            OkDownloadUtil.download(url, tempFile);
        } catch (IOException e) {
            e.printStackTrace();
            tempFile.delete();
            Result result = new Result();
            result.code = -1;
            result.errMsg = "下载失败";
            return result;
        }

        if(isCancelled()) {
            return null;
        }

        File targetDir = LocalData.getTargetFileRecordDir(uid, isLocal, generatedId);

        try {
            ZipUtil.unzip(tempFile.getAbsolutePath(), targetDir.getAbsolutePath());
        } catch (IOException e) {
            e.printStackTrace();
            KLog.w(TAG, "unzip error!");
            Files.rm_rf(targetDir);

            Result result = new Result();
            result.code = -2;
            result.errMsg = "解压失败";

            return result;
        }

        String s = Files.fileTree(targetDir);

        System.out.println(s);

        boolean check = checkResource(targetDir, pbnType);

        if(!check) {
            Files.rm_rf(targetDir);
            Result result = new Result();
            result.code = -3;
            result.errMsg = "资源文件错误";
            return result;
        }

        Result rs = new Result();
        rs.generatedId = generatedId;
        rs.pbnType = pbnType;
        rs.url = url;
        rs.code = RESULT_OK;
        rs.errMsg = "ok";

        PictureRecord record = new PictureRecord();
        record.setDownloadTime(System.currentTimeMillis());
        record.setId(generatedId);
        record.setPbntype(pbnType);
        record.setUid(uid);
        record.setBuildVersion(PictureRecord.VERSION_CURRENT);

        PbnDb.get().getPicRecordDao().insert(record);

        return rs;
    }

    //region.png origin.png  origin.pdf  optional{colored.jpg}  optional{thumbnail.jpg/thumbnail.png/thumbnail.jpeg}
    private static boolean checkResource(File dir, int type) {
        File pngFile = new File(dir, "origin.png");
        File pdfFile = new File(dir, "origin.pdf");
        File regionFile = new File(dir, "region.png");

        if(!pngFile.exists() || !pdfFile.exists() || !regionFile.exists()) {
            return false;
        }
        ResourceSizeCheckUtil.CheckResult rs = ResourceSizeCheckUtil.checkResource(pngFile, pdfFile);
        if(rs.isTooLarge) {
            return false;
        }
        return true;
    }
}
