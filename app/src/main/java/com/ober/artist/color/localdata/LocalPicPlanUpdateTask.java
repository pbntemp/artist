package com.ober.artist.color.localdata;

import androidx.core.util.Consumer;

import com.ober.artist.color.data.Center;
import com.ober.artist.pack.CenterGen;
import com.ober.artist.utils.Files;
import com.ober.artist.xlog.XLogger;
import com.socks.library.KLog;

import java.io.File;
import java.util.Map;

public class LocalPicPlanUpdateTask extends PlanUpdateTask {

    private static final String TAG = "LocalPicPlanUpdateTask";

    private String errMsg;
    private String targetDir;

    public LocalPicPlanUpdateTask(String uid,
                                  String id,
                                  String targetDir,
                                  boolean isLocal,
                                  Consumer<Boolean> callback) {
        super(uid, id, isLocal, callback);
        this.targetDir = targetDir;
    }

    /**
     * 生成plan，线框图更新的时候需要
     * @return
     */
    @Override
    protected File getTargetPlanFile() {
        FileImageBean2 imageBean = FileDataLoader.load2(uid, isLocal, pid);

        if(!imageBean.area.exists()) {
            errMsg = "当前素材缺少着色进度";
            return null;
        }

        //process center

        if(imageBean.center.exists()) {
            KLog.w(TAG, "delete exists center");
            imageBean.center.delete();
        }

        if(imageBean.plan.exists()) {
            KLog.w(TAG, "delete exists plan");
        }

        Map<Integer, Center> centerMap = CenterGen.genCenter2(imageBean);

        if(isCancelled()) {
            return null;
        }

        int[] order = LocalData.readSort(uid, isLocal, pid);

        try {
            LocalData.savePlanByAreaMap(imageBean.plan, imageBean.area, centerMap, order);
        } catch (Exception e){
            XLogger.e("PbnPaintGenerateTask.savePlanByAreaMap:" + e.getMessage());
            return null;
        }
        return super.getTargetPlanFile();
    }


    //region新图还在下载缓存里
    @Override
    protected File getTargetRegionFile() {
        return new File(targetDir, "region.png");
    }

    //region_old是以前用的图
    @Override
    protected File getTargetRegionOldFile() {
        return super.getTargetRegionFile();
    }

    private boolean movePngPdfRegion(){

        File pngFileTarget = LocalData.getTargetPngFile(uid, isLocal, pid);
        File regionFileTarget = LocalData.getTargetRegionFile(uid, isLocal, pid);
        File pdfFileTarget = LocalData.getTargetPdfFile(uid, isLocal, pid);

        if(!pngFileTarget.exists() || !pdfFileTarget.exists() || !regionFileTarget.exists()) {
            return false;
        }

        File pngFile = new File(targetDir, "origin.png");
        File pdfFile = new File(targetDir, "origin.pdf");
        File regionFile = new File(targetDir, "region.png");

//        pngFileTarget.delete();
//        regionFileTarget.delete();
//        pdfFileTarget.delete();

//        boolean result = pngFile.renameTo(pngFileTarget);
//        result = result && pdfFile.renameTo(pdfFileTarget);
//        result = result && regionFile.renameTo(regionFileTarget);

        boolean result = Files.copyFile(pngFile.getAbsolutePath(), pngFileTarget.getAbsolutePath());
        result &= Files.copyFile(pdfFile.getAbsolutePath(), pdfFileTarget.getAbsolutePath());
        result &= Files.copyFile(regionFile.getAbsolutePath(), regionFileTarget.getAbsolutePath());

        if (result) {
            Files.rm_rf(new File(targetDir));
        }
        return true;
    }

    @Override
    protected boolean handleResult(Boolean result) {
        if (result) {
            return movePngPdfRegion();
        }
        return false;
    }
}
