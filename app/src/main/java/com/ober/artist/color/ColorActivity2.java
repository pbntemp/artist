package com.ober.artist.color;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.util.Consumer;
import androidx.core.util.Pair;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.meevii.color.fill.FillColorImageView;
import com.meevii.color.fill.model.core.resp.FillColorResp;
import com.meevii.color.fill.model.core.resp.OverwriteColorResp;
import com.meevii.color.fill.model.core.resp.RevokeOverwriteResp;
import com.meevii.color.fill.model.core.resp.RevokeSingleColorResp;
import com.meevii.color.fill.model.core.FloodFillArea;
import com.meevii.color.fill.model.core.task.RunnableTask;
import com.meevii.color.fill.util.FillColorHelper;
import com.meevii.color.fill.view.gestures.SubsamplingScaleImageView;
import com.ober.artist.App;
import com.ober.artist.BuildConfig;
import com.ober.artist.R;
import com.ober.artist.analyze.PictureTimeRecorder;
import com.ober.artist.color.data.ColorRecord;

import com.ober.artist.color.localdata.LocalData;
import com.ober.artist.color.localdata.LocalPicPlanUpdateTask;
import com.ober.artist.color.localdata.PicRegionDownloader;
import com.ober.artist.color.localdata.PlanUpdateTask;
import com.ober.artist.db.PbnDb;
import com.ober.artist.db.PictureRecord;
import com.ober.artist.pack.PbnPaintGenerateTask;
import com.ober.artist.ui.pics3.VincentUploadPaint;
import com.ober.artist.ui.zbar.PbnScanResult;
import com.ober.artist.ui.zbar.ZBarActivity;
import com.ober.artist.utils.SToast;
import com.ober.artist.xlog.XLogger;
import com.ober.palette.Palette;
import com.ober.palette.PaletteColorBean;

import java.io.File;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

/**
 * Created by ober on 2018/12/4.
 */
public class ColorActivity2 extends AppCompatActivity implements FillColorImageView.FillColorListener, FillColorImageView.ImageOnLongClickListener {

    public static final int RESULT_INIT_FAILED = 111;
    private static final int REQUEST_ZBAR = 112;

    FillColorImageView mFillIV;

    private ImageView ivBack;
    private ImageView ivQR;
    private Palette paletteView;
    private RecyclerView rvRecord;
    private ImageView ivRevoke;
    private ImageView ivRevokeBack;
    private FrameLayout fTips;
    private ImageView ivReset;
    private TextView tvCount;
    private FrameLayout fPalette;
    private FrameLayout fOk;
    private TextView tvUiMode;

    private InitTask2 mInitTask;

    private ProgressDialog mProgressDialog;

    private String mUid;
    private boolean isLocal;
    private String mId;
    private String mRegion;
    private int mPbnType;

    private List<Integer> mBadAreas;

    private ColorRecordManager mColorRecordManager;
    private PictureTimeRecorder mPicTimeRecorder;

    private int mTotalCount;

    private PaletteSelection mPaletteSelection;

    private SingleColorModeHelper mSingleColorModeHelper;
    private ReplaceModeHelper mReplaceModeHelper;

    private boolean canRevoke;
    private boolean canRevokeBack;

    public static void startForResultNew(Activity a,
                                         String uid,
                                         String id,
                                         String region,
                                         boolean isLocal,
                                         int pbntype,
                                         int requestCode) {
        Intent intent = new Intent(a, ColorActivity2.class);
        intent.putExtra("uid", uid);
        intent.putExtra("islocal", isLocal);
        intent.putExtra("id", id);
        intent.putExtra("region", region);
        intent.putExtra("pbntype", pbntype);
        a.startActivityForResult(intent, requestCode);
    }

    private boolean parseIntentData(Intent intent) {
        if(intent == null) {
            return false;
        }
        mUid = intent.getStringExtra("uid");
        isLocal = intent.getBooleanExtra("islocal", false);
        mId = intent.getStringExtra("id");
        mRegion = intent.getStringExtra("region");
        mPbnType = intent.getIntExtra("pbntype", 0);
        return mUid != null && mId != null && mPbnType > 0;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_color);

        XLogger.d("ColorActivity onCreate");

        rvRecord = findViewById(R.id.rvRecord);
        mFillIV = findViewById(R.id.fillView);
        paletteView = findViewById(R.id.paletteView);
        fTips = findViewById(R.id.fTips);
        ivBack = findViewById(R.id.ivBack);
        ivQR = findViewById(R.id.ivQR);
        ivRevoke = findViewById(R.id.ivRevoke);
        ivRevokeBack = findViewById(R.id.ivRevokeBack);
        ivReset = findViewById(R.id.ivReset);
        tvCount = findViewById(R.id.tvColorCount);
        fPalette = findViewById(R.id.fPalette);
        fOk = findViewById(R.id.fSingleModeOk);
        fOk.setVisibility(View.GONE);

        tvUiMode = findViewById(R.id.tvUiMode);
        tvUiMode.setVisibility(View.GONE);

        tvCount.setOnClickListener(view -> changeTextColor(tvCount));
        tvCount.setTag(0);

        tvUiMode.setOnClickListener(v -> {

        });
        tvUiMode.setTag(1);
        tvUiMode.setTextColor(0xffff7182);

        fTips.setVisibility(View.GONE);
        fTips.setOnClickListener(view -> processTips());

        ivBack.setOnClickListener(v -> onBackPressed());
        ivQR.setOnClickListener(v -> onQR());

        fOk.setOnClickListener(v -> processOk());

        ivRevoke.setVisibility(View.INVISIBLE);
        ivRevokeBack.setVisibility(View.INVISIBLE);

        Intent intent = getIntent();
        if (intent == null) {
            finish();
            return;
        }

        if(!parseIntentData(intent)) {
            finish();
            return;
        }

        mFillIV.setEnableTouch(false);
        mFillIV.setPanLimit(SubsamplingScaleImageView.PAN_LIMIT_CENTER);

        mInitTask = new InitTask2(this, mUid, mId, isLocal, mPbnType);
        mInitTask.execute();

        mSingleColorModeHelper = new SingleColorModeHelper();
        mReplaceModeHelper = new ReplaceModeHelper();

        mPicTimeRecorder = new PictureTimeRecorder(ColorActivity2.this, mId);
        mPicTimeRecorder.start();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        XLogger.w("onSaveInstanceState");
        processOk();
        scheduleSave(new Runnable() {
            @Override
            public void run() {

            }
        }, true);
    }

    private void processOk() {

        if(mSingleColorModeHelper.getState() == SingleColorModeHelper.STATE_ON) {
            Map<Integer, Integer> record = mSingleColorModeHelper.getRecordData();
            mSingleColorModeHelper.setState(SingleColorModeHelper.STATE_PROCESS_BACK);
            mFillIV.scheduleRefillAll(record);
        } else if(mReplaceModeHelper.getState() == ReplaceModeHelper.STATE_ON) {
            mReplaceModeHelper.setState(ReplaceModeHelper.STATE_PROCESS_END);
            RunnableTask task = new RunnableTask();
            task.callback = () -> {
 
                final List<PaletteColorBean> paletteColorBeans = paletteView.getData();
                for(PaletteColorBean bean : paletteColorBeans) {
                    Arrays.fill(bean.state, 0);
                }
                final int paletteId = paletteView.getPaleteId();

                runOnUiThread(() -> {
                    mReplaceModeHelper.setTempColorSet(null);
                    paletteView.setData(paletteColorBeans, paletteId);
                    paletteView.notifyColorPanelDataSetChanged();

                    mReplaceModeHelper.setCurrentColor(0);
                    mReplaceModeHelper.setState(ReplaceModeHelper.STATE_OFF);
                    setUIReplaceMode(false);
                });
            };
            mFillIV.scheduleRunnableTask(task);
        }
    }

    private int tipIndex = 0;
    private void processTips() {

        if(mBadAreas.size() == 0) {
            return;
        }

        int i = tipIndex % mBadAreas.size();
        SToast.show("(" + i + ")");

        mFillIV.animateToArea(mBadAreas.get(i));
        tipIndex++;
    }

    @Override
    public void onBackPressed() {

        XLogger.d("ColorActivity onBackPressed");

        if(mSingleColorModeHelper.getState() != SingleColorModeHelper.STATE_OFF) {
            SToast.show("查看模式不可保存");
            return;
        }

        if(mReplaceModeHelper.getState() != ReplaceModeHelper.STATE_OFF) {
            SToast.show("替换模式不可保存");
            return;
        }

        scheduleSave(() -> runOnUiThread(() -> {

            if(mProgressDialog != null) {
                mProgressDialog.dismiss();
            }
            SToast.show("saved");

            Intent intent = new Intent();
            intent.putExtra("id", mId);
            intent.putExtra("regionUpdated", regionUpdated);
            setResult(RESULT_OK, intent);
            ColorActivity2.super.onBackPressed();
        }), false);
    }

    @Override
    protected void onStart() {
        super.onStart();
        XLogger.d("ColorActivity onStart");
    }

    @Override
    protected void onStop() {
        super.onStop();
        XLogger.d("ColorActivity onStop");
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mPicTimeRecorder != null) {
            mPicTimeRecorder.resume();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mPicTimeRecorder != null) {
            mPicTimeRecorder.pause();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (mInitTask != null) {
            mInitTask.cancel(true);
        }

        mFillIV.release();

        if(mProgressDialog != null) {
            mProgressDialog.dismiss();
        }

        XLogger.d("ColorActivity onDestroy");
    }

    void handleInitComplete(InitTask2.ResultSet rs) {
        if (rs == null) {
            String errMsg = mInitTask.getErrMsg();
            Toast.makeText(App.getInstance(),
                    "Error!", Toast.LENGTH_SHORT).show();
            Intent data = new Intent();
            data.putExtra("errMsg", errMsg);
            setResult(RESULT_INIT_FAILED, data);
            finish();
            return;
        }

        mTotalCount = rs.areaCount;

        mFillIV.setOnStateChangedListener(new ImageOnStateChangedListener());
        mFillIV.setOnImageEventListener(new ImageEventListener());
        mFillIV.initColorOriginImage(rs.colorOriginImage);
        mFillIV.setFillColorListener(this);
        mFillIV.setImageLongClickListener(this);
        mBadAreas = new LinkedList<>();
        mBadAreas.addAll(rs.badAreasSet);
        TextView tvBad = findViewById(R.id.tv_bad);
        tvBad.setText(String.valueOf(mBadAreas.size()));
        ivRevoke.setVisibility(View.INVISIBLE);
        ivRevoke.setOnClickListener(v -> mFillIV.scheduleRevoke());

        ivRevokeBack.setVisibility(View.INVISIBLE);
        ivRevokeBack.setOnClickListener(v -> mFillIV.scheduleRevokeBack());

        ivReset.setOnClickListener(v -> alertToReset());

        mPaletteSelection = new PaletteSelection();
        mPaletteSelection.init(rs);

        mColorRecordManager = new ColorRecordManager(rs.colorRecords, rs.recordCount, mPaletteSelection);

        List<PaletteColorBean> paletteColorBeans = mPaletteSelection.setSelection(PaletteSelection.PALETTE_COMMON);

        paletteView.setColorSelectedListener((selection, colorBean) -> {
            int color = colorBean.data[selection[1]];
            mFillIV.setColor(color);
        });

        switchPalette(paletteColorBeans, PaletteSelection.PALETTE_COMMON);

        mColorRecordManager.setupView(rvRecord);
        mColorRecordManager.setRecordItemClickListener((holder, bean, position) -> {
            handleColorRecordItemChange(bean);
        });

        mColorRecordManager.setRecordItemSwipeCallback((adapter, record, pos, direction) ->
        {
            if(direction == ItemTouchHelper.UP) {
                alertToDeleteColor(adapter, record);
            } else if(direction == ItemTouchHelper.DOWN) {
                alertToChangeColorMode(adapter, record, pos);
            }
        });

        tvCount.setText(String.format(Locale.US, "%d/%d", rs.recordCount, mTotalCount));

        mColorRecordManager.setCountChangeCallback(count -> {
            tvCount.setText(String.format(Locale.US, "%d/%d", count, mTotalCount));
            checkSelectedColor(paletteView.getData());
            paletteView.notifyColorPanelDataSetChanged();
        });


        fPalette.setOnClickListener(v -> showPaletteSwitch());
    }

    private void handleColorRecordItemChange(ColorRecord bean) {
        if(bean.paletteId == mPaletteSelection.getSelection()) {
            final int color = bean.color;
            paletteView.setSelectionByColor(color);
            mFillIV.setColor(color);
        } else {
            if(bean.paletteId == -1) {
                SToast.show("not found");
                return;
            }
            List<PaletteColorBean> data = mPaletteSelection.setSelection(bean.paletteId);
            switchPalette(data, bean.paletteId);
            final int color = bean.color;
            paletteView.setSelectionByColor(color);
            mFillIV.setColor(color);
            SToast.show("切换到 " + PaletteSelection.getPaletteName(bean.paletteId));
        }
    }

    private void setUISingleColorMode(boolean on) {
        if(on) {
            mFillIV.setClickFilter((x, y, color) -> true);
            ivBack.setVisibility(View.INVISIBLE);
            ivQR.setVisibility(View.INVISIBLE);
            ivRevoke.setVisibility(View.INVISIBLE);
            ivRevokeBack.setVisibility(View.INVISIBLE);
            fOk.setVisibility(View.VISIBLE);
            mColorRecordManager.setItemMovable(false);
            ivReset.setVisibility(View.INVISIBLE);
            tvUiMode.setVisibility(View.VISIBLE);
            tvUiMode.setText("查看模式");
        } else {
            mFillIV.setClickFilter(null);
            ivReset.setVisibility(View.VISIBLE);
            ivRevoke.setVisibility(canRevoke ? View.VISIBLE : View.INVISIBLE);
            ivRevokeBack.setVisibility(canRevokeBack ? View.VISIBLE : View.INVISIBLE);
            ivBack.setVisibility(View.VISIBLE);
            ivQR.setVisibility(View.VISIBLE);
            fOk.setVisibility(View.GONE);
            mColorRecordManager.setItemMovable(true);
            tvUiMode.setText("");
            tvUiMode.setVisibility(View.GONE);
        }
    }

    private void setUIReplaceMode(boolean on) {
        if(on) {
            mFillIV.setClickFilter((x, y, color) -> {
                if(color != null) {
                    processReplace(color);
                }
                return true;
            });
            ivBack.setVisibility(View.INVISIBLE);
            ivQR.setVisibility(View.INVISIBLE);
            fOk.setVisibility(View.VISIBLE);
            mColorRecordManager.setItemMovable(false);
            tvUiMode.setVisibility(View.VISIBLE);
            tvUiMode.setText("替换模式");
            ivReset.setVisibility(View.INVISIBLE);
            ivRevoke.setVisibility(View.INVISIBLE);
            ivRevokeBack.setVisibility(View.INVISIBLE);
        } else {
            mFillIV.setClickFilter(null);
            ivBack.setVisibility(View.VISIBLE);
            ivQR.setVisibility(View.VISIBLE);
            fOk.setVisibility(View.GONE);
            mColorRecordManager.setItemMovable(true);
            tvUiMode.setText("");
            tvUiMode.setVisibility(View.GONE);
            ivReset.setVisibility(View.VISIBLE);
            ivRevoke.setVisibility(canRevoke ? View.VISIBLE : View.INVISIBLE);
            ivRevokeBack.setVisibility(canRevokeBack ? View.VISIBLE : View.INVISIBLE);
        }
    }

    private void processReplace(int selectColor) {

        if(mReplaceModeHelper.getState() != ReplaceModeHelper.STATE_ON) {
            if(BuildConfig.DEBUG) {
                SToast.show("err");
            }
            return;
        }

        int originColor = mReplaceModeHelper.getCurrentColor();

        mFillIV.scheduleOverWriteColor(originColor, selectColor);

    }

    private void showPaletteSwitch() {
        mPaletteSelection.showSelectionDialog(this,
                paletteColorBeans -> {
                    switchPalette(paletteColorBeans, mPaletteSelection.getSelection());
                });
    }

    private void switchPalette(List<PaletteColorBean> data, int id) {

        if(mReplaceModeHelper.getState() == ReplaceModeHelper.STATE_ON) {
            Set<Integer> colorSet =  mReplaceModeHelper.getTempColorSet();

            for(PaletteColorBean bean : data) {
                int[] colors = bean.data;
                int len = colors.length;
                for(int i = 0; i < len; i++) {
                    if(colorSet.contains(colors[i])) {
                        bean.state[i] = 1;
                    } else {
                        bean.state[i] = 0;
                    }
                }
            }
        }
        checkSelectedColor(data);

        paletteView.setData(data, id);
        mColorRecordManager.setPaletteId(id);
    }

    private void checkSelectedColor(List<PaletteColorBean> data){
        List<ColorRecord> colorRecordList = mColorRecordManager.getColorRecords();
        final Set<Integer> colorSet = new HashSet<>();

        for(ColorRecord r : colorRecordList) {
            colorSet.add(r.color);
        }
        for(PaletteColorBean bean : data) {
            int[] colors = bean.data;
            int len = colors.length;
            for(int i = 0; i < len; i++) {
                bean.selected[i] = colorSet.contains(colors[i]) ? 1 : 0;
            }
        }
    }

    private void changeTextColor(TextView tvCount) {
        int tag = (int) tvCount.getTag();
        tag++;
        int[] color = {0xff969696, 0xffff7182, 0xff0500cd};
        tvCount.setTextColor(color[tag % 3]);
        tvCount.setTag(tag);
    }

    private void alertToReset() {
        new AlertDialog.Builder(this)
                .setMessage("确定重置吗(无法恢复)?")
                .setPositiveButton("确定", (dialog, which) -> {
                    dialog.dismiss();
                    mFillIV.scheduleReset();
                    PictureTimeRecorder.clearRecord(this, mId);
                })
                .setNegativeButton("取消", (dialog, which) -> dialog.dismiss())
                .show();
    }

    private void alertToDeleteColor(final RecyclerView.Adapter adapter, final ColorRecord record) {

        AlertDialog d = new AlertDialog.Builder(this)
                .setMessage("确定删除？")
                .setPositiveButton("确定", (dialog, which) -> {
                    ((Dialog)dialog).setOnDismissListener(null);
                    dialog.dismiss();
                    mFillIV.scheduleOverWriteColor(record.color, null);
                })
                .setNegativeButton("取消", (dialog, which) -> {
                    dialog.dismiss();
                }).show();
        d.setOnDismissListener(dialog -> adapter.notifyDataSetChanged());

    }

    private void alertToChangeColorMode(final RecyclerView.Adapter adapter, final ColorRecord record, int pos) {

        String[] items = new String[] {
                "查看模式",
                "替换模式"
        };

        AlertDialog d = new AlertDialog.Builder(this)
                .setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(which == 0) {
                            scheduleSingleColorMode(record);
                        } else {
                            scheduleReplaceMode(record);
                        }
                    }
                }).show();

        d.setOnDismissListener(dialog -> adapter.notifyDataSetChanged());
    }

    //schedule单颜色查看模式
    private void scheduleSingleColorMode(final ColorRecord record) {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
        }

        mProgressDialog.setMessage("Processing");
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();

        RunnableTask runnableTask = new RunnableTask();
        runnableTask.callback = () -> saveStateBeforeSingleColor(record.color);
        mFillIV.scheduleRunnableTask(runnableTask);
    }

    //准备replace模式
    private void scheduleReplaceMode(final ColorRecord record) {
        if(mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
        }
        mProgressDialog.setMessage("Processing");
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();

        mReplaceModeHelper.setState(ReplaceModeHelper.STATE_PROCESS_START);
        final int originColor = (record.color|0xff000000);
        final RunnableTask task = new RunnableTask();

        final List<ColorRecord> colorRecords =  mColorRecordManager.getColorRecords();

        task.callback = () -> {

            final Set<Integer> colorSet = new HashSet<>();

            for(ColorRecord r : colorRecords) {
                colorSet.add(r.color);
            }

            final List<PaletteColorBean> paletteColorBeans = paletteView.getData();
            int paletteId = paletteView.getPaleteId();

            for(PaletteColorBean bean : paletteColorBeans) {
                int[] data = bean.data;
                int len = data.length;
                for(int i = 0; i < len; i++) {
                    if(colorSet.contains(data[i])) {
                        bean.state[i] = 1;
                        bean.selected[i] = 1;
                    } else {
                        bean.state[i] = 0;
                        bean.selected[i] = 0;
                    }
                }
            }

            runOnUiThread(() -> {
                mReplaceModeHelper.setTempColorSet(colorSet);

                paletteView.setData(paletteColorBeans, paletteId);
                paletteView.notifyColorPanelDataSetChanged();

                mProgressDialog.dismiss();
                mReplaceModeHelper.setCurrentColor(originColor);

                mReplaceModeHelper.setState(ReplaceModeHelper.STATE_ON);
                setUIReplaceMode(true);
            });
        };

        mFillIV.scheduleRunnableTask(task);
    }

    private void scheduleSave(Runnable end, boolean isBackground) {

        XLogger.d("scheduleSave background=" + isBackground);
        if(!isBackground) {
            if (mProgressDialog == null) {
                mProgressDialog = new ProgressDialog(this);
            }
            mProgressDialog.setMessage("Saving");
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
        }
        RunnableTask runnableTask = new RunnableTask();
        runnableTask.callback = this::processSave;
        mFillIV.scheduleRunnableTask(runnableTask);


        if (isLocal) {
            runnableTask = new RunnableTask();
            runnableTask.callback = end;
            mFillIV.scheduleRunnableTask(runnableTask);
            return;
        }
        PbnPaintGenerateTask mPbnPaintGenerateTask = new PbnPaintGenerateTask(mUid, false,
                false, mId, (suc, errMsg) -> {
            //upload
            VincentUploadPaint vincentUploadPaint = new VincentUploadPaint(mUid, mId, false);
            vincentUploadPaint.doUpload("", mRegion, resp -> {

                RunnableTask run = new RunnableTask();
                run.callback = end;
                mFillIV.scheduleRunnableTask(run);
            });
        });
        mPbnPaintGenerateTask.execute();
    }

    private void saveStateBeforeSingleColor(int color) {
        final HashMap<Integer, FloodFillArea> areaMap = mFillIV.getAreaMap();
        final HashMap<Integer, Integer> dataMap = new HashMap<>();
        final HashMap<Integer, Integer> refillMap = new HashMap<>();
        for(Map.Entry<Integer, FloodFillArea> entry : areaMap.entrySet()) {
            FloodFillArea area = entry.getValue();
            if(area.color != null) {
                dataMap.put(entry.getKey(), area.color);

                if((area.color|0xff000000) == (color|0xff000000)) {
                    refillMap.put(entry.getKey(), color);
                }
            }
        }
        runOnUiThread(() -> {
            mSingleColorModeHelper.recordState(dataMap);
            boolean r = mFillIV.scheduleRefillAll(refillMap);
            if(r) {
                mSingleColorModeHelper.setState(SingleColorModeHelper.STATE_PROCESS_START);
            }
        });
    }

    private void processSave() {
        Runnable r = () -> {
            XLogger.d("save start");
            if (!regionUpdated){
                HashMap<Integer, FloodFillArea> areaMap = mFillIV.getAreaMap();
                LocalData.saveColorMapByAreaMap(mUid, isLocal, mId, areaMap);
            }

            Bitmap bmp = mFillIV.getEditedBitmap();
            int sWidth = bmp.getWidth();
            int sHeight = bmp.getHeight();
            LocalData.saveEditBitmap(mUid, isLocal, mId, bmp);
            File pdf = LocalData.getTargetPdfFile(mUid, isLocal, mId);
            Bitmap fore = FillColorHelper.loadPDF(pdf, sWidth, sHeight);
            Bitmap thumb = Bitmap.createBitmap(sWidth, sHeight, Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(thumb);
            Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
            paint.setFilterBitmap(true);
            canvas.drawBitmap(bmp, 0, 0, paint);
            canvas.drawBitmap(fore, 0, 0, paint);

            fore.recycle();
            thumb.setPremultiplied(false); //防止彩色线条看起来异常
            LocalData.saveThumbBitmap(mUid, isLocal, mId, thumb);

            thumb.recycle();

            PbnDb.get().getPicRecordDao()
                    .updateUpdateTime(mId, System.currentTimeMillis());

            XLogger.d("save end");

            runOnUiThread(() -> {

                if (!regionUpdated){
                    final int[] sort = mColorRecordManager.getSort();

                    LocalData.saveSort(mUid, isLocal, mId, sort);

                    if(mProgressDialog != null) {
                        mProgressDialog.dismiss();
                    }
                    SToast.show("saved");
                }
            });
        };
        r.run();
    }

    @Override
    public void onAreaFilled(int num, FillColorResp resp) {
        //0  : same color
        //1  : fill color
        //2  : over write color
        //3  : clear color
        //4  : none color
        final int fillType = resp.fillType;

        if(fillType == 0) {
            mColorRecordManager.onColorCleared(resp.preColor);
        } else if(fillType == 1) {
            mColorRecordManager.onColorFilled(resp.color);
        } else if(fillType == 2) {
            mColorRecordManager.onColorOverWrite(resp.color, resp.preColor);
        } else if(fillType == 3) {
            mColorRecordManager.onColorCleared(resp.preColor);
        }

    }

    @Override
    public void onRecalledFillColor(boolean success, RevokeSingleColorResp resp) {
        if(!success) {
            return;
        }

        final int fillType = resp.revokedFillType;

        if(!resp.isBack) {
            if (fillType == 0) {
                mColorRecordManager.onColorFilled(resp.preColor);
            } else if (fillType == 1) {
                mColorRecordManager.onColorCleared(resp.targetColor);
            } else if (fillType == 2) {
                mColorRecordManager.onColorOverWrite(resp.preColor, resp.targetColor);
            } else if (fillType == 3) {
                mColorRecordManager.onColorFilled(resp.preColor);
            }
        } else {
            if (fillType == 0) {
                mColorRecordManager.onColorCleared(resp.preColor);
            } else if (fillType == 1) {
                mColorRecordManager.onColorFilled(resp.targetColor);
            } else if (fillType == 2) {
                mColorRecordManager.onColorOverWrite(resp.targetColor, resp.preColor);
            } else if (fillType == 3) {
                mColorRecordManager.onColorCleared(resp.preColor);
            }
        }
    }

    @Override
    public void onRecalledOverwrite(boolean success, RevokeOverwriteResp resp) {
        if(!success) {
            return;
        }

        if(!resp.isBack) {
            mColorRecordManager.onColorOverWriteRevoke(resp.preColor,
                    resp.dstColor,
                    resp.areas.length);
        } else {
            mColorRecordManager.onColorOverWriteRevoke(resp.dstColor,
                    resp.preColor,
                    resp.areas.length);
        }
    }

    @Override
    public void onOverwriteColor(OverwriteColorResp resp) {
        mColorRecordManager.onColorsOverWrite(resp.srcColor, resp.dstColor, resp.areas.length);

        if(mReplaceModeHelper.getState() == ReplaceModeHelper.STATE_ON) {
            mReplaceModeHelper.setCurrentColor(resp.dstColor);
        }
    }

    @Override
    public void onReset() {
        mColorRecordManager.onReset();
        Runnable r = () -> PbnDb.get().getPicRecordDao().resetVersion(mId, PictureRecord.VERSION_CURRENT);
        new Thread(r).start();
    }

    @Override
    public void onRefilled() {
        if(mProgressDialog != null) {
            mProgressDialog.dismiss();
        }
        int singleColorState = mSingleColorModeHelper.getState();
        if(singleColorState == SingleColorModeHelper.STATE_PROCESS_START) {
            mSingleColorModeHelper.setState(SingleColorModeHelper.STATE_ON);
            setUISingleColorMode(true);
        } else if(singleColorState == SingleColorModeHelper.STATE_PROCESS_BACK) {
            mSingleColorModeHelper.setState(SingleColorModeHelper.STATE_OFF);
            mSingleColorModeHelper.recordState(null);
            setUISingleColorMode(false);
        }
    }

    @Override
    public void onRevokeCacheStateChanged(boolean empty) {
        canRevoke = !empty;
        ivRevoke.setVisibility(empty ? View.INVISIBLE : View.VISIBLE);
    }

    @Override
    public void onRevokeBackCacheStateChanged(boolean empty) {
        canRevokeBack = !empty;
        ivRevokeBack.setVisibility(empty ? View.INVISIBLE : View.VISIBLE);
    }

    @Override
    public boolean onImageLongClicked(int positionColor) {

        if(positionColor != Color.WHITE) {
            Pair<Integer, ColorRecord> rs = mColorRecordManager.getColorRecordByColor(positionColor);
            if(rs != null) {
                assert rs.first != null;
                assert rs.second != null;
                int palette = mPaletteSelection.getSelection();
                handleColorRecordItemChange(rs.second);
                if(palette != mPaletteSelection.getSelection()) {
                    SToast.show("切换到"+ PaletteSelection.getPaletteName(mPaletteSelection.getSelection())
                            + " 选中色号" + (rs.first + 1));
                } else {
                    SToast.show("选中色号" + (rs.first + 1));
                }
            }
        }
        return true;
    }

    private class ImageEventListener extends SubsamplingScaleImageView.DefaultOnImageEventListener {
        @Override
        public void onImageLoaded() {
            super.onImageLoaded();
            mFillIV.setEnableTouch(true);
            fTips.setVisibility(View.VISIBLE);
        }

        @Override
        public void onImageLoadError(Exception e) {
            super.onImageLoadError(e);
            setResult(RESULT_INIT_FAILED);
            //很可能是pdf异常（下载失败）,删掉重新重进会再下载
            LocalData.getTargetPdfFile(mUid, isLocal, mId).delete();
            finish();
        }
    }

    private class ImageOnStateChangedListener implements SubsamplingScaleImageView.OnStateChangedListener {

        @Override
        public void onScaleChanged(float newScale, int origin) {

        }

        @Override
        public void onCenterChanged(PointF newCenter, int origin) {

        }

        @Override
        public void onCenterOrScaleChanged(float newScale, PointF newCenter, int origin) {

        }
    }

    private void onQR(){

        if (!isLocal) {
            SToast.show("线上素材不支持本地替换线框图，请在着色平台更新pdf");
            return;
        }
        new AlertDialog.Builder(this)
                .setMessage("需要扫描二维码，更新线框图吗？ 请保证线框图一致性，否则可能导致着色进度丢失。")
                .setNegativeButton("取消", (dialog, which) -> dialog.dismiss())
                .setPositiveButton("确定", (dialog, which) -> {
                    dialog.dismiss();
                    startZbarScan();
                })
                .show();
    }

    private void startZbarScan() {
        SToast.show("正在打开相机");
        Intent intent = new Intent(this, ZBarActivity.class);
        startActivityForResult(intent, REQUEST_ZBAR);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REQUEST_ZBAR) {
            if(resultCode == RESULT_OK) {
                assert data != null;
                PbnScanResult rs = data.getParcelableExtra("data");
                startDownload(rs);
            }
        }
    }


    private void startDownload(PbnScanResult rs) {

        if(mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
        }

        mProgressDialog.show();
        mProgressDialog.setMessage("下载新的线框图素材...");
        mProgressDialog.setCancelable(false);

        PicRegionDownloader mDownloader = new PicRegionDownloader(mUid, isLocal, rs.getUrl(),
                rs.getPbnType(), result -> {
            mProgressDialog.dismiss();
            if(result.code == PicRegionDownloader.RESULT_OK) {
                new AlertDialog.Builder(this)
                        .setMessage("上次着色之后线框发生变化，是否保留上色数据?")
                        .setPositiveButton("是", (dialog, which) -> runPlanUpdate(result, isLocal))
                        .setNegativeButton("否", (dialog, which) -> { })
                        .show();

            } else {
                SToast.show(result.errMsg);
            }
        });
        mDownloader.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void runPlanUpdate(PicRegionDownloader.Result item, boolean isLocal) {
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage("正在更新着色方案.....");
        mProgressDialog.show();
        LocalPicPlanUpdateTask task = new LocalPicPlanUpdateTask(mUid, mId, item.targetDir, isLocal,
                rs -> {
                    mProgressDialog.dismiss();
                    handleUpdatePlanResult(rs);
                });
        task.execute();
    }

    private boolean regionUpdated = false;

    private void handleUpdatePlanResult(boolean rs) {
        if(!rs) {
            //something went wrong
            //todo send analyze event
            new AlertDialog.Builder(this)
                    .setMessage("更新着色方案出错")
                    .setPositiveButton("ok", (dialog, which) -> {
                    }).show();
        } else {
            SToast.show("更新着色方案成功！ 请重新进入......");
            regionUpdated = true;
            onBackPressed();
        }
    }
}
