package com.ober.artist.color.data;

import android.graphics.Bitmap;

import com.meevii.color.fill.model.core.origin.ColorOriginImage;

import java.util.Map;

/**
 * Created by ober on 2019-10-29.
 */
public class ColorPreparedData2 {

    public RegionData region;
    public ColorOriginImage colorOriginImage;
    public Map<Integer, Integer> colorMap;

    @Deprecated //not need anymore
    public Bitmap editBitmap;

    public String uid;
    public String id;
}
