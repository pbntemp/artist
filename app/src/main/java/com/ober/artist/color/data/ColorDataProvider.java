package com.ober.artist.color.data;

import android.content.Context;
import android.graphics.BitmapFactory;

import com.meevii.color.fill.FillColorConfig;
import com.meevii.color.fill.model.core.origin.ColorOriginImage;
import com.meevii.color.fill.model.core.origin.PdfOriginImage;
import com.ober.artist.color.localdata.LocalData;

import java.io.File;
import java.util.Map;

/**
 * Created by ober on 2018/12/4.
 */
public class ColorDataProvider {

    public static ColorPreparedData2 prepareData2(String uid, boolean isLocal, String id) {
        ColorPreparedData2 data = new ColorPreparedData2();

        ColorOriginImage colorOriginImage = PdfOriginImage.fromFile(
                LocalData.getTargetPdfFile(uid, isLocal, id), FillColorConfig.getScale());
        File regionFile = LocalData.getTargetRegionFile(uid, isLocal, id);

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;

        BitmapFactory.decodeFile(regionFile.getAbsolutePath(), options);
        if(options.outMimeType != null) {

            RegionData region = new RegionData();
            region.file = regionFile;
            region.mimetype = options.outMimeType;
            region.size = new int[] {options.outWidth, options.outHeight};
            data.region = region;
        }

        Map<Integer, Integer> colorMap = LocalData.loadColorMap(uid, isLocal, id);

        data.colorOriginImage = colorOriginImage;
        data.uid = uid;
        data.id = id;
        data.colorMap = colorMap;

        /*File editFile = LocalData.getTargetEditFile(uid, isLocal, id);
        if (editFile.exists()) {
            data.editBitmap = BitmapFactory.decodeFile(editFile.getAbsolutePath());
        }*/

        return data;
    }

    public static ColorPreparedData prepareData(Context c, String packName, String token) {
        ColorPreparedData data = new ColorPreparedData();

        ColorOriginImage colorOriginImage = PdfOriginImage.fromFile(
                LocalStore.getPdfFile(c, packName, token), FillColorConfig.getScale());
        File regionFile = LocalStore.getRegionFile(c, packName, token);

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;

        BitmapFactory.decodeFile(regionFile.getAbsolutePath(), options);
        if(options.outMimeType != null) {

            RegionData region = new RegionData();
            region.file = regionFile;
            region.mimetype = options.outMimeType;
            region.size = new int[] {options.outWidth, options.outHeight};
            data.region = region;
        }

        Map<Integer, Integer> colorMap = LocalStore.loadColorMap(c, packName, token);

        data.colorOriginImage = colorOriginImage;
        data.token = token;
        data.colorMap = colorMap;

        File editFile = LocalStore.getEditBmpFile(c, packName, token);
        if (editFile.exists()) {
            data.editBitmap = BitmapFactory.decodeFile(editFile.getAbsolutePath());
        }

        return data;
    }
}
