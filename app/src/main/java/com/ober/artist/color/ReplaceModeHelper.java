package com.ober.artist.color;

import java.util.Set;

/**
 * Created by ober on 2019-10-15.
 */
public class ReplaceModeHelper {

    public static final int STATE_OFF = 0;
    public static final int STATE_ON = 1;
    public static final int STATE_PROCESS_START = 2;
    public static final int STATE_PROCESS_END = 3;

    private int state = STATE_OFF;

    private int currentColor;

    private Set<Integer> mTempColorSet;

    public void setTempColorSet(Set<Integer> mTempColorSet) {
        this.mTempColorSet = mTempColorSet;
    }

    public Set<Integer> getTempColorSet() {
        return mTempColorSet;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public void setCurrentColor(int color) {
        this.currentColor = color;
    }

    public int getCurrentColor() {
        return currentColor;
    }
}
