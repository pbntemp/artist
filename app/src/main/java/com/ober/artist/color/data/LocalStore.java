package com.ober.artist.color.data;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Environment;

import com.google.gson.Gson;
import com.meevii.color.fill.model.core.FloodFillArea;
import com.ober.artist.utils.ColorUtil;
import com.ober.artist.utils.IOUtil;
import com.ober.artist.utils.PreColorCoder;
import com.ober.artist.utils.PreColorSorter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by ober on 2018/12/4.
 */
public class LocalStore {

    private static final String VERSION = "x2";

    private static final String ARCH_DIR = "arch" + VERSION;

    public static File getAreaMapFile(Context c, String packName, String token) {
        return new File(tokenDir(packName, token), "area");
    }

    public static File getPdfFile(Context c, String packName, String token) {
        return new File(tokenDir(packName, token), "pdf");
    }

    public static File getRegionFile(Context c, String packName, String token) {
        return new File(tokenDir(packName, token), "region");
    }

    public static File getPngFile(Context c, String packName, String token) {
        return new File(tokenDir(packName, token), "png");
    }

    public static File getEditBmpFile(Context c, String packName, String token) {
        return new File(tokenDir(packName, token), "edit");
    }

    public static File getThumbFile(Context c, String packName, String token) {
        return new File(tokenDir(packName, token), "thumb");
    }

    public static File getCenterFile(Context c, String packName, String token) {
        return new File(tokenDir(packName, token), "center");
    }

    public static File getPlanFile(Context c, String packName, String token) {
        return new File(tokenDir(packName, token), "plan");
    }

    public static File getSortFile(Context c, String packName, String token) {
        return new File(tokenDir(packName, token), "defaultSort");
    }

    public static File packDir(String packName) {
        File f = new File(Environment.getExternalStorageDirectory().getAbsolutePath()
                + "/AA_PBN/" + ARCH_DIR + "/" + packName);
        if (!f.exists()) {
            f.mkdirs();
        }
        return f;
    }

    private static File tokenDir(String packName, String token) {
        File f = new File(packDir(packName), token);
        if (!f.exists()) {
            f.mkdirs();
        }
        return f;
    }

    public static File archDir() {
        File f = new File(Environment.getExternalStorageDirectory().getAbsolutePath()
                + "/AA_PBN/" + ARCH_DIR);
        if (!f.exists()) {
            f.mkdirs();
        }
        return f;
    }

    public static File appDir() {
        File f = new File(Environment.getExternalStorageDirectory().getAbsolutePath()
                + "/AA_PBN");
        if (!f.exists()) {
            f.mkdirs();
        }
        return f;
    }

    public static void saveEditBitmap(Context c, String packName, String token, Bitmap bmp) {
        File f = getEditBmpFile(c, packName, token);
        f.delete();
        IOUtil.saveBitmapTo(bmp, f);
    }

    public static void saveThumbBitmap(Context c, String packName, String token, Bitmap bmp) {
        File f = getThumbFile(c, packName, token);
        f.delete();
        IOUtil.saveBitmapTo(bmp, f);
    }

    public static Map<Integer, Integer> loadColorMap(File f) {
        if (!f.exists()) {
            return null;
        }
        int[] data;
        try {
            String json = IOUtil.inputStream2String(
                    new FileInputStream(f),
                    "UTF-8");
            data = new Gson().fromJson(json, int[].class);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < data.length / 2; i++) {
            map.put(data[2 * i], data[2 * i + 1]);
        }
        return map;
    }

    public static Map<Integer, Integer> loadColorMap(Context c, String packName, String token) {
        File f = getAreaMapFile(c, packName, token);
        return loadColorMap(f);
    }

    public static void savePlanByAreaMap(Context c, File planFile,
                                         File areaFile, Map<Integer, Center> centerMap,
                                         int[] order) {

        Map<Integer, Integer> areaColorMap = loadColorMap(areaFile);

        if (areaColorMap == null) {
            return;
        }

        Iterator<Map.Entry<Integer, Integer>> it = areaColorMap.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<Integer, Integer> areaColor = it.next();
            if (!centerMap.containsKey(areaColor.getKey())) {
                it.remove();
            }
        }

        Map<Integer, Set<Integer>> colorAreas = new HashMap<>();
        for (Map.Entry<Integer, Integer> entry : areaColorMap.entrySet()) {
            Integer color = entry.getValue();
            int area = entry.getKey();
            Set<Integer> areaSet = colorAreas.get(color);
            if (areaSet == null) {
                areaSet = new HashSet<>();
                colorAreas.put(color, areaSet);
            }
            areaSet.add(area);
        }

        List<PreColor> preColors = new LinkedList<>();
        for (Map.Entry<Integer, Set<Integer>> entry : colorAreas.entrySet()) {
            int color = entry.getKey();
            int[] areas = new int[entry.getValue().size()];
            int j = 0;
            for (Integer i : entry.getValue()) {
                areas[j] = i;
                j++;
            }
            PreColor preColor = new PreColor();
            preColor.areas = areas;
            preColor.color = ColorUtil.color2String(color, false);
            preColors.add(preColor);
        }

        final List<PreColor> sorted;
        if(order == null) {
            sorted = PreColorSorter.defaultSort(preColors);
        } else {
            sorted = PreColorSorter.sort(preColors, order);
        }

        String encoded = PreColorCoder.encode(sorted);

        planFile.delete();
        try {
            IOUtil.stringToOutputStream(encoded,
                    new FileOutputStream(planFile));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void saveColorMapByAreaMap(Context c, String packName, String token, Map<Integer, FloodFillArea> areaMap) {
        Map<Integer, Integer> colorMap = new HashMap<>();
        for (Map.Entry<Integer, FloodFillArea> entry : areaMap.entrySet()) {
            FloodFillArea area = entry.getValue();
            if (area.color != null) {
                colorMap.put(entry.getKey(), area.color);
            }
        }
        saveColorMap(c, packName, token, colorMap);
    }

    public static void saveColorMap(Context c, String packName, String token, Map<Integer, Integer> map) {
        if (map == null || map.isEmpty()) {
            File f = getAreaMapFile(c, packName, token);
            f.delete();
            return;
        }

        int size = map.size();
        int[] data = new int[size * 2];

        Iterator<Map.Entry<Integer, Integer>> it = map.entrySet().iterator();
        int index = 0;
        while (it.hasNext()) {
            Map.Entry<Integer, Integer> entry = it.next();
            data[index * 2] = entry.getKey();
            data[index * 2 + 1] = entry.getValue();
            index++;
        }
        String json = new Gson().toJson(data);
        File f = getAreaMapFile(c, packName, token);
        f.delete();
        try {
            IOUtil.stringToOutputStream(json,
                    new FileOutputStream(f));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void saveSort(Context c, String packName, String token, int[] sort) {
        if(sort == null || sort.length == 0) {
            return;
        }
        File f = getSortFile(c, packName, token);
        String json = new Gson().toJson(sort);
        f.delete();
        try {
            IOUtil.stringToOutputStream(json,
                    new FileOutputStream(f));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static int[] readSort(Context c, String packName, String token) {
        File f = getSortFile(c, packName, token);
        if(!f.exists()) {
            return null;
        }
        int[] data;
        try {
            String json = IOUtil.inputStream2String(
                    new FileInputStream(f),
                    "UTF-8");
            data = new Gson().fromJson(json, int[].class);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return data;
    }
}
