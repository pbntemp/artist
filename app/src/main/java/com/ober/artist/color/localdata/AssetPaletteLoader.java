package com.ober.artist.color.localdata;

import android.content.res.AssetManager;
import android.graphics.Color;

import com.ober.palette.PaletteColorBean;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by ober on 19-5-6.
 */
public class AssetPaletteLoader {

    public static List<PaletteColorBean> loadFromAssets(AssetManager am, String file) {
        BufferedReader br = null;
        List<PaletteColorBean> result = new LinkedList<>();

        try {
            InputStream is = am.open(file);
            InputStreamReader reader = new InputStreamReader(is);
            br = new BufferedReader(reader);

            String line;
            int cursor = 0;
            int tempColor = 0;
            int[] tempData = null;

            while ((line = br.readLine()) != null) {
                if(cursor % 4 == 0) {

                } else if(cursor % 4 == 1) {
                    tempColor = (Color.parseColor("#" + line) | 0xff000000);
                } else if(cursor % 4 == 2) {
                    tempData = new int[12];
                    readData(line, tempData, 0);
                } else {
                    readData(line, tempData, 6);
                    PaletteColorBean bean = new PaletteColorBean(tempColor, tempData);
                    result.add(bean);
                }
                cursor++;
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if(br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return result;
    }


    private static void readData(String line, int[] dst, int offset) {
        String[] ss = line.split(",");
        for(int i = 0; i < ss.length; i++) {
            dst[i + offset] = (Color.parseColor("#" + ss[i]) | 0xff000000);
        }
    }
}
