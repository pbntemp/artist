package com.ober.artist.color;

import java.util.Map;

/**
 * Created by ober on 2019-10-15.
 */
public class SingleColorModeHelper {

    public static final int STATE_OFF = 0;
    public static final int STATE_PROCESS_START = 1;
    public static final int STATE_ON = 2;
    public static final int STATE_PROCESS_BACK = 3;

    private Map<Integer, Integer> mData;
    private int state = STATE_OFF;

    public void recordState(Map<Integer, Integer> data) {
        mData = data;
    }

    public Map<Integer, Integer> getRecordData() {
        return mData;
    }

    public void setState(int state) {
        this.state = state;
    }

    public int getState() {
        return state;
    }

}
