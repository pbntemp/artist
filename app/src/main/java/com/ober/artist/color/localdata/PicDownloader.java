package com.ober.artist.color.localdata;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import com.ober.artist.App;
import com.ober.artist.color.data.LocalStore;
import com.ober.artist.net.bean.Picture;
import com.ober.artist.utils.OkDownloadUtil;

import java.io.File;
import java.io.IOException;

/**
 * Created by ober on 2019/1/10.
 */
public class PicDownloader extends AsyncTask<Void, Integer, Integer> {

    private final String token;
    private final String packName;
    private final Picture picture;

    public PicDownloader(String packName, String token, Picture picture) {
        this.packName = packName;
        this.token = token;
        this.picture = picture;
    }

    @Override
    protected Integer doInBackground(Void... voids) {

        publishProgress(0);

        File region = LocalStore.getRegionFile(App.getInstance(), packName, token);
        if(region.exists()) {
            Bitmap regionBmp = BitmapFactory.decodeFile(region.getAbsolutePath());
            if (regionBmp == null) {
                region.delete();
            } else {
                regionBmp.recycle();
            }
        }
        if (region.exists()) {
            publishProgress(1);
        } else {
            try {
                OkDownloadUtil.download(picture.getRegion(), region);
            } catch (IOException e) {
                e.printStackTrace();
                region.delete();
                return -1;
            }

            Bitmap bmp = BitmapFactory.decodeFile(region.getAbsolutePath());
            if(bmp == null) {
                return -99;
            } else {
                bmp.recycle();
            }

            publishProgress(1);
        }

        if (isCancelled()) {
            return -10;
        }

        File pdf = LocalStore.getPdfFile(App.getInstance(), packName, token);
        if (pdf.exists()) {
            publishProgress(2);
        } else {
            try {
                OkDownloadUtil.download(picture.getPdf(), pdf);
            } catch (IOException e) {
                e.printStackTrace();
                pdf.delete();
                return -2;
            }
            publishProgress(2);
        }

        if (isCancelled()) {
            return -10;
        }

        File png = LocalStore.getPngFile(App.getInstance(), packName, token);
        if(png.exists()) {
            Bitmap pngBmp = BitmapFactory.decodeFile(png.getAbsolutePath());
            if(pngBmp == null) {
                png.delete();
            } else {
                pngBmp.recycle();
            }
        }
        if (png.exists()) {
            publishProgress(3);
        } else {
            try {
                OkDownloadUtil.download(picture.getPng(), png);
            } catch (IOException e) {
                e.printStackTrace();
                png.delete();
                return -3;
            }

            Bitmap bmp = BitmapFactory.decodeFile(png.getAbsolutePath());
            if(bmp == null) {
                return -99;
            } else {
                bmp.recycle();
            }

            publishProgress(3);
        }

        publishProgress(4);
        return 0;
    }
}
