package com.ober.artist.color.localdata;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;

import androidx.core.util.Consumer;

import com.ober.artist.App;
import com.ober.artist.BuildConfig;
import com.ober.artist.utils.SimpleFileEditor;
import com.ober.planrescue.ImageStruct;
import com.ober.planrescue.PlanRescue;

import java.io.File;

/**
 * Created by ober on 2020-03-17.
 */
public class PlanUpdateTask extends AsyncTask<Void, Void, Boolean> {

    private static final String TAG = "PlanUpdateTask";

    private Consumer<Boolean> callback;

    protected final String uid;
    final String pid;
    protected final boolean isLocal;

    public PlanUpdateTask(String uid, String pid,
                   boolean isLocal,
                   Consumer<Boolean> callback) {
        this.callback = callback;
        this.uid = uid;
        this.pid = pid;
        this.isLocal = isLocal;
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
        callback = null;
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        super.onPostExecute(aBoolean);
        Consumer<Boolean> callback = this.callback;
        if(callback != null) {
            callback.accept(aBoolean);
        }
    }

    @Override
    protected Boolean doInBackground(Void... voids) {
        File oldPlanFile = getTargetPlanFile();
        if(oldPlanFile == null || !oldPlanFile.exists()) {
            //没有进度
            return true;
        }

        File oldRegionFile = getTargetRegionOldFile();
        File newRegionFile = getTargetRegionFile();

        File tempPlanFile = new File(App.getInstance().getCacheDir(), "tplan_" + pid);
        if(tempPlanFile.exists()) {
            tempPlanFile.delete();
        }

        ImageStruct oldRegionImage = decodeBitmapFile(oldRegionFile);
        ImageStruct newRegionImage = decodeBitmapFile(newRegionFile);

        if(oldRegionImage == null || newRegionImage == null) {
            //文件逻辑有错
            Log.w(TAG, "region file not available");
            return false;
        }

        Log.i(TAG, "PlanRescue::repair start");
        boolean r = PlanRescue.repair(oldRegionImage,
                newRegionImage,
                oldPlanFile.getAbsolutePath(),
                tempPlanFile.getAbsolutePath(),
                BuildConfig.DEBUG);
        Log.i(TAG, "PlanRescue::repair end success=" + r);

        r &= handleResult(r);

        if(r) {
            String plan = SimpleFileEditor.readFromFile(tempPlanFile.getAbsolutePath());
            LocalData.saveDownloadedPlan(uid, isLocal, pid, plan);
        }
        return r;
    }

    private static ImageStruct decodeBitmapFile(File f) {
        Bitmap bmp = BitmapFactory.decodeFile(f.getAbsolutePath());
        if(bmp == null) {
            return null;
        }
        final int w = bmp.getWidth();
        final int h = bmp.getHeight();
        int[] data = new int[w*h];
        for(int y = 0; y < h; y++) {
            for(int x = 0; x < w; x++) {
                data[y * w + x] = (bmp.getPixel(x, y) & 0x00ffffff);
            }
        }
        bmp.recycle();
        ImageStruct imageStruct = new ImageStruct();
        imageStruct.w = w;
        imageStruct.h = h;
        imageStruct.data = data;
        return imageStruct;
    }

    protected File getTargetPlanFile(){
        return LocalData.getTargetPlanFile(uid, isLocal, pid);
    }

    protected File getTargetRegionOldFile(){
        return LocalData.getTargetRegionOldFile(uid, isLocal, pid);
    }


    protected File getTargetRegionFile(){
        return LocalData.getTargetRegionFile(uid, isLocal, pid);
    }

    protected boolean handleResult(Boolean result){
        return true;
    }
}
