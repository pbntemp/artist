package com.ober.artist.color.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

import com.ober.artist.R;

/**
 * Created by ober on 19-4-28.
 */
public class ColorCircleView extends View {

    private int mColor;
    private String mNumber;

    private int mTextSize;

    private int radius;

    private float mTextWidth;
    private float mTextHeightOffset;
    private int mTextColor;

    private Paint paint = new Paint();

    public ColorCircleView(Context context) {
        super(context);
        init();
    }

    public ColorCircleView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ColorCircleView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {

        mTextSize = getResources().getDimensionPixelSize(R.dimen.s12);

        paint.setDither(true);
        paint.setAntiAlias(true);
        paint.setTextSize(mTextSize);
        mTextHeightOffset = paint.descent() + paint.ascent();
    }

    public void setData(int number, int color) {
        mColor = color;
        mNumber = String.valueOf(number);

        mTextWidth = paint.measureText(mNumber);
        mTextColor = getTextColor(color);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        radius = w / 2;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if(radius <= 0) {
            return;
        }

        final int w = getWidth();
        final int h = getHeight();
        final int centerX = w / 2;
        final int centerY = h / 2;

        paint.setColor(mColor);
        canvas.drawCircle(centerX, centerY, radius, paint);

        if(mNumber != null) {
            final float startX = centerX - mTextWidth / 2 - 1;
            final float startY = centerY - mTextHeightOffset / 2;
            paint.setColor(mTextColor);
            canvas.drawText(mNumber, startX, startY, paint);
        }
    }

    private static int getTextColor(int color) {
        int r = Color.red(color);
        int g = Color.green(color);
        int b = Color.blue(color);

        int bright = (int) (0.3 * r + 0.6 * g + 0.1 * b);
        if (bright > 190) {
            return Color.parseColor("#232323");
        } else {
            return Color.WHITE;
        }
    }
}
