package com.ober.artist.color.data;

import java.io.File;

/**
 * Created by ober on 2019-10-29.
 */
public class RegionData {
    public File file;
    public int[] size;
    public String mimetype;
}
