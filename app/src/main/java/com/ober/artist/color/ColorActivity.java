package com.ober.artist.color;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.meevii.color.fill.FillColorImageView;
import com.meevii.color.fill.model.core.resp.FillColorResp;
import com.meevii.color.fill.model.core.resp.OverwriteColorResp;
import com.meevii.color.fill.model.core.resp.RevokeOverwriteResp;
import com.meevii.color.fill.model.core.resp.RevokeSingleColorResp;
import com.meevii.color.fill.model.core.FloodFillArea;
import com.meevii.color.fill.model.core.task.RunnableTask;
import com.meevii.color.fill.util.FillColorHelper;
import com.meevii.color.fill.view.gestures.SubsamplingScaleImageView;
import com.ober.artist.App;
import com.ober.artist.BuildConfig;
import com.ober.artist.R;
import com.ober.artist.color.data.ColorRecord;
import com.ober.artist.color.data.LocalStore;
import com.ober.artist.utils.SToast;
import com.ober.palette.Palette;
import com.ober.palette.PaletteColorBean;

import java.io.File;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * Created by ober on 2018/12/4.
 */

@Deprecated
public class ColorActivity extends AppCompatActivity implements FillColorImageView.FillColorListener, FillColorImageView.ImageOnLongClickListener {

    public static final int RESULT_INIT_FAILED = 111;

    FillColorImageView mFillIV;

    private ImageView ivBack;
    private Palette paletteView;
    private RecyclerView rvRecord;
    private ImageView ivRevoke;
    private ImageView ivRevokeBack;
    private FrameLayout fTips;
    private ImageView ivReset;
    private TextView tvCount;
    private FrameLayout fPalette;
    private FrameLayout fOk;
    private TextView tvUiMode;

    private InitTask mInitTask;

    private ProgressDialog mProgressDialog;

    private String mToken;
    private String mPackName;
    private int mPbntype;

    private List<Integer> mBadAreas;

    private ColorRecordManager mColorRecordManager;

    private int mTotalCount;

    private PaletteSelection mPaletteSelection;

    private SingleColorModeHelper mSingleColorModeHelper;
    private ReplaceModeHelper mReplaceModeHelper;

    private boolean canRevoke;
    private boolean canRevokeBack;

    public static void startForResult(Activity a,
                                      String packName,
                                      String token,
                                      int pbntype,
                                      int requestCode) {
        Intent intent = new Intent(a, ColorActivity.class);
        intent.putExtra("packName", packName);
        intent.putExtra("token", token);
        intent.putExtra("pbntype", pbntype);
        a.startActivityForResult(intent, requestCode);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_color);

        rvRecord = findViewById(R.id.rvRecord);
        mFillIV = findViewById(R.id.fillView);
        paletteView = findViewById(R.id.paletteView);
        fTips = findViewById(R.id.fTips);
        ivBack = findViewById(R.id.ivBack);
        ivRevoke = findViewById(R.id.ivRevoke);
        ivRevokeBack = findViewById(R.id.ivRevokeBack);
        ivReset = findViewById(R.id.ivReset);
        tvCount = findViewById(R.id.tvColorCount);
        fPalette = findViewById(R.id.fPalette);
        fOk = findViewById(R.id.fSingleModeOk);
        fOk.setVisibility(View.GONE);

        tvUiMode = findViewById(R.id.tvUiMode);
        tvUiMode.setVisibility(View.GONE);

        tvCount.setOnClickListener(view -> changeTextColor(tvCount));
        tvCount.setTag(0);

        tvUiMode.setOnClickListener(v -> {

        });
        tvUiMode.setTag(1);
        tvUiMode.setTextColor(0xffff7182);

        fTips.setVisibility(View.GONE);
        fTips.setOnClickListener(view -> processTips());

        ivBack.setOnClickListener(v -> onBackPressed());

        fOk.setOnClickListener(v -> processOk());

        ivRevoke.setVisibility(View.INVISIBLE);
        ivRevokeBack.setVisibility(View.INVISIBLE);

        Intent intent = getIntent();
        if (intent == null) {
            finish();
            return;
        }

        mToken = intent.getStringExtra("token");
        mPackName = intent.getStringExtra("packName");
        mPbntype = intent.getIntExtra("pbntype", -1);
        if(mPbntype == -1) {
            finish();
            return;
        }

        if (TextUtils.isEmpty(mToken) || TextUtils.isEmpty(mPackName)) {
            finish();
            return;
        }

        if (!checkFiles()) {
            SToast.show("file not found");
            finish();
            return;
        }

        mFillIV.setEnableTouch(false);

        mInitTask = new InitTask(this, mPackName, mToken, mPbntype);
        mInitTask.execute();

        mSingleColorModeHelper = new SingleColorModeHelper();
        mReplaceModeHelper = new ReplaceModeHelper();
    }

    private void processOk() {

        if(mSingleColorModeHelper.getState() == SingleColorModeHelper.STATE_ON) {
            Map<Integer, Integer> record = mSingleColorModeHelper.getRecordData();
            mSingleColorModeHelper.setState(SingleColorModeHelper.STATE_PROCESS_BACK);
            mFillIV.scheduleRefillAll(record);
        } else if(mReplaceModeHelper.getState() == ReplaceModeHelper.STATE_ON) {
            mReplaceModeHelper.setState(ReplaceModeHelper.STATE_PROCESS_END);
            RunnableTask task = new RunnableTask();
            task.callback = () -> {
                runOnUiThread(() -> {
                    mReplaceModeHelper.setCurrentColor(0);
                    mReplaceModeHelper.setState(ReplaceModeHelper.STATE_OFF);
                    setUIReplaceMode(false);
                });
            };
            mFillIV.scheduleRunnableTask(task);
        }
    }

    private boolean checkFiles() {
        File png = LocalStore.getPngFile(this, mPackName, mToken);
        File region = LocalStore.getRegionFile(this, mPackName, mToken);
        File pdf = LocalStore.getRegionFile(this, mPackName, mToken);
        if (pdf.exists() && png.exists() && region.exists()) {
            return true;
        }

        return false;
    }

    private int tipIndex = 0;
    private void processTips() {

        if(mBadAreas.size() == 0) {
            return;
        }

        int i = tipIndex % mBadAreas.size();
        SToast.show("(" + i + ")");

        mFillIV.animateToArea(mBadAreas.get(i));
        tipIndex++;
    }

    @Override
    public void onBackPressed() {

        if(mSingleColorModeHelper.getState() != SingleColorModeHelper.STATE_OFF) {
            SToast.show("查看模式不可保存");
            return;
        }

        if(mReplaceModeHelper.getState() != ReplaceModeHelper.STATE_OFF) {
            SToast.show("替换模式不可保存");
            return;
        }

        scheduleSave(() -> runOnUiThread(() -> {
            setResult(RESULT_OK);
            ColorActivity.super.onBackPressed();
        }));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (mInitTask != null) {
            mInitTask.cancel(true);
        }

        mFillIV.release();
    }

    void handleInitComplete(InitTask.ResultSet rs) {
        if (rs == null) {
            String errMsg = mInitTask.getErrMsg();
            Toast.makeText(App.getInstance(),
                        "Error!", Toast.LENGTH_SHORT).show();
            Intent data = new Intent();
            data.putExtra("errMsg", errMsg);
            setResult(RESULT_INIT_FAILED, data);
            finish();
            return;
        }

        mTotalCount = rs.areaCount;

        mFillIV.setOnStateChangedListener(new ImageOnStateChangedListener());
        mFillIV.setOnImageEventListener(new ImageEventListener());
        mFillIV.initColorOriginImage(rs.colorOriginImage);
        mFillIV.setFillColorListener(this);
        mFillIV.setImageLongClickListener(this);
        mBadAreas = new LinkedList<>();
        mBadAreas.addAll(rs.badAreasSet);
        TextView tvBad = findViewById(R.id.tv_bad);
        tvBad.setText(String.valueOf(mBadAreas.size()));
        ivRevoke.setVisibility(View.INVISIBLE);
        ivRevoke.setOnClickListener(v -> mFillIV.scheduleRevoke());

        ivRevokeBack.setVisibility(View.INVISIBLE);
        ivRevokeBack.setOnClickListener(v -> mFillIV.scheduleRevokeBack());

        ivReset.setOnClickListener(v -> alertToReset());

        mPaletteSelection = new PaletteSelection();
        mPaletteSelection.init(rs);

        mColorRecordManager = new ColorRecordManager(rs.colorRecords, rs.recordCount, mPaletteSelection);

        List<PaletteColorBean> paletteColorBeans = mPaletteSelection.setSelection(PaletteSelection.PALETTE_COMMON);

        paletteView.setColorSelectedListener((selection, colorBean) -> {
            int color = colorBean.data[selection[1]];
            mFillIV.setColor(color);
        });

        switchPalette(paletteColorBeans, PaletteSelection.PALETTE_COMMON);

        mColorRecordManager.setupView(rvRecord);
        mColorRecordManager.setRecordItemClickListener((holder, bean, position) -> {

            if(bean.paletteId == mPaletteSelection.getSelection()) {
                final int color = bean.color;
                paletteView.setSelectionByColor(color);
                mFillIV.setColor(color);
            } else {
                if(bean.paletteId == -1) {
                    SToast.show("not found");
                    return;
                }
                List<PaletteColorBean> data = mPaletteSelection.setSelection(bean.paletteId);
                switchPalette(data, bean.paletteId);
                final int color = bean.color;
                paletteView.setSelectionByColor(color);
                mFillIV.setColor(color);
                SToast.show("切换到 " + PaletteSelection.getPaletteName(bean.paletteId));
            }
        });

        mColorRecordManager.setRecordItemSwipeCallback((adapter, record, pos, direction) ->
        {
            if(direction == ItemTouchHelper.UP) {
                alertToDeleteColor(adapter, record);
            } else if(direction == ItemTouchHelper.DOWN) {
                alertToChangeColorMode(adapter, record, pos);
            }
        });

        tvCount.setText(String.format(Locale.US, "%d/%d", rs.recordCount, mTotalCount));

        mColorRecordManager.setCountChangeCallback(count ->
                tvCount.setText(String.format(Locale.US, "%d/%d", count, mTotalCount)));

        fPalette.setOnClickListener(v -> showPaletteSwitch());
    }

    private void setUISingleColorMode(boolean on) {
        if(on) {
            mFillIV.setClickFilter((x, y, color) -> true);
            ivBack.setVisibility(View.INVISIBLE);
            ivRevoke.setVisibility(View.INVISIBLE);
            ivRevokeBack.setVisibility(View.INVISIBLE);
            fOk.setVisibility(View.VISIBLE);
            mColorRecordManager.setItemMovable(false);
            ivReset.setVisibility(View.INVISIBLE);
            tvUiMode.setVisibility(View.VISIBLE);
            tvUiMode.setText("查看模式");
        } else {
            mFillIV.setClickFilter(null);
            ivReset.setVisibility(View.VISIBLE);
            ivRevoke.setVisibility(canRevoke ? View.VISIBLE : View.INVISIBLE);
            ivRevokeBack.setVisibility(canRevokeBack ? View.VISIBLE : View.INVISIBLE);
            ivBack.setVisibility(View.VISIBLE);
            fOk.setVisibility(View.GONE);
            mColorRecordManager.setItemMovable(true);
            tvUiMode.setText("");
            tvUiMode.setVisibility(View.GONE);
        }
    }

    private void setUIReplaceMode(boolean on) {
        if(on) {
            mFillIV.setClickFilter((x, y, color) -> {
                if(color != null) {
                    processReplace(color);
                }
                return true;
            });
            ivBack.setVisibility(View.INVISIBLE);
            fOk.setVisibility(View.VISIBLE);
            mColorRecordManager.setItemMovable(false);
            tvUiMode.setVisibility(View.VISIBLE);
            tvUiMode.setText("替换模式");
            ivReset.setVisibility(View.INVISIBLE);
            ivRevoke.setVisibility(View.INVISIBLE);
            ivRevokeBack.setVisibility(View.INVISIBLE);
        } else {
            mFillIV.setClickFilter(null);
            ivBack.setVisibility(View.VISIBLE);
            fOk.setVisibility(View.GONE);
            mColorRecordManager.setItemMovable(true);
            tvUiMode.setText("");
            tvUiMode.setVisibility(View.GONE);
            ivReset.setVisibility(View.VISIBLE);
            ivRevoke.setVisibility(canRevoke ? View.VISIBLE : View.INVISIBLE);
            ivRevokeBack.setVisibility(canRevokeBack ? View.VISIBLE : View.INVISIBLE);
        }
    }

    private void processReplace(int selectColor) {

        if(mReplaceModeHelper.getState() != ReplaceModeHelper.STATE_ON) {
            if(BuildConfig.DEBUG) {
                SToast.show("err");
            }
            return;
        }

        int originColor = mReplaceModeHelper.getCurrentColor();

        mFillIV.scheduleOverWriteColor(originColor, selectColor);

    }

    private void showPaletteSwitch() {
        mPaletteSelection.showSelectionDialog(this,
                paletteColorBeans -> {
                    switchPalette(paletteColorBeans, mPaletteSelection.getSelection());
                });
    }

    private void switchPalette(List<PaletteColorBean> data, int id) {
        paletteView.setData(data, id);
        mColorRecordManager.setPaletteId(id);
    }

    private void changeTextColor(TextView tvCount) {
        int tag = (int) tvCount.getTag();
        tag++;
        int[] color = {0xff969696, 0xffff7182, 0xff0500cd};
        tvCount.setTextColor(color[tag % 3]);
        tvCount.setTag(tag);
    }

    private void alertToReset() {
        new AlertDialog.Builder(this)
                .setMessage("确定重置吗(无法恢复)?")
                .setPositiveButton("确定", (dialog, which) -> {
                    dialog.dismiss();
                    mFillIV.scheduleReset();
                })
                .setNegativeButton("取消", (dialog, which) -> dialog.dismiss())
                .show();
    }

    private void alertToDeleteColor(final RecyclerView.Adapter adapter, final ColorRecord record) {

        AlertDialog d = new AlertDialog.Builder(this)
                .setMessage("确定删除？")
                .setPositiveButton("确定", (dialog, which) -> {
                    ((Dialog)dialog).setOnDismissListener(null);
                    dialog.dismiss();
                    mFillIV.scheduleOverWriteColor(record.color, null);
                })
                .setNegativeButton("取消", (dialog, which) -> {
                    dialog.dismiss();
                }).show();
        d.setOnDismissListener(dialog -> adapter.notifyDataSetChanged());

    }

    private void alertToChangeColorMode(final RecyclerView.Adapter adapter, final ColorRecord record, int pos) {

        String[] items = new String[] {
            "查看模式",
            "替换模式"
        };

        AlertDialog d = new AlertDialog.Builder(this)
                .setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(which == 0) {
                            scheduleSingleColorMode(record);
                        } else {
                            scheduleReplaceMode(record);
                        }
                    }
                }).show();

        d.setOnDismissListener(dialog -> adapter.notifyDataSetChanged());
    }

    //schedule单颜色查看模式
    private void scheduleSingleColorMode(final ColorRecord record) {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
        }

        mProgressDialog.setMessage("Processing");
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();

        RunnableTask runnableTask = new RunnableTask();
        runnableTask.callback = () -> saveStateBeforeSingleColor(record.color);
        mFillIV.scheduleRunnableTask(runnableTask);
    }

    private void scheduleReplaceMode(final ColorRecord record) {
        if(mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
        }
        mProgressDialog.setMessage("Processing");
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();

        mReplaceModeHelper.setState(ReplaceModeHelper.STATE_PROCESS_START);
        final int originColor = (record.color|0xff000000);
        final RunnableTask task = new RunnableTask();

        task.callback = () -> {

            runOnUiThread(() -> {
                mProgressDialog.dismiss();
                mReplaceModeHelper.setCurrentColor(originColor);
                mReplaceModeHelper.setState(ReplaceModeHelper.STATE_ON);
                setUIReplaceMode(true);
            });
        };

        mFillIV.scheduleRunnableTask(task);
    }

    private void scheduleSave(Runnable end) {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
        }

        mProgressDialog.setMessage("Saving");
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
        RunnableTask runnableTask = new RunnableTask();
        runnableTask.callback = this::processSave;
        mFillIV.scheduleRunnableTask(runnableTask);

        runnableTask = new RunnableTask();
        runnableTask.callback = end;
        mFillIV.scheduleRunnableTask(runnableTask);
    }

    private void saveStateBeforeSingleColor(int color) {
        final HashMap<Integer, FloodFillArea> areaMap = mFillIV.getAreaMap();
        final HashMap<Integer, Integer> dataMap = new HashMap<>();
        final HashMap<Integer, Integer> refillMap = new HashMap<>();
        for(Map.Entry<Integer, FloodFillArea> entry : areaMap.entrySet()) {
            FloodFillArea area = entry.getValue();
            if(area.color != null) {
                dataMap.put(entry.getKey(), area.color);

                if((area.color|0xff000000) == (color|0xff000000)) {
                    refillMap.put(entry.getKey(), color);
                }
            }
        }
        runOnUiThread(() -> {
            mSingleColorModeHelper.recordState(dataMap);
            boolean r = mFillIV.scheduleRefillAll(refillMap);
            if(r) {
                mSingleColorModeHelper.setState(SingleColorModeHelper.STATE_PROCESS_START);
            }
        });
    }

    private void processSave() {
        Runnable r = () -> {
            HashMap<Integer, FloodFillArea> areaMap = mFillIV.getAreaMap();
            LocalStore.saveColorMapByAreaMap(ColorActivity.this, mPackName, mToken, areaMap);
            Bitmap bmp = mFillIV.getEditedBitmap();
            int sWidth = bmp.getWidth();
            int sHeight = bmp.getHeight();
            LocalStore.saveEditBitmap(ColorActivity.this, mPackName, mToken, bmp);
            File pdf = LocalStore.getPdfFile(ColorActivity.this, mPackName, mToken);
            Bitmap fore = FillColorHelper.loadPDF(pdf, sWidth, sHeight);
            Bitmap thumb = Bitmap.createBitmap(sWidth, sHeight, Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(thumb);
            Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
            paint.setFilterBitmap(true);
            canvas.drawBitmap(bmp, 0, 0, paint);
            canvas.drawBitmap(fore, 0, 0, paint);

            fore.recycle();
            LocalStore.saveThumbBitmap(ColorActivity.this, mPackName, mToken, thumb);
            thumb.recycle();

            runOnUiThread(() -> {

                final int[] sort = mColorRecordManager.getSort();

                LocalStore.saveSort(ColorActivity.this, mPackName, mToken, sort);

                mProgressDialog.dismiss();
                SToast.show("saved");
            });
        };
        r.run();
    }

    @Override
    public void onAreaFilled(int num, FillColorResp resp) {
        //0  : same color
        //1  : fill color
        //2  : over write color
        //3  : clear color
        //4  : none color
        final int fillType = resp.fillType;

        if(fillType == 0) {
            mColorRecordManager.onColorCleared(resp.preColor);
        } else if(fillType == 1) {
            mColorRecordManager.onColorFilled(resp.color);
        } else if(fillType == 2) {
            mColorRecordManager.onColorOverWrite(resp.color, resp.preColor);
        } else if(fillType == 3) {
            mColorRecordManager.onColorCleared(resp.preColor);
        }

    }

    @Override
    public void onRecalledFillColor(boolean success, RevokeSingleColorResp resp) {
        if(!success) {
            return;
        }

        final int fillType = resp.revokedFillType;

        if(!resp.isBack) {
            if (fillType == 0) {
                mColorRecordManager.onColorFilled(resp.preColor);
            } else if (fillType == 1) {
                mColorRecordManager.onColorCleared(resp.targetColor);
            } else if (fillType == 2) {
                mColorRecordManager.onColorOverWrite(resp.preColor, resp.targetColor);
            } else if (fillType == 3) {
                mColorRecordManager.onColorFilled(resp.preColor);
            }
        } else {
            if (fillType == 0) {
                mColorRecordManager.onColorCleared(resp.preColor);
            } else if (fillType == 1) {
                mColorRecordManager.onColorFilled(resp.targetColor);
            } else if (fillType == 2) {
                mColorRecordManager.onColorOverWrite(resp.targetColor, resp.preColor);
            } else if (fillType == 3) {
                mColorRecordManager.onColorCleared(resp.preColor);
            }
        }
    }

    @Override
    public void onRecalledOverwrite(boolean success, RevokeOverwriteResp resp) {
        if(!success) {
            return;
        }

        if(!resp.isBack) {
            mColorRecordManager.onColorOverWriteRevoke(resp.preColor,
                    resp.dstColor,
                    resp.areas.length);
        } else {
            mColorRecordManager.onColorOverWriteRevoke(resp.dstColor,
                    resp.preColor,
                    resp.areas.length);
        }
    }

    @Override
    public void onOverwriteColor(OverwriteColorResp resp) {
        mColorRecordManager.onColorsOverWrite(resp.srcColor, resp.dstColor, resp.areas.length);

        if(mReplaceModeHelper.getState() == ReplaceModeHelper.STATE_ON) {
            mReplaceModeHelper.setCurrentColor(resp.dstColor);
        }
    }

    @Override
    public void onReset() {
        mColorRecordManager.onReset();
    }

    @Override
    public void onRefilled() {
        if(mProgressDialog != null) {
            mProgressDialog.dismiss();
        }
        int singleColorState = mSingleColorModeHelper.getState();
        if(singleColorState == SingleColorModeHelper.STATE_PROCESS_START) {
            mSingleColorModeHelper.setState(SingleColorModeHelper.STATE_ON);
            setUISingleColorMode(true);
        } else if(singleColorState == SingleColorModeHelper.STATE_PROCESS_BACK) {
            mSingleColorModeHelper.setState(SingleColorModeHelper.STATE_OFF);
            mSingleColorModeHelper.recordState(null);
            setUISingleColorMode(false);
        }
    }

    @Override
    public void onRevokeCacheStateChanged(boolean empty) {
        canRevoke = !empty;
        ivRevoke.setVisibility(empty ? View.INVISIBLE : View.VISIBLE);
    }

    @Override
    public void onRevokeBackCacheStateChanged(boolean empty) {
        canRevokeBack = !empty;
        ivRevokeBack.setVisibility(empty ? View.INVISIBLE : View.VISIBLE);
    }

    @Override
    public boolean onImageLongClicked(int positionColor) {

        if(positionColor != Color.WHITE) {
            mColorRecordManager.showColorInfo(this, positionColor);
        }
        return true;
    }

    private class ImageEventListener extends SubsamplingScaleImageView.DefaultOnImageEventListener {
        @Override
        public void onImageLoaded() {
            super.onImageLoaded();
            mFillIV.setEnableTouch(true);
            fTips.setVisibility(View.VISIBLE);
        }

        @Override
        public void onImageLoadError(Exception e) {
            super.onImageLoadError(e);
            LocalStore.getPdfFile(ColorActivity.this, mPackName, mToken).delete();
            LocalStore.getRegionFile(ColorActivity.this, mPackName, mToken).delete();
            setResult(RESULT_INIT_FAILED);
            finish();
        }
    }

    private class ImageOnStateChangedListener implements SubsamplingScaleImageView.OnStateChangedListener {

        @Override
        public void onScaleChanged(float newScale, int origin) {

        }

        @Override
        public void onCenterChanged(PointF newCenter, int origin) {

        }

        @Override
        public void onCenterOrScaleChanged(float newScale, PointF newCenter, int origin) {

        }
    }

}
