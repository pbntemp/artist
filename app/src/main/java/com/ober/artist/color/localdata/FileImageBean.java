package com.ober.artist.color.localdata;

import java.io.File;

/**
 * Created by ober on 2018/12/5.
 */
public class FileImageBean {
    public File pdf;
    public File region;
    public File origin;
    public File center;
    public File thumb;
    public File area;
    public File plan;

    public String token;

}
