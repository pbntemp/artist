package com.ober.artist.color.localdata;

import android.content.Context;

import com.ober.artist.color.data.LocalStore;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by ober on 2018/12/5.
 */
public class FileDataLoader {

    public static FileDataStruct loadDir(Context context, String packName) {
        File packDir = LocalStore.packDir(packName);
        if (!packDir.exists()) {
            return null;
        }
        File[] imgDirs = packDir.listFiles();
        if (imgDirs == null) {
            return null;
        }
        List<String> tokens = new LinkedList<>();
        for (File imgDir : imgDirs) {
            if (imgDir.isDirectory()
                    && !imgDir.getName().startsWith(".")) {
                tokens.add(imgDir.getName());
            }
        }
        return load(context, packName, tokens);
    }

    public static FileImageBean2 load2(String uid, boolean isLocal, String id) {
        FileImageBean2 imageBean = new FileImageBean2();
        imageBean.id = id;
        imageBean.uid = uid;
        imageBean.origin = LocalData.getTargetPngFile(uid, isLocal, id);
        imageBean.pdf = LocalData.getTargetPdfFile(uid, isLocal, id);
        imageBean.area = LocalData.getTargetAreaFile(uid, isLocal, id);
        imageBean.region = LocalData.getTargetRegionFile(uid, isLocal, id);
        imageBean.plan = LocalData.getTargetPlanFile(uid, isLocal, id);
        imageBean.center = LocalData.getTargetCenterFile(uid, isLocal, id);
        imageBean.thumb = LocalData.getTargetThumbFile(uid, isLocal, id);
        imageBean.colored = LocalData.getTargetColorFile(uid, isLocal, id);
        imageBean.thumbnailPng = LocalData.getTargetThumbnailFile(uid, isLocal, id);

        return imageBean;
    }

    public static FileDataStruct load(Context context, String packName, List<String> tokens) {
        List<FileImageBean> list = new ArrayList<>();
        for (String token : tokens) {
            FileImageBean imageBean = new FileImageBean();
            imageBean.token = token;
            imageBean.origin = LocalStore.getPngFile(context, packName, token);
            imageBean.pdf = LocalStore.getPdfFile(context, packName, token);
            imageBean.area = LocalStore.getAreaMapFile(context, packName, token);
            imageBean.region = LocalStore.getRegionFile(context, packName, token);
            imageBean.plan = LocalStore.getPlanFile(context, packName, token);
            imageBean.center = LocalStore.getCenterFile(context, packName, token);
            imageBean.thumb = LocalStore.getThumbFile(context, packName, token);
            list.add(imageBean);
        }
        FileDataStruct dataStruct = new FileDataStruct();
        dataStruct.imageList = list;

        return dataStruct;
    }

    public static FileImageBean load(Context context, String packName, String token) {
        FileImageBean imageBean = new FileImageBean();
        imageBean.token = token;
        imageBean.origin = LocalStore.getPngFile(context, packName, token);
        imageBean.pdf = LocalStore.getPdfFile(context, packName, token);
        imageBean.area = LocalStore.getAreaMapFile(context, packName, token);
        imageBean.region = LocalStore.getRegionFile(context, packName, token);
        imageBean.plan = LocalStore.getPlanFile(context, packName, token);
        imageBean.center = LocalStore.getCenterFile(context, packName, token);
        imageBean.thumb = LocalStore.getThumbFile(context, packName, token);
        return imageBean;
    }
}
