package com.ober.artist.color.localdata;

import java.io.File;

/**
 * Created by ober on 2019-10-30.
 */
public class FileImageBean2 {

    public File pdf;
    public File region;
    public File origin;
    public File center;
    public File thumb;
    public File area;
    public File plan;
    public File colored;
    public File thumbnailPng;

    public String id;
    public String uid;

    public int pbntype; //Picture.PBN_TYPE_xxx
}
