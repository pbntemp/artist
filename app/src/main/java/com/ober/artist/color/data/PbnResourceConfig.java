package com.ober.artist.color.data;


/**
 * Created by ober on 2020-02-05.
 */
public class PbnResourceConfig {

    public static final int RES_ZIP = 1;
    public static final int RES_PNG = 2;
    public static final int RES_PDF = 3;

    public static SizeLimit getSizeLimit(int resType) {
        switch (resType) {
            case RES_ZIP:
                return new SizeLimit(mb2Length(4f), mb2Length(2f));
            case RES_PNG:
                return new SizeLimit(mb2Length(2f), mb2Length(1f));
            case RES_PDF:
                return new SizeLimit(mb2Length(2f), mb2Length(1f));
        }
        return null;
    }

    public static class SizeLimit {
        public final long maxLength;
        public final long warningLength;
        SizeLimit(long maxLength, long warningLength) {
            this.maxLength = maxLength;
            this.warningLength = warningLength;
        }
    }

    private static long mb2Length(float mb) {
        return ((long)(mb * 1024L * 1024L));
    }

}
