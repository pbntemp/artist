package com.ober.artist.color;

import android.content.Context;
import android.content.DialogInterface;
import android.util.SparseIntArray;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.util.Consumer;

import com.ober.artist.utils.SToast;
import com.ober.palette.PaletteColorBean;

import java.util.HashMap;
import java.util.List;

/**
 * Created by ober on 19-9-6.
 */
public class PaletteSelection {

    public static final int PALETTE_COMMON = 1;
    public static final int PALETTE_ANCIENT = 2;
    public static final int PALETTE_SKIN = 3;

    private List<PaletteColorBean> paletteData1; //普通
    private List<PaletteColorBean> paletteData2; //古风

    @Nullable
    private List<PaletteColorBean> paletteData3; //皮肤


    private SparseIntArray colorPaletteIdMap;

    private int mSelection = -1;

    public PaletteSelection() {

    }

    public void init(InitTask.ResultSet rs) {
        paletteData1 = rs.palettes.get(PALETTE_COMMON);
        paletteData2 = rs.palettes.get(PALETTE_ANCIENT);

        putMapData(paletteData1, PALETTE_COMMON);
        putMapData(paletteData2, PALETTE_ANCIENT);
    }

    public void init(InitTask2.ResultSet rs) {
        paletteData1 = rs.palettes.get(PALETTE_COMMON);
        paletteData2 = rs.palettes.get(PALETTE_ANCIENT);
        paletteData3 = rs.palettes.get(PALETTE_SKIN);

        putMapData(paletteData1, PALETTE_COMMON);
        putMapData(paletteData2, PALETTE_ANCIENT);
        putMapData(paletteData3, PALETTE_SKIN);
    }

    public List<PaletteColorBean> setSelection(int palette) {
        if(palette != mSelection) {

            mSelection = palette;

            if(palette == PALETTE_COMMON) {
                return paletteData1;
            } else if(palette == PALETTE_ANCIENT) {
                return paletteData2;
            } else if(palette == PALETTE_SKIN) {
                return paletteData3;
            }

            throw new RuntimeException("unknown selection " + palette);

        } else {
            return null;
        }
    }

    public int getSelection() {
        return mSelection;
    }

    public static String getPaletteName(int id) {
        if(id == PALETTE_COMMON) {
            return "通用色板";
        } else if(id == PALETTE_ANCIENT) {
            return "古风色板";
        } else if(id == PALETTE_SKIN) {
            return "肤色色板";
        }
        return null;
    }

    public int getPaletteCount() {
        if(paletteData3 == null) {
            return 2;
        } else {
            return 3;
        }
    }

    public int findPaletteByColor(int color) {
        if(colorPaletteIdMap == null) {
            return -1;
        }

        return colorPaletteIdMap.get((color | 0xff000000), -1);
    }

    public void showSelectionDialog(Context context, Consumer<List<PaletteColorBean>> dataChangeHandler) {

        final String[] items;
        if(getPaletteCount() == 3) {
            items = new String[] {
                    "通用色板",
                    "古风色板",
                    "肤色色板"
            };
        } else {
            items = new String[] {
                    "通用色板",
                    "古风色板"
            };
        }

        int checkedItem;
        if(mSelection == PALETTE_COMMON) {
            checkedItem = 0;
        } else if(mSelection == PALETTE_ANCIENT) {
            checkedItem = 1;
        } else if(mSelection == PALETTE_SKIN) {
            checkedItem = 2;
        } else {
            SToast.show("err");
            return;
        }

        new AlertDialog.Builder(context)
                .setSingleChoiceItems(items, checkedItem, (dialog, which) -> {
                    dialog.dismiss();

                    List<PaletteColorBean> data;
                    if(which == 0) {
                        data = setSelection(PALETTE_COMMON);
                    } else if(which == 1) {
                        data = setSelection(PALETTE_ANCIENT);
                    } else if(which == 2) {
                        data = setSelection(PALETTE_SKIN);
                    } else {
                        SToast.show("err");
                        return;
                    }

                    if(data != null) {
                        dataChangeHandler.accept(data);
                    }
                })
                .show();

    }

    private void putMapData(List<PaletteColorBean> paletteData, int id) {

        if(paletteData == null) {
            return;
        }

        if(colorPaletteIdMap == null) {
            colorPaletteIdMap = new SparseIntArray();
        }

        for(PaletteColorBean bean : paletteData) {
            for(int data : bean.data) {
                colorPaletteIdMap.put(data, id);
            }
        }

    }

}
