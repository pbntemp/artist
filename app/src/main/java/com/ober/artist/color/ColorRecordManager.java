package com.ober.artist.color;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import androidx.annotation.Nullable;
import androidx.core.util.Pair;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.ItemTouchHelper;
import android.view.MotionEvent;

import com.meevii.color.fill.FillColorImageView;
import com.ober.artist.BuildConfig;
import com.ober.artist.R;
import com.ober.artist.color.data.ColorRecord;
import com.ober.artist.utils.ColorUtil;
import com.ober.artist.utils.SToast;
import com.ober.palette.Palette;
import com.ober.palette.PaletteColorBean;
import com.socks.library.KLog;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by ober on 19-4-28.
 */
public class ColorRecordManager {

    public interface OnColorRecordSwipeCallback {
        void onColorRecordSwiped(RecyclerView.Adapter adapter, ColorRecord record, int pos, int direction);
    }

    public interface OnColorCountChangeCallback {
        void onColorCountChanged(int count);
    }

    private static final String TAG = "ColorRecordManager";

    private final List<ColorRecord> mColorRecords;//shared data width adapter

    private ColorRecordAdapter mAdapter;

    private OnColorRecordSwipeCallback mItemSwipeCallback;

    private OnColorCountChangeCallback mCountChangeCallback;

    private boolean itemMoveable = true;

    private int mCount;

    private int mPaletteId;

    private final PaletteSelection mPaletteSelection;

    public ColorRecordManager(@Nullable List<ColorRecord> colorRecords, int originCount, PaletteSelection paletteSelection) {
        if(colorRecords != null) {
            mColorRecords = new ArrayList<>(colorRecords);
        } else {
            mColorRecords = new ArrayList<>();
        }

        mCount = originCount;
        mPaletteSelection = paletteSelection;
    }

    public List<ColorRecord> getColorRecords() {
        return mColorRecords;
    }

    public void setPaletteId(int paletteId) {
        this.mPaletteId = paletteId;
    }

    public void setCountChangeCallback(OnColorCountChangeCallback countChangeCallback) {
        this.mCountChangeCallback = countChangeCallback;
    }

    public void setItemMovable(boolean itemMovable) {
        this.itemMoveable = itemMovable;
    }

    public void onColorFilled(int color) {
        if(BuildConfig.DEBUG) {
            String s = ColorUtil.color2String(color, false);
            KLog.i(TAG, "onColorFilled " + s);
        }

        color = (color | 0xff000000);

        ColorRecord record = findByColor(color);
        if(record == null) {
            record = new ColorRecord(mPaletteId);
            record.color = color;
            record.count = 1;
            mColorRecords.add(record);
        } else {
            record.count ++;
        }
        mAdapter.notifyDataSetChanged();
        mCount ++;
        notifyCountChange();
    }

    public void onColorCleared(int color) {
        if(BuildConfig.DEBUG) {
            String s = ColorUtil.color2String(color, false);
            KLog.i(TAG, "onColorCleared " + s);
        }

        ColorRecord record = findByColor(color);
        assert record != null;
        if(record.count == 1) {
            mColorRecords.remove(record);
        } else {
            record.count --;
        }

        mAdapter.notifyDataSetChanged();
        mCount--;
        notifyCountChange();
    }

    public void onColorOverWrite(int color, int preColor) {
        if(BuildConfig.DEBUG) {
            String s = ColorUtil.color2String(color, false);
            String s2 = ColorUtil.color2String(preColor, false);
            KLog.i(TAG, "onColorOverWrite " + s2 + "-->" + s);
        }

        {
            ColorRecord record = findByColor(preColor);
            assert record != null;
            if (record.count == 1) {
                mColorRecords.remove(record);
            } else {
                record.count--;
            }
        }

        {
            ColorRecord record = findByColor(color);
            if (record == null) {
                int paletteId = mPaletteSelection.findPaletteByColor(color);
                if(paletteId == -1) {
                    paletteId = mPaletteId;
                    SToast.show("色板未找到颜色");
                }
                record = new ColorRecord(paletteId);
                record.color = color|0xff000000;
                record.count = 1;
                mColorRecords.add(record);
            } else {
                record.count++;
            }
        }

        mAdapter.notifyDataSetChanged();
        notifyCountChange();
    }

    public void onColorsOverWrite(int srcColor, Integer dstColor, int count) {
        if(BuildConfig.DEBUG) {
            String s = ColorUtil.color2String(srcColor, false);
            String s2;
            if(dstColor != null) {
                s2 = ColorUtil.color2String(dstColor, false);
            } else {
                s2 = null;
            }
            KLog.i(TAG, "onColorsOverWrite " + s + "-->" + s2 + "  count=" + count);
        }

        final int recordPos;

        {
            Pair<Integer, ColorRecord> rs = findByColorWithPosition(srcColor);
            assert rs != null;
            assert rs.first != null;
            assert rs.second != null;

            recordPos = rs.first;
            ColorRecord record = rs.second;

            mCount -= count;

            if(record.count <= count) {
                mColorRecords.remove(record);
            } else {
                record.count = record.count - count;
            }
        }

        if(dstColor != null) {

            mCount += count;

            ColorRecord dstRecord = findByColor(dstColor);

            if(dstRecord == null) {

                int paletteId = mPaletteSelection.findPaletteByColor(dstColor);
                if(paletteId == -1) {
                    paletteId = mPaletteId;
                    SToast.show("色板未找到颜色");
                }
                ColorRecord record = new ColorRecord(paletteId);
                record.color = dstColor | 0xff000000;
                record.count = count;
                mColorRecords.add(recordPos, record);
            } else {
                dstRecord.count = dstRecord.count + count;
            }
        }

        mAdapter.notifyDataSetChanged();
        notifyCountChange();
    }

    public void onColorOverWriteRevoke(Integer preColor, Integer fillColor, int count) {
        if(BuildConfig.DEBUG) {
            KLog.i(TAG, "onColorOverWriteRevoke color=" + preColor + ", count=" + count);
        }

        mCount += count;

        if(fillColor != null) {
            ColorRecord record = findByColor(fillColor);
            if(record != null) {
                if(record.count > count) {
                    record.count = record.count - count;
                } else {
                    mColorRecords.remove(record);
                }
            }
        }

        if(preColor != null) {
            ColorRecord record = findByColor(preColor);

            if (record != null) {
                record.count = record.count + count;
            } else {
                int paletteId = mPaletteSelection.findPaletteByColor(preColor);
                if(paletteId == -1) {
                    paletteId = mPaletteId;
                    SToast.show("色板未找到颜色");
                }
                record = new ColorRecord(paletteId);
                record.color = preColor | 0xff000000;
                record.count = count;
                mColorRecords.add(record);
            }
        }

        mAdapter.notifyDataSetChanged();

        notifyCountChange();
    }

    public void onReset() {
        mColorRecords.clear();
        mAdapter.notifyDataSetChanged();
        mCount = 0;
        notifyCountChange();
    }

    public void showColorInfo(Context context, int color) {
        ColorRecord record = findByColor(color);
        if(record == null) {
            SToast.show("err");
            return;
        }

        int index = mColorRecords.indexOf(record);

        if(index == -1) {
            SToast.show("not found");
            return;
        }

        SToast.show("色块" + (index + 1));
    }

    public Pair<Integer, ColorRecord> getColorRecordByColor(int color) {
        ColorRecord record = findByColor(color);
        if(record == null) {
            SToast.show("err");
            return null;
        }

        int index = mColorRecords.indexOf(record);

        return Pair.create(index, record);
    }

    @SuppressLint("ClickableViewAccessibility")
    public void setupView(RecyclerView rv) {

        setupRecyclerView(rv);

        mAdapter = new ColorRecordAdapter();
        mAdapter.setData(mColorRecords);
        rv.setAdapter(mAdapter);

        rv.setOnTouchListener((v, event) -> {
            final int action = event.getAction();
            if(action == MotionEvent.ACTION_UP) {
                if(itemMovedFlag) {
                    itemMovedFlag = false;
                    rv.post(() -> mAdapter.notifyDataSetChanged());
                }
            }
            return false;
        });

    }

    public void setRecordItemClickListener(ColorRecordAdapter.ItemClickListener listener) {
        mAdapter.setItemClickListener(listener);
    }

    public void setRecordItemSwipeCallback(OnColorRecordSwipeCallback callback) {
        mItemSwipeCallback = callback;
    }

    public int[] getSort() {

        if(mColorRecords.isEmpty()) {
            return new int[0];
        }

        final int count = mColorRecords.size();
        final int[] sort = new int[count];
        for(int i = 0; i < count; i++) {
            sort[i] = mColorRecords.get(i).color;
        }
        return sort;
    }

    private void notifyCountChange() {
        if(mCountChangeCallback != null) {
            mCountChangeCallback.onColorCountChanged(mCount);
        }
    }

    private void setupRecyclerView(RecyclerView rv) {
        final Resources rs = rv.getResources();
        final int screenW = rs.getDisplayMetrics().widthPixels;
        final int itemW = rs.getDimensionPixelSize(R.dimen.s45);
        final int itemMargin = rs.getDimensionPixelSize(R.dimen.s5);
        final int padding = (screenW - 6 * itemW - 12 * itemMargin) / 2;
        rv.setPadding(padding, 0, padding, 0);

        rv.setBackgroundColor(Color.parseColor("#efefef"));

        LinearLayoutManager lm = new LinearLayoutManager(rv.getContext());
        lm.setOrientation(LinearLayoutManager.HORIZONTAL);
        rv.setLayoutManager(lm);

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(ithCallback);
        itemTouchHelper.attachToRecyclerView(rv);
    }

    private ColorRecord findByColor(int color) {
        for(ColorRecord record : mColorRecords) {
            if(record.color == (color|0xff000000)) {
                return record;
            }
        }
        return null;
    }

    private Pair<Integer, ColorRecord> findByColorWithPosition(int color) {

        for(int i = 0; i < mColorRecords.size(); i++) {
            ColorRecord record = mColorRecords.get(i);
            if(record.color == (color|0xff000000)) {
                return new Pair<>(i, record);
            }
        }
        return null;
    }

    private boolean itemMovedFlag;

    private ItemTouchHelper.Callback ithCallback = new ItemTouchHelper.Callback() {
        @Override
        public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {

            if(!itemMoveable) {
                return 0;
            }

            return makeFlag(ItemTouchHelper.ACTION_STATE_DRAG,
                    ItemTouchHelper.DOWN | ItemTouchHelper.UP
                            | ItemTouchHelper.START | ItemTouchHelper.END) |
                    makeFlag(ItemTouchHelper.ACTION_STATE_SWIPE,
                            ItemTouchHelper.UP|ItemTouchHelper.DOWN);
        }

        @Override
        public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {

            int fromPosition = viewHolder.getAdapterPosition();
            int toPosition = target.getAdapterPosition();


            if (fromPosition < toPosition) {
                for (int i = fromPosition; i < toPosition; i++) {
                    Collections.swap(mColorRecords, i, i + 1);
                }
            } else {
                for (int i = fromPosition; i > toPosition; i--) {
                    Collections.swap(mColorRecords, i, i - 1);
                }
            }

            mAdapter.notifyItemMoved(fromPosition, toPosition);

            itemMovedFlag = true;

            return true;
        }

        @Override
        public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {

            int pos = viewHolder.getAdapterPosition();

            ColorRecord record = mColorRecords.get(pos);

            if(mItemSwipeCallback != null) {
                mItemSwipeCallback.onColorRecordSwiped(mAdapter, record, pos, direction);
            }
        }
    };

}
