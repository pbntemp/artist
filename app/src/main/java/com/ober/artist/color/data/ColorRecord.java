package com.ober.artist.color.data;

/**
 * Created by ober on 19-4-28.
 */
public class ColorRecord {

    public int color;

    public int count;

    public final int paletteId;

    public ColorRecord(int paletteId) {
        this.paletteId = paletteId;
    }

    public ColorRecord(ColorRecord record) {
        this.color = record.color;
        this.count = record.count;
        this.paletteId = record.paletteId;
    }

}
