package com.ober.artist.color.localdata;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Environment;

import androidx.annotation.Nullable;

import com.google.gson.Gson;
import com.meevii.color.fill.model.core.FloodFillArea;
import com.ober.artist.color.data.Center;
import com.ober.artist.color.data.PreColor;
import com.ober.artist.utils.ColorUtil;
import com.ober.artist.utils.IOUtil;
import com.ober.artist.utils.PreColorCoder;
import com.ober.artist.utils.PreColorSorter;
import com.ober.artist.utils.SToast;
import com.ober.artist.utils.SimpleFileEditor;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by ober on 2019-10-28.
 */
public class LocalData {

    public static File getAppRootDir() {
        File dir = new File(Environment.getExternalStorageDirectory(), "PbnArtist");
        if(!dir.exists()) {
            dir.mkdirs();
        }
        return dir;
    }

    public static File getAppLogDir() {
        File dir = new File(getAppRootDir(), "logs");
        if(!dir.exists()) {
            dir.mkdirs();
        }
        return dir;
    }

    public static File getFileRecordDir(String uid, boolean isLocal) {
        File dir = new File(getAppRootDir(), (isLocal ? "r_"  : "remote_")+ uid);
        if(!dir.exists()) {
            dir.mkdirs();
        }
        return dir;
    }


//    public static File getFileRecordDir(String uid) {
//        File dir = new File(getAppRootDir(), "r_" + uid);
//        if(!dir.exists()) {
//            dir.mkdirs();
//        }
//        return dir;
//    }

    public static File getTargetFileRecordDir(String uid, boolean isLocal, String id) {
        File dir = new File(getFileRecordDir(uid, isLocal), id);
        if(!dir.exists()) {
            dir.mkdirs();
        }
        return dir;
    }

    @Nullable
    public static File getTargetThumbnailFile(String uid, boolean isLocal, String id) {
        File dir = getTargetFileRecordDir(uid, isLocal, id);
        File f = new File(dir, "thumbnail.png");
        if(f.exists()) {
            return f;
        }
        f = new File(dir, "thumbnail.jpg");
        if(f.exists()) {
            return f;
        }
        f = new File(dir, "thumbnail.jpeg");
        if(f.exists()) {
            return f;
        }
        return null;
    }

    public static File getTargetPngFile(String uid, boolean isLocal, String id) {
        return new File(getTargetFileRecordDir(uid, isLocal, id), "origin.png");
    }

    public static File getTargetRegionFile(String uid, boolean isLocal, String id) {
        return new File(getTargetFileRecordDir(uid, isLocal, id), "region.png");
    }

    public static File getTargetRegionOldFile(String uid, boolean isLocal, String id) {
        return new File(getTargetFileRecordDir(uid, isLocal, id), "region_old.png");
    }

    @Nullable
    public static File getTargetColorFile(String uid, boolean isLocal, String id) {
        File dir = getTargetFileRecordDir(uid, isLocal, id);
        String[] names = dir.list((dir1, name) -> name.startsWith("colored"));

        File targetFile = null;
        if(names != null && names.length > 0) {
            targetFile = new File(dir, names[0]);
        }

        if(targetFile == null) {
            return null;
        }

        if(targetFile.getName().endsWith("jpeg") || targetFile.getName().endsWith("JPEG")) {
            File renamedFile = new File(targetFile.getParent(), "colored.jpg");
            targetFile.renameTo(renamedFile);
            return renamedFile;
        } else {
            return targetFile;
        }
    }

    public static File getTargetPdfFile(String uid, boolean isLocal, String id) {
        File dir = getTargetFileRecordDir(uid, isLocal, id);
        return new File(dir, "origin.pdf");
    }

    public static File getTargetThumbFile(String uid, boolean isLocal, String id) {
        File dir = getTargetFileRecordDir(uid, isLocal, id);
        return new File(dir, "thumb.png");
    }

    public static File getTargetAreaFile(String uid, boolean isLocal, String id) {
        return new File(getTargetFileRecordDir(uid, isLocal, id), "area");
    }

    public static File getTargetEditFile(String uid, boolean isLocal, String id) {
        return new File(getTargetFileRecordDir(uid, isLocal, id), "edit");
    }

    public static File getTargetCenterFile(String uid, boolean isLocal, String id) {
        return new File(getTargetFileRecordDir(uid, isLocal, id), "center");
    }

    public static File getTargetPlanFile(String uid, boolean isLocal, String id) {
        return new File(getTargetFileRecordDir(uid, isLocal, id), "plan");
    }

    public static File getTargetSortFile(String uid, boolean isLocal, String id) {
        return new File(getTargetFileRecordDir(uid, isLocal, id), "defaultSort");
    }

    public static Map<Integer, Integer> loadColorMap(String uid, boolean isLocal, String id) {
        File f = getTargetAreaFile(uid, isLocal, id);
        return loadColorMap(f);
    }

    private static Map<Integer, Integer> loadColorMap(File f) {
        if (!f.exists()) {
            return null;
        }
        int[] data;
        try {
            String json = IOUtil.inputStream2String(
                    new FileInputStream(f),
                    "UTF-8");
            data = new Gson().fromJson(json, int[].class);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < data.length / 2; i++) {
            map.put(data[2 * i], data[2 * i + 1]);
        }
        return map;
    }

    public static int[] readSort(String uid, boolean isLocal, String id) {
        File f = getTargetSortFile(uid, isLocal, id);
        if(!f.exists()) {
            return null;
        }
        int[] data;
        try {
            String json = IOUtil.inputStream2String(
                    new FileInputStream(f),
                    "UTF-8");
            data = new Gson().fromJson(json, int[].class);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return data;
    }

    public static void savePlanByAreaMap(File planFile,
                                         File areaFile, Map<Integer, Center> centerMap,
                                         int[] order) {

        Map<Integer, Integer> areaColorMap = loadColorMap(areaFile);

        if (areaColorMap == null) {
            return;
        }

        Iterator<Map.Entry<Integer, Integer>> it = areaColorMap.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<Integer, Integer> areaColor = it.next();
            if (!centerMap.containsKey(areaColor.getKey())) {
                it.remove();
            }
        }

        Map<Integer, Set<Integer>> colorAreas = new HashMap<>();
        for (Map.Entry<Integer, Integer> entry : areaColorMap.entrySet()) {
            Integer color = entry.getValue();
            int area = entry.getKey();
            Set<Integer> areaSet = colorAreas.get(color);
            if (areaSet == null) {
                areaSet = new HashSet<>();
                colorAreas.put(color, areaSet);
            }
            areaSet.add(area);
        }

        List<PreColor> preColors = new LinkedList<>();
        for (Map.Entry<Integer, Set<Integer>> entry : colorAreas.entrySet()) {
            int color = entry.getKey();
            int[] areas = new int[entry.getValue().size()];
            int j = 0;
            for (Integer i : entry.getValue()) {
                areas[j] = i;
                j++;
            }
            PreColor preColor = new PreColor();
            preColor.areas = areas;
            preColor.color = ColorUtil.color2String(color, false);
            preColors.add(preColor);
        }

        final List<PreColor> sorted;
        if(order == null) {
            sorted = PreColorSorter.defaultSort(preColors);
        } else {
            sorted = PreColorSorter.sort(preColors, order);
        }

        String encoded = PreColorCoder.encode(sorted);

        planFile.delete();
        try {
            IOUtil.stringToOutputStream(encoded,
                    new FileOutputStream(planFile));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void saveColorMapByAreaMap(String uid, boolean isLocal, String id, Map<Integer, FloodFillArea> areaMap) {
        Map<Integer, Integer> colorMap = new HashMap<>();
        for (Map.Entry<Integer, FloodFillArea> entry : areaMap.entrySet()) {
            FloodFillArea area = entry.getValue();
            if (area.color != null) {
                colorMap.put(entry.getKey(), area.color);
            }
        }
        saveColorMap(uid, isLocal, id, colorMap);
    }

    public static void saveColorMap(String uid, boolean isLocal, String id, Map<Integer, Integer> map) {
        if (map == null || map.isEmpty()) {
            File f = getTargetAreaFile(uid, isLocal, id);
            f.delete();
            return;
        }

        int size = map.size();
        int[] data = new int[size * 2];

        Iterator<Map.Entry<Integer, Integer>> it = map.entrySet().iterator();
        int index = 0;
        while (it.hasNext()) {
            Map.Entry<Integer, Integer> entry = it.next();
            data[index * 2] = entry.getKey();
            data[index * 2 + 1] = entry.getValue();
            index++;
        }
        String json = new Gson().toJson(data);
        File f = getTargetAreaFile(uid, isLocal, id);
        f.delete();
        try {
            IOUtil.stringToOutputStream(json,
                    new FileOutputStream(f));
        } catch (Exception e) {
            e.printStackTrace();
            f.delete();
        }
    }

    public static void saveSort(String uid, boolean isLocal, String id, int[] sort) {
        if(sort == null || sort.length == 0) {
            return;
        }
        File f = getTargetSortFile(uid, isLocal, id);
        String json = new Gson().toJson(sort);
        f.delete();
        try {
            IOUtil.stringToOutputStream(json,
                    new FileOutputStream(f));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void saveEditBitmap(String uid, boolean isLocal, String id, Bitmap bmp) {
        File f = getTargetEditFile(uid, isLocal, id);
        f.delete();
        IOUtil.saveBitmapTo(bmp, f);
    }

    public static void saveThumbBitmap(String uid, boolean isLocal, String id, Bitmap bmp) {
        File f = getTargetThumbFile(uid, isLocal, id);
        f.delete();
        IOUtil.saveBitmapTo(bmp, f);
    }

    public static void saveDownloadedPlan(String uid, boolean isLocal, String id, String planStr) {

        PreColor[] preColors = PreColorCoder.decode(planStr);

        File planFile = getTargetPlanFile(uid, isLocal, id);
        File areaFile = getTargetAreaFile(uid, isLocal, id);
        File sortFile = getTargetSortFile(uid, isLocal, id);

        //为空或者坏掉，认为没有进度
        if(preColors == null || preColors.length == 0) {
            planFile.delete();
            areaFile.delete();
            sortFile.delete();
            return;
        }

        //存area, sort
        final int colorCount = preColors.length;
        final Map<Integer, Integer> colorMap = new HashMap<>();
        final int[] sort = new int[colorCount];

        int sortIndex = 0;
        for(PreColor preColor : preColors) {
            final int color = Color.parseColor(preColor.color);
            for(int area : preColor.areas) {
                colorMap.put(area, color);
            }
            sort[sortIndex] = color;
            sortIndex++;
        }

        saveColorMap(uid, isLocal, id, colorMap);
        saveSort(uid, isLocal, id, sort);

        //存plan
        SimpleFileEditor.writeToFile(planFile.getAbsolutePath(), planStr);
    }

}
