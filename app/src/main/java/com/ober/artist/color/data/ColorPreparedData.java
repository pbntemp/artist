package com.ober.artist.color.data;

import android.graphics.Bitmap;

import com.meevii.color.fill.model.core.origin.ColorOriginImage;

import java.util.Map;

/**
 * Created by ober on 2018/12/4.
 */
public class ColorPreparedData {

    public RegionData region;
    public ColorOriginImage colorOriginImage;
    public Map<Integer, Integer> colorMap;
    public Bitmap editBitmap;
    public String token;
}
