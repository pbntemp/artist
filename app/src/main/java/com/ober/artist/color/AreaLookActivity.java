package com.ober.artist.color;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.content.FileProvider;
import androidx.core.util.Consumer;

import com.ober.artist.BuildConfig;
import com.ober.artist.R;
import com.ober.artist.utils.CircleAreaBitmapGenerate;
import com.ober.artist.utils.Files;
import com.ober.artist.utils.SToast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.UUID;


/**
 * Created by ober on 2019-12-09.
 */
public class AreaLookActivity extends AppCompatActivity {

    private ImageView imageView;
    private ProgressBar progressBar;
    private TextView textView;
    private CardView cardViewDD;
    private CardView cardViewContinue;

    private CircleAreaBitmapGenerate.Task mGenTask;

    private static final int START_TAG_FOR_LOOK = 1;
    private static final int START_TAG_FOR_UPLOAD = 2;
    private int mStartTag;
    private boolean isLocal;

    public static void startForLook(Context context, String id, boolean isLocal, String uid) {
        Intent intent = new Intent(context, AreaLookActivity.class);
        intent.putExtra("id", id);
        intent.putExtra("uid", uid);
        intent.putExtra("isLocal", isLocal);
        intent.putExtra("tag", START_TAG_FOR_LOOK);
        context.startActivity(intent);
    }

    public static void startForUpload(Activity a, String id, String uid, boolean isLocal, int requestCode) {
        Intent intent = new Intent(a, AreaLookActivity.class);
        intent.putExtra("id", id);
        intent.putExtra("uid", uid);
        intent.putExtra("isLocal", isLocal);
        intent.putExtra("tag", START_TAG_FOR_UPLOAD);
        a.startActivityForResult(intent, requestCode);
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_area_look);
        imageView = findViewById(R.id.imageView);
        progressBar = findViewById(R.id.progress);
        textView = findViewById(R.id.tv_title);
        cardViewDD = findViewById(R.id.cv_dd);
        cardViewContinue = findViewById(R.id.cv_continue);

        textView.setText("正在生成预览图");
        progressBar.setVisibility(View.VISIBLE);
        cardViewDD.setVisibility(View.GONE);
        cardViewContinue.setVisibility(View.GONE);
        imageView.setVisibility(View.INVISIBLE);

        Intent intent = getIntent();
        if(intent == null) {
            finish();
            return;
        }

        isLocal = intent.getBooleanExtra("isLocal", false);
        mStartTag = intent.getIntExtra("tag", 0);
        String id = intent.getStringExtra("id");
        String uid = intent.getStringExtra("uid");
        if(id == null || uid == null || mStartTag == 0) {
            finish();
            return;
        }

        if(mStartTag == START_TAG_FOR_UPLOAD) {
            showBottomBtn();
        }

        mGenTask = new CircleAreaBitmapGenerate.Task(uid, isLocal, id, new Consumer<CircleAreaBitmapGenerate.Result>() {
            @Override
            public void accept(CircleAreaBitmapGenerate.Result result) {
                handleGenerated(result);
            }
        });
        mGenTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private CircleAreaBitmapGenerate.Result mResult;


    private void handleGenerated(CircleAreaBitmapGenerate.Result result) {
        if(result.bmp == null) {
            setResult(RESULT_CANCELED);
            SToast.show(result.errMsg);
            finish();
        } else {
            mResult = result;
            imageView.setImageBitmap(result.bmp);
            progressBar.setVisibility(View.GONE);
            textView.setText(buildResultText(result));
            imageView.setVisibility(View.VISIBLE);

            if(mStartTag == START_TAG_FOR_LOOK) {
                showBottomBtn();
            }
        }
    }

    private void showBottomBtn() {
        if(mStartTag == START_TAG_FOR_LOOK) {
            cardViewDD.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    shareToDD();
                }
            });

            cardViewDD.setVisibility(View.VISIBLE);
            cardViewDD.animate().alpha(1f)
                    .setDuration(400)
                    .start();
        } else if(mStartTag == START_TAG_FOR_UPLOAD) {
            cardViewContinue.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setResult(RESULT_OK);
                    finish();
                }
            });

            cardViewContinue.setVisibility(View.VISIBLE);
        }
    }

    private void shareToDD() {
        File dirTemp = new File(Environment.getExternalStorageDirectory() + "/temp_artist_share");

        Files.rm_rf(dirTemp);

        dirTemp.mkdirs();

        File dst = new File(dirTemp, UUID.randomUUID().toString().replace("-", ""));

        boolean exists = dst.exists();
        if (exists) {
            dst.delete();
        }

        FileOutputStream fos;

        try {
            fos = new FileOutputStream(dst);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            SToast.show("error");
            return;
        }

        mResult.bmp.compress(Bitmap.CompressFormat.PNG, 100, fos);

        Uri uri = FileProvider.getUriForFile(this,
                BuildConfig.APPLICATION_ID + ".fileProvider", dst);

        Intent intent = new Intent();
        intent.setType("image/png");
        intent.setAction(Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_STREAM, uri);
        startActivity(Intent.createChooser(intent, "Send"));
    }

    private void destroyInternal() {
        if(mGenTask != null) {
            mGenTask.cancel(true);
            mGenTask = null;
        }
    }


    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED);
        super.onBackPressed();
        destroyInternal();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        destroyInternal();
    }

    private static String buildResultText(CircleAreaBitmapGenerate.Result result) {
        StringBuilder sb = new StringBuilder();
        sb.append("共" + result.countRed +"个大区域未着色(红色)\n")
                .append("共" + result.countGreen + "个小区域未着色(绿色)\n");
        return sb.toString();
    }
}





