package com.ober.artist.color;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ober.artist.R;
import com.ober.artist.color.data.ColorRecord;

import java.util.List;

/**
 * Created by ober on 19-5-5.
 */
public class ColorRecordAdapter extends RecyclerView.Adapter<ColorRecordHolder> {

    public interface ItemClickListener {
        void onColorItemClicked(ColorRecordHolder holder, ColorRecord bean, int position);
    }


    private List<ColorRecord> mData;
    private ItemClickListener mItemClickListener;

    public ColorRecordAdapter() {}

    public void setData(List<ColorRecord> data) {
        this.mData = data;
    }

    public void setItemClickListener(ItemClickListener listener) {
        mItemClickListener = listener;
    }

    @NonNull
    @Override
    public ColorRecordHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_color_record, parent, false);
        return new ColorRecordHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ColorRecordHolder holder, final int position) {

        final ColorRecord bean = mData.get(position);

        holder.tvCount.setText(String.valueOf(bean.count));

        holder.colorCircleView.setData(position + 1, bean.color);
        holder.colorCircleView.invalidate();

        holder.itemView.setOnClickListener(v -> {
            if(mItemClickListener != null) {
                mItemClickListener.onColorItemClicked(holder, bean, holder.getAdapterPosition());
            }
        });
    }

    @Override
    public int getItemCount() {
        return mData == null ? 0 : mData.size();
    }
}
