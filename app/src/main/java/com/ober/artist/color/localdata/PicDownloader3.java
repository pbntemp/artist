package com.ober.artist.color.localdata;

import android.os.AsyncTask;

import androidx.core.util.Consumer;

import com.ober.artist.App;
import com.ober.artist.db.PbnDb;
import com.ober.artist.db.PictureRecord;
import com.ober.artist.utils.Files;
import com.ober.artist.utils.OkDownloadUtil;
import com.ober.artist.pack.ZipUtil;
import com.ober.artist.utils.Files;
import com.ober.artist.utils.OkDownloadUtil;
import com.ober.artist.utils.ResourceSizeCheckUtil;
import com.socks.library.KLog;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class PicDownloader3 extends AsyncTask<Void, Integer, List<PicDownloader3.Result>> {

    private static final String TAG = "PicDownloader3";

    public static final int RESULT_OK = 0;

    public static class Result {
        public int code;
        public String errMsg;

        public String url;
        public String generatedId;
        public int pbnType;
    }

    public static class DownloadParam {

        public final String id;
        public final int pbnType;
        public List<String> urls;
        public List<String> targetDirs;

        public DownloadParam(String id, int pbnType){
            this.id = id;
            this.pbnType = pbnType;
            urls = new ArrayList<>();
            targetDirs = new ArrayList<>();
        }

    }

    private Consumer<List<Result>> mCallback;
    private List<DownloadParam> downloadParams;

    private final String uid;

    public PicDownloader3(String uid,
                          List<DownloadParam> downloadParams,
                          Consumer<List<Result>> callback) {
        this.uid = uid;
        this.downloadParams = downloadParams;
        this.mCallback = callback;
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
        mCallback = null;
    }

    @Override
    protected void onPostExecute(List<Result> result) {
        super.onPostExecute(result);
        Consumer<List<Result>> cb = mCallback;
        if(cb != null) {
            cb.accept(result);
        }
    }

    @Override
    protected List<Result> doInBackground(Void... voids) {

        File cacheDir = App.getInstance().getCacheDir();
        if(!cacheDir.exists()) {
            cacheDir.mkdirs();
        }
        List<Result> results = new ArrayList<>();
        boolean isFailed = false;
        for (DownloadParam downloadParam : downloadParams) {
            List<String> urls = downloadParam.urls;
            for (int i = 0; i < urls.size(); i++) {
                Result rs = downloadFile(urls.get(i), downloadParam.id, downloadParam.pbnType, downloadParam.targetDirs.get(i));
                if (rs != null && rs.code == RESULT_OK){
                    results.add(rs);
                } else {
                    isFailed = true;
                }
            }
        }

        if (!isFailed){
            PictureRecord record = new PictureRecord();
            record.setDownloadTime(System.currentTimeMillis());
            record.setId(results.get(0).generatedId);
            record.setPbntype(results.get(0).pbnType);
            record.setUid(uid);
            record.setBuildVersion(PictureRecord.VERSION_CURRENT);

            List<PictureRecord> list = PbnDb.get().getPicRecordDao().getById(record.getUid(), record.getId());
            if (list == null || list.size() == 0){
                PbnDb.get().getPicRecordDao().insert(record);
            }
        }

        return results;
    }

    private Result downloadFile(String url, String id, Integer pbnType, String targetDir){
        File targetFile = new File(targetDir);
        if(targetFile.exists()) {
            targetFile.delete();
        }
        KLog.i(TAG, "start download", "file = " + targetFile, "url = " + url);

        try {
            OkDownloadUtil.download(url, targetFile);
        } catch (IOException e) {
            e.printStackTrace();
            targetFile.delete();
            Result result = new Result();
            result.code = -1;
            result.errMsg = "下载失败";
            return result;
        }
        if(isCancelled()) {
            return null;
        }
        if(!targetFile.exists()) {
            Result result = new Result();
            result.code = -1;
            result.errMsg = "下载失败";
            return result;
        }

        String s = Files.fileTree(targetFile);
        System.out.println(s);
        Result rs = new Result();
        rs.generatedId = id;
        rs.pbnType = pbnType;
        rs.url = url;
        rs.code = RESULT_OK;
        rs.errMsg = "ok";
        return rs;
    }
}
