package com.ober.artist;

import android.graphics.BitmapFactory;
import android.text.TextUtils;

import com.ober.artist.color.localdata.LocalData;
import com.ober.artist.db.PbnDb;
import com.ober.artist.db.PicType;
import com.ober.artist.db.PictureRecord;

import java.io.File;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

/**
 * Created by ober on 2020-01-15.
 */
public class RecoveryLogic {


    public static void recovery() {
        String uid = new SUserManager2().getUID();

        if(TextUtils.isEmpty(uid)) {
            return;
        }

        List<PictureRecord> list =  PbnDb.get().getPicRecordDao().getAllByUid(uid);

        Set<String> idset = new HashSet<>();
        for(PictureRecord r : list) {
            idset.add(r.getId());
        }

        recovery(idset, uid, true);
        recovery(idset, uid, false);

    }

    private static void recovery(Set<String> idset, String uid, boolean isLocal){
        File dir = LocalData.getFileRecordDir(uid, isLocal);
        if(!dir.exists()) {
            return;
        }

        int fnameLen = UUID.randomUUID().toString().replace("-", "").length();

        File[] files = dir.listFiles();
        for(File f : files) {
            String name = f.getName();
            if(!f.isDirectory() || name.length() != fnameLen) {
                continue;
            }

            if(idset.contains(name)) {
                continue;
            }

            File regionPng = LocalData.getTargetRegionFile(uid, isLocal, name);
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(regionPng.getAbsolutePath(), options);
            int w = options.outWidth;
            if(w == -1) {
                continue;
            }

            int pbnType = getPbnTypeByRegionWidth(w);
            if(pbnType == -1) {
                continue;
            }

            PictureRecord record = new PictureRecord();
            record.setUid(uid);
            record.setPbntype(pbnType);
            record.setId(name);
            record.setDownloadTime(System.currentTimeMillis());
            record.setUpdateTime(System.currentTimeMillis());
            record.setUploadedTime(0);
            PbnDb.get().getPicRecordDao().insert(record);
        }
    }


    private static int getPbnTypeByRegionWidth(int w) {
        if(w == 1024) {
            return PicType.PBN_TYPE_NORMAL;
        } else if(w == 2048) {
            return PicType.PBN_TYPE_COLORED;
        } else if(w == 750) {
            return PicType.PBN_TYPE_WALL_NORMAL;
        } else if(w == 1500) {
            return PicType.PBN_TYPE_WALL_COLORED;
        }

        return -1;
    }
}
