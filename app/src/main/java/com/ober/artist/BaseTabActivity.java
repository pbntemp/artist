package com.ober.artist;

import android.app.ProgressDialog;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.ober.artist.ui.pics3.AFragment;

import java.util.ArrayList;


/**
 * Created by ober on 19-1-10.
 */
public abstract class BaseTabActivity extends AppCompatActivity implements AFragment.HostListener {

    protected ViewPager viewPager;
    protected TabLayout tabLayout;

    protected ArrayList<Fragment> fragmentArrayList;
    private ArrayList<String> titleArrayList;

    protected ProgressDialog mDialog;
    private MPagerAdapter mPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tab);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mDialog = new ProgressDialog(this);

        viewPager = findViewById(R.id.viewPager);
        tabLayout = findViewById(R.id.tabLayout);

        initList();

        mPagerAdapter = new MPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(mPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void initList(){
        fragmentArrayList = new ArrayList<>();
        titleArrayList = new ArrayList<>();
    }

    protected void addFragment(Fragment fragment, String title){
        fragmentArrayList.add(fragment);
        titleArrayList.add(title);
        mPagerAdapter.notifyDataSetChanged();
    }

    private class MPagerAdapter extends FragmentPagerAdapter {


        MPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return fragmentArrayList.get(position);
        }

        @Override
        public int getCount() {
            return fragmentArrayList.size();
        }


        @Override
        public CharSequence getPageTitle(int position) {

            return titleArrayList.get(position);
        }
    }

}
