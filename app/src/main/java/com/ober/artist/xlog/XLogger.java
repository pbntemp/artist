package com.ober.artist.xlog;

import com.elvishew.xlog.LogConfiguration;
import com.elvishew.xlog.LogLevel;
import com.elvishew.xlog.XLog;
import com.elvishew.xlog.flattener.DefaultFlattener;
import com.elvishew.xlog.flattener.Flattener2;
import com.elvishew.xlog.printer.ConsolePrinter;
import com.elvishew.xlog.printer.file.FilePrinter;
import com.elvishew.xlog.printer.file.naming.DateFileNameGenerator;
import com.ober.artist.color.localdata.LocalData;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by ober on 2019-12-23.
 */
public class XLogger {

    public static final boolean ON = true;

    public static void init() {

        if(!ON) {
            return;
        }

        FilePrinter filePrinter = new FilePrinter.Builder(new File(LocalData.getAppRootDir(), "xlog").toString())
                .fileNameGenerator(new DateFileNameGenerator())
                .flattener(new MyFlatten())
                .build();

        LogConfiguration config = new LogConfiguration.Builder()
                .logLevel(LogLevel.ALL)
                .t()
                .build();

        XLog.init(config, filePrinter);

        XLog.init(LogLevel.ALL, filePrinter, new ConsolePrinter());
    }

    public static void d(String msg) {
        if(!ON) {
            return;
        }
        XLog.d(msg);
    }

    public static void v(String msg) {
        if(!ON) {
            return;
        }
        XLog.v(msg);
    }

    public static void i(String msg) {
        if(!ON) {
            return;
        }
        XLog.i(msg);
    }

    public static void w(String msg) {
        if(!ON) {
            return;
        }
        XLog.w(msg);
    }

    public static void e(String msg) {
        if(!ON) {
            return;
        }
        XLog.e(msg);
    }

    private static class MyFlatten implements Flattener2 {

        final SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss", Locale.US);

        final Date date = new Date();

        @Override
        public CharSequence flatten(long timeMillis, int logLevel, String tag, String message) {
            date.setTime(timeMillis);
            return format.format(date)
                    + '|' + LogLevel.getShortLevelName(logLevel)
                    + '|' + message;

        }
    }

}

