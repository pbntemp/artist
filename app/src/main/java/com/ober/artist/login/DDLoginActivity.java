package com.ober.artist.login;

import android.os.Bundle;
import android.transition.Transition;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.ober.artist.BuildConfig;
import com.ober.artist.R;
import com.ober.artist.SUserManager2;
import com.ober.artist.utils.SToast;
import com.ober.artists.ddshare.DDClient;

/**
 * Created by ober on 2019-10-28.
 */

public class DDLoginActivity extends AppCompatActivity implements DDClient.AuthListener {


    private DDClient ddClient;

    private LoginFlow mLoginFlow;

    private TextView tvLoginLabel;
    private CardView cvLogin;

    private TextView tvProgressState;
    private View containerProgress;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_2);

        postponeEnterTransition();

        TextView tvVersion = findViewById(R.id.tv_version);
        tvVersion.setText("v" + BuildConfig.VERSION_NAME);

        ddClient = new DDClient(this);

        mLoginFlow = new LoginFlow(this::handleLoginFailed, this::handleLoginSuccess);

        String err = ddClient.checkSupport();

        if(err != null) {
            Toast.makeText(this, err, Toast.LENGTH_SHORT).show();
            finish();
            return;
        }

        tvLoginLabel = findViewById(R.id.tv_choose_label);

        cvLogin = findViewById(R.id.cv_login);

        tvProgressState = findViewById(R.id.tv_login_state);
        containerProgress = findViewById(R.id.l_progress);

        cvLogin.setOnClickListener(v -> {
            boolean r = ddClient.sendAuth();
            if(!r) {
                SToast.show("打开钉钉失败，尝试重启应用");
            }
        });

        ddClient.setAuthListener(this);

        Transition transition = getWindow().getSharedElementEnterTransition();
        if(transition != null) {
            transition.addListener(new Transition.TransitionListener() {
                @Override
                public void onTransitionStart(Transition transition) {

                }

                @Override
                public void onTransitionEnd(Transition transition) {

                }

                @Override
                public void onTransitionCancel(Transition transition) {

                }

                @Override
                public void onTransitionPause(Transition transition) {

                }

                @Override
                public void onTransitionResume(Transition transition) {

                }
            });
        }

        startPostponedEnterTransition();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ddClient.destroy();
        mLoginFlow.destroy();
    }

    @Override
    public void onDDAuthResult(int code, String msg, String authRs) {
        Log.w("DDLoginActivity", "onAuthResult " + authRs);

        if(code == DDClient.RESULT_OK) {
            showLoading(true);
            tvProgressState.setText("正在登录");
            mLoginFlow.startLogin(authRs, true);
            return;
        }

        showLoading(false);

        Log.w("DDLoginActivity", "onAuthResult ERR " + msg);

        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED);
        finish();
    }

    private void showLoading(boolean show) {
        if(show) {
            tvLoginLabel.setVisibility(View.INVISIBLE);
            cvLogin.setVisibility(View.GONE);

            containerProgress.setVisibility(View.VISIBLE);
        } else {
            tvLoginLabel.setVisibility(View.VISIBLE);
            cvLogin.setVisibility(View.VISIBLE);

            containerProgress.setVisibility(View.GONE);
        }
    }

    private void handleLoginFailed(String msg) {
        showLoading(false);
        SToast.show(msg);
    }

    private void handleLoginSuccess() {
        //showLoading(false);
        String uname = new SUserManager2().getUserName();
        SToast.show("hello " + uname);

        setResult(RESULT_OK);
        finish();
    }
}
