package com.ober.artist.login;

import androidx.core.util.Consumer;

import com.ober.artist.BuildConfig;
import com.ober.artist.SUserManager2;
import com.ober.artist.net.ApiCallback;
import com.ober.artist.net.pbn.PbnApi;
import com.ober.artist.net.pbn.rest.UacDDAuthResp;
import com.ober.artist.net.pbn.rest.UacLoginResp;
import com.ober.artist.xlog.XLogger;
import com.socks.library.KLog;

import okhttp3.Call;

/**
 * Created by ober on 2019-10-28.
 */
public class LoginFlow {

    private static final String TAG = "LoginFlow";

    private Consumer<String> onFailed;
    private Runnable onSuccess;

    private String username;
    private String userid;

    private Call call;

    public LoginFlow(Consumer<String> onFailed, Runnable onSuccess) {
        this.onFailed = onFailed;
        this.onSuccess = onSuccess;
    }

    public void destroy() {
        if(call != null) {
            call.cancel();
        }

        onFailed = null;
        onSuccess = null;
    }

    public void startLogin(String ddCode, boolean needLoginGp) {
        KLog.i(TAG, "startLogin");

        if(call != null) {
            call.cancel();
        }

        KLog.i(TAG, "startUacAuth");
        call = PbnApi.requestUacDDAuth(ddCode, new ApiCallback<UacDDAuthResp>() {
            @Override
            public void onResp(UacDDAuthResp uacDDAuthResp) {

                if(uacDDAuthResp != null && uacDDAuthResp.isOk()) {
                    KLog.i(TAG, "EndUacAuth ok");
                    username = uacDDAuthResp.getData().getName();
                    userid = uacDDAuthResp.getData().getUserid();
                    startUacLogin();
                } else {
                    KLog.i(TAG, "EndUacAuth err");
                    if(onFailed != null) {
                        onFailed.accept("uac授权失败");
                    }
                }
            }
        });
    }

    private void startUacLogin() {
        KLog.i(TAG, "startUacLogin");

        if(call != null) {
            call.cancel();
        }

        call = PbnApi.requestUacLogin(new ApiCallback<UacLoginResp>() {
            @Override
            public void onResp(UacLoginResp uacLoginResp) {
                if(uacLoginResp != null && uacLoginResp.isOk()) {
                    KLog.i(TAG, "endUacLogin ok");
                    startVincentLogin(uacLoginResp.getData());
//                    startCmsLogin(uacLoginResp.getData(), true);
                } else {
                    KLog.i(TAG, "endUacLogin err");
                    if(onFailed != null) {
                        onFailed.accept("uac登录失败");
                    }
                }


            }
        });
    }

    private void startVincentLogin(String token){
        KLog.i(TAG, "startVincentLogin " + token);

        if(call != null) {
            call.cancel();
        }

        call = PbnApi.requestVincentLogin(BuildConfig.DEBUG, token, simpleResp -> {
            if(simpleResp != null && simpleResp.isOk()) {
                KLog.i(TAG, "endCmsLogin ok");
                startCmsLogin(token, true);
            } else {
                KLog.i(TAG, "endCmsLogin err");
                if(onFailed != null) {
                    onFailed.accept("cms登录失败");
                }
            }
        });
    }

    private void startCmsLogin(String token, boolean needLoginGp) {
        KLog.i(TAG, "startCmsLogin " + token);

        if(call != null) {
            call.cancel();
        }

        call = PbnApi.requestCmsLogin(token, simpleResp -> {
            if(simpleResp != null && simpleResp.isOk()) {
                KLog.i(TAG, "endCmsLogin ok");
                if (needLoginGp){
                    PbnApi.setCmsApi(true);
                    startCmsLogin(token, false);
                    return;
                }
                handleLoginSuccess();
            } else {
                KLog.i(TAG, "endCmsLogin err");
                if(onFailed != null) {
                    onFailed.accept("cms登录失败");
                }
            }
        });
    }

    private void handleLoginSuccess() {
        new SUserManager2().setLoginInfo(username, userid);
        XLogger.d("DDLogin success " + username + "|" + userid);
        if(onSuccess != null) {
            onSuccess.run();
        }
    }

}
