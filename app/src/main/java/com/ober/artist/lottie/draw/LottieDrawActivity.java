package com.ober.artist.lottie.draw;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PointF;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.util.Consumer;

import com.airbnb.lottie.LottieDrawable;
import com.meevii.color.fill.FillColorImageView;
import com.meevii.color.fill.model.core.resp.FillColorResp;
import com.meevii.color.fill.model.core.resp.OverwriteColorResp;
import com.meevii.color.fill.model.core.resp.RevokeOverwriteResp;
import com.meevii.color.fill.model.core.resp.RevokeSingleColorResp;
import com.meevii.color.fill.view.gestures.SubsamplingScaleImageView;
import com.ober.artist.R;
import com.ober.artist.lottie.data.LottieInputData;
import com.ober.artist.lottie.draw.panel.LayerItemModel;
import com.ober.artist.lottie.draw.panel.LottieLayerAdapter;
import com.ober.artist.lottie.draw.panel.LottieSelectionView;
import com.ober.artist.utils.SToast;

import java.util.List;

/**
 * Created by ober on 19-9-4.
 */
public class LottieDrawActivity extends AppCompatActivity implements
        LottieDrawInitTask.LottieDrawHost,
        FillColorImageView.FillColorListener,
        LottieLayerAdapter.OnItemClickListener,
        RuntimeLevelManager.OnLayerAreaCountChangeCallback {

    private static final String TAG = "LottieDraw";

    public static void start(Context context, long resid) {
        Intent intent = new Intent(context, LottieDrawActivity.class);
        intent.putExtra("resid", resid);
        context.startActivity(intent);
    }

    private final static int COLOR_HINT = Color.parseColor("#9B9B9B");

    private FillColorImageView mFillIV;
    private LottieSelectionView mBottomPanel;
    private CardView btnPlay;
    private ImageView ivPlay;
    private CardView btnSave;

    private LottieDrawInitTask mInitTask;

    private LevelDataSaveTask mSaveTask;

    private long mId;

    private ProgressDialog mProgressDialog;
    private RuntimeLottieProvider mLottieProvider;
    private boolean isLottiePlaying;

    private RuntimeLevelManager mLevelManager;

    private boolean initComplete;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lottie_draw);
        mFillIV = findViewById(R.id.fiv);
        mFillIV.setEnableTouch(false);

        mBottomPanel = findViewById(R.id.bottom_panel);
        btnPlay = findViewById(R.id.btn_play);
        btnPlay.setVisibility(View.GONE);
        ivPlay = findViewById(R.id.iv_play);
        btnSave = findViewById(R.id.btn_save);

        if(getIntent() == null) {
            finish();
            return;
        }

        long id = getIntent().getLongExtra("resid", -1);
        if(id == -1) {
            finish();
            return;
        }
        mId = id;

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage("loading");
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();

        LottieInputData inputData = new LottieInputData();
        inputData.resId = id;
        mInitTask = new LottieDrawInitTask(inputData, this);
        mInitTask.execute();
    }

    @Override
    public void onBackPressed() {

        if(!initComplete) {
            super.onBackPressed();
            return;
        }

        mFillIV.setEnableTouch(false);
        mSaveTask = new LevelDataSaveTask(mId, mLevelManager.getData(), integer -> {
            mProgressDialog.dismiss();
            SToast.show("saved");
            super.onBackPressed();
        });
        mSaveTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

    }

    private void loadLottie() {
        int selection = mBottomPanel.getAdapter().getCurrentSelection();
        if(selection == -1) {
            SToast.show("请选择layer");
            return;
        }

        mLottieProvider.loadSingleLayer(selection, this::startLottiePlay);
    }

    private void startLottiePlay(LottieDrawable drawable) {
        mFillIV.setLottieDrawable(drawable);
        ivPlay.setImageResource(R.drawable.ic_pause);
        isLottiePlaying = true;
    }

    private void stopLottiePlay() {
        mFillIV.setLottieDrawable(null);
        ivPlay.setImageResource(R.drawable.ic_play);
        isLottiePlaying = false;
    }

    private void scheduleSave() {
        if(mSaveTask != null) {
            mSaveTask.cancel(true);
        }
        mProgressDialog.setMessage("Saving...");
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
        mSaveTask = new LevelDataSaveTask(mId, mLevelManager.getData(), integer -> {
            mProgressDialog.dismiss();
            SToast.show("saved");
        });
        mSaveTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    @Override
    public FillColorImageView getFIV() {
        return mFillIV;
    }

    @Override
    public void handleInitComplete(LottieDrawInitTask.ResultSet rs) {

        initComplete = true;

        mFillIV.setOnStateChangedListener(new ImageOnStateChangedListener());
        mFillIV.setOnImageEventListener(new ImageEventListener());
        mFillIV.initColorOriginImage(rs.colorOriginImage);
        mFillIV.setFillColorListener(this);

        mBottomPanel.getAdapter().setData(rs.layerItemModels);
        mBottomPanel.getAdapter().notifyDataSetChanged();

        mBottomPanel.getAdapter().setOnItemClickListener(this);

        mLottieProvider = new RuntimeLottieProvider(mId, rs.lottieOriginData);

        mLevelManager = new RuntimeLevelManager(rs.lottieLevelData);

        mLevelManager.setOnLayerAreaCountChangeCallback(this);

        btnPlay.setVisibility(View.VISIBLE);
        btnPlay.setOnClickListener(v -> {
            if(isLottiePlaying) {
                stopLottiePlay();
            } else {
                loadLottie();
            }
        });

        btnSave.setOnClickListener(v -> {
            scheduleSave();
        });
    }

    @Override
    public void handleInitFailed(String msg) {
        mProgressDialog.dismiss();
        SToast.show("err:" + msg);
    }

    @Override
    public void onAreaFilled(int num, FillColorResp color) {

        final int mSelectLayer = mBottomPanel.getAdapter().getCurrentSelection();
        if(mSelectLayer == -1) {
            return;
        }

        if(color.fillType == FillColorResp.FILL_SAME_COLOR) {
            mLevelManager.handleAreaUnfill(mSelectLayer, num);
        } else if(color.fillType == FillColorResp.FILL_COLOR) {
            mLevelManager.handleAreaFill(mSelectLayer, num);
        } else {
            //not reachable
            throw new RuntimeException("Program Error");
        }
    }

    @Override
    public void onRevokeCacheStateChanged(boolean empty) {

    }

    @Override
    public void onRevokeBackCacheStateChanged(boolean empty) {

    }

    @Override
    public void onRecalledFillColor(boolean success, RevokeSingleColorResp resp) {

    }

    @Override
    public void onRecalledOverwrite(boolean success, RevokeOverwriteResp resp) {

    }

    @Override
    public void onOverwriteColor(OverwriteColorResp resp) {

    }

    @Override
    public void onReset() {
        mFillIV.setEnableTouch(true);
    }

    @Override
    public void onRefilled() {

    }

    @Override
    public void onLayerItemClicked(LayerItemModel model, int pos) {

        mFillIV.setColor(COLOR_HINT);

        stopLottiePlay();
        mBottomPanel.getAdapter().setSelection(pos);
        mBottomPanel.getAdapter().notifyDataSetChanged();

        List<Integer> layerAreas =  mLevelManager.getLayerAreas(pos);

        mFillIV.setEnableTouch(false);
        mFillIV.scheduleReset(layerAreas, COLOR_HINT);
    }

    @Override
    public void onLayerAreaCountChanged(int layer, int areaCount) {
        mBottomPanel.getAdapter().changeItemData(layer, areaCount);
        mBottomPanel.getAdapter().notifyItemChanged(layer);
    }

    private class ImageEventListener extends SubsamplingScaleImageView.DefaultOnImageEventListener {
        @Override
        public void onImageLoaded() {
            super.onImageLoaded();
            mProgressDialog.dismiss();
            mFillIV.setEnableTouch(true);
        }

        @Override
        public void onImageLoadError(Exception e) {
            super.onImageLoadError(e);
            e.printStackTrace();
            mProgressDialog.dismiss();
            SToast.show("加载出错");
            finish();
        }
    }

    private class ImageOnStateChangedListener implements SubsamplingScaleImageView.OnStateChangedListener {

        @Override
        public void onScaleChanged(float newScale, int origin) {

        }

        @Override
        public void onCenterChanged(PointF newCenter, int origin) {

        }

        @Override
        public void onCenterOrScaleChanged(float newScale, PointF newCenter, int origin) {

        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(mInitTask != null) {
            mInitTask.cancel(true);
        }
        if(mSaveTask != null) {
            mSaveTask.cancel(true);
        }
        if(mLevelManager != null) {
            mLevelManager.destroy();
        }
        if(mLottieProvider != null) {
            mLottieProvider.destroy();
        }
    }
}
