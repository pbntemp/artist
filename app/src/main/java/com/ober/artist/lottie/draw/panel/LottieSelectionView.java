package com.ober.artist.lottie.draw.panel;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ober.artist.R;

/**
 * Created by ober on 19-9-4.
 */
public class LottieSelectionView extends LinearLayout {

    public LottieSelectionView(Context context) {
        super(context);
        init();
    }

    public LottieSelectionView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public LottieSelectionView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private RecyclerView recyclerView;

    private LottieLayerAdapter adapter;

    private LinearLayoutManager layoutManager;

    private void init() {
        setOrientation(VERTICAL);
        inflate(getContext(), R.layout.layout_lottie_pannel, this);
        recyclerView = findViewById(R.id.rv_layers);

        layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(RecyclerView.HORIZONTAL);
        adapter = new LottieLayerAdapter();
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

    }

    public LottieLayerAdapter getAdapter() {
        return adapter;
    }

}
