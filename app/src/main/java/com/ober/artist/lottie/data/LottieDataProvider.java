package com.ober.artist.lottie.data;

import android.graphics.BitmapFactory;

import com.google.gson.reflect.TypeToken;
import com.meevii.color.fill.FillColorConfig;
import com.meevii.color.fill.model.core.origin.ColorOriginImage;
import com.meevii.color.fill.model.core.origin.PdfOriginImage;
import com.ober.artist.net.bean.PbnImgEntity;
import com.ober.artist.utils.GsonUtil;
import com.ober.artist.utils.SimpleFileEditor;

import org.json.JSONException;

import java.io.File;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * Created by ober on 19-9-4.
 */
public class LottieDataProvider {

    public static LottiePreparedData preparedData(long id) {
        LottiePreparedData data = new LottiePreparedData();

        ColorOriginImage colorOriginImage = PdfOriginImage.fromFile(
                LottieLocalStore.pdfFile(id), FillColorConfig.getScale());
        File regionFile = LottieLocalStore.regionFile(id);

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;

        BitmapFactory.decodeFile(regionFile.getAbsolutePath(), options);
        if(options.outMimeType != null) {
            LottiePreparedData.Region region = new LottiePreparedData.Region();
            region.file = regionFile;
            region.mimetype = options.outMimeType;
            region.size = new int[] {options.outWidth, options.outHeight};
            data.region = region;
        }

        data.colorOriginImage = colorOriginImage;

        File lottieJsonFile = new File(LottieLocalStore.lottieDir(id), LottieLocalStore.LOTTIE_JSON_FILE_NAME);

        if(lottieJsonFile.exists()) {
            String lottieJson = SimpleFileEditor.readFromFile(lottieJsonFile.getAbsolutePath());
            try {
                LottieOriginData lottieOriginData = new LottieOriginData(lottieJson);
                data.lottieOriginData = lottieOriginData;
                data.lottieLevelData = new LottieLevelData(lottieOriginData.getLayerCount());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        if(data.lottieLevelData != null) {
            File levelFile = LottieLocalStore.levelFile(id);

            if(levelFile.exists()) {
                String json = SimpleFileEditor.readFromFile(levelFile.getAbsolutePath());
                List<Set<Integer>> set = GsonUtil.fromJson(json, new TypeToken<LinkedList<HashSet<Integer>>>() {}.getType());
                data.lottieLevelData.reset(set);
            }
        }

        File imgBeanFile = LottieLocalStore.imgBeanFile(id);
        String json = SimpleFileEditor.readFromFile(imgBeanFile.getAbsolutePath());
        PbnImgEntity entity = GsonUtil.fromJson(json, PbnImgEntity.class);
        data.imgEntity = entity;

        return data;
    }
}
