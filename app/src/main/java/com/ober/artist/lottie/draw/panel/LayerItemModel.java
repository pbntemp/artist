package com.ober.artist.lottie.draw.panel;

/**
 * Created by ober on 19-9-4.
 */
public class LayerItemModel {

    private int count = 0;

    public void setCount(int count) {
        this.count = count;
    }

    public int getCount() {
        return count;
    }
}
