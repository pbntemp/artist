package com.ober.artist.lottie.draw.panel;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ober.artist.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ober on 19-9-4.
 */
public class LottieLayerAdapter extends RecyclerView.Adapter<LayerItemHolder> {

    public interface OnItemClickListener {
        void onLayerItemClicked(LayerItemModel model, int pos);
    }

    private final List<LayerItemModel> mData;
    private final int colorNormal;
    private final int colorSelected;

    private int mSelection = -1;
    private OnItemClickListener mItemClickListener;

    public LottieLayerAdapter() {
        mData = new ArrayList<>();
        colorNormal = Color.parseColor("#eeeeee");
        colorSelected = Color.parseColor("#aeaeae");
    }

    public void setData(List<LayerItemModel> data) {
        mData.clear();
        mData.addAll(data);
    }

    public void changeItemData(int index, int count) {
        mData.get(index).setCount(count);
    }

    public void setSelection(int pos) {
        this.mSelection = pos;
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        mItemClickListener = listener;
    }

    public int getCurrentSelection() {
        return mSelection;
    }

    @NonNull
    @Override
    public LayerItemHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_layer_choose, parent, false);

        return new LayerItemHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull LayerItemHolder holder, int position) {
        LayerItemModel model = mData.get(position);
        boolean selected = position == mSelection;
        holder.colorCircleView.setData(position + 1, selected ? colorSelected : colorNormal);
        holder.tvCount.setText(String.valueOf(model.getCount()));
        holder.colorCircleView.setOnClickListener(v -> {
            if(mItemClickListener != null) {
                mItemClickListener.onLayerItemClicked(model, position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }
}
