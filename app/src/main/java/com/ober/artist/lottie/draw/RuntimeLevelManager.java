package com.ober.artist.lottie.draw;

import com.ober.artist.lottie.data.LottieLevelData;
import com.socks.library.KLog;

import java.util.List;

/**
 * Created by ober on 19-9-5.
 */
public class RuntimeLevelManager {

    public interface OnLayerAreaCountChangeCallback {
        void onLayerAreaCountChanged(int layer, int areaCount);
    }

    private static final String TAG = "RtLevelManager";

    private final LottieLevelData mData;

    private OnLayerAreaCountChangeCallback onLayerAreaCountChangeCallback;

    public RuntimeLevelManager(LottieLevelData levelData) {
        mData = levelData;
    }

    public boolean handleAreaFill(int layer, int area) {

        KLog.d(TAG, "handleAreaFill " + layer + "," + area);

        if(layer < 0 || area < 0) {
            return false;
        }

        LottieLevelData.LevelInfo info = mData.get(layer);

        boolean r = info.add(area);

        if(r) {
            notifyLayerAreaCountChange(layer, info.getCount());
        }

        return r;
    }

    public boolean handleAreaUnfill(int layer, int area) {
        KLog.d(TAG, "handleAreaUnfill " + layer + "," + area);

        if(layer < 0 || area < 0) {
            return false;
        }

        LottieLevelData.LevelInfo info = mData.get(layer);

        boolean r = info.remove(area);

        if(r) {
            notifyLayerAreaCountChange(layer, info.getCount());
        }

        return r;
    }

    public void setOnLayerAreaCountChangeCallback(OnLayerAreaCountChangeCallback callback) {
        onLayerAreaCountChangeCallback = callback;
    }

    public List<Integer> getLayerAreas(int layer) {
        return mData.get(layer).getAll();
    }

    public LottieLevelData getData() {
        return mData;
    }

    public void destroy() {
        onLayerAreaCountChangeCallback = null;
    }

    private void notifyLayerAreaCountChange(int layer, int areaCount) {
        if(onLayerAreaCountChangeCallback != null) {
            onLayerAreaCountChangeCallback.onLayerAreaCountChanged(layer, areaCount);
        }
    }
}
