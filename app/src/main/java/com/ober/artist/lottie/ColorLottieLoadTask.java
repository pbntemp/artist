package com.ober.artist.lottie;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.core.util.Consumer;

import com.airbnb.lottie.ImageAssetDelegate;
import com.airbnb.lottie.LottieComposition;
import com.airbnb.lottie.LottieCompositionFactory;
import com.airbnb.lottie.LottieResult;
import com.ober.artist.lottie.data.LottieLocalStore;
import com.ober.artist.lottie.data.LottieOriginData;
import com.socks.library.KLog;

import org.json.JSONException;

import java.io.File;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/**
 * Created by ober on 19-7-25.
 */
public class ColorLottieLoadTask extends AsyncTask<Void, Void, ColorLottieLoadTask.ResultSet> {

    public static Executor LOTTIE_LOAD_EXECUTOR = Executors.newFixedThreadPool(2);

    public static class ResultSet {
        @NonNull
        public final LottieComposition composition;
        @NonNull
        public final ImageAssetDelegate delegate;

        private ResultSet(@NonNull LottieComposition composition, @NonNull ImageAssetDelegate delegate) {
            this.composition = composition;
            this.delegate =  delegate;
        }
    }

    private static final String TAG = "ColorLottieLoadTask";

    private final long id;
    private final LottieOriginData lottieOriginData;
    private final boolean completeMode; //是否加载全部layer
    private final List<Integer> indexes;

    private Consumer<ResultSet> mCallback;

    public ColorLottieLoadTask(long id,
                               Consumer<ResultSet> callback,
                               LottieOriginData lottieOriginData,
                               boolean completeMode, List<Integer> indexes) {
        this.id = id;
        this.lottieOriginData = lottieOriginData;
        this.mCallback = callback;
        this.completeMode = completeMode;
        this.indexes = indexes;
    }

    @Override
    protected void onCancelled() {
        mCallback = null;
    }

    @Override
    protected ResultSet doInBackground(Void... voids) {

        final File dstDir = LottieLocalStore.lottieDir(id);

        if(completeMode) {

            final ImageAssetDelegate lottieImageDelegate = asset -> {
                final String absPath;
                if(!TextUtils.isEmpty(asset.getDirName())) {
                    absPath = dstDir.toString() + "/" + asset.getDirName() + "/" + asset.getFileName();
                } else {
                    absPath = dstDir.toString() + "/" + asset.getFileName();
                }

                Bitmap bitmap;
                try {
                    bitmap = BitmapFactory.decodeFile(absPath);
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
                final int w = asset.getWidth();
                final int h = asset.getHeight();
                if(w != 0 && h != 0 && (bitmap.getWidth() != w || bitmap.getHeight() != h)) {
                    bitmap = Bitmap.createBitmap(bitmap, 0, 0, w, h);
                }

                return bitmap;
            };

            String json = lottieOriginData.copy();

            if(TextUtils.isEmpty(json)) {
                KLog.e(TAG, "read lottie json failed!!");
                return null;
            }

            LottieResult<LottieComposition> r = LottieCompositionFactory.fromJsonStringSync(json, null);
            if(r.getException() != null || r.getValue() == null) {
                KLog.e(TAG, "load lottie fail");
                r.getException().printStackTrace();
                return null;
            }

            return new ResultSet(r.getValue(), lottieImageDelegate);
        }

        //通过进度过滤部分layer
        if(indexes == null || indexes.size() == 0) {
            return null;
        }

        if(isCancelled()) {
            //增加被cancel的机会
            return null;
        }

        final ImageAssetDelegate lottieImageDelegate = asset -> {
            final String absPath;
            if(!TextUtils.isEmpty(asset.getDirName())) {
                absPath = dstDir.toString() + "/" + asset.getDirName() + "/" + asset.getFileName();
            } else {
                absPath = dstDir.toString() + "/" + asset.getFileName();
            }

            Bitmap bitmap;
            try {
                bitmap = BitmapFactory.decodeFile(absPath);
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
            final int w = asset.getWidth();
            final int h = asset.getHeight();
            if(w != 0 && h != 0 && (bitmap.getWidth() != w || bitmap.getHeight() != h)) {
                try {
                    bitmap = Bitmap.createBitmap(bitmap, 0, 0, w, h);
                } catch (Exception e) {
                    bitmap = Bitmap.createScaledBitmap(bitmap, w, h, true);
                }

            }

            return bitmap;
        };

        String json = null;
        try {
            json = lottieOriginData.copyLayers(indexes);
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
        if(TextUtils.isEmpty(json)) {
            KLog.e(TAG, "rebuild lottie json failed!!");
            return null;
        }

        LottieResult<LottieComposition> r = LottieCompositionFactory.fromJsonStringSync(json, null);
        if(r.getException() != null || r.getValue() == null) {
            KLog.e(TAG, "load lottie fail");
            r.getException().printStackTrace();
            return null;
        }

        return new ResultSet(r.getValue(), lottieImageDelegate);
    }

    @Override
    protected void onPostExecute(ResultSet rs) {
        super.onPostExecute(rs);
        if(mCallback != null) {
            mCallback.accept(rs);
        }
    }
}
