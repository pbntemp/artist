package com.ober.artist.lottie.draw;

import android.graphics.Bitmap;
import android.os.AsyncTask;

import com.meevii.color.fill.FillColorImageView;
import com.meevii.color.fill.model.core.origin.ColorOriginImage;
import com.ober.artist.lottie.data.LottieDataProvider;
import com.ober.artist.lottie.data.LottieInputData;
import com.ober.artist.lottie.data.LottieLevelData;
import com.ober.artist.lottie.data.LottieOriginData;
import com.ober.artist.lottie.data.LottiePreparedData;
import com.ober.artist.lottie.draw.panel.LayerItemModel;
import com.ober.artist.net.bean.PbnImgEntity;
import com.ober.artist.net.bean.Picture;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by ober on 19-9-4.
 */
public class LottieDrawInitTask extends AsyncTask<Void, Void, LottieDrawInitTask.ResultSet> {

    public static class ResultSet {
        public LottieOriginData lottieOriginData;
        public LottieLevelData lottieLevelData;
        public ColorOriginImage colorOriginImage;
        public PbnImgEntity imgEntity;

        public List<LayerItemModel> layerItemModels;
    }

    public interface LottieDrawHost {
        FillColorImageView getFIV();
        void handleInitComplete(ResultSet resultSet);
        void handleInitFailed(String msg);
    }

    private final LottieInputData mInput;
    private LottieDrawHost mHost;
    private String errMsg;

    public LottieDrawInitTask(LottieInputData inputData, LottieDrawHost host) {
        mInput = inputData;
        mHost = host;
    }

    @Override
    protected void onPostExecute(ResultSet resultSet) {
        if(mHost == null) {
            return;
        }
        if(resultSet == null) {
            mHost.handleInitFailed(errMsg);
        } else {
            mHost.handleInitComplete(resultSet);
        }
    }

    @Override
    protected void onCancelled() {
        mHost = null;
    }

    @Override
    protected ResultSet doInBackground(Void... voids) {

        FillColorImageView fiv = mHost.getFIV();

        final int regionsize[] = new int[2];
        final int pdfsize[] = new int[2];

        LottiePreparedData preparedData = LottieDataProvider.preparedData(mInput.resId);

        LottiePreparedData.Region region = preparedData.region;

        if(region == null) {
            errMsg = "读取region出错";
            return null;
        }

        final int regionW = region.size[0];
        final int regionH = region.size[1];

        regionsize[0] = regionW;
        regionsize[1] = regionH;

        final Bitmap editBmp = Bitmap.createBitmap(regionW, regionH,
                Bitmap.Config.ARGB_8888);

        PbnImgEntity entity = preparedData.imgEntity;

        if(entity == null) {
            errMsg = "程序错误";
            return null;
        }

        int pbntype = resolvePbnType(entity);
        if(pbntype == -1) {
            return null;
        }

        if(pbntype == Picture.PBN_TYPE_NORMAL) {
            pdfsize[0] = 2048;
            pdfsize[1] = 2048;
        } else if(pbntype == Picture.PBN_TYPE_COLORED) {
            pdfsize[0] = 2048;
            pdfsize[1] = 2048;
        } else if(pbntype == Picture.PBN_TYPE_WALL_NORMAL) {
            pdfsize[0] = 750;
            pdfsize[1] = 1334;
        } else if(pbntype == Picture.PBN_TYPE_WALL_COLORED) {
            pdfsize[0] = 750*2;
            pdfsize[1] = 1334*2;
        } else {
            errMsg = "数据错误";
            return null;
        }

        try {
            fiv.initAndPreFill(editBmp, preparedData.region.file, null, false, pdfsize);
        } catch (Exception e) {
            e.printStackTrace();
            errMsg = "图片读取失败,请重试";
            return null;
        }

        ResultSet resultSet = new ResultSet();

        resultSet.colorOriginImage = preparedData.colorOriginImage;
        resultSet.imgEntity = preparedData.imgEntity;
        resultSet.lottieLevelData = preparedData.lottieLevelData;
        resultSet.lottieOriginData = preparedData.lottieOriginData;

        if(resultSet.lottieOriginData == null || resultSet.lottieLevelData == null) {
            errMsg = "lottie加载失败";
            return null;
        }

        resultSet.layerItemModels = generateLayerItemModels(preparedData.lottieLevelData);

        return resultSet;
    }

    private int resolvePbnType(PbnImgEntity entity) {
        String colortype = entity.getType();
        String sizetype = entity.getSizeType();
        if(colortype == null) {
            errMsg = "没有colorType";
            return -1;
        }
        if(sizetype == null) {
            errMsg = "没有sizeType";
            return -1;
        }

        if(colortype.equals(PbnImgEntity.TYPE_COLORED)) {
            if(sizetype.equals(PbnImgEntity.SIZE_TYPE_NORMAL)) {
                return Picture.PBN_TYPE_COLORED;
            } else if(sizetype.equals(PbnImgEntity.SIZE_TYPE_WALLPAPER)) {
                return Picture.PBN_TYPE_WALL_COLORED;
            } else {
                errMsg = "sizeType错误：" + sizetype;
                return -1;
            }
        } else {
            if(sizetype.equals(PbnImgEntity.SIZE_TYPE_NORMAL)) {
                return Picture.PBN_TYPE_NORMAL;
            } else if(sizetype.equals(PbnImgEntity.SIZE_TYPE_WALLPAPER)) {
                return Picture.PBN_TYPE_WALL_NORMAL;
            } else {
                errMsg = "sizeType错误：" + sizetype;
                return -1;
            }
        }
    }

    private static List<LayerItemModel> generateLayerItemModels(LottieLevelData lottieLevelData) {
        List<LayerItemModel> list = new LinkedList<>();
        int size = lottieLevelData.size();
        for(int i = 0; i < size; i++) {
            LayerItemModel model = new LayerItemModel();
            model.setCount(lottieLevelData.get(i).getCount());
            list.add(model);
        }

        return list;
    }

}
