package com.ober.artist.lottie.draw;

import android.os.AsyncTask;

import androidx.core.util.Consumer;

import com.ober.artist.lottie.data.LottieLevelData;
import com.ober.artist.lottie.data.LottieLocalStore;
import com.ober.artist.utils.GsonUtil;
import com.ober.artist.utils.SimpleFileEditor;
import com.socks.library.KLog;

import java.io.File;
import java.util.List;

/**
 * Created by ober on 19-9-5.
 */
public class LevelDataSaveTask extends AsyncTask<Void, Void, Integer> {

    private final List<List<Integer>> mData;
    private final long mId;
    private Consumer<Integer> mCallback;

    public LevelDataSaveTask(long resid, LottieLevelData data, Consumer<Integer> callback) {
        mData = data.copyAll();
        mId = resid;
        mCallback = callback;
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
        mCallback = null;
    }

    @Override
    protected void onPostExecute(Integer integer) {
        super.onPostExecute(integer);
        Consumer<Integer> c = mCallback;
        if(c != null) {
            c.accept(integer);
        }
    }

    @Override
    protected Integer doInBackground(Void... empty) {

        String json = GsonUtil.toJson(mData);

        File file = LottieLocalStore.levelFile(mId);

        file.delete();
        SimpleFileEditor.writeToFile(file.getAbsolutePath(), json);
        KLog.i("LottieDraw", "saved", json);

        return 0;
    }
}
