package com.ober.artist.lottie.data;

import com.meevii.color.fill.model.core.origin.ColorOriginImage;
import com.ober.artist.net.bean.PbnImgEntity;

import java.io.File;

/**
 * Created by ober on 19-9-3.
 */
public class LottiePreparedData {


    public static final class Region {
        public File file;
        public int[] size;
        public String mimetype;
    }

    public Region region;
    public ColorOriginImage colorOriginImage;

    public long id;

    public PbnImgEntity imgEntity;

    public LottieOriginData lottieOriginData;

    public LottieLevelData lottieLevelData;


}
