package com.ober.artist.lottie.data;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * lottie 原始数据
 * Created by ober on 19-7-24.
 */
public final class LottieOriginData {

    private final JSONObject mOriginData;
    private final int layerCount;

    public LottieOriginData(String json) throws JSONException {
        mOriginData = new JSONObject(json);
        JSONArray jsonArray = mOriginData.getJSONArray("layers");
        layerCount = jsonArray.length();
    }

    public final int getLayerCount() {
        return layerCount;
    }

    public String copyLayers(List<Integer> indexes) throws JSONException {
        JSONArray jsonArray = mOriginData.getJSONArray("layers");
        JSONArray newJArray = new JSONArray();
        for(int i : indexes) {
            newJArray.put(jsonArray.get(i));
        }
        JSONObject cpy = new JSONObject(mOriginData.toString());
        cpy.put("layers", newJArray);
        return cpy.toString();
    }

    public String copy() {
        return mOriginData.toString();
    }

//    public static String rebuildLayers(String json, int[] indexes) throws JSONException {
//        JSONObject originData = new JSONObject(json);
//        JSONArray jsonArray = originData.getJSONArray("layers");
//        JSONArray newJArray = new JSONArray();
//        for(int i : indexes) {
//            newJArray.put(jsonArray.get(i));
//        }
//        originData.put("layers", newJArray);
//        return originData.toString();
//    }
}
