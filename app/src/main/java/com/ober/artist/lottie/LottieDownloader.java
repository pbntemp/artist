package com.ober.artist.lottie;

import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import androidx.core.util.Consumer;

import com.ober.artist.App;
import com.ober.artist.lottie.data.LottieLocalStore;
import com.ober.artist.net.bean.LottieResBean;
import com.ober.artist.net.bean.PbnImgEntity;
import com.ober.artist.net.ober.OberApi;
import com.ober.artist.pack.ZipUtil;
import com.ober.artist.utils.Files;
import com.ober.artist.utils.GsonUtil;
import com.ober.artist.utils.OkDownloadUtil;
import com.ober.artist.utils.SimpleFileEditor;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

/**
 * Created by ober on 19-9-3.
 */
public class LottieDownloader extends AsyncTask<Void, Integer, Integer> {


    private final LottieResBean mResBean;
    private final PbnImgEntity mImage;

    private String errMsg;
    private Consumer<Integer> mCallback;
    private Consumer<String> mProgress;

    public LottieDownloader(LottieResBean bean, PbnImgEntity imgEntity,
                            Consumer<Integer> callback, Consumer<String> progress) {
        mResBean = bean;
        mImage = imgEntity;
        mCallback = callback;
        mProgress = progress;
    }

    @Override
    protected void onPostExecute(Integer rs) {
        Consumer<Integer> callback = mCallback;
        if(callback == null) {
            return;
        }
        callback.accept(rs);
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
        Consumer<String> progress = mProgress;
        if(progress != null) {
            progress.accept(getProgressMsg(values[0]));
        }
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
        mCallback = null;
        mProgress = null;
    }

    @Override
    protected Integer doInBackground(Void... voids) {

        final long resid = mResBean.getId();

        publishProgress(0);

        //保存图片bean
        {
            final boolean ready;
            File dstImgBeanFile = LottieLocalStore.imgBeanFile(resid);
            if(dstImgBeanFile.exists() && dstImgBeanFile.length() > 0) {
                ready = true;
            } else {
                ready = false;
            }

            if(!ready) {
                dstImgBeanFile.delete();
                SimpleFileEditor.writeToFile(dstImgBeanFile.getAbsolutePath(), GsonUtil.toJson(mImage));
            }
        }

        publishProgress(1);

        //下载图片资源

        //region
        {
            String regionUrl = mImage.getRegion();
            File dstRegionFile = LottieLocalStore.regionFile(resid);
            final boolean ready;
            if(dstRegionFile.exists()) {
                if(!checkImageFile(dstRegionFile)) {
                    ready = false;
                } else {
                    ready = true;
                }
            } else {
                ready = false;
            }

            if(!ready) {
                try {
                    OkDownloadUtil.download(regionUrl, dstRegionFile);
                } catch (IOException e) {
                    e.printStackTrace();
                    dstRegionFile.delete();
                    errMsg = "download region fail";
                    return -1;
                }
            }

        }

        publishProgress(2);

        //pdf
        {
            String pdfUrl = mImage.getPdf();
            File dstPdfFile = LottieLocalStore.pdfFile(resid);
            final boolean ready;
            if(dstPdfFile.exists()) {
               if(dstPdfFile.length() > 0) {
                   ready = true;
               } else {
                   ready = false;
               }
            } else {
                ready = false;
            }

            if(!ready) {
                try {
                    OkDownloadUtil.download(pdfUrl, dstPdfFile);
                } catch (IOException e) {
                    e.printStackTrace();
                    dstPdfFile.delete();
                    errMsg = "download pdf fail";
                    return -2;
                }
            }
        }

        publishProgress(3);

        //下载lottie zip包
        {
            final boolean ready;
            String lottieZipPath = mResBean.getPath();
            File lottieDir = LottieLocalStore.lottieDir(resid);
            File[] files = lottieDir.listFiles();
            if(files != null && files.length > 0) {
                ready = true;
            } else {
                ready = false;
                Files.rm_rf(lottieDir);
            }
            if(!ready) {
                File tempZipFile = new File(App.getInstance().getCacheDir(),
                        UUID.randomUUID().toString().replace("-", ""));

                try {
                    OkDownloadUtil.download(completeZipUrl(lottieZipPath), tempZipFile);
                } catch (IOException e) {
                    e.printStackTrace();
                    tempZipFile.delete();
                    errMsg = "download lottie package fail";
                    return -3;
                }

                publishProgress(4);

                try {
                    ZipUtil.unzip(tempZipFile.getAbsolutePath(), lottieDir.getAbsolutePath());
                    Files.cleanDir(lottieDir);
                } catch (IOException e) {
                    e.printStackTrace();
                    tempZipFile.delete();
                    Files.rm_rf(lottieDir);
                    errMsg = "unzip lottie package fail";
                    return -4;
                }
            }
        }

        return 0;
    }

    public String getErrMsg() {
        return errMsg;
    }

    private static boolean checkImageFile(File f) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(f.getAbsolutePath(), options);
        if(options.outWidth > 0 && options.outHeight > 0) {
            return true;
        }
        return false;
    }

    private static String completeZipUrl(String path) {
        return OberApi.HOST_HTTP_URL + path;
    }

    private static String getProgressMsg(final int progress) {
        switch (progress) {
            case 0:
                return "保存图片信息...";
            case 1:
                return "下载region...";
            case 2:
                return "下载pdf...";
            case 3:
                return "下载lottie包...";
            case 4:
                return "解压lottie包...";
            default:
                return "Loading...";
        }
    }
}
