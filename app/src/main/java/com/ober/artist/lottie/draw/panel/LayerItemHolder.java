package com.ober.artist.lottie.draw.panel;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ober.artist.R;
import com.ober.artist.color.widget.ColorCircleView;

/**
 * Created by ober on 19-9-4.
 */
public class LayerItemHolder extends RecyclerView.ViewHolder {

    public final TextView tvCount;
    public final ColorCircleView colorCircleView;

    public LayerItemHolder(@NonNull View itemView) {
        super(itemView);
        this.tvCount = itemView.findViewById(R.id.tv_count);
        this.colorCircleView = itemView.findViewById(R.id.view_circle);
    }
}
