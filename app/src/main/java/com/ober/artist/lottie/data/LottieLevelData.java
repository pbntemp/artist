package com.ober.artist.lottie.data;

import android.util.Log;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * Created by ober on 19-9-3.
 */
public class LottieLevelData {

    public static class LevelInfo {
        final Set<Integer> areas;

        LevelInfo() {
            this.areas = new HashSet<>();
        }

        public int getCount() {
            return areas.size();
        }

        public boolean add(int area) {
            return areas.add(area);
        }

        public boolean remove(int area) {
            return areas.remove(area);
        }

        public List<Integer> getAll() {
            return new LinkedList<>(areas);
        }
    }

    private final List<LevelInfo> mData;//每个lottie-layer对应一个LevelInfo
    private final int mSize;

    public LottieLevelData(int size) {
        mData = new ArrayList<>(size);
        mSize = size;
        for(int i = 0; i < size; i++) {
            mData.add(new LevelInfo());
        }
    }

    public final int size() {
        return mSize;
    }

    public LevelInfo get(int index) {
        return mData.get(index);
    }

    public List<List<Integer>> copyAll() {
        List<List<Integer>> data = new LinkedList<>();
        for(int i = 0; i < mSize; i++) {
            data.add(mData.get(i).getAll());
        }
        return data;
    }

    public final void reset(List<Set<Integer>> data) {
        if(data == null) {
            for(LevelInfo info : mData) {
                info.areas.clear();
            }
        } else {
            if(data.size() != mSize) {
                Log.e("LottieLevelData", "reset err size not match");
                return;
            }

            for(int i = 0; i < mSize; i++) {
                Set<Integer> areas = mData.get(i).areas;
                areas.clear();
                areas.addAll(data.get(i));
            }
        }
    }

}
