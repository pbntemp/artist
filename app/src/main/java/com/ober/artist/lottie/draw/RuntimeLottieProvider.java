package com.ober.artist.lottie.draw;

import androidx.core.util.Consumer;

import com.airbnb.lottie.ImageAssetDelegate;
import com.airbnb.lottie.LottieComposition;
import com.airbnb.lottie.LottieCompositionFactory;
import com.airbnb.lottie.LottieDrawable;
import com.airbnb.lottie.LottieResult;
import com.ober.artist.lottie.ColorLottieLoadTask;
import com.ober.artist.lottie.data.LottieInputData;
import com.ober.artist.lottie.data.LottieOriginData;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by ober on 19-9-4.
 */
public class RuntimeLottieProvider {

    private final ExecutorService es;

    private final LottieOriginData mLottieOriginData;
    private final long mId;

    private ColorLottieLoadTask mLoadTask;

    public RuntimeLottieProvider(long resid, LottieOriginData lottieOriginData) {
        mLottieOriginData = lottieOriginData;
        mId = resid;
        es = Executors.newSingleThreadExecutor();
    }

    public void loadSingleLayer(int layer, Consumer<LottieDrawable> callback) {
        cancel();

        List<Integer> indexes = new LinkedList<>();
        indexes.add(layer);
        mLoadTask = new ColorLottieLoadTask(mId, rs -> {
            LottieDrawable d = createLottieDrawable(rs.delegate, rs.composition);
            callback.accept(d);
        }, mLottieOriginData, false, indexes);
        mLoadTask.executeOnExecutor(es);
    }

    public void cancel() {
        if(mLoadTask != null) {
            mLoadTask.cancel(true);
            mLoadTask = null;
        }
    }

    public void destroy() {
        cancel();
        es.shutdown();
    }

    private static LottieDrawable createLottieDrawable(ImageAssetDelegate delegate, LottieComposition composition) {
        LottieDrawable lottieDrawable = new LottieDrawable();
        lottieDrawable.setImageAssetDelegate(delegate);
        lottieDrawable.setRepeatCount(-1);
        lottieDrawable.setComposition(composition);
        return lottieDrawable;
    }
}
