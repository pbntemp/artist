package com.ober.artist.lottie.data;

import android.os.Environment;

import java.io.File;

/**
 * Created by ober on 19-9-3.
 */
public class LottieLocalStore {

    private static final String ARCH_DIR = "lottieV0";

    public static final String LOTTIE_JSON_FILE_NAME = "dynamic_lottie.json";

    public static File regionFile(long resId) {
        return new File(resourceDir(resId), "region");
    }

    public static File pdfFile(long resId) {
        return new File(resourceDir(resId), "pdf");
    }

    /**
     * {@link com.ober.artist.net.bean.PbnImgEntity}
     */
    public static File imgBeanFile(long resId) {
        return new File(resourceDir(resId), "imgBean");
    }

    /**
     * List<int[]>
     */
    public static File levelFile(long resId) {
        return new File(resourceDir(resId), "level");
    }

    public static File lottieDir(long resId) {
        File resDir = resourceDir(resId);
        File dir = new File(resDir, "lottie");
        if(!dir.exists()) {
            dir.mkdirs();
        }
        return dir;
    }

    public static File resourceDir(long resId) {
        File dir = new File(archDir(), "res_" + resId);
        if(!dir.exists()) {
            dir.mkdirs();
        }
        return dir;
    }

    public static File archDir() {
        File f = new File(Environment.getExternalStorageDirectory().getAbsolutePath()
                + "/AA_PBN/" + ARCH_DIR);
        if (!f.exists()) {
            f.mkdirs();
        }
        return f;
    }
}
