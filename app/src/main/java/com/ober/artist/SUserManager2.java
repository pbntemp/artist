package com.ober.artist;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by ober on 2019-10-28.
 */
public class SUserManager2 {

    private static final String PREF_NAME = "UserPref2";

    private SharedPreferences mPref;

    public SUserManager2() {
        mPref = App.getInstance()
                .getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
    }

    public void setLoginInfo(String username, String userid) {
        mPref.edit().putString("uname", username)
                .putString("uid", userid)
                .putLong("timestamp", System.currentTimeMillis())
                .apply();
    }

    public String getUserName() {
        return mPref.getString("uname", "null");
    }

    public String getUID() {
        return mPref.getString("uid", "0123");
    }

}
