package com.ober.artist.pack;

import android.os.AsyncTask;

import com.google.gson.reflect.TypeToken;
import com.ober.artist.App;
import com.ober.artist.color.data.PreColor;
import com.ober.artist.lottie.data.LottieLocalStore;
import com.ober.artist.net.bean.PbnImgEntity;
import com.ober.artist.utils.Files;
import com.ober.artist.utils.GsonUtil;
import com.ober.artist.utils.PreColorCoder;
import com.ober.artist.utils.SimpleFileEditor;
import com.socks.library.KLog;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * Created by ober on 19-9-5.
 */
public class ZipLottieLevelTask extends AsyncTask<Void, Void, Integer> {

    public interface Callback {
        void onResult(File file);
    }

    private static final String TAG = "ZipLottieTask";

    private final long id;
    private final File dstFile;

    private Callback mCallback;

    public ZipLottieLevelTask(long id, File dst, Callback callback) {
        this.id = id;
        this.dstFile = dst;
        this.mCallback = callback;
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
        mCallback = null;
    }

    @Override
    protected void onPostExecute(Integer integer) {
        super.onPostExecute(integer);
        Callback c = mCallback;
        if(c != null) {
            if(integer == 0) {
                c.onResult(dstFile);
            } else {
                c.onResult(null);
            }
        }
    }

    @Override
    protected Integer doInBackground(Void... voids) {

        KLog.d(TAG, "start");

        File tempFile = new File(App.getInstance().getCacheDir(), "dynamic_layer.json");
        if(tempFile.exists()) {
            Files.rm_rf(tempFile);
        }

        File levelFile = LottieLocalStore.levelFile(id);

        if(!levelFile.exists()) {
            return -1;
        }

        if(dstFile.exists()) {
            Files.rm_rf(tempFile);
        }

        String json = SimpleFileEditor.readFromFile(levelFile.getAbsolutePath());
        KLog.d(TAG, "level data read", json);

        List<Set<Integer>> data = GsonUtil.fromJson(json, new TypeToken<LinkedList<HashSet<Integer>>>() {}.getType());

        json = SimpleFileEditor.readFromFile(LottieLocalStore.imgBeanFile(id).getAbsolutePath());

        KLog.d(TAG, "img entity read", json);

        PbnImgEntity entity = GsonUtil.fromJson(json, PbnImgEntity.class);

        KLog.d(TAG, "process level data and plan");

        String planStr = entity.getPlans()[0];
        PreColor[] preColors = PreColorCoder.decode(planStr);

        Set<Integer> availableAreas = new HashSet<>();
        for(PreColor preColor : preColors) {
            for(int area : preColor.areas) {
                availableAreas.add(area);
            }
        }

        KLog.d(TAG, "availableAreas read count=" + availableAreas.size());

        int layerIndex = 0;
        for(Set<Integer> layerAreas : data) {
            if(layerAreas.isEmpty()) {
                layerIndex++;
                continue;
            }
            Iterator<Integer> it = layerAreas.iterator();
            while (it.hasNext()) {
                Integer area = it.next();
                if(!availableAreas.contains(area)) {
                    it.remove();
                    KLog.w(TAG, "remove invalid area (" + area + ") in (" + layerIndex + ")");
                }
            }
            layerIndex++;
        }


        json = GsonUtil.toJson(data);

        KLog.d(TAG, "process level data and plan end", json);

        SimpleFileEditor.writeToFile(tempFile.getAbsolutePath(), json);

        try {
            ZipUtil.zip(tempFile.getAbsolutePath(), dstFile.getAbsolutePath());
        } catch (IOException e) {
            e.printStackTrace();
            dstFile.delete();
            return -2;
        } finally {
            tempFile.delete();
        }

        return 0;
    }
}
