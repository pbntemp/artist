package com.ober.artist.pack;

import android.os.AsyncTask;

import com.ober.artist.color.data.Center;
import com.ober.artist.color.localdata.FileDataLoader;
import com.ober.artist.color.localdata.FileImageBean2;
import com.ober.artist.color.localdata.LocalData;
import com.ober.artist.db.PbnDb;
import com.ober.artist.db.PictureRecord;
import com.socks.library.KLog;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * Created by ober on 2019-12-09.
 */
public class FilledColorCheckTask extends AsyncTask<Void, Void, FilledColorCheckTask.Rs> {

    private static final String TAG = "FillResultCheckTask";

    public static class Rs {
        public final Set<Integer> uncoloredAreas;

        Rs(Set<Integer> set) {
            uncoloredAreas = set;
        }
    }

    public interface Callback {
        void onResult(Rs suc, String errMsg);
    }

    private final Callback callback;
    private final String uid;
    private final String id;
    private final boolean isLocal;

    private String errMsg;

    public FilledColorCheckTask(String uid,
                                boolean isLocal,
                                String id,
                                Callback callback) {
        this.uid = uid;
        this.id = id;
        this.isLocal = isLocal;
        this.callback = callback;
    }


    @Override
    protected void onPostExecute(Rs rs) {
        super.onPostExecute(rs);
        if(rs == null) {
            callback.onResult(null, errMsg);
        } else {
            callback.onResult(rs, "ok");
        }
    }

    @Override
    protected Rs doInBackground(Void... voids) {


        KLog.d(TAG, "doInBackground", id, uid);

        List<PictureRecord> queryRs = PbnDb.get().getPicRecordDao().getById(uid, id);
        if(queryRs.isEmpty()) {
            errMsg = "未找到当前素材";
            return null;
        }

        FileImageBean2 imageBean = FileDataLoader.load2(uid, isLocal, id);
        imageBean.pbntype = queryRs.get(0).getPbntype();

        if(!imageBean.area.exists()) {
            errMsg = "当前素材缺少着色进度";
            return null;
        }

        //process center

        Map<Integer, Integer> areaColorMap = LocalData.loadColorMap(uid, isLocal, id);

        Map<Integer, Center> centerMap = CenterGen.genCenter2(imageBean);

        if(isCancelled()) {
            return null;
        }

        if(centerMap == null) {
            errMsg = "生成center失败";
            return null;
        }

        Set<Integer> unColoredArea = new HashSet<>();
        for(Integer areaInCenter : centerMap.keySet()) {
            if(areaColorMap.get(areaInCenter) == null) {
                unColoredArea.add(areaInCenter);
            }
        }

        Rs rs = new Rs(unColoredArea);
        return rs;
    }


}
