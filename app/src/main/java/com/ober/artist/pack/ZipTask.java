package com.ober.artist.pack;

import android.os.AsyncTask;

import com.ober.artist.App;
import com.ober.artist.color.data.Center;
import com.ober.artist.color.data.LocalStore;
import com.ober.artist.color.localdata.FileDataLoader;
import com.ober.artist.color.localdata.FileDataStruct;
import com.ober.artist.color.localdata.FileImageBean;
import com.ober.artist.net.bean.Picture;
import com.socks.library.KLog;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by ober on 18-12-6.
 */
public class ZipTask extends AsyncTask<Void, String, Boolean> {

    public interface Callback {
        void onProgress(String s);

        void onResult(File dst);
    }

    private final File dst;
    private final Callback callback;
    private final String packName;

    private final Set<String> mTargetPicNames;

    public ZipTask(String packName,
                   List<Picture> targetPics,
                   File dst,
                   Callback callback) {
        this.dst = dst;
        this.callback = callback;
        this.packName = packName;
        mTargetPicNames = new HashSet<>();
        for(Picture picture : targetPics) {
            mTargetPicNames.add(picture.getName());
        }
    }

    @Override
    protected void onProgressUpdate(String... values) {
        super.onProgressUpdate(values);
        Callback callback = this.callback;
        if (callback != null) {
            callback.onProgress(values[0]);
        }
    }

    @Override
    protected Boolean doInBackground(Void[] objects) {

        FileDataStruct dataStruct = FileDataLoader
                .loadDir(App.getInstance(), packName);

        if (dataStruct == null || dataStruct.imageList == null
                || dataStruct.imageList.isEmpty()) {
            return false;
        }

        Iterator<FileImageBean> it = dataStruct.imageList.iterator();
        while (it.hasNext()) {
            FileImageBean imageBean = it.next();
            if (!imageBean.area.exists()) {
                it.remove();
            } else if(!mTargetPicNames.contains(imageBean.token)) {
                it.remove();
            }
        }

        if (dst.exists()) {
            dst.delete();
        }

        final int count = dataStruct.imageList.size();

        int i = 0;
        for (FileImageBean imageBean : dataStruct.imageList) {
            if (isCancelled()) {
                return false;
            }
            publishProgress("process center : " + i + "/" + count);

            imageBean.center.delete();
            Map<Integer, Center> centerMap = CenterGen.genCenter(imageBean);

            int[] order = LocalStore.readSort(App.getInstance(), packName, imageBean.token);

            LocalStore.savePlanByAreaMap(App.getInstance(),
                    imageBean.plan, imageBean.area, centerMap, order);
            i++;
        }

        publishProgress("process zip");

        try {
            ZipUtil.zipPack(LocalStore.packDir(packName),
                    dst.getAbsolutePath());
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    protected void onPostExecute(Boolean o) {
        if (callback == null) {
            KLog.e("onPostExecute", "Callback is NULL");
            return;
        }
        if (o == Boolean.valueOf(true)) {
            callback.onResult(dst);
        } else {
            callback.onResult(null);
        }
    }
}
