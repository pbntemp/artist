package com.ober.artist.pack;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;

import com.ober.artist.alg.AlgorithmCenter2;
import com.ober.artist.color.data.Center;
import com.ober.artist.color.localdata.FileImageBean;
import com.ober.artist.color.localdata.FileImageBean2;
import com.ober.artist.utils.CenterCoder;
import com.ober.artist.utils.SimpleFileEditor;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by ober on 2019/1/11.
 */
public class CenterGen {

    public static Map<Integer, Center> genCenter2(FileImageBean2 struct) {
        if (struct.center.exists()) {
            struct.center.delete();
        }
        if (!struct.area.exists() || !struct.thumb.exists()) {
            return null;
        }

        Bitmap region = BitmapFactory.decodeFile(struct.region.getAbsolutePath());
        if (region == null) {
            return null;
        }
        Bitmap png = BitmapFactory.decodeFile(struct.origin.getAbsolutePath());
        if (png == null) {
            return null;
        }

        if(png.getWidth() != region.getWidth() || png.getHeight() != region.getHeight()) {
            float scaleX = (float) region.getWidth() / (float)png.getWidth();
            float scaleY = (float) region.getHeight() / (float)png.getHeight();
            Matrix matrix = new Matrix();
            matrix.postScale(scaleX, scaleY);
            png = Bitmap.createScaledBitmap(png, region.getWidth(), region.getHeight(), true);
        }

        String encoded = AlgorithmCenter2.process(png, region);

        HashMap<Integer, Center> centerMap = CenterCoder.decode(encoded);

        SimpleFileEditor.writeToFile(struct.center.getAbsolutePath(), encoded);
        return centerMap;
    }

    public static Map<Integer, Center> genCenter(FileImageBean struct) {
        if (struct.center.exists()) {
            return null;
        }
        if (!struct.area.exists() || !struct.thumb.exists()) {
            return null;
        }

        Bitmap region = BitmapFactory.decodeFile(struct.region.getAbsolutePath());
        if (region == null) {
            return null;
        }
        Bitmap png = BitmapFactory.decodeFile(struct.origin.getAbsolutePath());
        if (png == null) {
            return null;
        }

        if(png.getWidth() != region.getWidth() || png.getHeight() != region.getHeight()) {
            float scaleX = (float) region.getWidth() / (float)png.getWidth();
            float scaleY = (float) region.getHeight() / (float)png.getHeight();
            Matrix matrix = new Matrix();
            matrix.postScale(scaleX, scaleY);
            png = Bitmap.createScaledBitmap(png, region.getWidth(), region.getHeight(), true);
        }

        String encoded = AlgorithmCenter2.process(png, region);

        HashMap<Integer, Center> centerMap = CenterCoder.decode(encoded);

        SimpleFileEditor.writeToFile(struct.center.getAbsolutePath(), encoded);
        return centerMap;
    }
}
