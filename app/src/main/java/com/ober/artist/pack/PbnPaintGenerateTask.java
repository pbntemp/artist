package com.ober.artist.pack;

import android.os.AsyncTask;

import com.ober.artist.analyze.base.ArtistAnalyze;
import com.ober.artist.color.data.Center;
import com.ober.artist.color.localdata.FileDataLoader;
import com.ober.artist.color.localdata.FileImageBean2;
import com.ober.artist.color.localdata.LocalData;
import com.ober.artist.db.PbnDb;
import com.ober.artist.db.PictureRecord;
import com.ober.artist.utils.ResourceDimenCheckUtil;
import com.ober.artist.xlog.XLogger;
import com.socks.library.KLog;

import java.util.List;
import java.util.Map;

/**
 * 生成plan和center
 *
 * Created by ober on 19-10-30.
 */
public class PbnPaintGenerateTask extends AsyncTask<Void, String, FileImageBean2> {

    private static final String TAG = "PaintProcessTask";

    public interface Callback {
        void onResult(FileImageBean2 suc, String errMsg);
    }

    private final Callback callback;
    private final String uid;
    private final String id;
    private final boolean isLocal;
    private final boolean analyze;

    private String errMsg;

    public PbnPaintGenerateTask(String uid,
                                boolean isLocal,
                                boolean analyze,
                                String id,
                                Callback callback) {
        this.uid = uid;
        this.id = id;
        this.isLocal = isLocal;
        this.analyze = analyze;
        this.callback = callback;
    }

    @Override
    protected FileImageBean2 doInBackground(Void[] objects) {

        KLog.d(TAG, "doInBackground", id, uid);

        List<PictureRecord> rs = PbnDb.get().getPicRecordDao().getById(uid, id);
        if(rs.isEmpty()) {
            errMsg = "未找到当前素材";
            return null;
        }

        FileImageBean2 imageBean = FileDataLoader.load2(uid, isLocal, id);
        imageBean.pbntype = rs.get(0).getPbntype();

        if(!imageBean.area.exists()) {
            errMsg = "当前素材缺少着色进度";
            return null;
        }

        //process center

        if(imageBean.center.exists()) {
            KLog.w(TAG, "delete exists center");
            imageBean.center.delete();
        }

        if(imageBean.plan.exists()) {
            KLog.w(TAG, "delete exists plan");
        }

        Map<Integer, Center> centerMap = CenterGen.genCenter2(imageBean);

        if(isCancelled()) {
            return null;
        }

        int[] order = LocalData.readSort(uid, isLocal, id);

        try {
            LocalData.savePlanByAreaMap(imageBean.plan, imageBean.area, centerMap, order);
        } catch (Exception e){
            XLogger.e("PbnPaintGenerateTask.savePlanByAreaMap:" + e.getMessage());
            return null;
        }

        String[] outErrmsg = new String[1];
        boolean r = ResourceDimenCheckUtil.check(imageBean, outErrmsg);
        if(!r) {
            errMsg = outErrmsg[0];
            return null;
        }

        if (analyze) {
            ArtistAnalyze.trackEventKeyValue(ArtistAnalyze.Picture.event_cost_box,
                    String.format(ArtistAnalyze.Common.pic_id, id),
                    centerMap == null ? 0 : centerMap.keySet().size());
        }
        return imageBean;
    }

    @Override
    protected void onPostExecute(FileImageBean2 o) {
        if (callback == null) {
            KLog.e("onPostExecute", "Callback is NULL");
            return;
        }
        callback.onResult(o, errMsg);
    }
}

