package com.ober.artist.db;

/**
 * Created by ober on 2019-10-28.
 */
public class PicType {

    public static final int PBN_TYPE_NORMAL = 1;
    public static final int PBN_TYPE_COLORED = 2;
    public static final int PBN_TYPE_WALL_NORMAL = 3;
    public static final int PBN_TYPE_WALL_COLORED = 4;

    public static int[] regionSize(int pbntype) {
        if(pbntype == PBN_TYPE_NORMAL) {
            return new int[] {1024, 1024};
        } else if(pbntype == PBN_TYPE_COLORED) {
            return new int[] {2048, 2048};
        } else if(pbntype == PBN_TYPE_WALL_NORMAL) {
            return new int[] {750, 1334};
        } else if(pbntype == PBN_TYPE_WALL_COLORED) {
            return new int[] {1500, 2668};
        } else {
            throw new RuntimeException("unknown pbntype " + pbntype);
        }
    }

    public static int[] pdfSize(int pbntype) {
        if(pbntype == PBN_TYPE_NORMAL) {
            return new int[] {2048, 2048};
        } else if(pbntype == PBN_TYPE_COLORED) {
            return new int[] {2048, 2048};
        } else if(pbntype == PBN_TYPE_WALL_NORMAL) {
            return new int[] {750, 1334};
        } else if(pbntype == PBN_TYPE_WALL_COLORED) {
            return new int[] {1500, 2668};
        } else {
            throw new RuntimeException("unknown pbntype " + pbntype);
        }
    }

    public static int[] pngSize(int pbntype) {
        if(pbntype == PBN_TYPE_NORMAL) {
            return new int[] {2048, 2048};
        } else if(pbntype == PBN_TYPE_COLORED) {
            return new int[] {2048, 2048};
        } else if(pbntype == PBN_TYPE_WALL_NORMAL) {
            return new int[] {1500, 2668};
        } else if(pbntype == PBN_TYPE_WALL_COLORED) {
            return new int[] {1500, 2668};
        } else {
            throw new RuntimeException("unknown pbntype " + pbntype);
        }
    }

    public static int[] coloredSize(int pbntype) {
        if(pbntype == PBN_TYPE_NORMAL) {
            return new int[] {1024, 1024};
        } else if(pbntype == PBN_TYPE_COLORED) {
            return new int[] {2048, 2048};
        } else if(pbntype == PBN_TYPE_WALL_NORMAL) {
            return new int[] {750, 1334};
        } else if(pbntype == PBN_TYPE_WALL_COLORED) {
            return new int[] {1500, 2668};
        } else {
            throw new RuntimeException("unknown pbntype " + pbntype);
        }
    }

}
