package com.ober.artist.db;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.ober.artist.App;
import com.ober.artist.db.migrate.Migration1_2;

import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by ober on 2019-10-28.
 */

@Database(entities = {
        PictureRecord.class
}, version = 2, exportSchema = true)
public abstract class PbnDb extends RoomDatabase {

    public static final String DB_NAME = "arts.db";

    public abstract PicRecordDao getPicRecordDao();

    private static PbnDb mDb;

    private static AtomicBoolean initFlag = new AtomicBoolean(false);

    private static synchronized void init() {
        if(initFlag.get()) {
            return;
        }

        initFlag.set(true);
        mDb = Room.databaseBuilder(App.getInstance(), PbnDb.class, DB_NAME)
                //.fallbackToDestructiveMigration()
                .addMigrations(new Migration1_2())
                .build();
    }

    public static PbnDb get() {
        init();
        return mDb;
    }

}
