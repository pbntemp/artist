package com.ober.artist.db;

import androidx.annotation.NonNull;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

/**
 * Created by ober on 2019-10-28.
 */

@Dao
public interface PicRecordDao {

    @Query("select * from pic_record where uid=:uid order by download_time desc")
    List<PictureRecord> getAllByUid(@NonNull String uid);

    @Query("select * from pic_record where uid=:uid and id=:id")
    List<PictureRecord> getById(String uid, String id);

    @Query("update pic_record set update_time=:updateTime where id=:id")
    void updateUpdateTime(String id, long updateTime);

    @Query("update pic_record set uploaded_time=:uploadTime where id=:id")
    void updateUploadTime(String id, long uploadTime);

    @Query("update pic_record set build_version=:version where id=:id")
    void resetVersion(String id, int version);

    @Insert
    void insert(PictureRecord record);

    @Query("delete from pic_record where uid=:uid and id=:id")
    void deleteById(String uid, String id);

}
