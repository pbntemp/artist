package com.ober.artist.db;


import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

/**
 * Created by ober on 2019-10-28.
 */

@Entity(tableName = "pic_record")
public class PictureRecord {

    public static final int VERSION_1 = 0; //最开始的版本
    public static final int VERSION_2 = 1; //增加肤色色板

    public static final int VERSION_CURRENT = 1;
    @NonNull
    @PrimaryKey
    @ColumnInfo(name = "id")
    private String id;

    @ColumnInfo(name = "pbntype")
    private int pbntype;

    @ColumnInfo(name = "download_time")
    private long downloadTime;

    @ColumnInfo(name = "uid")
    @NonNull
    private String uid;

    @ColumnInfo(name = "update_time")
    private long updateTime;

    @ColumnInfo(name = "uploaded_time")
    private long uploadedTime;

    @ColumnInfo(name = "build_version")
    private int buildVersion = 0;

    public PictureRecord() {

    }

    @NonNull
    public String getId() {
        return id;
    }

    public void setId(@NonNull String id) {
        this.id = id;
    }

    public long getDownloadTime() {
        return downloadTime;
    }

    public void setDownloadTime(long downloadTime) {
        this.downloadTime = downloadTime;
    }

    public int getPbntype() {
        return pbntype;
    }

    public void setPbntype(int pbntype) {
        this.pbntype = pbntype;
    }

    @NonNull
    public String getUid() {
        return uid;
    }

    public void setUid(@NonNull String uid) {
        this.uid = uid;
    }

    public long getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(long updateTime) {
        this.updateTime = updateTime;
    }

    public long getUploadedTime() {
        return uploadedTime;
    }

    public void setUploadedTime(long uploadedTime) {
        this.uploadedTime = uploadedTime;
    }

    public int getBuildVersion() {
        return buildVersion;
    }

    public void setBuildVersion(int buildVersion) {
        this.buildVersion = buildVersion;
    }
}
