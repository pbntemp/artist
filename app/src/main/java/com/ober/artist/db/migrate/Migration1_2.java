package com.ober.artist.db.migrate;

import androidx.annotation.NonNull;
import androidx.room.migration.Migration;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.ober.artist.xlog.XLogger;

/**
 * Created by ober on 2020-01-06.
 */
public class Migration1_2 extends Migration {

    public Migration1_2() {
        super(1, 2);
    }

    @Override
    public void migrate(@NonNull SupportSQLiteDatabase database) {

        String sql = "alter table pic_record add column build_version INTEGER NOT NULL default 0";

        XLogger.d("[PbnDB] " + sql);

        try {
            database.execSQL(sql);
        } catch (Exception e) {
            XLogger.e("[PbnDB] " + e.getMessage());
            throw e;
        }

    }
}
