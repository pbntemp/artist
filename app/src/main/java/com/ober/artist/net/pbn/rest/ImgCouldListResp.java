package com.ober.artist.net.pbn.rest;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;
import com.ober.artist.db.PicType;
import com.ober.artist.utils.TextUtil;

public class ImgCouldListResp extends BaseResp<ImgCouldListResp.Data> {


    public static final String TYPE_DEF = "normal";
    public static final String TYPE_COLORED = "colored";

    public static final String SIZE_TYPE_NORMAL = "normal";
    public static final String SIZE_TYPE_WALLPAPER = "wallpaper";


    @Override
    public boolean isOk() {
        return super.isOk() && getData() != null && getData().getImages() != null;
    }

    public static class Data {
        @SerializedName("list")
        private Image[] images;

        @SerializedName("total")
        private int totall;

        public Image[] getImages() {
            return images;
        }

        public int getTotal() {
            return totall;
        }
    }

    public static class Image {
        @SerializedName("id")
        private String id;

        @SerializedName("type")
        private String type;

        @SerializedName("pdf")
        private String pdf;

        @SerializedName("region")
        private String region;

        @SerializedName("region_old")
        private String region_old;

        @SerializedName("png")
        private String png;

        @SerializedName("size_type")
        private String sizeType;

        @SerializedName("color")
        private String color;

        @SerializedName("plan")
        private String plan;

        @SerializedName("center")
        private String center;

        @SerializedName("thumbnail")
        private String thumbnail;

        @SerializedName("colored")
        private String colored;

        @SerializedName("gif")
        private String gif;

        public String getId() {
            return id;
        }

        public String getType() {
            return type;
        }

        public String getPdf() {
            return pdf;
        }

        public String getRegion() {
            return region;
        }

        public String getRegion_old() {
            return region_old;
        }

        public String getPng() {
            return png;
        }

        public String getSizeType() {
            return sizeType;
        }

        public String getColor() {
            return color;
        }

        public String getPlan() {
            return plan;
        }

        public String getCenter() {
            return center;
        }

        public String getThumbnail() {
            return thumbnail;
        }

        public String getColored() {
            return colored;
        }

        public String getGif() {
            return gif;
        }

        public int getPbnType() {

            if (TextUtil.isTextEmpty(sizeType)){
                return PicType.PBN_TYPE_NORMAL;
            }

            if (type.equals(ImgCouldListResp.TYPE_DEF)){
                if (sizeType.equals(ImgCouldListResp.SIZE_TYPE_NORMAL)) {
                    return PicType.PBN_TYPE_NORMAL;
                }
                return PicType.PBN_TYPE_WALL_NORMAL;
            }

            if (type.equals(ImgCouldListResp.TYPE_COLORED)){
                if (sizeType.equals(ImgCouldListResp.SIZE_TYPE_NORMAL)) {
                    return PicType.PBN_TYPE_COLORED;
                }
                return PicType.PBN_TYPE_WALL_COLORED;
            }
            return PicType.PBN_TYPE_NORMAL;
        }

        //利用现有URL，免重复上传到CMS
        public String getRegionForCmsUpload(){
            return getURLForUpload(region);
        }

        public String getPngForCmsUpload(){
            return getURLForUpload(png);
        }

        public String getPdfForCmsUpload(){
            return getURLForUpload(pdf);
        }

        public String getColoredForCmsUpload() {
            return getURLForUpload(colored);
        }

        String getURLForUpload(String url){
            if (TextUtil.isTextEmpty(url)) return "";

            if (url.startsWith("http://lens.idailybread.com") ||
                    url.startsWith("https://lens.idailybread.com")){
                return "";
            }
            return url;
        }
    }

}
