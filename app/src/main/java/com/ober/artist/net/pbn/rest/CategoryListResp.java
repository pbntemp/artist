package com.ober.artist.net.pbn.rest;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ober on 2019-10-31.
 */
public class CategoryListResp extends BaseResp<CategoryListResp.Data> {

    @Override
    public boolean isOk() {
        return super.isOk() && getData() != null && getData().getCates() != null;
    }

    public static class Data {
        @SerializedName("categoryList")
        private Cate[] cates;

        public Cate[] getCates() {
            return cates;
        }
    }

    public static class Cate {
        @SerializedName("id")
        private String id;

        @SerializedName("name")
        private String name;

        public String getId() {
            return id;
        }

        public String getName() {
            return name;
        }
    }

}
