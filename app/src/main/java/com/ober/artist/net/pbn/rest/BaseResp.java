package com.ober.artist.net.pbn.rest;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ober on 18-9-11.
 */
public abstract class BaseResp<T> {

    @SerializedName("status")
    private Status status;

    @SerializedName("data")
    private T data;

    public Status getStatus() {
        return status;
    }

    public T getData() {
        return data;
    }

    public boolean isOk() {
        return status != null && status.getCode() == 0;
    }
}
