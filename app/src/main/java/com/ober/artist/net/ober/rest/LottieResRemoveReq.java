package com.ober.artist.net.ober.rest;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ober on 19-9-6.
 */
public class LottieResRemoveReq {

    @SerializedName("id")
    public long id;

}
