package com.ober.artist.net.ober.rest;

import com.google.gson.annotations.SerializedName;
import com.ober.artist.net.bean.LottieResBean;

import java.util.List;

/**
 * Created by ober on 19-9-3.
 */
public class LottieResResp {

    @SerializedName("lotties")
    public List<LottieResBean> data;

    public List<LottieResBean> getData() {
        return data;
    }
}
