package com.ober.artist.net.pbn;

import android.content.Context;
import android.os.Build;


import com.ober.artist.BuildConfig;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import okhttp3.internal.Version;

/**
 *
 * 所有请求都带这个header
 *
 * Created by ober on 2018/6/12.
 */
final class MeeviiCommonHttpHeaderFactory {

    private static final String PARAMS_KEY_USER_AGENT = "User-Agent";
    private static final String PARAMS_KEY_APP = "app";
    private static final String PARAMS_KEY_VERSION_NAME = "version";
    private static final String PARAMS_KEY_COUNTRY = "country";
    private static final String PARAMS_KEY_LANGUAGE = "language";
    private static final String PARAMS_KEY_VERSION_NUM = "versionNum";
    private static final String PARAMS_KEY_API_VERSION = "apiVersion";
    private static final String PARAMS_KEY_PLATFORM = "platform";
    private static final String PARAMS_KEY_TODAY = "today";
    private static final String PARAMS_KEY_TIMEZONE = "timezone";
    private static final String API_VERSION = "2";

    static Map<String, String> create() {

        final String mPackageName = BuildConfig.APPLICATION_ID;

        final String mVersionName = BuildConfig.VERSION_NAME;
        final int mVersionNumber = BuildConfig.VERSION_CODE;
        final String mToday = getTodayTimeStamp();
        final String mCountry = Locale.getDefault().getCountry();
        final String mLanguage = "en";
        final String mTimezone = TimeZone.getDefault().getID();

        Map<String, String> headers = new HashMap<>();
        headers.put(PARAMS_KEY_USER_AGENT, getUaTitle() + " " + mPackageName + "/" + mVersionName);
        headers.put(PARAMS_KEY_TODAY, mToday);
        headers.put(PARAMS_KEY_APP, mPackageName);
        headers.put(PARAMS_KEY_VERSION_NAME, mVersionName);
        headers.put(PARAMS_KEY_VERSION_NUM, String.valueOf(mVersionNumber));
        headers.put(PARAMS_KEY_PLATFORM, "android");
        headers.put(PARAMS_KEY_COUNTRY, mCountry);
        headers.put(PARAMS_KEY_LANGUAGE, mLanguage);
        headers.put(PARAMS_KEY_API_VERSION, API_VERSION);
        headers.put(PARAMS_KEY_TIMEZONE, mTimezone);

        return headers;
    }

    private static String getTodayTimeStamp() {
        SimpleDateFormat ft = new SimpleDateFormat("yyyyMMdd", Locale.US);
        Date date = new Date(System.currentTimeMillis());
        return ft.format(date);
    }


    private static String getUaTitle() {
        return "android/" + Build.VERSION.SDK_INT;
    }
}
