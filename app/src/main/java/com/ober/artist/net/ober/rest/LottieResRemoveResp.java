package com.ober.artist.net.ober.rest;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ober on 19-9-6.
 */
public class LottieResRemoveResp {

    @SerializedName("code")
    public int code;

    @SerializedName("msg")
    public String msg;
}
