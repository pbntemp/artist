package com.ober.artist.net.bean;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ober on 19-1-10.
 */
public class Package {
    @SerializedName("name")
    private String name;

    @SerializedName("id")
    private long id;

    @SerializedName("who")
    private String who;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getWho() {
        return who;
    }

    public void setWho(String who) {
        this.who = who;
    }
}
