package com.ober.artist.net;

/**
 * Created by ober on 2020-02-12.
 */
public interface ApiCallback3<T> {

    void onResp(T t, String errmsg);
}
