package com.ober.artist.net.ober;


import android.os.Handler;
import android.os.Looper;

import com.google.gson.Gson;
import com.ober.artist.net.ApiBase;
import com.ober.artist.net.ApiCallback;
import com.ober.artist.net.ober.rest.LottieResRemoveReq;
import com.ober.artist.net.ober.rest.LottieResRemoveResp;
import com.ober.artist.net.ober.rest.LottieResResp;
import com.ober.artist.net.ober.rest.PackageReq;
import com.ober.artist.net.ober.rest.PackageResp;
import com.ober.artist.net.ober.rest.PictureReq;
import com.ober.artist.net.ober.rest.PictureResp;
import com.ober.artist.utils.GsonUtil;
import com.socks.library.KLog;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by ober on 19-1-10.
 */
public class OberApi extends ApiBase {

    public static final String HOST_HTTP_URL = "http://192.168.20.214:8080";

    private static final String BASE_URL = "http://192.168.20.214:8080/pbngen2";

    public static Call removeLottie(long id, ApiCallback<LottieResRemoveResp> callback) {
        String url = BASE_URL + "/lottie/remove";

        LottieResRemoveReq req = new LottieResRemoveReq();
        req.id = id;

        String json = GsonUtil.toJson(req);
        RequestBody body = RequestBody.create(MediaType.parse("application/json"), json);

        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();

        Call call = okHttpClient.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
                new Handler(Looper.getMainLooper()).post(() -> callback.onResp(null));
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                LottieResRemoveResp resp = null;
                try {
                    String s = response.body().string();
                    KLog.i(s);
                    resp = new Gson().fromJson(s, LottieResRemoveResp.class);
                } catch (Exception e) {
                    e.printStackTrace();
                    new Handler(Looper.getMainLooper()).post(() -> callback.onResp(null));
                }
                response.close();
                final LottieResRemoveResp r = resp;
                new Handler(Looper.getMainLooper()).post(() -> callback.onResp(r));
            }
        });

        return call;
    }

    public static Call requestLotties(ApiCallback<LottieResResp> callback) {
        String url = BASE_URL + "/lottie/all";
        Request request = new Request.Builder()
                .url(url)
                .get()
                .build();

        Call call = okHttpClient.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
                new Handler(Looper.getMainLooper()).post(() -> callback.onResp(null));
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                LottieResResp resp = null;
                try {
                    String s = response.body().string();
                    KLog.i(s);
                    resp = new Gson().fromJson(s, LottieResResp.class);
                } catch (Exception e) {
                    e.printStackTrace();
                    new Handler(Looper.getMainLooper()).post(() -> callback.onResp(null));
                }
                response.close();
                final LottieResResp r = resp;
                new Handler(Looper.getMainLooper()).post(() -> callback.onResp(r));
            }
        });

        return call;
    }


    public static Call requestPacks(String who, ApiCallback<PackageResp> callback) {
        String url = BASE_URL + "/pbn/getUserPack";
        PackageReq req = new PackageReq();
        req.who = who;
        RequestBody body = simpleBody(req);
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();
        Call call = okHttpClient.newCall(request);
        call.enqueue(new okhttp3.Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
                new Handler(Looper.getMainLooper()).post(() -> callback.onResp(null));
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                PackageResp resp = null;
                try {
                    String s = response.body().string();
                    KLog.i(s);
                    resp = new Gson().fromJson(s, PackageResp.class);
                } catch (Exception e) {
                    e.printStackTrace();
                    new Handler(Looper.getMainLooper()).post(() -> callback.onResp(null));
                }
                response.close();
                final PackageResp r = resp;
                new Handler(Looper.getMainLooper()).post(() -> callback.onResp(r));
            }
        });
        return call;
    }

    public static Call requestPictures(long packageId, ApiCallback<PictureResp> callback) {
        String url = BASE_URL + "/pbn/getPackPicture";
        PictureReq req = new PictureReq();
        req.packageId = packageId;
        RequestBody body = simpleBody(req);
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();
        Call call = okHttpClient.newCall(request);
        call.enqueue(new okhttp3.Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
                new Handler(Looper.getMainLooper()).post(() -> callback.onResp(null));
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                PictureResp resp = null;
                try {
                    String s = response.body().string();
                    KLog.i(s);
                    resp = new Gson().fromJson(s, PictureResp.class);
                } catch (Exception e) {
                    e.printStackTrace();
                    new Handler(Looper.getMainLooper()).post(() -> callback.onResp(null));
                }
                response.close();
                final PictureResp r = resp;
                new Handler(Looper.getMainLooper()).post(() -> callback.onResp(r));
            }
        });
        return call;
    }

    private static RequestBody simpleBody(Object o) {
        MediaType type = MediaType.parse("application/json");
        RequestBody body = RequestBody.create(type, new Gson().toJson(o));
        return body;
    }
}
