package com.ober.artist.net;

/**
 * Created by ober on 2020-02-04.
 */
public interface ApiCallback2<T> {
    void onResp(T t, String msg, int httpRespCode);
}
