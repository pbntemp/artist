package com.ober.artist.net.pbn.rest;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ober on 2019-10-31.
 */
public class CmsUploadPaintResp extends BaseResp<CmsUploadPaintResp.Data> {

    public String getId() {
        Data data = getData();
        if(data == null) {
            return null;
        }

        Paint paint = data.getPaint();
        if(paint == null) {
            return null;
        }

        return paint.id;
    }

    @Override
    public boolean isOk() {
        return super.isOk() && !TextUtils.isEmpty(getId());
    }

    public static class Data {
        @SerializedName("paint")
        private Paint paint;

        public void setPaint(Paint paint) {
            this.paint = paint;
        }

        public Paint getPaint() {
            return paint;
        }
    }

    public static class Paint {
        @SerializedName("id")
        private String id;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }


}
