package com.ober.artist.net.pbn.rest;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ober on 2019-10-31.
 */
public class CmsUploadPaintReq {

    public static final int BUSINESS_TYPE_VIDEO = 10;

    @SerializedName("png")
    private String png;

    @SerializedName("pdf")
    private String pdf;

    @SerializedName("thumbnail")
    private String thumbnail;

    @SerializedName("region")
    private String region;

    @SerializedName("center")
    private String center;

    @SerializedName("plans")
    private String[] plans;

    @SerializedName("coloredImgurlList")
    private String[] coloredUrlList;

    @SerializedName("encryptedImgList")
    private String[] encryptedColoredUrlList;

    @SerializedName("size")
    private String sizeType;

    @SerializedName("type")
    private String colorType;

    @SerializedName("zip_file")
    private String zip;

    @SerializedName("min_zip_file")
    private String minZip;

    @SerializedName("paintId")
    private String paintId;

    @SerializedName("pngSize")
    private String pngSize;

    @SerializedName("pdfSize")
    private String pdfSize;

    @SerializedName("regionSize")
    private String regionSize;

    @SerializedName("coloredSize")
    private String coloredSize;

    @SerializedName("line")
    private String line = "normal";

    @SerializedName("categoryIdList")
    private String[] categoryIdList;

    @SerializedName("deleted")
    private boolean deleted = true;

    @SerializedName("name")
    private String name;

    @SerializedName("img_type")
    private String imgType = "test";

    @SerializedName("business_type")
    private Integer businessType;

    public String getPng() {
        return png;
    }

    public void setPng(String png) {
        this.png = png;
    }

    public String getPdf() {
        return pdf;
    }

    public void setPdf(String pdf) {
        this.pdf = pdf;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getCenter() {
        return center;
    }

    public void setCenter(String center) {
        this.center = center;
    }

    public String[] getPlans() {
        return plans;
    }

    public void setPlans(String[] plans) {
        this.plans = plans;
    }

    public String[] getColoredUrlList() {
        return coloredUrlList;
    }

    public void setColoredUrlList(String[] coloredUrlList) {
        this.coloredUrlList = coloredUrlList;
    }

    public String[] getEncryptedColoredUrlList() {
        return encryptedColoredUrlList;
    }

    public void setEncryptedColoredUrlList(String[] encryptedColoredUrlList) {
        this.encryptedColoredUrlList = encryptedColoredUrlList;
    }

    public String getSizeType() {
        return sizeType;
    }

    public void setSizeType(String sizeType) {
        this.sizeType = sizeType;
    }

    public String getColorType() {
        return colorType;
    }

    public void setColorType(String colorType) {
        this.colorType = colorType;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getMinZip() {
        return minZip;
    }

    public void setMinZip(String minZip) {
        this.minZip = minZip;
    }

    public void setPaintId(String paintId) {
        this.paintId = paintId;
    }

    public String getPaintId() {
        return paintId;
    }

    public String getPngSize() {
        return pngSize;
    }

    public void setPngSize(String pngSize) {
        this.pngSize = pngSize;
    }

    public String getPdfSize() {
        return pdfSize;
    }

    public void setPdfSize(String pdfSize) {
        this.pdfSize = pdfSize;
    }

    public String getRegionSize() {
        return regionSize;
    }

    public void setRegionSize(String regionSize) {
        this.regionSize = regionSize;
    }

    public String getColoredSize() {
        return coloredSize;
    }

    public void setColoredSize(String coloredSize) {
        this.coloredSize = coloredSize;
    }

    public String getLine() {
        return line;
    }

    public void setLine(String line) {
        this.line = line;
    }

    public String[] getCategoryIdList() {
        return categoryIdList;
    }

    public void setCategoryIdList(String[] categoryIdList) {
        this.categoryIdList = categoryIdList;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImgType() {
        return imgType;
    }

    public void setImgType(String imgType) {
        this.imgType = imgType;
    }

    public Integer getBusinessType() {
        return businessType;
    }

    public void setBusinessType(Integer businessType) {
        this.businessType = businessType;
    }
}

