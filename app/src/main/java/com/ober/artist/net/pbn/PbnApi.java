package com.ober.artist.net.pbn;

import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;


import com.google.gson.Gson;
import com.ober.artist.BuildConfig;
import com.ober.artist.net.ApiBase;
import com.ober.artist.net.ApiCallback;
import com.ober.artist.net.ApiCallback2;
import com.ober.artist.net.ApiCallback3;
import com.ober.artist.net.pbn.rest.BaseResp;
import com.ober.artist.net.pbn.rest.CategoryListResp;
import com.ober.artist.net.pbn.rest.CdnUploadResp;
import com.ober.artist.net.pbn.rest.CmsUploadPaintReq;
import com.ober.artist.net.pbn.rest.CmsUploadPaintResp;
import com.ober.artist.net.pbn.rest.GenZipReq;
import com.ober.artist.net.pbn.rest.ImgCouldListResp;
import com.ober.artist.net.pbn.rest.ImgDetailResp;
import com.ober.artist.net.pbn.rest.SimpleResp;
import com.ober.artist.net.pbn.rest.UacDDAuthResp;
import com.ober.artist.net.pbn.rest.UacLoginResp;
import com.ober.artist.net.pbn.rest.GenZipResp;
import com.ober.artist.net.pbn.rest.VincentUploadPaintReq;
import com.ober.artist.net.pbn.rest.VincentUploadPaintResp;
import com.ober.artist.utils.GsonUtil;
import com.ober.artist.xlog.XLogger;
import com.socks.library.KLog;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by ober on 19-2-25.
 */
public class PbnApi extends ApiBase {
    private static final String BASE_URL = "http://paint-api.dailyinnovation.biz";
    private static final String BASE_URL_TEST = "http://paint-api-test.dailyinnovation.biz";

    private static final String BASE_URL_UAC = "https://uac.dailyinnovation.biz";

    //private static final String BASE_URL_CMS = "http://paint-cms.lexinshengwen.com";
    private static final String BASE_URL_CMS_CN = "https://paint-cms.lexinshengwen.com";
    private static final String BASE_URL_CMS_GP = "https://paint-cms.dailyinnovation.biz";

    private static final String BASE_URL_CMS_FAR = "https://paint-cms.dailyinnovation.biz";

    private static final String URL_UPLOAD_CDN = "https://cw-lens.dailyinnovation.biz/upload";

    private static final String URL_CREATION_PAINT = "https://vincent.lexinshengwen.com/vincent/v1";
    private static final String URL_CREATION_PAINT_TEST = "https://vincent-test.lexinshengwen.com/vincent/v1";

    private static String BASE_URL_CMS = BASE_URL_CMS_CN;

    public static Call requestCmsLogin(String uacToken, ApiCallback<SimpleResp> callback) {
        KLog.i("requestCmsLogin " + uacToken);

        Request request = cmsLoginRequest(uacToken);
        return requestLogin(request, callback);
    }

    public static Call requestVincentLogin(boolean debug, String uacToken, ApiCallback<SimpleResp> callback) {
        KLog.i("requestVincentLogin " + uacToken);

        Request request = vincentLoginRequest(uacToken, debug);
        return requestLogin(request, callback);
    }

    private static Call requestLogin(Request request, ApiCallback<SimpleResp> callback){
        Call call = okHttpClient.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
                mHandler.post(() -> callback.onResp(null));
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if(!response.isSuccessful()) {
                    KLog.i("RESP CODE " + response.code());
                    response.close();
                    mHandler.post(() -> callback.onResp(null));
                    return;
                }

                SimpleResp resp = null;
                try {
                    String s = response.body().string();
                    KLog.i(s);
                    resp = GsonUtil.fromJson(s, SimpleResp.class);
                } catch (Exception e) {
                    e.printStackTrace();
                    mHandler.post(() -> callback.onResp(null));
                }
                response.close();
                final SimpleResp r = resp;
                mHandler.post(() -> callback.onResp(r));
            }
        });

        return call;
    }

    public static Call requestUacLogin(ApiCallback<UacLoginResp> callback) {
        KLog.i("requestUacLogin");

        Request request = uacLoginRequest();

        Call call = okHttpClient.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
                mHandler.post(() -> callback.onResp(null));
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if(!response.isSuccessful()) {
                    KLog.i("RESP CODE " + response.code());
                    response.close();
                    mHandler.post(() -> callback.onResp(null));
                    return;
                }

                UacLoginResp resp = null;
                try {
                    String s = response.body().string();
                    KLog.i(s);
                    resp = GsonUtil.fromJson(s, UacLoginResp.class);
                } catch (Exception e) {
                    e.printStackTrace();
                    mHandler.post(() -> callback.onResp(null));
                }
                response.close();
                final UacLoginResp r = resp;
                mHandler.post(() -> callback.onResp(r));
            }
        });

        return call;
    }

    public static Call requestUacDDAuth(String ddCode, ApiCallback<UacDDAuthResp> callback) {
        KLog.i("requestUacDDAuth " + ddCode);

        Request request = uacDDAuthRequest(ddCode);

        Call call = okHttpClient.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
                mHandler.post(() -> callback.onResp(null));
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if(!response.isSuccessful()) {
                    KLog.i("RESP CODE " + response.code());
                    response.close();
                    mHandler.post(() -> callback.onResp(null));
                    return;
                }

                UacDDAuthResp resp = null;
                try {
                    String s = response.body().string();
                    KLog.i(s);
                    resp = GsonUtil.fromJson(s, UacDDAuthResp.class);
                } catch (Exception e) {
                    e.printStackTrace();
                    mHandler.post(() -> callback.onResp(null));
                }
                response.close();
                final UacDDAuthResp r = resp;
                mHandler.post(() -> callback.onResp(r));
            }
        });

        return call;
    }


    public static Call requestImgDetail(String imgId, ApiCallback<ImgDetailResp> callback, boolean testServer) {

        KLog.i("requestImgDetail " + imgId);

        Request request = imgDetailRequest(imgId, testServer);

        Call call = okHttpClient.newCall(request);
        call.enqueue(new okhttp3.Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
                new Handler(Looper.getMainLooper()).post(() -> callback.onResp(null));
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                if(!response.isSuccessful()) {
                    KLog.i("RESP CODE " + response.code());
                    response.close();
                    new Handler(Looper.getMainLooper()).post(() -> callback.onResp(null));
                    return;
                }

                ImgDetailResp resp = null;
                try {
                    String s = response.body().string();
                    KLog.i(s);
                    resp = new Gson().fromJson(s, ImgDetailResp.class);
                } catch (Exception e) {
                    e.printStackTrace();
                    new Handler(Looper.getMainLooper()).post(() -> callback.onResp(null));
                }
                response.close();
                final ImgDetailResp r = resp;
                new Handler(Looper.getMainLooper()).post(() -> callback.onResp(r));
            }
        });
        return call;
    }

    public static Call uploadPaintToCms(CmsUploadPaintReq req, ApiCallback3<CmsUploadPaintResp> callback) {

        if(BuildConfig.DEBUG) {
            KLog.i("uploadPaintToCms", GsonUtil.toJson(req));
        }

        final String url = BASE_URL_CMS + "/paint/v1/cms/operationPaint";

        RequestBody body = RequestBody.create(MediaType.parse("application/json"), GsonUtil.toJson(req));

        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();

        Call call = okHttpClient.newCall(request);
        call.enqueue(new okhttp3.Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
                new Handler(Looper.getMainLooper()).post(() -> callback.onResp(null, getOnFailureDesc(e)));
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                if(!response.isSuccessful()) {
                    KLog.i("RESP CODE " + response.code());
                    response.close();
                    new Handler(Looper.getMainLooper()).post(() -> callback.onResp(null, "http" + response.code()));
                    return;
                }

                CmsUploadPaintResp resp = null;
                try {
                    String s = response.body().string();
                    KLog.i(s);
                    resp = new Gson().fromJson(s, CmsUploadPaintResp.class);
                } catch (Exception e) {
                    e.printStackTrace();
                    new Handler(Looper.getMainLooper()).post(() -> callback.onResp(null, "Response解析失败"));
                }
                response.close();
                final CmsUploadPaintResp r = resp;
                new Handler(Looper.getMainLooper()).post(() -> callback.onResp(r, "ok"));
            }
        });
        return call;
    }

    public static Call requestGenPbnZip(GenZipReq req, ApiCallback2<GenZipResp> callback) {
        KLog.i("requestGenPbnZip", GsonUtil.toJson(req));

        final String url = BASE_URL_CMS_FAR + "/paint/v1/cms/misc/generateZip";
        final MediaType type = MediaType.parse("application/json");
        if(type == null) {
            throw new RuntimeException("BAD CONTENT-TYPE");
        }

        RequestBody body = RequestBody.create(type, GsonUtil.toJson(req));

        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();

        Call call = okHttpClient.newCall(request);

        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
                mHandler.post(() -> callback.onResp(null, getOnFailureDesc(e), -1));
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                final int code = response.code();

                if(!response.isSuccessful()) {
                    KLog.w("RESP CODE " + code);
                    try {
                        String s = response.body().string();
                        KLog.w(s);
                    } catch (Exception ignore) {}

                    response.close();
                    mHandler.post(() -> callback.onResp(null, "http" + code, code));
                    return;
                }

                GenZipResp resp = null;
                try {
                    String s = response.body().string();
                    KLog.i(s);
                    resp = new Gson().fromJson(s, GenZipResp.class);
                } catch (Exception e) {
                    e.printStackTrace();
                    mHandler.post(() -> callback.onResp(null, "Response解析失败", code));
                }
                response.close();
                final GenZipResp r = resp;
                mHandler.post(() -> callback.onResp(r, "ok", code));
            }
        });

        return call;

    }

    public static Call queryCmsCategoryList(ApiCallback3<CategoryListResp> callback) {
        KLog.i("queryCmsCategory");

        final String url = BASE_URL_CMS + "/paint/v1/cms/category";
        Request request = new Request.Builder()
                .url(url)
                .get()
                .build();

        Call call = okHttpClient.newCall(request);

        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();

                mHandler.post(() -> callback.onResp(null, getOnFailureDesc(e)));
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if(!response.isSuccessful()) {
                    KLog.w("RESP CODE " + response.code());
                    try {
                        String s = response.body().string();
                        KLog.w(s);
                    } catch (Exception ignore) {

                    }

                    response.close();
                    mHandler.post(() -> callback.onResp(null, "请求失败http" + response.code()));
                    return;
                }

                CategoryListResp resp = null;
                try {
                    String s = response.body().string();
                    KLog.i(s);
                    resp = new Gson().fromJson(s, CategoryListResp.class);
                } catch (Exception e) {
                    e.printStackTrace();
                    mHandler.post(() -> callback.onResp(null, "数据解析失败"));
                }
                response.close();
                final CategoryListResp r = resp;
                mHandler.post(() -> callback.onResp(r, "ok"));
            }
        });

        return call;
    }

    public static Call encryptImgToCdn(File localFile, ApiCallback3<CdnUploadResp> callback) {
        KLog.i("encryptImgToCdn " + localFile);

        final String url = BASE_URL_CMS_FAR + "/paint/v1/cms/paint/encryptImg";
        final MediaType type;
        final String name;
        if(localFile.toString().endsWith("jpg")) {
            type = MediaType.parse("image/jpg");
            name = "name.jpg";
        } else {
            type = MediaType.parse("image/*");
            name = "name";
        }
        if(type == null) {
            throw new RuntimeException("BAD CONTENT-TYPE");
        }
        if(!localFile.exists()) {
            throw new RuntimeException("FILE NOT FOUND");
        }
        RequestBody fileBody = RequestBody.create(type, localFile);

        RequestBody body = new MultipartBody.Builder()
                .addFormDataPart("file", name, fileBody)
                .setType(MultipartBody.FORM)
                .build();

        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();

        Call call = okHttpClient.newCall(request);

        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
                mHandler.post(() -> callback.onResp(null, getOnFailureDesc(e)));
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if(!response.isSuccessful()) {
                    XLogger.w("PbnApi " + url + " failed code=" + response.code());
                    KLog.w("RESP CODE " + response.code());
                    try {
                        String s = response.body().string();
                        KLog.w(s);
                    } catch (Exception ignore) {}

                    response.close();
                    mHandler.post(() -> callback.onResp(null, "http" + response.code()));
                    return;
                }

                CdnUploadResp resp = null;
                String s = null;
                try {
                    s = response.body().string();
                    KLog.i(s);
                    resp = new Gson().fromJson(s, CdnUploadResp.class);
                } catch (Exception e) {
                    e.printStackTrace();
                    XLogger.e("PbnApi " + s);
                    mHandler.post(() -> callback.onResp(null, "Response解析出错"));
                }
                response.close();
                final CdnUploadResp r = resp;
                mHandler.post(() -> callback.onResp(r, "ok"));
            }
        });

        return call;
    }

    public static Call uploadImageFileToCdn(File localFile, String uploadPath, ApiCallback3<CdnUploadResp> callback) {
        KLog.i("uploadImageFileToCdn", localFile, uploadPath);

        if(!localFile.exists()) {
            throw new RuntimeException("FILE NOT FOUND");
        }

        String fileName = localFile.getName();
        boolean isPng = fileName.endsWith("png");
        boolean isJpg = fileName.endsWith("jpg") || fileName.endsWith("jpeg");
        boolean isPdf = fileName.endsWith("pdf");

        final String url = URL_UPLOAD_CDN;
        MediaType type = null;
        String name;
        if(isJpg) {
            type = MediaType.parse("image/jpg");
            name = "name.jpg";
        } else if(isPng) {
            type = MediaType.parse("image/png");
            name = "name.png";
        } else if(isPdf) {
            type = MediaType.parse("image/pdf");
            name = "name.pdf";
        } else {
            throw new RuntimeException("unknown " + fileName);
        }

        if(type == null) {
            throw new RuntimeException("BAD CONTENT-TYPE");
        }

        RequestBody fileBody = RequestBody.create(type, localFile);

        RequestBody body = new MultipartBody.Builder()
                .addFormDataPart("path", uploadPath)
                .addFormDataPart("file", name, fileBody)
                .setType(MultipartBody.FORM)
                .build();

        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();

        Call call = okHttpClient.newCall(request);

        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
                mHandler.post(() -> callback.onResp(null, getOnFailureDesc(e)));
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if(!response.isSuccessful()) {
                    KLog.w("RESP CODE " + response.code());
                    try {
                        String s = response.body().string();
                        KLog.w(s);
                    } catch (Exception ignore) {}

                    response.close();
                    mHandler.post(() -> callback.onResp(null, "http" + response.code()));
                    return;
                }

                CdnUploadResp resp = null;
                try {
                    String s = response.body().string();
                    KLog.i(s);
                    resp = new Gson().fromJson(s, CdnUploadResp.class);
                } catch (Exception e) {
                    e.printStackTrace();
                    mHandler.post(() -> callback.onResp(null, "Response解析出错"));
                }
                response.close();
                final CdnUploadResp r = resp;
                mHandler.post(() -> callback.onResp(r, "ok"));
            }
        });

        return call;
    }

    public static Call queryCloudImageList(int offset, int limit, boolean debug, ApiCallback<ImgCouldListResp> callback) {
        KLog.i("queryCloudImageList");

        final String url = (debug ? URL_CREATION_PAINT_TEST : URL_CREATION_PAINT) + "/paint/my?offset=" + offset + "&limit=" +limit;
        Request request = new Request.Builder()
                .url(url)
                .get()
                .build();

        Call call = okHttpClient.newCall(request);

        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();

                mHandler.post(() -> callback.onResp(null));
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if(!response.isSuccessful()) {
                    KLog.w("RESP CODE " + response.code());
                    try {
                        String s = response.body().string();
                        KLog.w(s);
                    } catch (Exception ignore) {

                    }

                    response.close();
                    mHandler.post(() -> callback.onResp(null));
                    return;
                }

                ImgCouldListResp resp = null;
                try {
                    String s = response.body().string();
                    KLog.i(s);
                    resp = new Gson().fromJson(s, ImgCouldListResp.class);
                } catch (Exception e) {
                    e.printStackTrace();
                    mHandler.post(() -> callback.onResp(null));
                }
                response.close();
                final ImgCouldListResp r = resp;
                mHandler.post(() -> callback.onResp(r));
            }
        });

        return call;
    }

    public static Call uploadPaintToVincent(boolean debug, VincentUploadPaintReq req, ApiCallback3<VincentUploadPaintResp> callback) {

        if(BuildConfig.DEBUG) {
            KLog.i("uploadPaintToVincent", GsonUtil.toJson(req));
        }

        final String url = (debug ? URL_CREATION_PAINT_TEST : URL_CREATION_PAINT) + "/paint/" + req.getId();

        RequestBody body = RequestBody.create(MediaType.parse("application/json"), GsonUtil.toJson(req));

        Request request = new Request.Builder()
                .url(url)
                .put(body)
                .build();

        Call call = okHttpClient.newCall(request);
        call.enqueue(new okhttp3.Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
                new Handler(Looper.getMainLooper()).post(() -> callback.onResp(null, getOnFailureDesc(e)));
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                if(!response.isSuccessful()) {
                    KLog.i("RESP CODE " + response.code());
                    response.close();
                    new Handler(Looper.getMainLooper()).post(() -> callback.onResp(null, "http" + response.code()));
                    return;
                }

                VincentUploadPaintResp resp = null;
                try {
                    String s = response.body().string();
                    KLog.i(s);
                    resp = new Gson().fromJson(s, VincentUploadPaintResp.class);
                } catch (Exception e) {
                    e.printStackTrace();
                    new Handler(Looper.getMainLooper()).post(() -> callback.onResp(null, "Response解析失败"));
                }
                response.close();
                final VincentUploadPaintResp r = resp;
                new Handler(Looper.getMainLooper()).post(() -> callback.onResp(r, "ok"));
            }
        });
        return call;
    }

    private static Request cmsLoginRequest(String uacToken) {

        String url = BASE_URL_CMS + "/paint/v1/cms/uac/login";

        JSONObject jObj = new JSONObject();
        try {
            jObj.put("uacLoginToken", uacToken);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        RequestBody body = RequestBody.create(MediaType.parse("application/json"),
                jObj.toString());

        return new Request.Builder()
                .url(url)
                .post(body)
                .build();
    }


    ///登陆到创作平台
    private static Request vincentLoginRequest(String uacToken, boolean debug) {

        String url = (debug ? URL_CREATION_PAINT_TEST : URL_CREATION_PAINT) + "/user/uac/login";

        JSONObject jObj = new JSONObject();
        try {
            jObj.put("uacLoginToken", uacToken);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        RequestBody body = RequestBody.create(MediaType.parse("application/json"),
                jObj.toString());

        return new Request.Builder()
                .url(url)
                .post(body)
                .build();
    }

    private static Request uacDDAuthRequest(String ddCode) {
        String url = BASE_URL_UAC + "/v1/user/login?app=pbn&tmp_auth_code=" + ddCode;

        return new Request.Builder()
                .get().url(url)
                .build();
    }

    private static Request uacLoginRequest() {
        String url = BASE_URL_UAC + "/v1/sso/req?next=http%3A%2F%2Fpaint-cms.lexinshengwen.com%2F%23%2Fpaint%2Flist";

        return new Request.Builder()
                .get().url(url)
                .build();
    }

    private static Request imgDetailRequest(String imgId, boolean testServer) {
        String url = (testServer ? BASE_URL_TEST : BASE_URL) + "/paint/v1/paint/" + imgId;

        return setupHeader(
                new Request.Builder()
                        .get()
                        .url(url))
                .build();
    }

    private static Request.Builder setupHeader(Request.Builder reqBuilder) {
        Map<String, String> headerMap = MeeviiCommonHttpHeaderFactory.create();

        for(Map.Entry<String, String> entry : headerMap.entrySet()) {
            reqBuilder.header(entry.getKey(), entry.getValue());
        }

        return reqBuilder;
    }


    private static String getOnFailureDesc(IOException e) {

        String msg = e.getMessage();
        if(TextUtils.isEmpty(msg)) {
            return "Unknown";
        }

        return msg;
    }

    public static void setCmsApi(boolean isGp){
        BASE_URL_CMS = isGp ? BASE_URL_CMS_GP : BASE_URL_CMS_CN;
    }

}
