package com.ober.artist.net.pbn.rest;

import com.google.gson.annotations.SerializedName;

import java.util.Objects;

/**
 * Created by ober on 2019-10-31.
 */
public class GenZipReq {

    @SerializedName("png")
    private String pngUrl;

    @SerializedName("pdf")
    private String pdfUrl;

    @SerializedName("region")
    private String regionUrl;

    @SerializedName("coloredImgurlList")
    private String[] coloredUrlList;

    @SerializedName("type")
    private String type;

    @SerializedName("center")
    private String center;

    @SerializedName("plans")
    private String[] plans;

    public GenZipReq() {

    }

    public static GenZipReq createNormal(String png, String pdf, String region, String thumb) {
        GenZipReq req = new GenZipReq();
        req.pngUrl = Objects.requireNonNull(png);
        req.pdfUrl = Objects.requireNonNull(pdf);
        req.regionUrl = Objects.requireNonNull(region);
        req.coloredUrlList = new String[] {Objects.requireNonNull(thumb)};
        req.type = "normal";
        return req;
    }

    public static GenZipReq createColored(String png, String pdf, String region, String colored) {
        GenZipReq req = new GenZipReq();
        req.pngUrl = Objects.requireNonNull(png);
        req.pdfUrl = Objects.requireNonNull(pdf);
        req.regionUrl = Objects.requireNonNull(region);
        req.coloredUrlList = new String[] {Objects.requireNonNull(colored)};
        req.type = "colored";
        return req;
    }

    public String getPngUrl() {
        return pngUrl;
    }

    public void setPngUrl(String pngUrl) {
        this.pngUrl = pngUrl;
    }

    public String getPdfUrl() {
        return pdfUrl;
    }

    public void setPdfUrl(String pdfUrl) {
        this.pdfUrl = pdfUrl;
    }

    public String getRegionUrl() {
        return regionUrl;
    }

    public void setRegionUrl(String regionUrl) {
        this.regionUrl = regionUrl;
    }

    public String[] getColoredUrlList() {
        return coloredUrlList;
    }

    public void setColoredUrlList(String[] coloredUrlList) {
        this.coloredUrlList = coloredUrlList;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCenter() {
        return center;
    }

    public void setCenter(String center) {
        this.center = center;
    }

    public String[] getPlans() {
        return plans;
    }

    public void setPlans(String[] plans) {
        this.plans = plans;
    }
}
