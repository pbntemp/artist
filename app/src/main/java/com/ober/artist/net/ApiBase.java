package com.ober.artist.net;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;

import androidx.annotation.NonNull;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.Cookie;
import okhttp3.CookieJar;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;

/**
 * Created by ober on 19-2-25.
 */
public class ApiBase {
    public static OkHttpClient okHttpClient;
    private static PersistentCookieStore cookieStore;
    public static Handler mHandler;

    public static void init(Context app) {
        mHandler = new Handler(Looper.getMainLooper());
        okHttpClient = new OkHttpClient
                .Builder()
                .connectTimeout(60000, TimeUnit.MILLISECONDS)
                .readTimeout(60000, TimeUnit.MILLISECONDS)
                .cookieJar(makeCookieJar(app))
                .retryOnConnectionFailure(true).build();
    }

    private static boolean checkUrlShouldReceiveCookie(String url) {
        return true;
    }

    private static boolean checkUrlShouldUseCookie(HttpUrl url) {
        return true;
    }

    private static CookieJar makeCookieJar(Context app) {

        if(cookieStore == null) {
            cookieStore = new PersistentCookieStore(app);
        }

        return new CookieJar() {
            @Override
            public void saveFromResponse(@NonNull HttpUrl url, @NonNull List<Cookie> cookies) {

                if(cookies.size() == 0) {
                    return;
                }

                if(checkUrlShouldReceiveCookie(url.toString())) {
                    for(Cookie cookie : cookies) {
                        cookieStore.add(url, cookie);
                    }
                }
            }

            @Override
            public List<Cookie> loadForRequest(@NonNull HttpUrl url) {

                if(checkUrlShouldUseCookie(url)) {
                    return cookieStore.get(url);
                }

                return Collections.emptyList();
            }
        };
    }

}
