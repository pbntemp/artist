package com.ober.artist.net.pbn.rest;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ober on 2019-10-28.
 */
public class UacDDAuthResp extends BaseResp<UacDDAuthResp.Data> {


    public static class Data {
        @SerializedName("userid")
        private String userid;

        @SerializedName("name")
        private String name;

        public String getName() {
            return name;
        }

        public String getUserid() {
            return userid;
        }

        public void setName(String name) {
            this.name = name;
        }

        public void setUserid(String userid) {
            this.userid = userid;
        }
    }

}
