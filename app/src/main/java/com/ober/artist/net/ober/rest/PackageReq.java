package com.ober.artist.net.ober.rest;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ober on 19-1-10.
 */
public class PackageReq {

    @SerializedName("who")
    public String who;
}
