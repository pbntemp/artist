package com.ober.artist.net.pbn.rest;

import android.text.TextUtils;

/**
 * Created by ober on 2019-10-28.
 */
public class UacLoginResp extends BaseResp<String> {

    @Override
    public boolean isOk() {
        return super.isOk() && !TextUtils.isEmpty(getData());
    }
}
