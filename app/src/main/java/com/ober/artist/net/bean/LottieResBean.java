package com.ober.artist.net.bean;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ober on 19-9-3.
 */
public class LottieResBean {

    @SerializedName("id")
    private long id;

    @SerializedName("path")
    private String path;

    @SerializedName("name")
    private String name;

    @SerializedName("imgId")
    private String imgId;

    @SerializedName("createTime")
    private long createTime;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImgId() {
        return imgId;
    }

    public void setImgId(String imgId) {
        this.imgId = imgId;
    }

    public long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(long createTime) {
        this.createTime = createTime;
    }
}
