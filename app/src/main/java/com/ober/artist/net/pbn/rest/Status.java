package com.ober.artist.net.pbn.rest;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ober on 18-9-11.
 */
public class Status {
    @SerializedName("message")
    private String message;

    @SerializedName("code")
    private int code;

    public String getMessage() {
        return message;
    }

    public int getCode() {
        return code;
    }
}
