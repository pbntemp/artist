package com.ober.artist.net.ober.rest;

import com.google.gson.annotations.SerializedName;
import com.ober.artist.net.bean.Package;

import java.util.List;

/**
 * Created by ober on 19-1-10.
 */
public class PackageResp {

    @SerializedName("packages")
    public List<Package> packages;
}
