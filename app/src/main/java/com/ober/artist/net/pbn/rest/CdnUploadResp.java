package com.ober.artist.net.pbn.rest;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ober on 2019-10-30.
 */
public class CdnUploadResp extends BaseResp<CdnUploadResp.Data> {


    @Override
    public boolean isOk() {
        return super.isOk() && getData() != null && !TextUtils.isEmpty(getData().getUrl());
    }

    public static class Data {
        @SerializedName("url")
        private String url;

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }
    }
}
