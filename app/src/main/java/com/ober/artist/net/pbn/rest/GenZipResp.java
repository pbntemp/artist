package com.ober.artist.net.pbn.rest;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ober on 2019-10-31.
 */
public class GenZipResp extends BaseResp<GenZipResp.Data> {

    public static final int STATUS_ZIP_SIZE_WARNING = 1003;

    static class Data {
        @SerializedName("zip_file")
        private String zipFile;

        @SerializedName("min_zip_file")
        private String minZipFile;

        @SerializedName("paintId")
        private String paintId;
    }

    @Override
    public boolean isOk() {
        return getStatus() != null && (getStatus().getCode() == 0 || getStatus().getCode() == STATUS_ZIP_SIZE_WARNING);
    }

    public String getMinZipFile() {
        return getData().minZipFile;
    }

    public String getZipFile() {
        return getData().zipFile;
    }

    public String getPaintId() {
        return getData().paintId;
    }
}
