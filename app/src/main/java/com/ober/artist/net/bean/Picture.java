package com.ober.artist.net.bean;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ober on 19-1-10.
 */
public class Picture {

    public static final int PBN_TYPE_NORMAL = 1;
    public static final int PBN_TYPE_COLORED = 2;
    public static final int PBN_TYPE_WALL_NORMAL = 3;
    public static final int PBN_TYPE_WALL_COLORED = 4;

    @SerializedName("name")
    private String name;

    @SerializedName("id")
    private long id;

    @SerializedName("png")
    private String png;

    @SerializedName("region")
    private String region;

    @SerializedName("pdf")
    private String pdf;

    @SerializedName("pbntype")
    private int pbntype;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPng() {
        return png;
    }

    public void setPng(String png) {
        this.png = png;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getPdf() {
        return pdf;
    }

    public void setPdf(String pdf) {
        this.pdf = pdf;
    }

    public int getPbntype() {
        return pbntype;
    }

    public void setPbntype(int pbntype) {
        this.pbntype = pbntype;
    }

    public static String getStrType(int type) {
        if (type == Picture.PBN_TYPE_NORMAL) {
            return "普通";
        } else if (type == Picture.PBN_TYPE_COLORED) {
            return "彩绘";
        } else if (type == Picture.PBN_TYPE_WALL_NORMAL) {
            return "普通壁纸";
        } else if (type == Picture.PBN_TYPE_WALL_COLORED) {
            return "彩绘壁纸";
        } else {
            return "Unknown";
        }
    }
}
