package com.ober.artist.net.bean;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ober on 18-9-11.
 */

public class PbnImgEntity implements Parcelable {

    public static final String TYPE_DEF = "normal";
    public static final String TYPE_COLORED = "colored";

    public static final String SIZE_TYPE_NORMAL = "normal";
    public static final String SIZE_TYPE_WALLPAPER = "wallpaper";

    @NonNull
    @SerializedName("id")
    private String id;

    @SerializedName("png")
    private String png;

    @SerializedName("pdf")
    private String pdf;

    @SerializedName("region")
    private String region;

    @SerializedName("defaultSort")
    private long publish;

    @SerializedName("plans")
    private String[] plans;

    @SerializedName("center")
    private String center;

    @SerializedName("day")
    private int day;

    @SerializedName("colored_imgurl_list")
    private String[] coloredUrls;

    @SerializedName("type")
    private String type;

    @SerializedName("size")
    private String sizeType;

    @Expose
    private String thumbPng;

    public PbnImgEntity() {}

    protected PbnImgEntity(Parcel in) {
        id = in.readString();
        png = in.readString();
        pdf = in.readString();
        region = in.readString();
        publish = in.readLong();
        plans = in.createStringArray();
        center = in.readString();
        thumbPng = in.readString();
        coloredUrls = in.createStringArray();
        type = in.readString();
        sizeType = in.readString();
    }

    public static final Creator<PbnImgEntity> CREATOR = new Creator<PbnImgEntity>() {
        @Override
        public PbnImgEntity createFromParcel(Parcel in) {
            return new PbnImgEntity(in);
        }

        @Override
        public PbnImgEntity[] newArray(int size) {
            return new PbnImgEntity[size];
        }
    };

    @NonNull
    public String getId() {
        return id;
    }

    public void setId(@NonNull String id) {
        this.id = id;
    }

    public String getPng() {
        return png;
    }

    public void setPng(String png) {
        this.png = png;
    }

    public String getPdf() {
        return pdf;
    }

    public void setPdf(String pdf) {
        this.pdf = pdf;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String[] getPlans() {
        return plans;
    }

    public void setPlans(String[] plans) {
        this.plans = plans;
    }

    public String getCenter() {
        return center;
    }

    public void setCenter(String center) {
        this.center = center;
    }

    public long getPublish() {
        return publish;
    }

    public void setPublish(long publish) {
        this.publish = publish;
    }

    public void setThumbPng(String thumbPng) {
        this.thumbPng = thumbPng;
    }

    public String getThumbPng(int size) {
        if(thumbPng == null) {
            return null;
        }
        return thumbPng.replace("{size}", String.valueOf(size));
    }

    public String getThumbPng() {
        return thumbPng;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public void setColoredUrls(String[] coloredUrls) {
        this.coloredUrls = coloredUrls;
    }

    public String[] getColoredUrls() {
        return coloredUrls;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSizeType() {
        return sizeType;
    }

    public void setSizeType(String sizeType) {
        this.sizeType = sizeType;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(png);
        dest.writeString(pdf);
        dest.writeString(region);
        dest.writeLong(publish);
        dest.writeStringArray(plans);
        dest.writeString(center);
        dest.writeString(thumbPng);
        dest.writeStringArray(coloredUrls);
        dest.writeString(type);
        dest.writeString(sizeType);
    }
}
