package com.ober.artist.net;

/**
 * Created by ober on 19-2-25.
 */
public interface ApiCallback<T> {
    void onResp(T t);
}
