package com.ober.artist.net.pbn.rest;


public class VincentUploadPaintReq {

    private String id;

    private String plan;

    private String colored;

    private String region;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPlan() {
        return plan;
    }

    public void setPlan(String plan) {
        this.plan = plan;
    }

    public String getColored() {
        return colored;
    }

    public void setColored(String colored) {
        this.colored = colored;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }
}
