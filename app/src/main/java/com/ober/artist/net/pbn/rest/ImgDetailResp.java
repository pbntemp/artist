package com.ober.artist.net.pbn.rest;

import com.ober.artist.net.bean.PbnImgEntity;

/**
 * Created by ober on 18-9-12.
 */
public class ImgDetailResp extends BaseResp<PbnImgEntity> {

    public static final int STATUS_NOT_FOUND = 1;
}
