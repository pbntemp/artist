package com.ober.artist.analyze.base;


import android.app.Application;
import android.content.Context;

import com.socks.library.KLog;

final class Analyze {

    private static final String TAG = "Analyze";

    private static Application sContext;
    private static boolean enableAnalyzeEvent = false;
    private static boolean enableLog = false;

    public static void setEnableAnalyzeEvent(boolean _enableAnalyzeEvent) {
        Analyze.enableAnalyzeEvent = _enableAnalyzeEvent;
    }

    public static boolean isEnableAnalyzeEvent() {
        return Analyze.enableAnalyzeEvent;
    }

    public static void setEnableLog(boolean _enableLog) {
        Analyze.enableLog = _enableLog;
    }

    private Analyze() {
    }

    static Context getApplication() {
        return sContext;
    }

    static void init(Application context) {

        sContext = context;
        AnalyzePlatformMngr.init(context);
    }

    public static void trackEventKey(String event) {
        if(!enableAnalyzeEvent) {
            return;
        }
        if(enableLog) KLog.e(TAG, "firebase:" + event);
        AnalyzePlatformMngr.trackEvent(event);
    }

    static void trackEventKey(String event, String key) {
        if(!enableAnalyzeEvent) {
            return;
        }
        if(enableLog) KLog.e(TAG, "firebase:" + event + "\t\t" + key);
        AnalyzePlatformMngr.trackEventKey(event, key);
    }

    static void trackEventKeyValue(String event, String key, String value) {
        if(!enableAnalyzeEvent) {
            return;
        }
        if(enableLog) KLog.e(TAG, "firebase:" + event + "\t\t" + key + "\t\t" + value);
        AnalyzePlatformMngr.trackEventKeyValue(event, key, value);
    }

    static void trackEventKeyValue(String event, String key, long value) {
        if(!enableAnalyzeEvent) {
            return;
        }
        if(enableLog) KLog.e(TAG, "firebase:" + event + "\t\t" + key + "\t\t" + value);
        AnalyzePlatformMngr.trackEventKeyValue(event, key, value);
    }
}
