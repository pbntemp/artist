package com.ober.artist.analyze.base;

import android.app.Application;
import android.text.TextUtils;

import com.ober.artist.BuildConfig;
import com.umeng.analytics.MobclickAgent;
import com.umeng.commonsdk.UMConfigure;

import java.util.HashMap;
import java.util.Map;

class UmengAnalyze {
    public static void init(Application context){
        UMConfigure.setLogEnabled(BuildConfig.DEBUG);
        UMConfigure.init(context, "5e74746e978eea0774044f29", "default", UMConfigure.DEVICE_TYPE_PHONE, "");
        // 选用AUTO页面采集模式
        MobclickAgent.setPageCollectionMode(MobclickAgent.PageMode.AUTO);
    }


    static void trackEventKey(String event, String key) {
        umengNormalEvent(event, key);
    }

    static void trackEventKeyValue(String event, String key, String value) {
        if (TextUtils.isEmpty(value))
            value = key;
        umengNormalEvent(event, key, value);
    }

    static void trackEventKeyValue(String event, String key, int value) {
        umengCountEvent(event, key, value);
    }

    static void trackEventKeyValue(String event, String key, long value) {
        umengCountEvent(event, key, (int)value);
    }

    //////////////
    // 计数事件
    //后台添加事件时选择“多参数类型事件”。
    private static void umengNormalEvent(String event, String value){
        MobclickAgent.onEvent(Analyze.getApplication(), event, value);
    }

    //////////////
    // 计数事件
    //后台添加事件时选择“多参数类型事件”。
    private static void umengNormalEvent(String event, String key, String value){
        Map<String, Object> datas = new HashMap<>();
        datas.put(key, value);
        MobclickAgent.onEventObject(Analyze.getApplication(), event, datas);
    }

    //计算事件 (按用户累加)
    private static void umengCountEvent(String event, String key, int count){
        Map<String, String> datas = new HashMap<>();
        datas.put(key, "" + count);
        MobclickAgent.onEventValue(Analyze.getApplication(), event, datas, count);
    }
}
