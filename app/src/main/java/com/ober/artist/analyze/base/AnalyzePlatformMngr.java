package com.ober.artist.analyze.base;


import android.app.Application;

class AnalyzePlatformMngr {

    public static void init(Application context) {
        UmengAnalyze.init(context);
    }

    static void trackEvent(String event) {
        //not support for umeng?
        UmengAnalyze.trackEventKey(event, "");
    }

    static void trackEventKey(String event, String key) {

        UmengAnalyze.trackEventKey(event, key);
    }

    static void trackEventKeyValue(String event, String key, String value) {
        UmengAnalyze.trackEventKeyValue(event, key, value);
    }

    static void trackEventKeyValue(String event, String key, long value) {
        UmengAnalyze.trackEventKeyValue(event, key, value);
    }

}

