package com.ober.artist.analyze.base;

import android.app.Application;

public class ArtistAnalyze {

    public static class Common {

        public static final String pic_id = "p_%s";
        public static final String local = "local";
        public static final String cloud = "cloud";
        public static final String fail = "fail";
    }

    public static class Picture {
        public static final String event_cost_time = "pic_cost_time";
        public static final String event_cost_box = "pic_cost_box";
        public static final String event_upload_from = "pic_upload_from";
        public static final String event_author = "pic_author";
        public static final String event_pic_upload = "pic_upload";

        public static final String event_mapping_region_server = "mapping_region_server";
        public static final String event_mapping_region_local = "mapping_region_local";
    }

    public static void init(Application context) {
        Analyze.init(context);
        Analyze.setEnableAnalyzeEvent(true);
        Analyze.setEnableLog(true);
    }

    public static void trackEventKey(String event, String key) {
        Analyze.trackEventKey(event, key);
    }

    public static void trackEventKeyValue(String event, String key, String value) {
        Analyze.trackEventKeyValue(event, key, value);
    }

    public static void trackEventKeyValue(String event, String key, long value) {
        Analyze.trackEventKeyValue(event, key, value);
    }
}