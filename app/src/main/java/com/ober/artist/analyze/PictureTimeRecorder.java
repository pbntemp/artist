package com.ober.artist.analyze;


import android.content.Context;
import android.content.SharedPreferences;

import com.socks.library.KLog;

public class PictureTimeRecorder {

    private static final String TAG = "Analyze-Pic";

    private static final String PREF_NAME = "pbn_picture_record";

    private final SharedPreferences mPref;
    private final String id;

    private int mStatus = STATE_NONE;

    private static final int STATE_NONE = 0;
    private static final int STATE_RUNNING = 1;
    private static final int STATE_PAUSE = 2;

    private long mStartTime;
    private long mCost;

    public PictureTimeRecorder(Context c, String id) {
        mPref = c.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        this.id = id;
    }

    //返回true表示new start
    public boolean start() {
        if(mStatus != STATE_NONE) {
            throw new IllegalStateException();
        }
        KLog.i(TAG, "start");
        mStartTime = System.currentTimeMillis();
        mStatus = STATE_RUNNING;
        long costTime = mPref.getLong(id, 0);
        if(costTime == 0) {
            mCost = 0;
            return true;
        } else {
            mCost = costTime;
            return false;
        }
    }

    public void resume() {
        if(mStatus == STATE_PAUSE) {
            KLog.i(TAG, "resume with status=" + mStatus);
            mStartTime = System.currentTimeMillis();
            mStatus = STATE_RUNNING;
        } else {
            KLog.w(TAG, "resume with status=" + mStatus);
        }
    }

    public void pause() {
        if(mStatus == STATE_RUNNING) {
            KLog.i(TAG, "pause with status=" + mStatus);
            mStatus = STATE_PAUSE;

            record();

        } else {
            KLog.w(TAG, "pause with status=" + mStatus);
        }
    }

    public void stop() {
        if(mStatus == STATE_NONE) {
            return;
        }

        KLog.i(TAG, "stop with status=" + mStatus);

        record();

        mStatus = STATE_NONE;
    }

    public long getCost() {
        return mPref.getLong(id, 0);
    }

    private void record() {
        long now = System.currentTimeMillis();
        long cost = (now - mStartTime) / 1000;
        long newCost = cost + mCost;

        KLog.i(TAG, "record cost:" + mCost + "->" + newCost);
        mCost = newCost;

        if(cost > 0) {
            mPref.edit().putLong(id, newCost).apply();
        }
    }

    public static void clearRecord(Context c, String id) {
        KLog.w(TAG, "onClear record id=" + id);
        new PictureTimeRecorder(c, id).mPref
                .edit().remove(id).apply();
    }
}

