package com.ober.artist.upload.category;

import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ober.artist.R;

/**
 * Created by ober on 2019-12-06.
 */
public class CategoryHolder extends RecyclerView.ViewHolder {

    @LayoutRes
    public static final int layoutId = R.layout.item_category;

    public final CheckBox checkBox;
    public final TextView textView;

    private CateItemBean cateItemBean;

    public CategoryHolder(@NonNull View itemView) {
        super(itemView);
        checkBox = itemView.findViewById(R.id.check);
        textView = itemView.findViewById(R.id.tv);
        checkBox.setOnCheckedChangeListener((buttonView, isChecked) -> cateItemBean.checked = isChecked);

        textView.setOnClickListener(v -> checkBox.toggle());
    }

    public void bindData(CateItemBean bean) {
        cateItemBean = bean;
        textView.setText(bean.cate.getName());
        checkBox.setChecked(cateItemBean.checked);
    }
}
