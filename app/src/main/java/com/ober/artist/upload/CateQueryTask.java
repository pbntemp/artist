package com.ober.artist.upload;

import android.os.AsyncTask;

import androidx.core.util.Consumer;

import com.ober.artist.net.ApiCallback;
import com.ober.artist.net.ApiCallback3;
import com.ober.artist.net.pbn.PbnApi;
import com.ober.artist.net.pbn.rest.CategoryListResp;

import java.util.concurrent.CountDownLatch;

import okhttp3.Call;

/**
 * Created by ober on 2019-10-31.
 */
public class CateQueryTask extends AsyncTask<Void, Void, CateQueryTask.Result> {

    public static class Result {
        public boolean success;
        public String msg;

        public String cateId;
    }

    private Consumer<Result> callback;
    private final String mCateName;

    private Call mRunningCall;

    public CateQueryTask(String cateName, Consumer<Result> callback) {
        this.mCateName = cateName;
        this.callback = callback;
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
        if(mRunningCall != null) {
            mRunningCall.cancel();
        }
    }

    @Override
    protected void onPostExecute(Result result) {
        super.onPostExecute(result);
        if(result != null && callback != null) {
            callback.accept(result);
        }
    }

    @Override
    protected Result doInBackground(Void... voids) {

        Result result = new Result();

        final CategoryListResp[] holder = new CategoryListResp[1];

        final CountDownLatch latch = new CountDownLatch(1);

        mRunningCall = PbnApi.queryCmsCategoryList(new ApiCallback3<CategoryListResp>() {
            @Override
            public void onResp(CategoryListResp categoryListResp, String msg) {
                mRunningCall = null;
                holder[0] = categoryListResp;
                latch.countDown();
            }
        });

        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
            return null;
        }

        CategoryListResp resp = holder[0];

        if(resp == null) {
            result.success = false;
            result.msg = "请求Category列表失败";
            return result;
        }

        if(!resp.isOk()) {
            result.success = false;
            result.msg = "请求Category列表失败:" + resp.getStatus().getMessage();
            return result;
        }

        CategoryListResp.Cate[] cates = resp.getData().getCates();

        CategoryListResp.Cate resultCate = null;
        for(CategoryListResp.Cate cate : cates) {
            if(cate.getName().equalsIgnoreCase(mCateName)) {
                resultCate = cate;
                break;
            }
        }

        if(resultCate == null) {
            result.success = false;
            result.msg = "分类未找到:" + mCateName;
            return result;
        }

        result.success = true;
        result.msg = "ok";
        result.cateId = resultCate.getId();

        return result;
    }

}
