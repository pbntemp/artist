package com.ober.artist.upload;

import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;

import androidx.core.util.Consumer;

import com.ober.artist.color.localdata.FileImageBean2;
import com.ober.artist.db.PicType;
import com.ober.artist.net.ApiCallback3;
import com.ober.artist.net.pbn.PbnApi;
import com.ober.artist.net.pbn.rest.CdnUploadResp;
import com.ober.artist.net.pbn.rest.ImgCouldListResp;
import com.ober.artist.ui.pics3.PicDownloadRecordManager;
import com.ober.artist.utils.GsonUtil;
import com.ober.artist.xlog.XLogger;
import com.socks.library.KLog;

import java.io.File;
import java.util.concurrent.CountDownLatch;

import okhttp3.Call;

/**
 * Created by ober on 2019-10-30.
 */
public class FilesUploadTask extends AsyncTask<Void, Integer, FilesUploadTask.Result> {

    private static final String TAG = "UPLOAD";

    private static final String UPLOAD_PATH_COMMON = "pbnartist";

    private final FileImageBean2 bean;
    private final int maxProgress;
    private final boolean hasThumbnailPng;

    private final ImgCouldListResp.Image image;

    public static class Result {
        public boolean success;
        public String msg;

        public String regionUrl;
        public String pdfUrl;
        public String pngUrl;
        public String thumbnailUrl;
        public String encryptColoredUrl;

        public String thumbUrl; //普通素材
        public String coloredUrl; //彩绘素材

        @Override
        public String toString() {
            return "Result{" +
                    "success=" + success +
                    ", msg='" + msg + '\'' +
                    ", regionUrl='" + regionUrl + '\'' +
                    ", pdfUrl='" + pdfUrl + '\'' +
                    ", pngUrl='" + pngUrl + '\'' +
                    ", thumbnailUrl='" + thumbnailUrl + '\'' +
                    ", encryptColoredUrl='" + encryptColoredUrl + '\'' +
                    ", thumbUrl='" + thumbUrl + '\'' +
                    ", coloredUrl='" + coloredUrl + '\'' +
                    '}';
        }
    }

    public final int maxProgress() {
        return maxProgress;
    }

    private Consumer<Result> callback;
    private Consumer<Integer> onProgress;

    FilesUploadTask(FileImageBean2 bean, Consumer<Result> callback, Consumer<Integer> onProgress) {
        this.bean = bean;
        this.callback = callback;
        this.onProgress = onProgress;
        this.hasThumbnailPng = bean.thumbnailPng != null;
        if(bean.pbntype == PicType.PBN_TYPE_COLORED || bean.pbntype == PicType.PBN_TYPE_WALL_COLORED) {
            maxProgress = 5 + (hasThumbnailPng ? 1 : 0);
        } else {
            maxProgress = 5 + (hasThumbnailPng ? 1 : 0);
        }

        image = PicDownloadRecordManager.getInstance().getImage(bean.id);
    }

    private Call mRunningCall;

    @Override
    protected void onCancelled() {
        super.onCancelled();
        if(mRunningCall != null) {
            mRunningCall.cancel();
        }
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
        if(onProgress != null) {
            onProgress.accept(values[0]);
        }
    }

    @Override
    protected void onPostExecute(Result result) {
        super.onPostExecute(result);
        if(callback != null) {
            callback.accept(result);
        }
    }

    @Override
    protected Result doInBackground(Void... voids) {

        Result result = new Result();

        KLog.d(TAG, "doInBackground");
        XLogger.d("FileUploadTask start");

        publishProgress(1);

        String[] msg = new String[1];

        XLogger.d("upload origin.png");
        KLog.d(TAG, "UPLOAD PNG");
        String pngUrl = needCheckURL() ? image.getPngForCmsUpload() : "";
        if (TextUtils.isEmpty(pngUrl)) pngUrl = uploadImage(bean.origin, msg);

        if(pngUrl == null) {
            result.success = false;
            result.msg = "上传PNG失败:" + msg[0];
            XLogger.w("upload origin.png failed " + msg[0]);
            return result;
        }

        result.pngUrl = pngUrl;

        if(isCancelled()) {
            return null;
        }

        publishProgress(2);
        XLogger.d("upload origin.pdf");

        msg = new String[1];

        String pdfUrl = needCheckURL() ? image.getPdfForCmsUpload() : "";
        if (TextUtils.isEmpty(pdfUrl)) pdfUrl = uploadImage(bean.pdf, msg);

        if(pdfUrl == null) {
            result.success = false;
            result.msg = "上传PDF失败:" + msg[0];
            XLogger.w("upload origin.pdf failed " + msg[0]);
            return result;
        }

        result.pdfUrl = pdfUrl;

        if(isCancelled()) {
            return null;
        }

        publishProgress(3);

        msg = new String[1];

        XLogger.d("upload region");

        String regionUrl = needCheckURL() ? image.getRegionForCmsUpload() : "";
        if (TextUtils.isEmpty(regionUrl)) regionUrl = uploadImage(bean.region, msg);

        if(regionUrl == null) {
            result.success = false;
            result.msg = "上传Region失败:" + msg[0];
            XLogger.w("upload region failed " + msg[0]);
            return result;
        }

        result.regionUrl = regionUrl;

        if(isCancelled()) {
            return null;
        }

        if(hasThumbnailPng) {
            XLogger.d("upload thumbnail");
            msg = new String[1];
            String thumbnailUrl = uploadImage(bean.thumbnailPng, msg);
            if(thumbnailUrl == null) {
                result.success = false;
                result.msg = "上传Thumbnail失败:" + msg[0];
                XLogger.w("upload thumbnail failed " + msg[0]);
                return result;
            }
            result.thumbnailUrl = thumbnailUrl;

            if(isCancelled()) {
                return null;
            }
        }

        if(bean.pbntype == PicType.PBN_TYPE_COLORED || bean.pbntype == PicType.PBN_TYPE_WALL_COLORED) {

            publishProgress(4);

            XLogger.d("upload colored");

            File colored = bean.colored;
            if(!colored.exists()) {
                result.success = false;
                result.msg = "colored不存在";
                XLogger.e("upload colored failed not exist");
                return result;
            }

            msg = new String[1];

            String coloredUrl = needCheckURL() ? image.getColoredForCmsUpload() : "";
            if (TextUtils.isEmpty(coloredUrl)) coloredUrl = uploadImage(colored, msg);

            if(coloredUrl == null) {
                result.success = false;
                result.msg = "上传colored失败:" + msg[0];
                XLogger.w("upload colored failed");
                return result;
            }
            result.coloredUrl = coloredUrl;

            publishProgress(5);

            XLogger.w("upload encryptImage");

            msg = new String[1];

            String encyptColoredUrl = encryptImage(colored, msg);

            if(encyptColoredUrl == null) {
                result.success = false;
                result.msg = "加密colored失败:" + msg[0];
                XLogger.w("upload encryptImage failed");
                return result;
            }

            result.encryptColoredUrl = encyptColoredUrl;
        } else {

            publishProgress(4);

            XLogger.d("upload thumb");

            File thumb = bean.thumb;
            if(!thumb.exists()) {
                result.success = false;
                result.msg = "thumb不存在";
                XLogger.e("upload thumb failed not exists");
                return result;
            }
            msg = new String[1];
            String thumbUrl = uploadImage(thumb, msg);

            if(thumbUrl == null) {
                result.success = false;
                result.msg = "上传thumb失败:" + msg[0];
                XLogger.e("upload thumb failed");
                return result;
            }
            result.thumbUrl = thumbUrl;

            publishProgress(5);

            XLogger.d("upload encryptImage");

            msg = new String[1];

            String encyptColoredUrl = encryptImage(thumb, msg);

            if(encyptColoredUrl == null) {
                result.success = false;
                result.msg = "加密colored失败:" + msg[0];
                XLogger.w("upload encryptImage failed");
                return result;
            }

            result.encryptColoredUrl = encyptColoredUrl;
        }

        result.success = true;
        result.msg = "ok";

        XLogger.i("UploadTask end ok");

        return result;
    }

    private String encryptImage(File file, String[] outMsg) {
        int count = 0;

        while (count < 2) {

            if(count != 0) {
                KLog.w(TAG, "retry encryptImage");
            }

            final CountDownLatch latch = new CountDownLatch(1);
            final String[] result = new String[1];
            Log.w("AAAA", "encryptImage " + file + ", " + file.length());
            mRunningCall = PbnApi.encryptImgToCdn(file, (cdnUploadResp, errmsg) -> {
                if(outMsg != null) {
                    outMsg[0] = errmsg;
                }
                if (cdnUploadResp == null) {
                    KLog.w("UPLOAD", "FAILED!!");
                } else {
                    if(cdnUploadResp.isOk()) {
                        KLog.i("UPLOAD", "OK", cdnUploadResp.getData().getUrl());
                        result[0] = cdnUploadResp.getData().getUrl();
                        Log.w("AAAA", "encryptImage result=" + result[0]);
                    } else {
                        KLog.w("UPLOAD", "FAILED", GsonUtil.toJson(cdnUploadResp));
                        XLogger.w("PbnApi " + GsonUtil.toJson(cdnUploadResp));
                    }
                }
                latch.countDown();
            });

            try {
                latch.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
                return null;
            }

            if(result[0] != null) {
                return result[0];
            }

            count++;
        }
        return null;
    }

    private String uploadImage(File file, String[] errmsgOut) {

        int count = 0;

        while (count < 2) {

            if(count != 0) {
                KLog.w(TAG, "retry uploadImage");
            }

            final CountDownLatch latch = new CountDownLatch(1);
            final String[] result = new String[1];

            Log.w("AAAAA", "upload " + file + " size=" + file.length());

            mRunningCall = PbnApi.uploadImageFileToCdn(file, UPLOAD_PATH_COMMON, new ApiCallback3<CdnUploadResp>() {
                @Override
                public void onResp(CdnUploadResp cdnUploadResp, String errmsg) {
                    if(errmsgOut != null) {
                        errmsgOut[0] = errmsg;
                    }
                    if (cdnUploadResp == null) {
                        KLog.w("UPLOAD", "FAILED!!");
                    } else {
                        if(cdnUploadResp.isOk()) {
                            KLog.i("UPLOAD", "OK", cdnUploadResp.getData().getUrl());
                            result[0] = cdnUploadResp.getData().getUrl();
                            Log.w("AAAA", "result url = " + result[0]);
                        } else {
                            KLog.w("UPLOAD", "FAILED", GsonUtil.toJson(cdnUploadResp));
                        }
                    }
                    latch.countDown();
                }
            });

            try {
                latch.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
                return null;
            }

            if(result[0] != null) {
                return result[0];
            }

            count++;
        }

        return null;
    }

    private boolean needCheckURL(){
        return (image != null);
    }

}
