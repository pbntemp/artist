package com.ober.artist.upload.category;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by ober on 2019-12-06.
 */
public class CategoryAdapter extends RecyclerView.Adapter<CategoryHolder> {

    private final List<CateItemBean> mData;

    public CategoryAdapter() {
        mData = new ArrayList<>();
    }

    public void putData(List<CateItemBean> data) {
        mData.addAll(data);
    }

    public List<CateItemBean> getAllCheckedItems() {
        List<CateItemBean> rs = new LinkedList<>();
        for(CateItemBean bean : mData) {
            if(bean.checked) {
                rs.add(bean);
            }
        }
        return rs;
    }

    @NonNull
    @Override
    public CategoryHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(CategoryHolder.layoutId, parent, false);

        return new CategoryHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryHolder holder, int position) {
        final CateItemBean bean = mData.get(position);
        holder.bindData(bean);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }
}
