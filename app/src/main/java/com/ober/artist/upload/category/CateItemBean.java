package com.ober.artist.upload.category;

import com.ober.artist.net.pbn.rest.CategoryListResp;

/**
 * Created by ober on 2019-12-06.
 */
public class CateItemBean {

    public final CategoryListResp.Cate cate;
    public final boolean isDaily;

    public boolean checked;

    CateItemBean(CategoryListResp.Cate cate) {
        this.cate = cate;
        this.isDaily = cate.getName().equalsIgnoreCase("daily");
        checked = false;
    }

}
