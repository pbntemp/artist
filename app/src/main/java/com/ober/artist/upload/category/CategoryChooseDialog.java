package com.ober.artist.upload.category;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.core.util.Consumer;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ober.artist.R;
import com.ober.artist.utils.SToast;

import java.util.List;

/**
 * Created by ober on 2019-12-06.
 */
public class CategoryChooseDialog extends Dialog {

    private Consumer<List<CateItemBean>> output;

    public CategoryChooseDialog(@NonNull Context context, Consumer<List<CateItemBean>> output) {
        super(context, R.style.ColorImgPrepareDialog);
        this.output = output;
    }

    private CategoryAdapter mAdapter;

    private ProgressBar mProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_categories);

        mProgressBar = findViewById(R.id.progress);

        mAdapter = new CategoryAdapter();

        RecyclerView recyclerView = findViewById(R.id.recycler);
        LinearLayoutManager lm = new LinearLayoutManager(getContext());
        lm.setOrientation(RecyclerView.VERTICAL);
        recyclerView.setLayoutManager(lm);
        recyclerView.setAdapter(mAdapter);

        findViewById(R.id.btn_ok).setOnClickListener(v -> {
            if(sendData()) {
                dismiss();
            }
        });
    }

    void putData(List<CateItemBean> list) {
        mAdapter.putData(list);
        mAdapter.notifyDataSetChanged();
        mProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void dismiss() {
        super.dismiss();
    }

    private boolean sendData() {
        List<CateItemBean> items =  mAdapter.getAllCheckedItems();
        int count = items.size();
        if(count == 0) {
            SToast.show("至少选择一个分类");
            return false;
        }
        if(count > 1) {
            boolean hasDaily = false;
            for (CateItemBean item : items) {
                if (item.isDaily) {
                    hasDaily = true;
                    break;
                }
            }
            if(hasDaily) {
                SToast.show("Daily与其他分类互斥");
                return false;
            }
        }

        output.accept(items);

        return true;
    }


}
