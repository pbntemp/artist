package com.ober.artist.upload;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.util.Log;

import androidx.appcompat.app.AlertDialog;
import androidx.core.util.Consumer;

import com.ober.artist.SUserManager2;
import com.ober.artist.analyze.base.ArtistAnalyze;
import com.ober.artist.analyze.PictureTimeRecorder;
import com.ober.artist.color.localdata.LocalData;
import com.ober.artist.ui.pics3.VincentUploadPaint;
import com.ober.artist.upload.category.CategoryChoose;
import com.ober.artist.color.localdata.FileImageBean2;
import com.ober.artist.db.PicType;
import com.ober.artist.net.pbn.rest.GenZipReq;
import com.ober.artist.pack.PbnPaintGenerateTask;
import com.ober.artist.utils.ResourceSizeCheckUtil;
import com.ober.artist.xlog.XLogger;

import java.io.File;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by ober on 2019-10-30.
 */
public class UploadFlow {

    public interface Callback {
        void onUploadSuccess(String id);
        void onUploadFailed(int code, String msg);
        void onUploadProgress(String msg);
        void onUploadCanceled();
    }

    public static class UploadSession {
        String id;
        boolean isLocal;
        long startTime;
        boolean isDataChecked;
        String[] selectedCates;
    }

    private final String uid;

    private CategoryChoose mCategoryChoose;

    private PbnPaintGenerateTask mPbnPaintGenerateTask;
    private FilesUploadTask mFilesUploadTask;
    private ZipRequestTask mZipReqTask;
    private CmsUploadTask mCmsUploadTask;

    private final AtomicBoolean running = new AtomicBoolean(false);
    private UploadSession mSession;

    private Callback mCallback;

    private Context mContext;

    public UploadFlow(Context activityContext, String uid) {
        this.uid = uid;
        mContext = activityContext;
    }

    public void setCallback(Callback callback) {
        this.mCallback = callback;
    }

    public UploadSession start(String id, boolean isLocal) {
        if(running.get()) {
            cancel();
        }
        running.set(true);

        mSession = new UploadSession();
        mSession.startTime = System.currentTimeMillis();
        mSession.id = id;
        mSession.isLocal = isLocal;
        checkToStart(id, isLocal);

        PictureTimeRecorder mPicTimeRecorder = new PictureTimeRecorder(mContext, id);
        mPicTimeRecorder.stop();
        ArtistAnalyze.trackEventKeyValue(ArtistAnalyze.Picture.event_cost_time,
                String.format(ArtistAnalyze.Common.pic_id, id),
                mPicTimeRecorder.getCost());
        return mSession;
    }

    public UploadSession retry(UploadSession session) {
        if(running.get()) {
            cancel();
        }

        running.set(true);
        if(!session.isDataChecked) {
            return start(session.id, session.isLocal);
        }
        if(session.selectedCates == null) {
            startCategoryChoose(session.id, session.isLocal);
            return session;
        }
        startPbnGenerate(session.id, session.isLocal, false, session.selectedCates);
        return session;
    }

    public boolean isRunning() {
        return running.get();
    }

    private void checkToStart(String id, boolean isLocal) {
        File png = LocalData.getTargetPngFile(uid, isLocal, id);
        File pdf = LocalData.getTargetPdfFile(uid, isLocal, id);
        if(!png.exists() || !pdf.exists()) {
            notifyResult(-9, "资源文件丢失");
            return;
        }
        ResourceSizeCheckUtil.CheckResult rs = ResourceSizeCheckUtil.checkResource(png, pdf);
        if(rs.isTooLarge) {
            notifyResult(-9, "资源异常");
            return;
        }
        if(!rs.isPerfect) {
            new AlertDialog.Builder(mContext)
                    .setTitle("注意")
                    .setMessage(rs.msg)
                    .setCancelable(false)
                    .setPositiveButton(grayBtnText("继续上传"), (dialog, which) -> startCategoryChoose(id, isLocal))
                    .setNegativeButton("取消", (dialog, which) -> notifyCancel()).show();
        } else {
            startCategoryChoose(id, isLocal);
        }
    }

    private void startCategoryChoose(String id, boolean isLocal) {

        mSession.isDataChecked = true;

        mCategoryChoose = new CategoryChoose(mContext, uid, new Consumer<String[]>() {
            @Override
            public void accept(String[] cates) {
                startPbnGenerate(id, isLocal, true, cates);
            }
        }, s -> {
            notifyResult(-5, s);
        }, () -> {
            notifyCancel();
        });
        mCategoryChoose.sendRequest(id);
    }

    private void startPbnGenerate(String id, boolean isLocal, boolean analyze, String[] cateIds) {
        notifyProgress("正在处理资源");

        mPbnPaintGenerateTask = new PbnPaintGenerateTask(uid, isLocal, analyze, id, (suc, errMsg) -> {
            mPbnPaintGenerateTask = null;
            if(suc == null) {
                notifyResult(-1, errMsg);
            } else {
                startFilesUpload(isLocal, suc, cateIds);

                if (analyze) {
                    ArtistAnalyze.trackEventKeyValue(ArtistAnalyze.Picture.event_upload_from,
                            String.format(ArtistAnalyze.Common.pic_id, id),
                            isLocal ? ArtistAnalyze.Common.local : ArtistAnalyze.Common.cloud);

                    String username = new SUserManager2().getUserName();
                    ArtistAnalyze.trackEventKeyValue(ArtistAnalyze.Picture.event_author,
                            String.format(ArtistAnalyze.Common.pic_id, id),username);
                }
            }
        });
        mPbnPaintGenerateTask.execute();
    }

    private void startFilesUpload(boolean isLocal, FileImageBean2 bean, String[] cateIds) {
        notifyProgress("正在上传素材");
        mSession.selectedCates = cateIds;
        mFilesUploadTask = new FilesUploadTask(bean, result -> {
            mFilesUploadTask = null;
            if(result.success) {
                //upload Vincent
                XLogger.d("upload files result =" + result);
                VincentUploadPaint vincentUploadPaint = new VincentUploadPaint(bean.uid, bean.id, isLocal);
                vincentUploadPaint.doUpload(result.coloredUrl, "", resp -> startRequestZip(bean, result, cateIds));
            } else {
                notifyResult(-2, result.msg);
            }
        }, progress -> {
            int max = mFilesUploadTask.maxProgress();
            notifyProgress("正在上传素材 " + progress + "/" + max);
        });

        mFilesUploadTask.execute();
    }

    private void startRequestZip(FileImageBean2 bean, FilesUploadTask.Result uploadResult, String[] cateIds) {
        notifyProgress("正在请求打包ZIP");

        XLogger.d("upload zip");

        GenZipReq req = createZipReq(bean, uploadResult);

        mZipReqTask = new ZipRequestTask(bean, req, result -> {
            mZipReqTask = null;
            XLogger.d("upload zip result=" + result.success);
            if(result.success) {
                if(result.zipSizeWarning) {
                    new AlertDialog.Builder(mContext)
                            .setTitle("注意")
                            .setMessage("ZIP包体积较大(超过2mb)，是否继续上传？")
                            .setPositiveButton(grayBtnText("继续上传"), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    startUploadCms(bean, uploadResult, result, cateIds);
                                }
                            })
                            .setNegativeButton("取消上传", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    notifyCancel();
                                }
                            }).show();
                } else {
                    startUploadCms(bean, uploadResult, result, cateIds);
                }
            } else {
                notifyResult(-3, result.errMsg);
            }
        });
        mZipReqTask.execute();
    }

    private void startUploadCms(FileImageBean2 bean,
                                FilesUploadTask.Result uploadResult,
                                ZipRequestTask.Result zipResult,
                                String[] cateIds) {
        notifyProgress("正在上传CMS");

        XLogger.d("upload to cms");

        Log.w("AAAA", "upload cms");

        mCmsUploadTask = new CmsUploadTask(bean, uploadResult, zipResult, cateIds, result -> {
            mCmsUploadTask = null;

            XLogger.d("upload to cms result=" + result.success);

            if(result.success) {
                XLogger.i("upload result id=" + result.cmsId);
                notifyResult(0, result.cmsId);
            } else {
                notifyResult(-4, result.msg);

                ArtistAnalyze.trackEventKeyValue(ArtistAnalyze.Picture.event_pic_upload,
                        String.format(ArtistAnalyze.Common.pic_id, bean.id),result.msg);
            }
        });

        mCmsUploadTask.execute();
    }

    public void cancel() {

        if(mCategoryChoose != null) {
            mCategoryChoose.cancel();
        }

        if(mPbnPaintGenerateTask != null) {
            mPbnPaintGenerateTask.cancel(true);
            mPbnPaintGenerateTask = null;
        }

        if(mFilesUploadTask != null) {
            mFilesUploadTask.cancel(true);
            mFilesUploadTask = null;
        }

        if(mZipReqTask != null) {
            mZipReqTask.cancel(true);
            mZipReqTask = null;
        }

        if(mCmsUploadTask != null) {
            mCmsUploadTask.cancel(true);
            mCmsUploadTask = null;
        }

        running.set(false);
    }


    private void notifyCancel() {
        running.set(false);
        if(mCallback != null) {
            mCallback.onUploadCanceled();
        }
    }

    private void notifyResult(int code, String msg) {
        running.set(false);

        if(code == 0) {
            //success
            if (mCallback != null) {
                mCallback.onUploadSuccess(msg);
            }
        } else {
            //failed
            if(mCallback != null) {
                mCallback.onUploadFailed(code, msg);
            }
        }
    }

    private void notifyProgress(String msg) {
        if(mCallback != null) {
            mCallback.onUploadProgress(msg);
        }
    }

    private static GenZipReq createZipReq(FileImageBean2 bean, FilesUploadTask.Result uploadResult) {
        final boolean isColored = bean.pbntype == PicType.PBN_TYPE_WALL_COLORED || bean.pbntype == PicType.PBN_TYPE_COLORED;

        if(isColored) {
            return GenZipReq.createColored(
                    uploadResult.pngUrl,
                    uploadResult.pdfUrl,
                    uploadResult.regionUrl,
                    uploadResult.coloredUrl);
        } else {
            return GenZipReq.createNormal(
                    uploadResult.pngUrl,
                    uploadResult.pdfUrl,
                    uploadResult.regionUrl,
                    uploadResult.thumbUrl);
        }
    }

    private static CharSequence grayBtnText(String text) {
        int len = text.length();
        int color = Color.parseColor("#999999");
        SpannableStringBuilder sb = new SpannableStringBuilder(text);
        sb.setSpan(new ForegroundColorSpan(color), 0, len, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        return sb;
    }
}
