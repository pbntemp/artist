package com.ober.artist.upload;

import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;

import androidx.core.util.Consumer;

import com.google.gson.Gson;
import com.ober.artist.color.localdata.FileImageBean2;
import com.ober.artist.net.ApiCallback2;
import com.ober.artist.net.pbn.PbnApi;
import com.ober.artist.net.pbn.rest.GenZipReq;
import com.ober.artist.net.pbn.rest.GenZipResp;
import com.ober.artist.utils.SimpleFileEditor;
import com.socks.library.KLog;

import java.util.concurrent.CountDownLatch;

import okhttp3.Call;

/**
 * Created by ober on 2019-10-31.
 */
public class ZipRequestTask extends AsyncTask<Void, Void, ZipRequestTask.Result> {

    private final GenZipReq req;
    private final FileImageBean2 bean;

    private Call mRunningCall;
    private Consumer<Result> callback;

    public ZipRequestTask(FileImageBean2 fileImageBean2, GenZipReq req, Consumer<Result> callback) {
        this.req = req;
        this.callback = callback;
        this.bean = fileImageBean2;
    }

    public static class Result {

        public boolean zipSizeError;
        public boolean zipSizeWarning;
        public boolean success;
        public String errMsg;

        public String zipFileUrl;
        public String minZipFileUrl;
        public String paintId;
    }

    @Override
    protected void onPostExecute(Result result) {
        super.onPostExecute(result);
        if(callback != null) {
            callback.accept(result);
        }
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
        if(mRunningCall != null) {
            mRunningCall.cancel();
        }
    }

    @Override
    protected Result doInBackground(Void... voids) {

        Result result = new Result();

        String centerStr = SimpleFileEditor.readFromFile(bean.center.getAbsolutePath());
        if(TextUtils.isEmpty(centerStr)) {
            result.success = false;
            result.errMsg = "center无效";
            return result;
        }
        req.setCenter(centerStr);

        String planStr = SimpleFileEditor.readFromFile(bean.plan.getAbsolutePath());
        if(TextUtils.isEmpty(planStr)) {
            result.success = false;
            result.errMsg = "plan无效";
            return result;
        }
        String[] plans = new String[1];
        plans[0] = planStr;
        req.setPlans(plans);

        int[] outCode = new int[1];
        String[] outMsg = new String[1];
        GenZipResp resp = requestPbnZip(outCode, outMsg);
        int httpCode = outCode[0];

        if(resp == null) {

            if(httpCode == 400) {
                KLog.w("UPLOAD", "zip resp code=400");
                result.success = false;
                result.errMsg = "生成zip过大";
                result.zipSizeError = false;
                return result;
            }

            KLog.w("UPLOAD", "retry request zip");
            resp = requestPbnZip(outCode, outMsg);
            httpCode = outCode[0];
        }

        Log.w("AAAAA", "request zip result: \n" + new Gson().toJson(resp));

        if(resp == null) {

            if(httpCode == 400) {
                KLog.w("UPLOAD", "zip resp code=400");
                result.success = false;
                result.errMsg = "生成ZIP过大, 请压缩资源";
                result.zipSizeError = false;
                return result;
            }

            result.success = false;
            result.errMsg = "请求ZIP失败:" + outMsg[0];
            return result;
        }

        if(!resp.isOk()) {
            result.success = false;
            result.errMsg = "请求zip失败:" + resp.getStatus().getMessage();
            return result;
        }

        if(TextUtils.isEmpty(resp.getMinZipFile()) || TextUtils.isEmpty(resp.getZipFile())) {
            result.success = false;
            result.errMsg = "请求ZIP错误";
            return result;
        }

        result.success = true;
        result.errMsg = "ok";
        result.zipFileUrl = resp.getZipFile();
        result.minZipFileUrl = resp.getMinZipFile();
        result.paintId = resp.getPaintId();
        if(resp.getStatus().getCode() == GenZipResp.STATUS_ZIP_SIZE_WARNING) {
            result.zipSizeWarning = true;
        }

        return result;
    }


    private GenZipResp requestPbnZip(int[] httpCode, String[] outmsg) {

        Log.w("AAAAA", "request zip");

        final CountDownLatch latch = new CountDownLatch(1);

        final GenZipResp[] rs = new GenZipResp[1];

        mRunningCall = PbnApi.requestGenPbnZip(req, new ApiCallback2<GenZipResp>() {
            @Override
            public void onResp(GenZipResp genZipResp, String msg, int code) {

                mRunningCall = null;

                if(httpCode != null) {
                    httpCode[0] = code;
                }

                if(outmsg != null) {
                    outmsg[0] = msg;
                }

                if(genZipResp != null) {
                    rs[0] = genZipResp;
                }

                latch.countDown();
            }
        });

        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
            return null;
        }

        final GenZipResp resp = rs[0];

        if(resp == null) {
            return null;
        }

        return resp;
    }

}
