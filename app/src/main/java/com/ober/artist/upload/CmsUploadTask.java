package com.ober.artist.upload;

import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;

import androidx.core.util.Consumer;

import com.google.gson.Gson;
import com.ober.artist.SUserManager2;
import com.ober.artist.color.localdata.FileImageBean2;
import com.ober.artist.db.PbnDb;
import com.ober.artist.db.PicType;
import com.ober.artist.net.ApiCallback3;
import com.ober.artist.net.pbn.PbnApi;
import com.ober.artist.net.pbn.rest.CmsUploadPaintReq;
import com.ober.artist.net.pbn.rest.CmsUploadPaintResp;
import com.ober.artist.utils.GsonUtil;
import com.ober.artist.utils.SimpleFileEditor;
import com.ober.artist.xlog.XLogger;
import com.socks.library.KLog;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;
import java.util.concurrent.CountDownLatch;

import okhttp3.Call;

/**
 * Created by ober on 2019-10-31.
 */
public class CmsUploadTask extends AsyncTask<Void, Void, CmsUploadTask.Result> {

    public static class Result {
        public boolean success;
        public String msg;
        public String cmsId;
    }

    private final FileImageBean2 bean;
    private final FilesUploadTask.Result uploadResult;
    private final ZipRequestTask.Result zipResult;
    private final String[] cateIds;

    private Call mRunningCall;
    private Consumer<Result> callback;

    public CmsUploadTask(FileImageBean2 bean,
                         FilesUploadTask.Result uploadResult,
                         ZipRequestTask.Result zipResult,
                         String[] cateIds,
                         Consumer<Result> callback) {
        this.bean = bean;
        this.uploadResult = uploadResult;
        this.zipResult = zipResult;
        this.callback = callback;
        this.cateIds = cateIds;
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
        if(mRunningCall != null) {
            mRunningCall.cancel();
        }
    }

    @Override
    protected void onPostExecute(Result result) {
        super.onPostExecute(result);
        if(result != null && callback != null) {
            callback.accept(result);
        }
    }

    @Override
    protected Result doInBackground(Void... voids) {

        final int pbntype = bean.pbntype;

        Result result = new Result();

        //gen request

        CmsUploadPaintReq req = new CmsUploadPaintReq();

        req.setPng(Objects.requireNonNull(uploadResult.pngUrl));
        req.setPdf(Objects.requireNonNull(uploadResult.pdfUrl));
        req.setRegion(Objects.requireNonNull(uploadResult.regionUrl));

        if(uploadResult.thumbnailUrl != null) {
            req.setThumbnail(uploadResult.thumbnailUrl);
        }

        String centerStr = SimpleFileEditor.readFromFile(bean.center.getAbsolutePath());
        if(TextUtils.isEmpty(centerStr)) {
            result.success = false;
            result.msg = "center无效";
            return result;
        }
        req.setCenter(centerStr);

        String planStr = SimpleFileEditor.readFromFile(bean.plan.getAbsolutePath());
        if(TextUtils.isEmpty(planStr)) {
            result.success = false;
            result.msg = "plan无效";
            return result;
        }
        req.setPlans(new String[] {planStr});

        req.setEncryptedColoredUrlList(new String[] {uploadResult.encryptColoredUrl});

        if(pbntype == PicType.PBN_TYPE_COLORED || pbntype == PicType.PBN_TYPE_WALL_COLORED) {
            req.setColoredUrlList(new String[] {uploadResult.coloredUrl});
            req.setColorType("colored");
            req.setBusinessType(CmsUploadPaintReq.BUSINESS_TYPE_VIDEO);
        } else {
            req.setColoredUrlList(new String[] {uploadResult.thumbUrl});
            req.setColorType("normal");
        }

        if(pbntype == PicType.PBN_TYPE_WALL_COLORED || pbntype == PicType.PBN_TYPE_WALL_NORMAL) {
            req.setSizeType("wallpaper");
        } else {
            req.setSizeType("normal");
        }

        req.setZip(zipResult.zipFileUrl);
        req.setMinZip(zipResult.minZipFileUrl);
        req.setPaintId(zipResult.paintId);

        final String name;
        {
            String username = new SUserManager2().getUserName();
            SimpleDateFormat format = new SimpleDateFormat("yyyy_MM_dd_HH_mm", Locale.US);
            name = username + "_" + format.format(new Date(System.currentTimeMillis()));
        }
        req.setName(name);
        req.setCategoryIdList(cateIds);

        int[] regionSize = PicType.regionSize(pbntype);
        req.setRegionSize(getSizeStr(regionSize));

        int[] pdfSize = PicType.pdfSize(pbntype);
        req.setPdfSize(getSizeStr(pdfSize));

        int[] pngSize = PicType.pngSize(pbntype);
        req.setPngSize(getSizeStr(pngSize));

        int[] coloredSize = PicType.coloredSize(pbntype);
        req.setColoredSize(getSizeStr(coloredSize));

        //req组装完成
        final CountDownLatch latch = new CountDownLatch(1);

        final CmsUploadPaintResp[] respHolder = new CmsUploadPaintResp[1];
        final String[] outmsg = new String[1];


        KLog.w("AAAA", "upload to cms");

        mRunningCall = PbnApi.uploadPaintToCms(req, new ApiCallback3<CmsUploadPaintResp>() {
            @Override
            public void onResp(CmsUploadPaintResp cmsUploadPaintResp, String msg) {

                req.setCenter(null);
                req.setPlans(null);
                KLog.w("AAAA", new Gson().toJson(req));

                mRunningCall = null;

                outmsg[0] = msg;

                if(cmsUploadPaintResp == null || !cmsUploadPaintResp.isOk()) {
                    KLog.w("UPLOAD", "cms resp err", GsonUtil.toJson(cmsUploadPaintResp));
                    XLogger.w("upload cms failed");
                    XLogger.w(GsonUtil.toJson("" + cmsUploadPaintResp));
                }

                Log.w("AAAA", "upload result");

                respHolder[0] = cmsUploadPaintResp;
                latch.countDown();
            }
        });

        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
            return null;
        }

        CmsUploadPaintResp resp = respHolder[0];
        if(resp == null) {
            result.success = false;
            result.msg = "上传CMS失败:" + outmsg[0];
            return result;
        }

        if(!resp.isOk()) {
            result.success = false;
            result.msg = resp.getStatus().getMessage();
            return result;
        }

        result.success = true;
        result.msg = "ok";
        result.cmsId = resp.getId();

        PbnDb.get().getPicRecordDao()
                .updateUploadTime(bean.id, System.currentTimeMillis());

        return result;
    }

    private static String getSizeStr(int[] size) {
        return size[0] + " " + size[1];
    }

}
