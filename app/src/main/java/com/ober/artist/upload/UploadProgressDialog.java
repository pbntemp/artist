package com.ober.artist.upload;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.ober.artist.R;

/**
 * Created by ober on 2019-10-31.
 */
public class UploadProgressDialog extends Dialog {

    private Runnable cancelAction;
    private Runnable retryAction;

    public UploadProgressDialog(@NonNull Context context, Runnable cancelAction, Runnable retryAction) {
        super(context, R.style.ColorImgPrepareDialog);
        this.cancelAction = cancelAction;
        this.retryAction = retryAction;
    }

    private TextView tvTitle;
    private ProgressBar pbLoading;
    private TextView tvMsg;
    private TextView tvBtn;
    private TextView tvResult;
    private TextView tvRetry;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_uploading);
        tvTitle = findViewById(R.id.tv_title);
        pbLoading = findViewById(R.id.progress);
        tvMsg = findViewById(R.id.tv_msg);
        tvBtn = findViewById(R.id.tv_btn);
        tvResult = findViewById(R.id.tv_result);
        tvRetry = findViewById(R.id.tv_retry);
    }

    public void setUILoading() {
        tvTitle.setText("素材上传");
        pbLoading.setVisibility(View.VISIBLE);
        tvMsg.setVisibility(View.VISIBLE);
        tvMsg.setText("");
        tvBtn.setText("取消");
        tvResult.setVisibility(View.GONE);
        tvRetry.setVisibility(View.GONE);


        tvBtn.setOnClickListener(v -> {
            dismiss();
            if(cancelAction != null) {
                cancelAction.run();
            }
        });
    }

    public void setUIResult(boolean success, String result) {
        tvTitle.setText(success ? "上传成功" : "上传失败");
        pbLoading.setVisibility(View.GONE);
        tvMsg.setVisibility(View.GONE);
        tvBtn.setText("确定");
        tvResult.setVisibility(View.VISIBLE);
        tvResult.setText(result);

        if(success) {
            tvResult.setTextIsSelectable(true);
        } else {
            tvResult.setTextIsSelectable(false);
        }

        tvBtn.setOnClickListener(v -> dismiss());

        if(success) {
            tvRetry.setVisibility(View.GONE);
        } else {
            tvRetry.setOnClickListener(v -> {
                dismiss();
                if(retryAction != null) {
                    retryAction.run();
                }
            });
            tvRetry.setVisibility(View.VISIBLE);
        }
    }

    public void setProgressMsg(String msg) {
        tvMsg.setText(msg);
    }
}
