package com.ober.artist.upload.category;

import com.ober.artist.net.ApiCallback3;
import com.ober.artist.net.pbn.PbnApi;
import com.ober.artist.net.pbn.rest.CategoryListResp;

import okhttp3.Call;

/**
 * Created by ober on 2019-12-06.
 */
public class CategoryLoader {

    private Call mRunningCall;

    public void load(ApiCallback3<CategoryListResp> resp) {
        mRunningCall  = PbnApi.queryCmsCategoryList(resp);
    }

    public void cancel() {
        if(mRunningCall != null) {
            mRunningCall.cancel();
            mRunningCall = null;
        }
    }
}
