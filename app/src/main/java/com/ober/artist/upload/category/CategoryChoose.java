package com.ober.artist.upload.category;

import android.content.Context;
import android.content.DialogInterface;

import androidx.core.util.Consumer;

import com.ober.artist.db.PbnDb;
import com.ober.artist.net.ApiCallback3;
import com.ober.artist.net.bean.Picture;
import com.ober.artist.net.pbn.rest.CategoryListResp;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by ober on 2019-12-06.
 */
public class CategoryChoose {

    private CategoryLoader mLoader;

    private CategoryChooseDialog mDialog;

    private final String mUID;

    private Context mContext;

    private Consumer<String> onFailed;

    private Consumer<String[]> onSuccess;

    private Runnable onCancel;

    public CategoryChoose(Context context, String uid, Consumer<String[]> onSuccess, Consumer<String> onFailed, Runnable onCancel) {
        mUID = uid;
        mContext = context;
        mLoader = new CategoryLoader();
        this.onSuccess = onSuccess;
        this.onFailed = onFailed;
        this.onCancel = onCancel;
    }

    public void sendRequest(String id) {

        if(mDialog != null && mDialog.isShowing()) {
            mDialog.dismiss();
        }

        mDialog = new CategoryChooseDialog(mContext, this::notifySuccess);
        mDialog.show();
        mDialog.setCanceledOnTouchOutside(false);
        mDialog.setOnCancelListener(dialog -> onCancel.run());

        new Thread(() -> {
            final int pbnType = PbnDb.get().getPicRecordDao().getById(mUID, id).get(0).getPbntype();
            mLoader.load((categoryListResp, errmsg) -> handleDataLoaded(categoryListResp, errmsg, pbnType));
        }).start();
    }

    private void handleDataLoaded(CategoryListResp categoryListResp, String errmsg, int pbnType) {
        if(categoryListResp == null || !categoryListResp.isOk()) {
            if(onFailed != null) {
                if(mDialog.isShowing()) {
                    mDialog.dismiss();
                }
                if(categoryListResp == null) {
                    onFailed.accept("请求分类失败:" + errmsg);
                } else {
                    onFailed.accept(categoryListResp.getStatus().getMessage());
                }
            }
        } else {
            CategoryListResp.Cate[] cates = categoryListResp.getData().getCates();
            putDataToDialog(cates, true, pbnType == Picture.PBN_TYPE_COLORED
                    || pbnType == Picture.PBN_TYPE_WALL_COLORED);
        }
    }

    public void cancel() {
        if(mLoader != null) {
            mLoader.cancel();
        }

        if(mDialog != null) {
            mDialog.dismiss();
        }
    }

    private void putDataToDialog(CategoryListResp.Cate[] cates, boolean defaultSelectNew, boolean defaultSelectSpecial) {

        if(mDialog == null || !mDialog.isShowing()) {
            return;
        }

        List<CateItemBean> itemBeans = new LinkedList<>();
        for(CategoryListResp.Cate cate : cates) {
            CateItemBean itemBean = new CateItemBean(cate);
            itemBeans.add(itemBean);

            if(defaultSelectNew) {
                if((itemBean.cate.getName().equalsIgnoreCase("new"))) {
                    itemBean.checked = true;
                    defaultSelectNew = false;
                }
            }

            if(defaultSelectSpecial) {
                if (itemBean.cate.getName().equalsIgnoreCase("special")) {
                    itemBean.checked = true;
                    defaultSelectSpecial = false;
                }
            }
        }

        mDialog.putData(itemBeans);
    }

    private void notifySuccess(List<CateItemBean> cateItemBeans) {
        int size = cateItemBeans.size();
        String[] rs = new String[size];
        for(int i = 0; i < size; i++) {
            rs[i] = cateItemBeans.get(i).cate.getId();
        }

        if(onSuccess != null) {
            onSuccess.accept(rs);
        }
    }

}
