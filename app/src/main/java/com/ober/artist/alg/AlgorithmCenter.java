package com.ober.artist.alg;

import android.graphics.Bitmap;
import android.graphics.Color;

import com.meevii.color.fill.model.core.FloodFillArea;
import com.ober.artist.color.data.Center;
import com.ober.artist.color.data.LocalStore;
import com.socks.library.KLog;

import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by ober on 2019/1/11.
 */
@Deprecated
public class AlgorithmCenter {

    private static final String TAG = "AlgorithmCenter";

    public static Map<Integer, Center> process(Bitmap region, Bitmap origin, File area) {
        if (!area.exists()) {
            return null;
        }

        Map<Integer, Integer> areaColorMap = LocalStore.loadColorMap(area);

        if (areaColorMap == null) {
            return null;
        }

        Map<Integer, Center> centerMap = process(region, origin);

        Iterator<Map.Entry<Integer, Center>> it = centerMap.entrySet().iterator();

        while (it.hasNext()) {
            Map.Entry<Integer, Center> entry = it.next();
            if (!areaColorMap.containsKey(entry.getKey())) {
                it.remove();
            }
        }

        return centerMap;
    }

    private int[] mCalcPix;
    private int[] mForePix;
    private int mWidth = 1024;
    private int mHeight = 1024;

    private AlgorithmCenter(int[] calcPixels, int[] originPixels) {
        this.mCalcPix = calcPixels;
        this.mForePix = originPixels;
    }

    public static Map<Integer, Center> process(Bitmap region, Bitmap origin) {

        int[] regionPixels = new int[1024 * 1024];
        int[] originPixels = new int[2048 * 2048];
        region.getPixels(regionPixels, 0, 1024, 0, 0, 1024, 1024);
        origin.getPixels(originPixels, 0, 2048, 0, 0, 2048, 2048);

        for (int i = 0; i < 1024 * 1024; i++) {
            regionPixels[i] = (regionPixels[i] & 0x00ffffff);
        }

        HashMap<Integer, FloodFillArea> areaMap = new HashMap<>();
        preFill(areaMap, regionPixels, 1024, 1024);
        removeBadAreas(areaMap);
        Map<Integer, Center> map = new AlgorithmCenter(regionPixels, originPixels)
                .calcCenterMap(areaMap);
        return map;
    }

    private Map<Integer, Center> calcCenterMap(HashMap<Integer, FloodFillArea> areaMap) {

        Map<Integer, Center> centerMap = new HashMap<>(areaMap.size());
        for (Map.Entry<Integer, FloodFillArea> entry : areaMap.entrySet()) {
            Center center = calcArea(entry.getKey(), entry.getValue());
            if (center != null) {
                centerMap.put(entry.getKey(), center);
            }
        }

        return centerMap;

    }

    private static void preFill(HashMap<Integer, FloodFillArea> areaMap,
                                int[] mCalculatePixels,
                                int mWidth, int mHeight) {

        //初始化mAreaMap
        for (int x = 0; x < mWidth; x++) {
            for (int y = 0; y < mHeight; y++) {
                int pxIdx = (mWidth * y) + x;
                int number = mCalculatePixels[pxIdx]; //所以number是pxIdx位置的RGB
                if (number <= 0) { // color=0x000000的点不操作
                    continue;
                }
                FloodFillArea area = areaMap.get(number);//所以area是number颜色对应的区域
                if (area == null) {
                    area = new FloodFillArea();
                    area.init(x, y); //初始化为一个点
                    areaMap.put(number, area); //所以mAreaMap存放颜色值对应的area
                    continue;
                }
                area.record(x, y);
            }
        }
    }

    private static void removeBadAreas(HashMap<Integer, FloodFillArea> areaMap) {
        final int w = 1024;
        final int h = 1024;
        List<Integer> badAreas = new LinkedList<>();
        Iterator<Map.Entry<Integer, FloodFillArea>> it = areaMap.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<Integer, FloodFillArea> entry = it.next();
            FloodFillArea area = entry.getValue();

            if (area.getWidth() * area.getHeight() <= 2) {
                it.remove();
                badAreas.add(entry.getKey());
            }
        }

        if (!badAreas.isEmpty()) {
            KLog.i("remove bad exAreas:" + badAreas.toString());
        }
    }

    private Center calcArea(int number, FloodFillArea area) {

        if (number > 10000) {
            KLog.i("Ignore large number " + number);
            return null;
        }

        KLog.i(TAG, "calcArea " + number);

        int areaW = area.right - area.left;
        int areaH = area.bottom - area.top;
        int min = Math.min(areaW, areaH);
        int searchStart = Math.min(min, 100);

        for (int i = searchStart; i > 0; i--) {
            int[] xy = calcAreaForSize(number, area, i);
            if (xy != null) {
                Center center = new Center();
                center.x = xy[0];
                center.y = xy[1];
                center.size = i;
                return center;
            }
            if(i > 80) {
                i--;
            }
        }

        KLog.i("calcAreaForSize first failed, try blur");

        for (int i = searchStart; i > 0; i--) {
            int[] xy = calcAreaForSizeBlur(number, area, i);
            if (xy != null) {
                Center center = new Center();
                center.x = xy[0];
                center.y = xy[1];
                center.size = i;
                return center;
            }
            if(i > 80) {
                i--;
            }
        }

        KLog.i("calcAreaForSize second failed, try cal small");

        Center center = calcAreaForSmall(number, area);

        if (center == null) {
            KLog.i("calcAreaForSize final failed, return null");
            return null;
        }

        if(center.size >= 80) {
            KLog.w("large area found " + number + "--" + center.size);
        }

        return center;
    }

    private int[] calcAreaForSize(int number, FloodFillArea area, int size) {
        KLog.d(TAG, "calcAreaForSize " + number + "," + size);
        final int startX = area.left + size;
        final int startY = area.top + size;
        final int endX = area.right - size;
        final int endY = area.bottom - size;

        if (startX >= endX || startY >= endY) {
            return null;
        }

        for (int i = startX; i < endX; i++) {
            for (int j = startY; j < endY; j++) {
                if (isPointCenter(number, i, j, size)) {
                    int[] xy = new int[]{i, j};
                    return xy;
                }
            }
        }

        return null;
    }

    private int[] calcAreaForSizeBlur(int number, FloodFillArea area, int size) {
        KLog.d(TAG, "calcAreaForSizeBlur " + number + "," + size);
        final int startX = area.left + size;
        final int startY = area.top + size;
        final int endX = area.right - size;
        final int endY = area.bottom - size;

        if (startX >= endX || startY >= endY) {
            return null;
        }

        for (int i = startX; i < endX; i++) {
            for (int j = startY; j < endY; j++) {
                if (isPointCenterBlur(number, i, j, size)) {
                    int[] xy = new int[]{i, j};
                    return xy;
                }
            }
        }

        return null;
    }

    private Center calcAreaForSmall(int number, FloodFillArea area) {
        KLog.d(TAG, "calcAreaForSizeSmall" + number);
        final int startX = area.left;
        final int startY = area.top;
        final int endX = area.right;
        final int endY = area.bottom;

        if (startX >= endX || startY >= endY) {
            return null;
        }

        for (int i = startX; i < endX; i++) {
            for (int j = startY; j < endY; j++) {
                if (isPointCenterBlur(number, i, j, 2)) {
                    Center center = new Center();
                    center.x = i;
                    center.y = j;
                    center.size = 2;
                    return center;
                }
            }
        }

        for (int i = startX; i < endX; i++) {
            for (int j = startY; j < endY; j++) {
                if (isPointCenterBlur(number, i, j, 1)) {
                    Center center = new Center();
                    center.x = i;
                    center.y = j;
                    center.size = 1;
                    return center;
                }
            }
        }

        for (int i = startX; i < endX; i++) {
            for (int j = startY; j < endY; j++) {
                if (isPointInArea(number, i, j)) {
                    Center center = new Center();
                    center.size = 0;
                    center.x = i;
                    center.y = j;
                    return center;
                }
            }
        }

        return null;
    }


    private boolean isPointCenter(int number, int x, int y, int size) {

        int[] ii = {-1, 0, 1};

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                int a = x + ii[i] * size;
                int b = y + ii[j] * size;
                if (!isPointInArea(number, a, b)) {
                    return false;
                }

                if (isPointBlack(number, a, b)) {
                    return false;
                }
            }
        }

        for (int i = x - size; i < x + size; i++) {
            for (int j = y - size; j < y + size; j++) {
                if (!isPointInArea(number, i, j)) {
                    return false;
                }

                if (isPointBlack(number, i, j)) {
                    return false;
                }
            }
        }

        return true;
    }

    private boolean isPointCenterBlur(int number, int x, int y, int size) {

        int[] ii = {-1, 0, 1};

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                int a = x + ii[i] * size;
                int b = y + ii[j] * size;
                if (!isPointInArea(number, a, b)) {
                    return false;
                }

                if (isPointBlackDeeper(number, a, b)) {
                    return false;
                }
            }
        }

        for (int i = x - size; i < x + size; i++) {
            for (int j = y - size; j < y + size; j++) {
                if (!isPointInArea(number, i, j)) {
                    return false;
                }

                if (isPointBlackDeeper(number, i, j)) {
                    return false;
                }
            }
        }

        return true;
    }

    private boolean isPointInArea(int number, int x, int y) {
        if (x < 0 || x >= mWidth || y < 0 || y >= mHeight) {
            return false;
        }
        int calcColor = mCalcPix[mWidth * y + x];
        return (number) == calcColor;
    }

    private boolean isPointBlack(int number, int x, int y) {

        x = x * 2;
        y = y * 2;
        int color = mForePix[y * mWidth * 2 + x];

        int a = Color.alpha(color);

        if (a > 100) {
            return true;
        }

        return false;
    }

    private boolean isPointBlackDeeper(int number, int x, int y) {

        x = x * 2;
        y = y * 2;
        int color = mForePix[y * mWidth * 2 + x];

        int a = Color.alpha(color);

        if (a > 200) {
            return true;
        }

        return false;
    }
}
