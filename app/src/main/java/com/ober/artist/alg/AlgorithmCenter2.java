package com.ober.artist.alg;

import android.graphics.Bitmap;

import com.meevii.color.fill.filler.FillColorFillerN;

/**
 * Created by ober on 19-3-27.
 */
public class AlgorithmCenter2 {


    public static String process(Bitmap origin, Bitmap region) {
        return FillColorFillerN.nGenCenterStr(region, origin);
    }
}
