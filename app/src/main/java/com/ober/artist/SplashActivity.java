package com.ober.artist;

import android.Manifest;
import android.animation.Animator;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.util.Pair;
import androidx.core.view.ViewCompat;
import androidx.appcompat.app.AppCompatActivity;

import android.transition.ChangeBounds;
import android.transition.ChangeImageTransform;
import android.transition.TransitionSet;
import android.view.ViewAnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.ober.artist.login.DDLoginActivity;
import com.ober.artist.ui.lotties.LottieListActivity;
import com.ober.artist.ui.pics3.PicRecordActivity3;
import com.ober.artist.utils.DpPxUtil;
import com.ober.artist.utils.SToast;

/**
 * Created by ober on 18-12-5.
 */
public class SplashActivity extends AppCompatActivity {

    private Handler handler;

    private ImageView imageView;
    private TextView tvPbn;
    private TextView tvTitle;
    private TextView tvVersion;

    private static final int REQUEST_LOGIN = 111;

    private ProgressDialog mDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        handler = new Handler();
        imageView = findViewById(R.id.iv);
        tvPbn = findViewById(R.id.tv_pbn);
        tvTitle = findViewById(R.id.tv_title);
        tvVersion = findViewById(R.id.tv_version);

        TextView tvVersion = findViewById(R.id.tv_version);
        tvVersion.setText("v" + BuildConfig.VERSION_NAME);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int r = checkCallingOrSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            int r2 = checkCallingOrSelfPermission(Manifest.permission.CAMERA);
            if (r == PackageManager.PERMISSION_DENIED
                || r2 == PackageManager.PERMISSION_DENIED) {
                requestPermissions(new String[]{
                                Manifest.permission.CAMERA,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.READ_EXTERNAL_STORAGE},
                        1);
            } else {
                scheduleSkip();
            }

        } else {
            int r = checkCallingOrSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            if (r == PackageManager.PERMISSION_DENIED) {
                SToast.show("Permission Denied");
                finish();
                return;
            }
            r = checkCallingOrSelfPermission(Manifest.permission.CAMERA);
            if (r == PackageManager.PERMISSION_DENIED) {
                SToast.show("Permission Denied");
                finish();
                return;
            }
        }

        int w = DpPxUtil.dp2px(this, 100);
        imageView.post(() -> {
            if (!ViewCompat.isAttachedToWindow(imageView)) {
                return;
            }
            Animator mAnim = ViewAnimationUtils.createCircularReveal(imageView,
                    w / 2,
                    w / 2,
                    0,
                    w / 2 + 1);
            mAnim.start();
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 1) {
            if (grantResults[0] == grantResults[1]
                    && grantResults[1] == grantResults[2]
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                scheduleSkip();
            } else {
                SToast.show("Good Bye");
                finish();
            }
        }
    }

    private void scheduleSkip() {
        handler.postDelayed(this::enterByUsage,
                1500);
    }

    private void enterByUsage() {
        if(BuildConfig.FLAVOR.equals("color")) {
            //SUserManager.showDialog(this, skipToColorTask);
            handler.post(() -> gotoDDLogin());
        } else {
            LottieListActivity.start(this);
        }
    }

    private void gotoDDLogin() {

        if(getWindow() == null) {
            return;
        }

        ChangeBounds changeBounds = new ChangeBounds();
        ChangeImageTransform changeImageTransform = new ChangeImageTransform();
        TransitionSet set = new TransitionSet();
        set.addTransition(changeBounds);
        set.addTransition(changeImageTransform);
        set.setDuration(500);

        Bundle opt = ActivityOptionsCompat.makeSceneTransitionAnimation(this,
                Pair.create(imageView, imageView.getTransitionName()),
                Pair.create(tvPbn, tvPbn.getTransitionName()),
                Pair.create(tvTitle, tvTitle.getTransitionName()),
                Pair.create(tvVersion, tvVersion.getTransitionName()))
                .toBundle();

        Intent intent = new Intent(this, DDLoginActivity.class);
        ActivityCompat.startActivityForResult(this, intent, REQUEST_LOGIN, opt);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REQUEST_LOGIN) {
            if(resultCode == RESULT_OK) {
                mDialog = new ProgressDialog(this);
                mDialog.setMessage("数据加载中");
                mDialog.show();
                mDialog.setCancelable(false);
                Runnable r = () -> {
                    RecoveryLogic.recovery();
                    runOnUiThread(() -> {
                        mDialog.dismiss();
                        PicRecordActivity3.start(SplashActivity.this);
                        finish();
                    });
                };
                new Thread(r).start();
            } else {
                finish();
            }
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        handler.removeCallbacksAndMessages(null);
    }
}
