package com.ober.artist;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ober.artist.utils.SToast;

/**
 * Created by ober on 18-11-27.
 */
@Deprecated
public class SUserManager {


    private static final String KEY_USER_NAME = "debug_splash_n_remind";
    private static final String PREF_NAME = "UserPref";

    private SharedPreferences mPref;

    private SUserManager() {
        mPref = App.getInstance()
                .getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
    }

    public static String getUserName() {
        return new SUserManager()
                .mPref.getString(KEY_USER_NAME, null);
    }

    public static void showDialog(Context c, Runnable callback) {
        Dialog d = new InputDialog(c, callback);
        d.setCancelable(false);
        d.show();
        try {
            WindowManager.LayoutParams lp = d.getWindow().getAttributes();
            lp.width = (int) (c.getResources().getDisplayMetrics().widthPixels * 0.6);
            d.getWindow().setAttributes(lp);
        } catch (Exception ignore) {
        }
    }

    private static class InputDialog extends Dialog {

        Runnable task;
        SUserManager userManager;

        InputDialog(@NonNull Context context, Runnable task) {
            super(context, R.style.ColorImgPrepareDialog);
            this.task = task;
            userManager = new SUserManager();
        }

        EditText et;
        Button btn;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            CardView fl = new CardView(getContext());
            fl.setCardElevation(10);
            fl.setCardBackgroundColor(Color.parseColor("#F1F4F5"));
            fl.setRadius(16);
            fl.setLayoutParams(new ViewGroup.LayoutParams(-2, -2));
            LinearLayout ll = new LinearLayout(getContext());
            ll.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
            fl.addView(ll, new ViewGroup.LayoutParams(-1, -1));
            setContentView(fl);
            ll.setOrientation(LinearLayout.VERTICAL);
            ll.setGravity(Gravity.CENTER_HORIZONTAL);
            et = new EditText(getContext());
            btn = new Button(getContext());

            TextView tvTitle = new TextView(getContext());
            LinearLayout.LayoutParams tvLp = new LinearLayout.LayoutParams(-2, -2);
            tvLp.setMargins(70, 20, 70, 20);
            ll.addView(tvTitle, tvLp);
            tvTitle.setText("User Name");
            tvTitle.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
            tvTitle.setTextColor(getContext().getResources().getColor(R.color.colorBlack));


            LinearLayout.LayoutParams etLp = new LinearLayout.LayoutParams(-1, -2);
            etLp.gravity = Gravity.CENTER_HORIZONTAL;
            etLp.topMargin = 20;
            etLp.leftMargin = 20;
            etLp.rightMargin = 20;
            et.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
            et.setInputType(EditorInfo.TYPE_CLASS_TEXT);
            et.setTextColor(getContext().getResources().getColor(R.color.colorBlack));
            et.setImeOptions(EditorInfo.IME_ACTION_DONE);
            ll.addView(et, etLp);

            LinearLayout.LayoutParams btnLp = new LinearLayout.LayoutParams(-2, -2);
            btnLp.gravity = Gravity.CENTER_HORIZONTAL;
            btnLp.topMargin = 20;
            btnLp.bottomMargin = 30;

            ll.addView(btn, btnLp);

            final String who = userManager.mPref.getString(KEY_USER_NAME, "");
            et.setText(who);

            btn.setText("OK");

            btn.setOnClickListener(v -> {
                String s = et.getText().toString();
                if (TextUtils.isEmpty(s)) {
                    SToast.show("? ? ?");
                }

                userManager.mPref.edit()
                        .putString(KEY_USER_NAME, s)
                        .apply();

                SToast.show("Hello " + s);
                dismiss();
                task.run();
            });

        }
    }


}
