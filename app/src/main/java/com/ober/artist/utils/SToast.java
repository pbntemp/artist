package com.ober.artist.utils;

import android.widget.Toast;

import com.ober.artist.App;

/**
 * Created by ober on 2018/12/5.
 */
public class SToast {

    public static void show(String msg) {
        Toast.makeText(App.getInstance(), msg, Toast.LENGTH_SHORT).show();
    }

}
