package com.ober.artist.utils;


import androidx.core.text.TextUtilsCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.LayoutDirection;
import android.view.View;
import android.widget.RelativeLayout;

import com.ober.artist.App;

import java.util.Locale;

public class InnerUtil {

    /**
     * 将指定url转为裁剪图片的url
     */
    public static String getScaleImageUrl(String url, int width, int height) {
        if (url.startsWith("http") && width > 0) {
            String[] array = url.split("/");
            StringBuilder sub = new StringBuilder();
            sub.append(array[0]).append("//").append(array[2]);
            sub.append("/imageView/crop/").append(width).append("/").append(height).append("/");
            for (int i = 3; i < array.length; i++) {
                sub.append(array[i]);
                if (i < array.length - 1) {
                    sub.append("/");
                }
            }
            return sub.toString();
        }
        return url;
    }


    public static int getGalleryImageDetailsViewWH() {
        return DevicesUtil.getScreenWidth(App.getInstance())
                - DpPxUtil.dp2px(App.getInstance(), 12);
    }

    public static boolean handleRecyclerViewBack(RecyclerView recyclerView) {
        LinearLayoutManager manager = (LinearLayoutManager) recyclerView.getLayoutManager();
        if (manager.findFirstVisibleItemPosition() != 0) {
            recyclerView.smoothScrollToPosition(0);
            return true;
        }
        return false;
    }

    /**
     * 判断是否是从右到左布局，比如阿拉伯语言
     *
     * @return
     */
    public static boolean isRTL() {
        return TextUtilsCompat.getLayoutDirectionFromLocale(Locale.getDefault()) == LayoutDirection.RTL;
    }


    public static void resetViewAlignForRTL(View imageView) {
        if (isRTL() && imageView != null) {
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) imageView.getLayoutParams();
            params.removeRule(RelativeLayout.ALIGN_PARENT_RIGHT);
            params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
            imageView.setLayoutParams(params);
        }
    }
}
