package com.ober.artist.utils;

import com.ober.artist.color.data.PbnResourceConfig;

import java.io.File;

/**
 * Created by ober on 2020-02-05.
 */
public class ResourceSizeCheckUtil {

    public static class CheckResult {
        public static final int STATE_PERFECT = 0;
        public static final int STATE_WARNING = 1;
        public static final int STATE_TOO_LARGE = 2;

        public int pngState;
        public int pdfState;

        public boolean isPerfect;
        public boolean isTooLarge;
        public String msg;
    }

    public static CheckResult checkResource(File png, File pdf) {
        long pngLen = png.length();
        long pdfLen = pdf.length();
        PbnResourceConfig.SizeLimit pngLimit = PbnResourceConfig.getSizeLimit(PbnResourceConfig.RES_PNG);
        PbnResourceConfig.SizeLimit pdfLimit = PbnResourceConfig.getSizeLimit(PbnResourceConfig.RES_PDF);
        assert pngLimit != null;
        assert pdfLimit != null;

        final int pngState = justState(pngLen, pngLimit);
        final int pdfState = justState(pdfLen, pdfLimit);

        CheckResult rs = new CheckResult();
        rs.pngState = pngState;
        rs.pdfState = pdfState;
        if(pngState == CheckResult.STATE_PERFECT && pdfState == CheckResult.STATE_PERFECT) {
            rs.isPerfect = true;
            rs.msg = "ok";
            return rs;
        }
        rs.isPerfect = false;
        StringBuilder sb = new StringBuilder();
        if(pngState == CheckResult.STATE_TOO_LARGE) {
            sb.append("PNG文件大小超过限制(超过2mb)，无法上传\n");
            rs.isTooLarge = true;
        } else if(pngState == CheckResult.STATE_WARNING) {
            sb.append("PNG资源较大，请谨慎上传\n");
        }
        if(pdfState == CheckResult.STATE_TOO_LARGE) {
            sb.append("PDF文件大小超过限制(超过2mb)，无法上传\n");
            rs.isTooLarge = true;
        } else if(pdfState == CheckResult.STATE_WARNING) {
            sb.append("PDF资源较大，请谨慎上传\n");
        }

        rs.msg = sb.toString();

        return rs;
    }

    private static int justState(long size, PbnResourceConfig.SizeLimit limit) {
        if(size <= limit.warningLength) {
            return CheckResult.STATE_PERFECT;
        } else if(size <= limit.maxLength) {
            return CheckResult.STATE_WARNING;
        } else {
            return CheckResult.STATE_TOO_LARGE;
        }
    }

}
