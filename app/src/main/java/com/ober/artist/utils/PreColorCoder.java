package com.ober.artist.utils;

import com.ober.artist.color.data.PreColor;

import java.util.List;

public class PreColorCoder {

    public static String encode(PreColor[] preColors) {
        StringBuilder sb = new StringBuilder();
        int len = preColors.length;
        for (int i = 0; i < len; i++) {
            sb.append(encodeIntArr(preColors[i].areas));
            sb.append("#");
            sb.append(preColors[i].color.replace("#FF", ""));
            if (i != len - 1) {
                sb.append("|");
            }
        }

        return sb.toString();
    }

    public static String encode(List<PreColor> preColors) {
        StringBuilder sb = new StringBuilder();
        int len = preColors.size();
        for (int i = 0; i < len; i++) {
            sb.append(encodeIntArr(preColors.get(i).areas));
            sb.append("#");
            sb.append(preColors.get(i).color.replace("#FF", ""));
            if (i != len - 1) {
                sb.append("|");
            }
        }

        return sb.toString();
    }

    public static PreColor[] decode(String s) {
        String[] ss = s.split("\\|");
        int len = ss.length;
        PreColor[] preColors = new PreColor[len];
        for (int i = 0; i < len; i++) {
            String[] parts = ss[i].split("#");
            PreColor preColor = new PreColor();
            preColor.areas = decodeIntArr(parts[0]);
            preColor.color = "#FF" + parts[1];
            preColors[i] = preColor;
        }
        return preColors;
    }

    private static String encodeIntArr(int[] ints) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < ints.length; i++) {
            sb.append(ints[i]);
            if (i != ints.length - 1) {
                sb.append(",");
            }
        }
        return sb.toString();
    }

    private static int[] decodeIntArr(String s) {
        String[] ss = s.split(",");
        int len = ss.length;
        int[] ints = new int[len];
        for (int i = 0; i < len; i++) {
            ints[i] = Integer.valueOf(ss[i]);
        }
        return ints;
    }

}
