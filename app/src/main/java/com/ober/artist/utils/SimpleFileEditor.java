package com.ober.artist.utils;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class SimpleFileEditor {

    public static String readFromFile(String file) {
        File f = new File(file);

        StringBuilder sb = new StringBuilder();

        try {
            FileReader fileReader = new FileReader(f);
            BufferedReader reader = new BufferedReader(fileReader);
            String s;
            while ((s = reader.readLine()) != null) {
                sb.append(s);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return sb.toString();
    }

    public static void writeToFile(String file, String content) {
        File f = new File(file);
        if (f.exists()) {
            f.delete();
        }

        try {
            FileOutputStream fos = new FileOutputStream(file);
            BufferedOutputStream bos = new BufferedOutputStream(fos, 4096);
            bos.write(content.getBytes());
            bos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void appendToFile(String file, String content) {
        FileWriter fileWritter = null;
        try {
            fileWritter = new FileWriter(file, true);
            BufferedWriter bufferWritter = new BufferedWriter(fileWritter);
            bufferWritter.write(content);
            bufferWritter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
