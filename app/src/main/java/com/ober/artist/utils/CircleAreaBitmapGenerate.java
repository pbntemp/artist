package com.ober.artist.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;

import androidx.core.util.Consumer;

import com.ober.artist.App;
import com.ober.artist.R;
import com.ober.artist.color.data.Center;
import com.ober.artist.color.localdata.FileDataLoader;
import com.ober.artist.color.localdata.FileImageBean2;
import com.ober.artist.color.localdata.LocalData;
import com.ober.artist.pack.CenterGen;
import com.ober.artist.xlog.XLogger;
import com.socks.library.KLog;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Created by ober on 2019-12-09.
 */
public class CircleAreaBitmapGenerate  {

    public static class Result {

        public String errMsg;

        public int countRed;
        public int countGreen;
        public Bitmap bmp;
    }

    public static class Task extends AsyncTask<Void, Void, Result> {

        private String uid;
        private String id;
        private boolean isLocal;
        private Consumer<Result> cb;

        public Task(String uid, boolean isLocal, String id,  Consumer<Result> cb) {
            this.uid = uid;
            this.id = id;
            this.cb = cb;
            this.isLocal = isLocal;
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            this.cb = null;
        }

        @Override
        protected void onPostExecute(Result result) {
            super.onPostExecute(result);
            if(result != null && cb != null) {
                cb.accept(result);
            }
        }

        @Override
        protected Result doInBackground(Void... voids) {

            FileImageBean2 imageBean2 = FileDataLoader.load2(uid, isLocal, id);

            return generate(imageBean2, isLocal);

        }
    }


    private static Result generate(FileImageBean2 imageBean2, boolean isLocal) {

        XLogger.d("CircleAreaBitmap generate " + imageBean2.id);

        Map<Integer, Center> centerMap = CenterGen.genCenter2(imageBean2);
        if(centerMap == null) {
            Result result = new Result();
            result.errMsg = "数据错误: center";
            return result;
        }

        Map<Integer, Integer> areaColorMap = LocalData.loadColorMap(imageBean2.uid, isLocal, imageBean2.id);

        if(areaColorMap == null) {
            Result result = new Result();
            result.errMsg = "数据错误: area";
            return result;
        }

        Set<Integer> unColoredAreas = new HashSet<>();
        for(Integer areaInCenter : centerMap.keySet()) {
            if(areaColorMap.get(areaInCenter) == null) {
                unColoredAreas.add(areaInCenter);
            }
        }

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inMutable = true;

        Bitmap thumbBmp = BitmapFactory.decodeFile(imageBean2.thumb.getAbsolutePath(), options);

        if(thumbBmp == null) {
            Result result = new Result();
            result.errMsg = "数据错误: thumb";
            return result;
        }

        options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(imageBean2.region.getAbsolutePath(), options);

        if(options.outWidth == 0) {
            Result result = new Result();
            result.errMsg = "数据错误: region";
            return result;
        }

        final int w = options.outWidth;
        final int h = options.outHeight;

        if(thumbBmp.getWidth() != w) {
            thumbBmp = Bitmap.createScaledBitmap(thumbBmp, w, h, true);
        }

        int strokeWidth = (int) App.getInstance().getResources().getDimension(R.dimen.s2);
        int radius = (int) App.getInstance().getResources().getDimension(R.dimen.s10);

        Paint paint = new Paint();
        paint.setStrokeWidth(strokeWidth);
        paint.setDither(true);
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.STROKE);
        paint.setColor(Color.RED);

        if(!thumbBmp.isMutable()) {
            Bitmap b = thumbBmp;
            thumbBmp = Bitmap.createBitmap(b);
            b.recycle();
            XLogger.w("recreate bitmap for mutable");
        }

        Canvas canvas = new Canvas(thumbBmp);

        int count_red = 0;
        int count_green = 0;

        int i = 0;
        for(Integer area : unColoredAreas) {
            Log.d("CircleGen", "progress=" + i);
            i++;
            if(i % 10 == 0) {
                if(Thread.currentThread().isInterrupted()) {
                    Result result = new Result();
                    result.errMsg = "canceled";
                    return result;
                }
            }
            Center center = centerMap.get(area);
            if(center == null) {
                KLog.w("logic err");
                continue;
            }

            if(center.size == 0) {
                continue;
            }

            if(center.size == 1) {
                paint.setColor(Color.GREEN);
                count_green++;
            } else {
                paint.setColor(Color.RED);
                count_red++;
            }

            canvas.drawCircle(center.x, center.y, radius, paint);
        }

        if(Thread.currentThread().isInterrupted()) {
            Result result = new Result();
            result.errMsg = "canceled";
            return result;
        }

        Result result = new Result();
        result.bmp = thumbBmp;
        result.errMsg = "ok";
        result.countGreen = count_green;
        result.countRed = count_red;

        return result;
    }


}
