package com.ober.artist.utils;

import android.graphics.Color;

import com.ober.artist.color.data.PreColor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * Created by ober on 2019/1/10.
 */
public class PreColorSorter {

    public static List<PreColor> sort(List<PreColor> preColors, int[] sort) {
        Collections.sort(preColors, new Comparator<PreColor>() {
            @Override
            public int compare(PreColor o1, PreColor o2) {
                return indexof(sort, Color.parseColor(o1.color))
                        - indexof(sort, Color.parseColor(o2.color));
            }
        });

        return preColors;
    }

    private static int indexof(int[] ints, int a) {
        for(int i = 0; i < ints.length; i++) {
            if(a == ints[i]) {
                return i;
            }
        }
        return -1;
    }

    public static List<PreColor> defaultSort(List<PreColor> preColors) {
        Collections.sort(preColors, (o1, o2) -> o2.areas.length - o1.areas.length);

        int count = preColors.size();

        List<PreColor> a = new ArrayList<>();
        List<PreColor> b = new ArrayList<>();
        List<PreColor> c = new ArrayList<>();
        List<PreColor> d = new ArrayList<>();

        int c4 = count / 4;
        int[] cuts;

        if (count % 4 == 0) {
            cuts = new int[]{c4, c4, c4, c4};
        } else if (count % 4 == 1) {
            cuts = new int[]{c4 + 1, c4, c4, c4};
        } else if (count % 4 == 2) {
            cuts = new int[]{c4 + 1, c4 + 1, c4, c4};
        } else {
            cuts = new int[]{c4 + 1, c4 + 1, c4 + 1, c4};
        }

        Queue<PreColor> queue = new LinkedList<>();
        for (PreColor preColor : preColors) {
            queue.add(preColor);
        }

        for (int i = 0; i < cuts[0]; i++) {
            a.add(queue.poll());
        }
        for (int i = 0; i < cuts[1]; i++) {
            b.add(queue.poll());
        }
        for (int i = 0; i < cuts[2]; i++) {
            c.add(queue.poll());
        }
        for (int i = 0; i < cuts[3]; i++) {
            d.add(queue.poll());
        }

        List<PreColor> resultList = new LinkedList<>();
        resultList.addAll(b);
        resultList.addAll(a);
        resultList.addAll(d);
        resultList.addAll(c);
        return resultList;
    }
}
