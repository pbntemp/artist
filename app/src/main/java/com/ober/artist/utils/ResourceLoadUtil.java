package com.ober.artist.utils;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;

import com.ober.artist.App;

public class ResourceLoadUtil {

    public static final String ID = "id";
    public static final String LAYOUT = "layout";
    public static final String DRAWABLE = "drawable";
    public static final String STYLE = "style";
    public static final String STRING = "string";
    public static final String COLOR = "color";
    public static final String DIMEN = "dimen";

    public static int getId(Resources resource, String packageName, String name) {
        int result = -1;
        try {
            result = getIdentifier(resource, ID, packageName, name);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String getString(Resources resource, String packageName, String name) {
        String result = null;
        try {
            result = resource.getString(getIdentifier(resource, STRING, packageName, name));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static int getColor(Resources resource, String packageName, String name) {
        int result = -1;
        try {
            result = resource.getColor(getIdentifier(resource, COLOR, packageName, name));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static Drawable getDrawable(Resources resource, String packageName, String name) {
        Drawable result = null;
        try {
            result = resource.getDrawable(getIdentifier(resource, DRAWABLE, packageName, name));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static int getIdentifier(Resources resource, String type, String packageName, String name) {
        return resource.getIdentifier(name, type, packageName);
    }

    public static int getDrawableId(String resName) {
        return getIdentifier(App.getInstance().getResources(),
                ResourceLoadUtil.DRAWABLE,
                App.getInstance().getPackageName(), resName);
    }

}
