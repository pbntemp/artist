package com.ober.artist.utils;

import android.content.Context;
import android.os.Environment;
import android.os.StatFs;

import com.ober.artist.App;
import com.socks.library.KLog;

import java.io.File;

/**
 * 管理SD卡等外部存储设备的工具类。
 *
 * @author cutler
 */
public class StorageUtils {

    private static final String TAG = "StorageUtils";

    /**
     * 获取外部存储设备的剩余可用字节。
     */
    public static long getAvailableStorage() {
        KLog.v(TAG, "getAvailableStorage");
        if (isSdCardWriteAble()) {
            KLog.v(TAG, "isSdCardWriteAble=true");
            try {
                File f = App.getInstance().getExternalFilesDir("");
                if (f != null && f.exists()) {
                    String storageDirectory = f.getAbsolutePath();
                    StatFs stat = new StatFs(storageDirectory);
                    return (stat.getAvailableBlocksLong() * stat.getBlockSizeLong());
                } else {
                    return 0;
                }
            } catch (RuntimeException ex) {
                ex.printStackTrace();
                return 0;
            }
        } else {
            KLog.v(TAG, "isSdCardWriteAble=false");
            try {
                File f = App.getInstance().getFilesDir();
                if (f != null && f.exists()) {
                    String storageDirectory = f.getAbsolutePath();
                    StatFs stat = new StatFs(storageDirectory);
                    return (stat.getAvailableBlocksLong() * stat.getBlockSizeLong());
                } else {
                    return 0;
                }
            } catch (RuntimeException ex) {
                ex.printStackTrace();
                return 0;
            }
        }
    }

    /**
     * 检测出sd卡是否有足够的剩余内存可用。
     */
    public static boolean checkAvailableStorage() {
        return getAvailableStorage() >= 1024 * 1024 * 10;   //10MB
    }

    /**
     * 检测外部存储设备是否已经装载成功并可写。
     */
    public static boolean isSdCardWriteAble() {
        return Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState());
    }

    /**
     * 获取本地目录。
     */
    public static File getDiskFileDir(Context context, String uniqueName) {
        String filePath = null;
        // 若SD卡已就绪，或者SD卡不可移除。
        if (isSdCardWriteAble()) {
            try {
                // 路径为：/Android/data/packageName/file
                filePath = context.getExternalFilesDir("").getPath();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (filePath == null) {
            // 路径为：/data/data/packageName/file
            filePath = context.getFilesDir().getPath();
        }
        File file = new File(filePath, uniqueName);
        if (!file.exists()) {
            file.mkdirs();
        }
        return file;
    }


    /**
     * 获取本地缓存目录。
     */
    public static File getDiskCacheDir(Context context, String uniqueName) {
        String cachePath = null;
        // 若SD卡已就绪，或者SD卡不可移除。
        if (isSdCardWriteAble()) {
            try {
                // 缓存路径为：/Android/data/packageName/cache
                cachePath = context.getExternalCacheDir().getPath();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (cachePath == null) {
            // 缓存路径为：/data/data/packageName/cache
            cachePath = context.getCacheDir().getPath();
        }
        return new File(cachePath, uniqueName);
    }
}