package com.ober.artist.utils;

import android.content.Context;
import android.telephony.TelephonyManager;
import android.text.TextUtils;

import java.util.Locale;

public class LocaleUtil {

    public static final String LANGUAGE_JP = "ja";

    public static String getLanguage() {
        String language = "en";
        String l = Locale.getDefault().getLanguage();
        if (LANGUAGE_JP.equals(l)) {
            language = l;
        }
        return language;
    }

    public static String getOtherLanguage() {
        return Locale.getDefault().getLanguage();
    }

    public static boolean isKoLanguage(Context context) {
        if (context == null) {
            return false;
        }
        return context.getResources().getConfiguration().locale.getLanguage().equals("ko");
    }

    public static String getCurrentLanguage(Context context) {
        return context.getResources().getConfiguration().locale.getLanguage();
    }

    public static String getCurrentCountryCode(Context context) {
        return context.getResources().getConfiguration().locale.getCountry();
    }

    public static boolean isUSCountryCode(Context context) {
        return "US".equals(getCurrentCountryCode(context));
    }

    public static String getCountry() {
        return Locale.getDefault().getCountry();
    }

    public static String getSimCountry(Context context) {
        try {
            TelephonyManager manager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            if (manager != null) {
                String country = manager.getSimCountryIso();
                if (!TextUtils.isEmpty(country)) {
                    return country;
                }
            }
        } catch (Exception | Error e) {
            e.printStackTrace();
        }

        return Locale.getDefault().getCountry();
    }
}
