package com.ober.artist.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Arrays;

/**
 * Created by ober on 19-9-3.
 */
public class Files {

    public static void rm_rf(File f) {

        if(f.isFile()) {
            f.delete();
            return;
        }

        File[] files = f.listFiles();
        if(files != null && files.length > 0) {
            for(File sub : files) {
                rm_rf(sub);
            }
        }
        f.delete();
    }

    public static void cleanDir(File dir) {
        if(dir.isDirectory()) {
            File[] files = dir.listFiles();
            if(files != null) {
                for(File f : files) {
                    String name = f.getName();
                    if (name.startsWith("__MACOSX") || name.startsWith(".DS_")) {
                        rm_rf(f);
                    }
                }
            }
        }
    }

    public static String fileTree(File dir) {
        Printer printer = new Printer();
        printer.listPath(dir);
        return printer.sb.toString();
    }

    private static class Printer {
        int indentLevel;

        StringBuilder sb;

        Printer() {
            indentLevel = 0;
            sb = new StringBuilder();
        }

        void listPath(File path) {
            File files[];
            indentLevel++;
            files = path.listFiles();
            if(files != null) {
                Arrays.sort(files);
                for (int i = 0, n = files.length; i < n; i++) {
                    for (int indent = 0; indent < indentLevel; indent++) {
                        sb.append("  ");
                    }
                    File file = files[i];
                    if (file.isDirectory()) {
                        sb.append(file.getAbsolutePath())
                                .append("\n");
                    } else {
                        sb.append(file.getName())
                                .append("\n");
                    }
                    if (files[i].isDirectory()) {
                        listPath(files[i]);
                    }
                }
            }
            indentLevel--;
        }

    }

    /**
     * 复制单个文件
     *
     * @param oldPathName String 原文件路径+文件名
     * @param newPathName String 复制后路径+文件名
     * @return <code>true</code> if and only if the file was copied;
     * <code>false</code> otherwise
     */
    public static boolean copyFile(String oldPathName, String newPathName) {
        try {
            File oldFile = new File(oldPathName);

            if (!oldFile.exists() || !oldFile.isFile() || !oldFile.canRead()) {
                return false;
            }
            FileInputStream fileInputStream = new FileInputStream(oldPathName); //读入原文件
            FileOutputStream fileOutputStream = new FileOutputStream(newPathName);
            byte[] buffer = new byte[1024];
            int byteRead;
            while ((byteRead = fileInputStream.read(buffer)) != -1) {
                fileOutputStream.write(buffer, 0, byteRead);
            }
            fileInputStream.close();
            fileOutputStream.flush();
            fileOutputStream.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

}
