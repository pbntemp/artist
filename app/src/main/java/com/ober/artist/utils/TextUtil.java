package com.ober.artist.utils;

import android.graphics.Color;
import android.text.TextUtils;

/**
 * Created by DS on 12/15/16.
 */

public class TextUtil {
    public static boolean isTextEmpty(String string) {
        return TextUtils.isEmpty(string) || string.equals("null");
    }

    public static String upperFirstCase(String string) {
        String ret = "";
        if (!TextUtil.isTextEmpty(string)) {
            ret = string.toLowerCase();
            ret = Character.toString(ret.charAt(0)).toUpperCase() + ret.substring(1);
        }
        return ret;
    }

    public static String upperAllFirstCase(String string) {
        String[] array = string.split("\\s");
        StringBuilder sub = new StringBuilder();
        for (String item : array) {
            sub.append(upperFirstCase(item)).append(" ");
        }
        return sub.toString();
    }

    public static int stringToColor(String colorStr) {
        return Color.parseColor("#" + colorStr);
    }

    /**
     * 将时间转为 HH:MM:SS的格式
     */
    public static String formatDuring(long mss) {
        long hours = (mss % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60);
        long minutes = (mss % (1000 * 60 * 60)) / (1000 * 60);
        long seconds = (mss % (1000 * 60)) / 1000;
        return String.format("%02d:%02d:%02d", hours, minutes, seconds);
    }

    public static String getEmojiStringByUnicode(int unicode) {
        return new String(Character.toChars(unicode));
    }

    public static float getPriceFromString(String str) {
        float price = 0;
        if (!isTextEmpty(str)) {
            int index = -1;
            char[] array = str.toCharArray();
            for (int i = 0; i < array.length; i++) {
                if (Character.isDigit(array[i])) {
                    index = i;
                    break;
                }
            }
            try {
                price = Float.parseFloat(str.substring(index));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return price;
    }
}
