package com.ober.artist.utils;

import android.content.res.AssetManager;
import android.database.Cursor;
import android.graphics.Bitmap;

import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;

/**
 * IO读取的工具类
 *
 * @author cutler
 */
public class IOUtil {
    /**
     * 将InputStream中的数据读出来，放入一个byte[]中。
     */
    public static byte[] inputStream2ByteArray(InputStream input) {
        if (input == null) {
            return null;
        }
        ByteArrayOutputStream output = null;
        byte[] data = null;
        try {
            data = new byte[1024 * 32];
            output = new ByteArrayOutputStream();
            int len;
            while ((len = input.read(data)) != -1) {
                output.write(data, 0, len);
            }
            data = output.toByteArray();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close(input);
            close(output);
        }
        return data;
    }

    /**
     * 将InputStream中的数据以字符串的形式返回。
     */
    public static String inputStream2String(InputStream input, String encoding) {
        String str = null;
        try {
            str = new String(inputStream2ByteArray(input), encoding);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return str;
    }

    /**
     * 将InputStream中的数据写入到OutputStream中，完毕后关闭两个流。
     */
    public static boolean inputStream2OutputStream(InputStream input, OutputStream output) {
        boolean isFinish = true;
        try {
            byte[] array = new byte[1024];
            int len = -1;
            while ((len = input.read(array)) != -1) {
                output.write(array, 0, len);
            }
        } catch (Exception e) {
            e.printStackTrace();
            isFinish = false;
        } finally {
            close(input);
            close(output);
        }
        return isFinish;
    }

    /**
     * 将str写入到OutputStream中。
     */
    public static void stringToOutputStream(String str, OutputStream output) {
        if (str != null && output != null) {
            try {
                output.write(str.getBytes());
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                close(output);
            }
        }
    }

    /**
     * 关闭流。
     */
    public static void close(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    public static void closeCursor(Cursor c) {
        if (c != null) {
            try {
                c.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void saveBitmapTo(Bitmap bitmap, File dest) {
        FileOutputStream fout = null;
        try {
            fout = new FileOutputStream(dest);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fout);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close(fout);
        }
    }


    public static boolean copyAssetFolder(AssetManager assetManager,
                                          String fromAssetPath, String toPath) {
        try {
            String[] files = assetManager.list(fromAssetPath);
            new File(toPath).mkdirs();
            boolean res = true;
            for (String file : files)
                if (file.contains("."))
                    res &= copyAssetFile(assetManager,
                            fromAssetPath + "/" + file,
                            toPath + "/" + file);
                else
                    res &= copyAssetFolder(assetManager,
                            fromAssetPath + "/" + file,
                            toPath + "/" + file);
            return res;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean copyAssetFile(AssetManager assetManager,
                                        String fromAssetPath, String toPath) {
        InputStream in = null;
        OutputStream out = null;
        try {
            in = assetManager.open(fromAssetPath);
            new File(toPath).createNewFile();
            out = new FileOutputStream(toPath);
            copyFile(in, out);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            if (out != null) {
                try {
                    out.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private static void copyFile(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        int read;
        while ((read = in.read(buffer)) != -1) {
            out.write(buffer, 0, read);
        }
    }
}
