package com.ober.artist.utils;

import android.content.Context;

import androidx.appcompat.app.AlertDialog;

/**
 * Created by ober on 2019-10-30.
 */
public class AlertDialogHelper {

    public static AlertDialog showSimple(Context context, String msg, Runnable onNext) {
        return new AlertDialog.Builder(context)
                .setMessage(msg)
                .setPositiveButton("确定", (dialog, which) -> onNext.run())
                .setNegativeButton("取消", (dialog, which) -> {

                }).show();
    }



}
