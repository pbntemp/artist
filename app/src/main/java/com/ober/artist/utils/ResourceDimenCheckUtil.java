package com.ober.artist.utils;

import android.graphics.BitmapFactory;

import com.ober.artist.BuildConfig;
import com.ober.artist.color.localdata.FileImageBean2;
import com.ober.artist.net.bean.Picture;
import com.socks.library.KLog;

import java.io.File;

/**
 *
 *
 PNG
 PDF
 region
 colored
 普通素材
 2048*2048
 2048*2048
 1024*1024
 N/A
 普通壁纸素材
 1500*2668
 750*1334
 750 × 1334
 N/A
 彩绘素材
 2048*2048
 2048*2048
 2048*2048
 2048*2048
 彩绘壁纸素材
 1500*2668
 1500*2668
 1500*2668
 1500*2668

 暂不校验缩略图


 * Created by ober on 2020-02-14.
 */
public class ResourceDimenCheckUtil {


    public static boolean check(FileImageBean2 bean, String[] errmsg) {

        String TAG = "ResourceDimenCheckUtil";
        if(BuildConfig.DEBUG) {
            KLog.d(TAG, "check " + GsonUtil.toJson(bean));
        }

        int type = bean.pbntype;
        int[] pngSize;
        int[] regionSize;
        int[] coloredSize;

        if(type == Picture.PBN_TYPE_COLORED) {
            pngSize = new int[] {2048, 2048};
            regionSize = new int[] {2048, 2048};
            coloredSize = new int[] {2048, 2048};
        } else if(type == Picture.PBN_TYPE_NORMAL) {
            pngSize = new int[] {2048, 2048};
            regionSize = new int[] {1024, 1024};
            coloredSize = null;
        } else if(type == Picture.PBN_TYPE_WALL_NORMAL) {
            pngSize = new int[] {1500, 2668};
            regionSize = new int[] {750, 1334};
            coloredSize = null;
        } else if(type == Picture.PBN_TYPE_WALL_COLORED) {
            pngSize = new int[] {1500, 2668};
            regionSize = new int[] {1500, 2668};
            coloredSize = new int[] {1500, 2668};
        } else {
            return false;
        }

        if(pngSize != null) {
            if(!checkFile(bean.origin, pngSize)) {
                errmsg[0] = "png尺寸异常";
                return false;
            }
        }

        if(regionSize != null) {
            if(!checkFile(bean.region, regionSize)) {
                errmsg[0] = "region尺寸异常";
                return false;
            }
        }

        if(coloredSize != null) {
            if(!checkFile(bean.colored, coloredSize)) {
                errmsg[0] = "colored尺寸异常";
                return false;
            }
        }

        errmsg[0] = "ok";

        if(BuildConfig.DEBUG) {
            KLog.d(TAG, "check result ok");
        }

        return true;
    }

    private static boolean checkFile(File f, int[] size) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        try {
            BitmapFactory.decodeFile(f.getAbsolutePath(), options);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        KLog.d("ResourceDimenCheckUtil.checkFile", "file:" + f.getAbsolutePath() + " width:" + options.outWidth +  " " + size[0] + " height:" + options.outHeight + " " + size[1]);
        return options.outWidth == size[0] && options.outHeight == size[1];
    }

    private static boolean checkFile(File f, int[] size1, int[] size2) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        try {
            BitmapFactory.decodeFile(f.getAbsolutePath(), options);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        if(options.outWidth == size1[0] && options.outHeight == size1[1]) {
            return true;
        }

        return options.outWidth == size2[0] && options.outHeight == size2[1];
    }


}
