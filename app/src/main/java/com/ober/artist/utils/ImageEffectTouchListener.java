package com.ober.artist.utils;

import android.graphics.Color;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

/**
 * Created by ober on 18-8-8.
 */
public class ImageEffectTouchListener implements View.OnTouchListener {

    private ImageView iv;

    private final int color;

    public ImageEffectTouchListener(ImageView iv) {
        this(iv, Color.parseColor("#33000000"));
    }

    public ImageEffectTouchListener(ImageView iv, int color) {
        this.iv = iv;
        this.color = color;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_UP) {
            iv.clearColorFilter();
        } else if (event.getAction() == MotionEvent.ACTION_DOWN) {
            iv.setColorFilter(color);
        } else if (event.getAction() == MotionEvent.ACTION_CANCEL) {
            iv.clearColorFilter();
        } else if (event.getAction() == MotionEvent.ACTION_OUTSIDE) {
            iv.clearColorFilter();
        }
        return false;
    }
}
