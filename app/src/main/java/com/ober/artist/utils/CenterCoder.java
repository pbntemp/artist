package com.ober.artist.utils;

import com.ober.artist.color.data.Center;

import java.util.HashMap;
import java.util.Map;

public class CenterCoder {


    public static String encode(Map<Integer, Center> centerMap) {
        StringBuilder sb = new StringBuilder();
        int len = centerMap.size();
        int index = 0;
        for (Map.Entry<Integer, Center> entry : centerMap.entrySet()) {
            Center center = entry.getValue();
            sb.append(entry.getKey())
                    .append(",")
                    .append(center.x)
                    .append(",")
                    .append(center.y)
                    .append(",")
                    .append(center.size);
            if (index != len - 1) {
                sb.append("|");
            }
            index++;
        }
        return sb.toString();
    }

    public static HashMap<Integer, Center> decode(String s) {
        String[] ss = s.split("\\|");
        HashMap<Integer, Center> map = new HashMap<>();
        for (String part : ss) {
            String[] parts = part.split(",");
            Center center = new Center();
            int key = Integer.valueOf(parts[0]);
            center.x = Integer.valueOf(parts[1]);
            center.y = Integer.valueOf(parts[2]);
            center.size = Integer.valueOf(parts[3]);
            map.put(key, center);
        }
        return map;
    }

}
