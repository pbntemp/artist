package com.ober.artist.utils;

import android.text.TextUtils;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

public class NumberUtil {

    private static final String SUFFIX = "kMGTPE";

    public static String format(int n) {
        boolean negative = n < 0;
        int i = Math.abs(n);
        String out;
        if (i < 10000) {
            out = NumberFormat.getInstance().format(n);
        } else {
            int exp = (int) (Math.log(i) / Math.log(1000));
            out = String.format(Locale.US, "%.1f %c", i / Math.pow(1000, exp), SUFFIX.charAt(exp - 1));
        }
        return (negative) ? "-" + out : out;
    }

    public static int parse(String num) {
        int out = 0;

        if (TextUtils.isEmpty(num)) {
            return out;
        }

        try {
            out = NumberFormat.getInstance().parse(num).intValue();
        } catch (ParseException pe) {

        }

        return out;
    }

    public static String numberToKString(int number, int maxNumber) {
        if (number >= maxNumber) {
            return new StringBuilder().append(number / maxNumber * (maxNumber / 1000)).append("k+").toString();
        }
        return String.valueOf(number);
    }
}
