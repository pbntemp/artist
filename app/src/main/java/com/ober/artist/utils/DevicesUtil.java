package com.ober.artist.utils;

import android.content.Context;
import android.provider.Settings;
import android.util.DisplayMetrics;
import android.view.WindowManager;

import java.util.Random;

/**
 * 设备信息获取
 */
public class DevicesUtil {

    public static int getScreenHeight(Context context) {
        if (context == null) {
            return 0;
        }

        return getScreenMetric(context).heightPixels;
    }

    public static int getScreenWidth(Context context) {

        if (context == null) {
            return 0;
        }

        return getScreenMetric(context).widthPixels;
    }

    private static DisplayMetrics getScreenMetric(Context context) {
        DisplayMetrics metrics = new DisplayMetrics();
        WindowManager windowManager = (WindowManager) context
                .getSystemService(Context.WINDOW_SERVICE);
        windowManager.getDefaultDisplay().getMetrics(metrics);
        return metrics;
    }

    /**
     * @param context
     * @return [0] width [1]height
     */
    public static int[] getScreenSize(Context context) {
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics metrics = new DisplayMetrics();
        windowManager.getDefaultDisplay().getMetrics(metrics);
        return new int[]{metrics.widthPixels, metrics.heightPixels};
    }

    public static String getDeviceId(Context context) {
        return getAndroidId(context);
    }

    public static String getAndroidId(Context context) {
        String androidID = "";
        try {
            androidID = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return (androidID == null || androidID.length() == 0) ? "0000000000000000" : androidID.toLowerCase();
    }

    private static long getRandomLong() {
        Random random = new Random();
        return random.nextLong();
    }

}
