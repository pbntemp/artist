package com.ober.artist.utils;

import android.util.Log;

import java.io.File;
import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okio.BufferedSink;
import okio.Okio;
import okio.Sink;

/**
 * Created by ober on 2019/1/11.
 */
public class OkDownloadUtil {

    private static final OkHttpClient okHttpClient = new OkHttpClient();

    public static void download(final String url, final File dest) throws IOException {

        if (dest.exists()) {
            dest.delete();
        }

        final long startTime = System.currentTimeMillis();
        Log.i("DOWNLOAD", "startTime=" + startTime);

        Request request = new Request.Builder().url(url).build();

        Response response = okHttpClient.newCall(request).execute();

        Sink sink;
        BufferedSink bufferedSink = null;
        try {
            sink = Okio.sink(dest);
            bufferedSink = Okio.buffer(sink);
            bufferedSink.writeAll(response.body().source());
            Log.i("DOWNLOAD", "download success");
            Log.i("DOWNLOAD", "totalTime=" + (System.currentTimeMillis() - startTime));
        } catch (Exception e) {
            e.printStackTrace();
            Log.i("DOWNLOAD", "download failed");
            dest.delete();
        } finally {
            if (bufferedSink != null) {
                bufferedSink.close();
            }
        }
    }
}
