package com.ober.artist.utils;

import android.content.Context;
import androidx.annotation.Nullable;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class GsonUtil {

    enum GsonWrapper {
        INSTANCE;
        Gson gson = new Gson();
    }

    public static Gson getInstance() {
        return GsonWrapper.INSTANCE.gson;
    }

    public static String toJson(Object object) {
        return GsonWrapper.INSTANCE.gson.toJson(object);
    }

    public static <T> T fromJson(String jsonString, Class<T> tClass) {
        return GsonWrapper.INSTANCE.gson.fromJson(jsonString, tClass);
    }

    public static <T> T fromJson(String json, Type typeOfT) {
        return GsonWrapper.INSTANCE.gson.fromJson(json, typeOfT);
    }

    public static String loadJSONFromAsset(Context context, String jsonFileName) {
        String json;
        try {
            InputStream is = context.getAssets().open(jsonFileName);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    public static Map<String, Object> getMapFromJson(String json) throws Exception {

        JsonParser parser = new JsonParser();
        JsonObject jsonObj = parser.parse(json).getAsJsonObject();

        Map<String, Object> map = new HashMap<>();
        Set<Map.Entry<String, JsonElement>> entrySet = jsonObj.entrySet();
        for (Iterator<Map.Entry<String, JsonElement>> iter = entrySet.iterator(); iter.hasNext(); ) {
            Map.Entry<String, JsonElement> entry = iter.next();
            String key = entry.getKey();
            Object value = entry.getValue();
            if (value instanceof JsonArray) {
                map.put(key, toList((JsonArray) value));
            } else if (value instanceof JsonObject) {
                map.put(key, toMap((JsonObject) value));
            } else if (value instanceof JsonPrimitive) {
                JsonPrimitive primitive = (JsonPrimitive) value;
                if (primitive.isBoolean()) {
                    map.put(key, primitive.getAsBoolean());
                } else if (primitive.isNumber()) {
                    // TODO: 2016/10/25 work around
                    map.put(key, primitive.getAsNumber().longValue());
                } else {
                    map.put(key, primitive.getAsString());
                }
            } else if (value instanceof JsonNull) {
                map.put(key, "");
            } else {
                map.put(key, value);
            }
        }
        return map;
    }

    private static Map<String, Object> toMap(JsonObject json) {
        Map<String, Object> map = new HashMap<>();
        Set<Map.Entry<String, JsonElement>> entrySet = json.entrySet();
        for (Iterator<Map.Entry<String, JsonElement>> iter = entrySet.iterator(); iter.hasNext(); ) {
            Map.Entry<String, JsonElement> entry = iter.next();
            String key = entry.getKey();
            Object value = entry.getValue();
            if (value instanceof JsonArray) {
                map.put(key, toList((JsonArray) value));
            } else if (value instanceof JsonObject) {
                map.put(key, toMap((JsonObject) value));
            } else if (value instanceof JsonPrimitive) {
                JsonPrimitive primitive = (JsonPrimitive) value;
                if (primitive.isBoolean()) {
                    map.put(key, primitive.getAsBoolean());
                } else if (primitive.isNumber()) {
                    // TODO: 2016/10/25 work around
                    map.put(key, primitive.getAsNumber().longValue());
                } else {
                    map.put(key, primitive.getAsString());
                }
            } else if (value instanceof JsonNull) {
                map.put(key, "");
            } else {
                map.put(key, value);
            }
        }
        return map;
    }

    private static List<Object> toList(JsonArray json) {
        List<Object> list = new ArrayList<>();
        for (int i = 0; i < json.size(); i++) {
            Object value = json.get(i);
            if (value instanceof JsonArray) {
                list.add(toList((JsonArray) value));
            } else if (value instanceof JsonObject) {
                list.add(toMap((JsonObject) value));
            } else if (value instanceof JsonPrimitive) {
                JsonPrimitive primitive = (JsonPrimitive) value;
                if (primitive.isBoolean()) {
                    list.add(primitive.getAsBoolean());
                } else if (primitive.isNumber()) {
                    Number number = primitive.getAsNumber();
                    // TODO: 2016/10/25 work around
                    list.add(number.longValue());
                } else {
                    list.add(primitive.getAsString());
                }
            } else if (value instanceof JsonNull) {
                //ignore
            } else {
                list.add(value);
            }
        }
        return list;
    }

    @Nullable
    public static String getLocaleJsonStringFromJson(Context context, String jsonString) {
        if (TextUtils.isEmpty(jsonString)) {
            return null;
        }
        try {
            Map<String, Object> map = GsonUtil.getMapFromJson(jsonString);
            Object obj = getLocalObjectFromMap(context, map);
            if (obj != null) {
                Gson gson = new Gson();
                return gson.toJson(obj);
            } else {
                return "";
            }
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String getLocaleValueStringFromJson(Context context, String jsonString) {
        try {
            Map<String, Object> map = GsonUtil.getMapFromJson(jsonString);
            Object obj = getLocalObjectFromMap(context, map);
            if (obj != null) {
                return obj.toString();
            } else {
                return "";
            }
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    private static Object getLocalObjectFromMap(Context context, Map map) {
        String language = LocaleUtil.getCurrentLanguage(context);
        if (map.get(language) == null) {
            language = "default";
        }
        if (map.get(language) == null) {
            language = "en";
        }
        if (map.get(language) != null) {
            return map.get(language);
        } else {
            return null;
        }
    }
}