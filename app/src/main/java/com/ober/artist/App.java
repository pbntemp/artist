package com.ober.artist;

import android.app.Application;

import com.meevii.color.fill.FillColorConfig;
import com.ober.artist.analyze.base.ArtistAnalyze;
import com.ober.artist.net.ApiBase;
import com.ober.artist.xlog.XLogger;
import com.tencent.bugly.Bugly;

/**
 * Created by ober on 2018/12/4.
 */
public class App extends Application {

    private static App app;

    @Override
    public void onCreate() {
        super.onCreate();
        app = this;

        XLogger.init();

        Bugly.init(getApplicationContext(), "fcbfc2ea39", false);
        ArtistAnalyze.init(this);

        FillColorConfig.initPDFStyle(4, 1024);

        ApiBase.init(this);
    }

    public static App getInstance() {
        return app;
    }
}
