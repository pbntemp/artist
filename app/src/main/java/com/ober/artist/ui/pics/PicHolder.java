package com.ober.artist.ui.pics;

import android.graphics.drawable.Drawable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.ober.artist.GlideApp;
import com.ober.artist.R;
import com.ober.artist.color.data.LocalStore;
import com.ober.artist.net.bean.Picture;

import java.io.File;

/**
 * Created by ober on 2018/12/5.
 */
public class PicHolder extends RecyclerView.ViewHolder {

    CardView cardView;
    ImageView imageView;
    ProgressBar progressBar;
    TextView tvType;

    public PicHolder(@NonNull View itemView) {
        super(itemView);
        cardView = itemView.findViewById(R.id.cardView);
        imageView = itemView.findViewById(R.id.imageView);
        progressBar = itemView.findViewById(R.id.pb_loading);
        tvType = itemView.findViewById(R.id.tvType);
    }

    public void bindData(String packName, Picture data) {
        final String token = data.getName();
        File thumb = LocalStore.getThumbFile(itemView.getContext(), packName, token);
        progressBar.setVisibility(View.VISIBLE);
        if (thumb.exists()) {
            GlideApp.with(itemView)
                    .load(thumb)
                    .skipMemoryCache(true)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .priority(Priority.HIGH)
                    .into(imageView);
            progressBar.setVisibility(View.GONE);
        } else {
            GlideApp.with(itemView)
                    .load(data.getPng())
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            progressBar.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            progressBar.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .into(imageView);
        }

        setupTypeUI(data);
    }

    private void setupTypeUI(Picture data) {
        final int type = data.getPbntype();
        if(type == 0) {
            tvType.setText("");
            tvType.setBackground(null);
            tvType.setVisibility(View.INVISIBLE);
            return;
        }
        tvType.setVisibility(View.VISIBLE);
        tvType.setText(getStrType(type));
        tvType.setBackgroundResource(R.drawable.shape_pic_flag);
    }

    private static String getStrType(int type) {
        if(type == Picture.PBN_TYPE_NORMAL) {
            return "普通";
        } else if(type == Picture.PBN_TYPE_COLORED) {
            return "彩绘";
        } else if(type == Picture.PBN_TYPE_WALL_NORMAL) {
            return "普通壁纸";
        } else if(type == Picture.PBN_TYPE_WALL_COLORED) {
            return "彩绘壁纸";
        } else {
            return "Unknown";
        }
    }


}
