package com.ober.artist.ui.pics;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ober.artist.R;
import com.ober.artist.net.bean.Picture;

import java.util.List;

/**
 * Created by ober on 2018/12/5.
 */
public abstract class PicAdapter extends RecyclerView.Adapter<PicHolder> {

    private List<Picture> mData;

    private final String mPackName;

    public void setData(List<Picture> imageBeans) {
        this.mData = imageBeans;
    }

    public List<Picture> getData() {
        return mData;
    }

    protected abstract void onItemClicked(int pos, Picture item, boolean longClick);

    public PicAdapter(String packName) {
        mPackName = packName;
    }

    @NonNull
    @Override
    public PicHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View root = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_pics, viewGroup, false);
        return new PicHolder(root);
    }

    @Override
    public void onBindViewHolder(@NonNull PicHolder mainHolder, int i) {
        final Picture item = mData.get(i);
        mainHolder.cardView.setOnClickListener(view ->
                onItemClicked(mainHolder.getAdapterPosition(), item, false));
        mainHolder.cardView.setOnLongClickListener(v -> {
            onItemClicked(mainHolder.getAdapterPosition(), item, true);
            return true;
        });
        mainHolder.bindData(mPackName, item);
    }

    @Override
    public int getItemCount() {
        return mData == null ? 0 : mData.size();
    }
}
