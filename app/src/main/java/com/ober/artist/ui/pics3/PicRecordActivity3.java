package com.ober.artist.ui.pics3;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.util.Consumer;

import com.ober.artist.BaseTabActivity;
import com.ober.artist.R;
import com.ober.artist.SUserManager2;
import com.ober.artist.analyze.base.ArtistAnalyze;
import com.ober.artist.color.AreaLookActivity;
import com.ober.artist.color.ColorActivity2;
import com.ober.artist.color.localdata.LocalData;
import com.ober.artist.color.localdata.PicDownloader2;
import com.ober.artist.ui.pics2.PicAdapter;
import com.ober.artist.color.localdata.PlanUpdateTask;
import com.ober.artist.ui.pics2.PicRecordItemBean;
import com.ober.artist.ui.pics2.PicRecordLoader;
import com.ober.artist.ui.zbar.PbnScanResult;
import com.ober.artist.ui.zbar.ZBarActivity;
import com.ober.artist.upload.UploadFlow;
import com.ober.artist.upload.UploadProgressDialog;
import com.ober.artist.utils.SToast;

import java.util.List;

/**
 * Created by ober on 2019-10-28.
 */
public class PicRecordActivity3 extends BaseTabActivity implements UploadFlow.Callback{

    public static void start(Context context) {
        Intent intent = new Intent(context, PicRecordActivity3.class);
        context.startActivity(intent);
    }

    private int fragment_index_could = 0;
    private int fragment_index_local = 1;

    private static final int REQUEST_ZBAR = 11;

    private static final int REQUEST_COLOR = 12;
    private static final int REQUEST_PREVIEW_BEFORE_UPLOAD = 13;

    private PicDownloader2 mPicDownloader;

    private ProgressDialog mProgressDialog;

    private String uid;

    private UploadFlow mUploadFlow;
    private UploadFlow.UploadSession mUploadSession;
    private UploadProgressDialog mUploadingDialog;
    private PicRecordItemBean mPendingUploadItem;
    private boolean mPendingUploadItemLocal;
    private long mLastOnBackPressTimeStamp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        uid = new SUserManager2().getUID();
        mProgressDialog = new ProgressDialog(this);

        mUploadFlow = new UploadFlow(this, uid);
        mUploadFlow.setCallback(this);

        mUploadingDialog = new UploadProgressDialog(this, () -> {
            mUploadFlow.cancel();
            mUploadSession = null;
        }, () -> {
            if(mUploadSession != null) {
                doUploadRetry(mUploadSession);
            }
        });
        mUploadingDialog.setCancelable(false);

        addFragment(new CouldImgFragment(), "Cloud");
        addFragment(new LocalFragment(), "Local");

        fragment_index_could = 0;
        fragment_index_local = 1;
    }

    @Override
    public void onBackPressed() {
        long now = System.currentTimeMillis();
        if(now - mLastOnBackPressTimeStamp < 2000) {
            super.onBackPressed();
        } else {
            mLastOnBackPressTimeStamp = now;
            SToast.show("再次点击返回退出");
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if(mPicDownloader != null) {
            mPicDownloader.cancel(true);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_pic_record, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_add) {
            startZbarScan();
        }
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REQUEST_ZBAR) {
            if(resultCode == RESULT_OK) {
                assert data != null;
                PbnScanResult rs = data.getParcelableExtra("data");
                startDownload(rs);
            }
        } else if(requestCode == REQUEST_COLOR) {
            if(resultCode == RESULT_OK) {
                if(data != null) {
                    String id = data.getStringExtra("id");
                    boolean regionUpdated = data.getBooleanExtra("regionUpdated", false);

                    if (regionUpdated) {
                        startColorActivity(id);
                        return;
                    }
                    if(id != null) {
                        notifyUpdateItem(id);
                    }
                }
            } else if(resultCode == ColorActivity2.RESULT_INIT_FAILED) {
                String errMsg = null;
                if(data != null) {
                    errMsg = data.getStringExtra("errMsg");
                }
                SToast.show("加载失败:" + errMsg);
            }
        } else if(requestCode == REQUEST_PREVIEW_BEFORE_UPLOAD) {
            PicRecordItemBean bean = mPendingUploadItem;
            if(bean == null) {
                return;
            }
            mPendingUploadItem = null;
            doUpload(bean);
        }
    }

    private void notifyUpdateItem(String id) {
        PicAdapter picAdapter = getAdapter();
        if (picAdapter == null) return;

        List<PicRecordItemBean> list = picAdapter.getData();
        if (list == null || list.size() == 0) return;

        final int size = list.size();
        for(int i = 0; i < size; i++) {
            PicRecordItemBean bean = list.get(i);
            if(bean.id.equals(id)) {
                bean.setHasArchive(bean.thumbFile != null && bean.thumbFile.exists());
                getAdapter().notifyItemChanged(i);
                return;
            }
        }
    }

    private void startColorActivity(String id) {
        PicAdapter picAdapter = getAdapter();
        if (picAdapter == null) return;

        List<PicRecordItemBean> list = picAdapter.getData();
        if (list == null || list.size() == 0) return;

        final int size = list.size();
        for(int i = 0; i < size; i++) {
            PicRecordItemBean bean = list.get(i);
            if(bean.id.equals(id)) {
                bean.setHasArchive(bean.thumbFile != null && bean.thumbFile.exists());
                getAdapter().notifyItemChanged(i);

                ColorActivity2.startForResultNew(
                        PicRecordActivity3.this,
                        uid, bean.id, bean.getRegionURL(), bean.isLocal, bean.type, REQUEST_COLOR);
                return;
            }
        }
    }

    private PicAdapter getAdapter(){
        int currentIndex = viewPager.getCurrentItem();
        return ((AFragment)fragmentArrayList.get(currentIndex)).mAdapter;
    }

    private void startZbarScan() {
        SToast.show("正在打开相机");
        Intent intent = new Intent(this, ZBarActivity.class);
        startActivityForResult(intent, REQUEST_ZBAR);
    }

    private void startDownload(PbnScanResult rs) {
        mProgressDialog.show();
        mProgressDialog.setMessage("下载中");
        mProgressDialog.setCancelable(false);

        mPicDownloader = new PicDownloader2(uid, true, rs.getUrl(),
                rs.getPbnType(), result -> {
            mProgressDialog.dismiss();
            if(result.code == PicDownloader2.RESULT_OK) {
                PicRecordItemBean bean = PicRecordLoader.loadForIdSync(uid, result.generatedId, result.pbnType);
                insertToView(bean);
            } else {
                SToast.show(result.errMsg);
            }
        });
        mPicDownloader.executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
    }

    private void insertToView(PicRecordItemBean itemBean) {
        ((AFragment)(fragmentArrayList.get(fragment_index_local))).mAdapter.getData().add(0, itemBean);
        ((AFragment)(fragmentArrayList.get(fragment_index_local))).mAdapter.notifyItemInserted(0);
    }


    @Override
    public void onUploadSuccess(String id) {

        if(mUploadingDialog != null && mUploadingDialog.isShowing()) {
            mUploadingDialog.setUIResult(true, id);
        }

    }

    @Override
    public void onUploadFailed(int code, String msg) {
        if(mUploadingDialog != null && mUploadingDialog.isShowing()) {
            mUploadingDialog.setUIResult(false, msg);
        }
    }

    @Override
    public void onUploadProgress(String msg) {
        if(mUploadingDialog != null && mUploadingDialog.isShowing()) {
            mUploadingDialog.setProgressMsg(msg);
        }
    }

    @Override
    public void onUploadCanceled() {
        if(mUploadingDialog != null && mUploadingDialog.isShowing()) {
            mUploadingDialog.dismiss();
        }
    }

    @Override
    public void onPicItemClicked(boolean isLocal, int pos, PicRecordItemBean item) {

        if(item.isRegionUpdate && LocalData.getTargetRegionOldFile(uid, item.isLocal,item.id).exists()) {
            new AlertDialog.Builder(this)
                    .setMessage("上次着色之后线框发生变化，是否保留上色数据?")
                    .setPositiveButton("是", (dialog, which) -> runPlanUpdate(item, isLocal))
                    .setNegativeButton("否", (dialog, which) -> { })
                    .show();

        } else {
            ColorActivity2.startForResultNew(
                    PicRecordActivity3.this,
                    uid, item.id, item.getRegionURL(), isLocal, item.type, REQUEST_COLOR);
        }
    }

    @Override
    public void onPicItemUploadAction(int pos, PicRecordItemBean item) {
        mPendingUploadItem = item;
        AreaLookActivity.startForUpload(this, item.id, uid, item.isLocal, REQUEST_PREVIEW_BEFORE_UPLOAD);
    }

    private void doUpload(PicRecordItemBean bean) {

        mUploadingDialog.show();
        mUploadingDialog.setUILoading();

        mUploadSession = mUploadFlow.start(bean.id, bean.isLocal);
    }

    private void doUploadRetry(UploadFlow.UploadSession uploadSession) {

        mUploadingDialog.show();
        mUploadingDialog.setUILoading();
        mUploadSession = mUploadFlow.retry(uploadSession);

    }

    private void runPlanUpdate(PicRecordItemBean item, boolean isLocal) {
        mDialog.setCancelable(false);
        mDialog.setMessage("正在更新着色方案.....");
        mDialog.show();
        long startTime = System.currentTimeMillis();
        PlanUpdateTask task = new PlanUpdateTask(uid, item.id, isLocal, new Consumer<Boolean>() {
            @Override
            public void accept(Boolean rs) {
                mDialog.dismiss();
                handleUpdatePlanResult(rs, item, isLocal);

                long endTime = System.currentTimeMillis();

                ArtistAnalyze.trackEventKeyValue(isLocal ? ArtistAnalyze.Picture.event_mapping_region_local
                                : ArtistAnalyze.Picture.event_mapping_region_server,
                        String.format(ArtistAnalyze.Common.pic_id, item.id),
                        (endTime - startTime) / 1000);
            }
        });
        task.execute();
    }

    private void handleUpdatePlanResult(boolean rs, PicRecordItemBean item, boolean isLocal) {
        if(!rs) {
            //something went wrong
            //todo send analyze event
            new AlertDialog.Builder(this)
                    .setMessage("更新着色方案出错")
                    .setPositiveButton("ok", (dialog, which) -> {
                    }).show();
        } else {
            SToast.show("更新着色方案成功！");
            LocalData.getTargetRegionOldFile(uid, item.isLocal,item.id).delete();
            item.setIsRegionUpdate(false);

            ColorActivity2.startForResultNew(
                    PicRecordActivity3.this,
                    uid, item.id, item.getRegionURL(), isLocal, item.type, REQUEST_COLOR);
        }
    }

}
