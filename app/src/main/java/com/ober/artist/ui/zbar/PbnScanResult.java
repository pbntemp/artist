package com.ober.artist.ui.zbar;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by ober on 2019-10-28.
 */
public class PbnScanResult implements Parcelable {

    private int pbnType;

    private String url;

    public PbnScanResult() {}

    protected PbnScanResult(Parcel in) {
        pbnType = in.readInt();
        url = in.readString();
    }

    public static final Creator<PbnScanResult> CREATOR = new Creator<PbnScanResult>() {
        @Override
        public PbnScanResult createFromParcel(Parcel in) {
            return new PbnScanResult(in);
        }

        @Override
        public PbnScanResult[] newArray(int size) {
            return new PbnScanResult[size];
        }
    };

    public int getPbnType() {
        return pbnType;
    }

    public void setPbnType(int pbnType) {
        this.pbnType = pbnType;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(pbnType);
        dest.writeString(url);
    }
}
