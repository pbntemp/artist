package com.ober.artist.ui.pics3;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.util.Consumer;

import com.ober.artist.ui.pics2.PicRecordItemBean;
import com.ober.artist.ui.pics2.PicRecordLoader;

import java.util.List;

public class LocalFragment extends AFragment {

    private PicRecordLoader.LoadAllTask mLoadTask;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        return view;
    }

    @Override
    protected void doLoadData(String uid, Consumer<List<PicRecordItemBean>> callback) {

        if (isLoadingMore) {
            isLoadingMore = false;
            callback.accept(null);
            return;
        }
        if(mLoadTask != null) {
            mLoadTask.cancel(true);
        }
        mLoadTask = new PicRecordLoader.LoadAllTask(uid, picRecordItemBeans -> {
            callback.accept(picRecordItemBeans);
            mLoadTask = null;
        });
        mLoadTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    @Override
    protected boolean isLocal() {
        return true;
    }

    @Override
    protected boolean preItemClick(int pos, PicRecordItemBean item) {
        return true;
    }
}
