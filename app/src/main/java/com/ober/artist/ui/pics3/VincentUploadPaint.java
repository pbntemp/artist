package com.ober.artist.ui.pics3;

import androidx.core.util.Consumer;

import com.ober.artist.BuildConfig;
import com.ober.artist.color.localdata.LocalData;
import com.ober.artist.net.pbn.PbnApi;
import com.ober.artist.net.pbn.rest.VincentUploadPaintReq;
import com.ober.artist.net.pbn.rest.VincentUploadPaintResp;
import com.ober.artist.utils.SimpleFileEditor;

import okhttp3.Call;

public class VincentUploadPaint {

    private String uUid;
    private String pid;
    private boolean isLocal;

    private Call call;

    public VincentUploadPaint(String uUid, String pid, boolean isLocal){

        this.uUid = uUid;
        this.pid = pid;
        this.isLocal = isLocal;
    }

    public void doUpload(String colorUrl, String region, Consumer<VincentUploadPaintResp> callback){

        VincentUploadPaintReq req = new VincentUploadPaintReq();
        req.setId(pid);
        String planStr = SimpleFileEditor.readFromFile(LocalData.getTargetPlanFile(uUid, isLocal, pid).getAbsolutePath());
        req.setPlan(planStr);
        req.setRegion(region);
        req.setColored(colorUrl);
        call = PbnApi.uploadPaintToVincent(BuildConfig.DEBUG, req, (resp, message) -> {
            if (resp == null) {
                callback.accept(null);
                return;
            }
            callback.accept(resp);
        });
    }
}
