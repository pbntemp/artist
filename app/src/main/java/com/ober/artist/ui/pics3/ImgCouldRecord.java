package com.ober.artist.ui.pics3;

import com.ober.artist.color.localdata.PicDownloader3;

import java.util.HashMap;

public class ImgCouldRecord {

    private static ImgCouldRecord Instance;

    public static  ImgCouldRecord getInstance(){

        if (Instance == null){
            synchronized (ImgCouldRecord.class){
                if (Instance == null){
                    Instance = new ImgCouldRecord();
                }
            }
        }
        return Instance;
    }

//    private Sparsea<String, PicDownloader3.DownloadParam> imgCouldMap = new SparseArray<>();

    private HashMap<String, PicDownloader3.DownloadParam> imgCouldMap = new HashMap<>();

    private ImgCouldRecord(){

    }

    void addImageRecord(String pid, PicDownloader3.DownloadParam downloadParam){
        imgCouldMap.put(pid, downloadParam);
    }

    public PicDownloader3.DownloadParam getImageById(String pid){
        return imgCouldMap.get(pid);
    }

    public void removeImageById(String pid){
        imgCouldMap.remove(pid);
    }
}
