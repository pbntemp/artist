package com.ober.artist.ui.lookup;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.ober.artist.R;
import com.ober.artist.net.bean.PbnImgEntity;

import java.util.concurrent.Callable;

/**
 * Created by ober on 19-2-26.
 */
public class LookupDetailActivity extends AppCompatActivity {

    public static void start(Context context, PbnImgEntity entity) {
        Intent intent = new Intent(context, LookupDetailActivity.class);
        intent.putExtra("entity", entity);
        context.startActivity(intent);
    }


    private ImageView iv;
    private TextView tv;
    private ProgressBar pbLoading;
    private TextView tvErr;

    private PbnImgEntity entity;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lookup_detail);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("PBN Image Detail");

        Intent intent = getIntent();
        if (intent == null) {
            finish();
            return;
        }

        entity = intent.getParcelableExtra("entity");
        if (entity == null) {
            finish();
            return;
        }

        iv = findViewById(R.id.iv);
        tv = findViewById(R.id.tv);

        pbLoading = findViewById(R.id.pb_loading);
        tvErr = findViewById(R.id.tv_err);

        loadImage();

        loadDetail();
    }

    private void loadImage() {
        Glide.with(this)
                .load(entity.getColoredUrls()[0])
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        tvErr.setVisibility(View.VISIBLE);
                        pbLoading.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        tvErr.setVisibility(View.GONE);
                        pbLoading.setVisibility(View.GONE);
                        return false;
                    }
                })
                .into(iv);
    }

    private void loadDetail() {
        tv.setText("Loading...");
        final Callable<String> task = new ImageDetailTask(entity);
        Runnable runnable = () -> {
            try {
                String s = task.call();
                if (s != null && !s.equals(ImageDetailTask.ERR)) {
                    onDetailResult(s);
                } else {
                    onDetailResult(null);
                }
            } catch (Exception e) {
                e.printStackTrace();
                onDetailResult(null);
            }
        };
        new Thread(runnable).start();
    }

    private void onDetailResult(String rs) {
        if (isDestroyed()) {
            return;
        }
        new Handler(Looper.getMainLooper()).post(() -> {
            if (rs == null) {
                tv.setText("Error");
            } else {
                tv.setText(rs);
            }
        });
    }

}
