package com.ober.artist.ui.lotties;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ober.artist.BaseRecyclerActivity;
import com.ober.artist.BuildConfig;
import com.ober.artist.R;
import com.ober.artist.color.data.LocalStore;
import com.ober.artist.lottie.LottieDownloader;
import com.ober.artist.lottie.data.LottieLocalStore;
import com.ober.artist.lottie.draw.LottieDrawActivity;
import com.ober.artist.net.bean.LottieResBean;
import com.ober.artist.net.bean.PbnImgEntity;
import com.ober.artist.net.ober.OberApi;
import com.ober.artist.net.ober.rest.LottieResResp;
import com.ober.artist.pack.ZipLottieLevelTask;
import com.ober.artist.utils.Files;
import com.ober.artist.utils.SToast;

import java.io.File;

import okhttp3.Call;

/**
 * Created by ober on 19-9-3.
 */
public class LottieListActivity extends BaseRecyclerActivity implements LottieResAdapter.OnItemClickListener {

    public static void start(Context context) {
        Intent intent = new Intent(context, LottieListActivity.class);
        context.startActivity(intent);
    }

    private LottieResAdapter mAdapter;

    private Call mListReqCall;

    private ImgDetailLoader mImgDetailLoader;

    private LottieDownloader mDownloader;

    private ZipLottieLevelTask mZipTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mDialog.setCancelable(false);

        mAdapter = new LottieResAdapter();

        DividerItemDecoration divider = new DividerItemDecoration(this,
                DividerItemDecoration.VERTICAL);
        Drawable d = getDrawable(R.drawable.divider);
        assert d != null;
        divider.setDrawable(d);

        LinearLayoutManager lm = new LinearLayoutManager(this);
        lm.setOrientation(RecyclerView.VERTICAL);

        recyclerView.setAdapter(mAdapter);
        recyclerView.setLayoutManager(lm);

        recyclerView.addItemDecoration(divider);

        ItemTouchHelper touchHelper = new ItemTouchHelper(new ItemTouchHelperCallback());
        touchHelper.attachToRecyclerView(recyclerView);

        swipeRefreshLayout.setOnRefreshListener(this::loadData);

        mAdapter.setItemClickListener(this);

        swipeRefreshLayout.setRefreshing(true);

        mImgDetailLoader = new ImgDetailLoader();

        loadData();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if(mZipTask != null) {
            mZipTask.cancel(true);
        }

        if(mListReqCall != null) {
            mListReqCall.cancel();
        }
        mImgDetailLoader.cancel();
    }

    private void loadData() {

        if(mListReqCall != null) {
            mListReqCall.cancel();
        }

        mListReqCall = OberApi.requestLotties(this::handleDataLoaded);
    }

    private void handleDataLoaded(LottieResResp resp) {
        if(resp == null) {
            SToast.show("error");
            return;
        }

        swipeRefreshLayout.setRefreshing(false);

        mAdapter.setData(resp.data);
        mAdapter.notifyDataSetChanged();
    }

    private void requestImgDetail(final LottieResBean bean) {

        mImgDetailLoader.cancel();

        mDialog.show();
        mDialog.setMessage("请求图片信息");

        mImgDetailLoader.load(bean, imgEntity -> {
            if(imgEntity == null) {
                mDialog.dismiss();
                SToast.show("请求图片信息失败");
            } else {
                downloadResources(bean, imgEntity);
            }
        });
    }

    private void downloadResources(LottieResBean bean, PbnImgEntity imgEntity) {

        if(!checkImg(imgEntity)) {
            mDialog.dismiss();
            SToast.show("图片信息错误");
            return;
        }

        mDownloader = new LottieDownloader(bean, imgEntity, rs -> {
            mDialog.dismiss();
            if (rs == 0) {
                handleDownloadComplete(bean);
            } else {
                String msg = mDownloader.getErrMsg();
                SToast.show("ERR: " + msg);
            }
        }, s -> mDialog.setMessage(s));
        mDownloader.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void handleDownloadComplete(LottieResBean bean) {

        LottieDrawActivity.start(this, bean.getId());
    }

    private boolean checkImg(PbnImgEntity entity) {
        if(entity == null) {
            return false;
        }

        if(entity.getRegion() == null) {
            return false;
        }

        if(entity.getPdf() == null) {
            return false;
        }

        if(entity.getPlans() == null || entity.getPlans().length == 0) {
            return false;
        }

        return true;
    }

    private void exportLevelFile(LottieResBean bean) {
        if(mZipTask != null) {
            mZipTask.cancel(true);
        }

        mDialog.show();
        mDialog.setMessage("Loading...");

        File dst = new File(LocalStore.appDir(), "lottie_" + bean.getImgId() + ".zip");

        mZipTask = new ZipLottieLevelTask(bean.getId(), dst, new ZipLottieLevelTask.Callback() {
            @Override
            public void onResult(File file) {
                mDialog.dismiss();
                if(file == null) {
                    SToast.show("生成失败");
                } else {
                    handleOutFileGenerated(file);
                }
            }
        });
        mZipTask.execute();
    }

    private void handleOutFileGenerated(File file) {
        Uri uri = FileProvider.getUriForFile(this,
                BuildConfig.APPLICATION_ID + ".fileProvider", file);

        Intent intent = new Intent();
        intent.setType("*/*");
        intent.setAction(Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_STREAM, uri);
        startActivity(Intent.createChooser(intent, "Send"));
    }

    private void alertRemove(LottieResBean bean, int pos) {
        new AlertDialog.Builder(this)
                .setMessage("确定删除?")
                .setNegativeButton("取消", (dialog, which) -> {
                    mAdapter.notifyDataSetChanged();
                })
                .setPositiveButton("确定", (dialog, which) -> {
                    mAdapter.removeByPosition(pos);
                    mAdapter.notifyItemRemoved(pos);
                    sendRemoveReqSilently(bean);
                })
                .show();
    }

    private static void sendRemoveReqSilently(LottieResBean bean) {
        OberApi.removeLottie(bean.getId(), resp -> {
            if(resp == null) {
                SToast.show("删除失败");
            } else {
                if(resp.code == 0) {
                    SToast.show("removed");
                    File dir = LottieLocalStore.resourceDir(bean.getId());
                    Files.rm_rf(dir);
                } else {
                    SToast.show("删除错误:" + resp.code + "," + resp.msg);
                }
            }
        });
    }

    @Override
    public void onItemClicked(LottieResBean bean, int pos) {
        requestImgDetail(bean);
    }

    @Override
    public void onItemLongClicked(LottieResBean bean, int pos) {
        final File levelFile = LottieLocalStore.levelFile(bean.getId());

        if(!levelFile.exists()) {
            SToast.show("没有进度文件");
        } else {
            new AlertDialog.Builder(this)
                    .setMessage("导出Lottie关卡?")
                    .setPositiveButton("确定", (dialog, which) -> exportLevelFile(bean))
                    .setNegativeButton("取消", (dialog, which) -> dialog.dismiss())
                    .show();
        }
    }

    private class ItemTouchHelperCallback extends ItemTouchHelper.Callback {

        ItemTouchHelperCallback() {

        }

        @Override
        public int getMovementFlags(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder) {
            return makeMovementFlags(0, ItemTouchHelper.LEFT);
        }

        @Override
        public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
            return false;
        }

        @Override
        public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
            int pos = viewHolder.getAdapterPosition();
            LottieResBean bean =  mAdapter.getItem(pos);
            alertRemove(bean, pos);
        }
    }


}
