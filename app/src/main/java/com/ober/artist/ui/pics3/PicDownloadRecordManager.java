package com.ober.artist.ui.pics3;

import com.ober.artist.net.pbn.rest.ImgCouldListResp;

import java.util.HashMap;

public class PicDownloadRecordManager {

    private static PicDownloadRecordManager instance;

    private static final Object instanceLock = new Object();

    public static PicDownloadRecordManager getInstance() {
        if (instance == null) {
            synchronized (instanceLock){
                if (instance == null) {
                    instance = new PicDownloadRecordManager();
                }
            }
        }
        return instance;
    }

    private HashMap<String, ImgCouldListResp.Image> imageHashMap;

    private PicDownloadRecordManager(){
        imageHashMap = new HashMap<>();
    }

    private boolean isNull(){
        return imageHashMap == null;
    }

    private boolean isEmpty(){
        return imageHashMap == null || imageHashMap.isEmpty();
    }

    private void ensure(){
        if (isNull()){
            imageHashMap = new HashMap<>();
        }
    }

    void Clear(){
        if (isEmpty()) return;
        imageHashMap.clear();
    }

    public ImgCouldListResp.Image getImage(String pid){
        if (isEmpty()) return null;
        return imageHashMap.get(pid);
    }

    void putImage(ImgCouldListResp.Image image) {
       this.putImage(image.getId(), image);
    }

    private void putImage(String pid, ImgCouldListResp.Image image) {
        ensure();
        imageHashMap.put(pid, image);
    }

}
