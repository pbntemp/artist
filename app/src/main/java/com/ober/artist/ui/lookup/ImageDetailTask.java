package com.ober.artist.ui.lookup;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.meevii.color.fill.model.core.FloodFillArea;
import com.ober.artist.App;
import com.ober.artist.color.data.PreColor;
import com.ober.artist.net.bean.PbnImgEntity;
import com.ober.artist.utils.OkDownloadUtil;
import com.ober.artist.utils.PreColorCoder;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;

/**
 * Created by ober on 19-2-26.
 */
public class ImageDetailTask implements Callable<String> {

    public static final String ERR = "err";

    private final PbnImgEntity entity;

    ImageDetailTask(PbnImgEntity entity) {
        this.entity = entity;
    }

    @Override
    public String call() throws Exception {
        File f = tempFile();
        f.delete();
        try {
            OkDownloadUtil.download(entity.getRegion(), f);
        } catch (IOException e) {
            e.printStackTrace();
            return ERR;
        }
        Bitmap regionBmp = BitmapFactory.decodeFile(f.getAbsolutePath());
        if (regionBmp == null) {
            return ERR;
        }


        String planStr = entity.getPlans()[0];
        PreColor[] preColors = PreColorCoder.decode(planStr);
        Set<Integer> coloredAreaSet = new HashSet<>();
        final int colorCount = preColors.length;
        int colorAreaCount = 0;
        for (PreColor preColor : preColors) {
            colorAreaCount += preColor.areas.length;
            for (int i : preColor.areas) {
                coloredAreaSet.add(i);
            }
        }


        int[] out = getTotalAreaCount(regionBmp, coloredAreaSet);
        final int countArea = out[0];
        final int countSmall = out[1];
        final int countColoredSmall = out[2];

        StringBuilder sb = new StringBuilder();
        sb.append("颜色数:")
                .append(colorCount)
                .append("\n")
                .append("已填色区域数:")
                .append(colorAreaCount)
                .append("(小区域=")
                .append(countColoredSmall)
                .append(")")
                .append("\n")
                .append("可填色区域数:")
                .append(countArea + countSmall)
                .append("(")
                .append("小区域=")
                .append(countSmall)
                .append(")");

        return sb.toString();
    }

    private static File tempFile() {
        return new File(App.getInstance().getCacheDir(), "temp_region");
    }

    private static int[] getTotalAreaCount(Bitmap region, Set<Integer> coloredAreas) {
        final int size = 1024;
        final int[] pixels = new int[size * size];
        region.getPixels(pixels, 0, size, 0, 0, size, size);
        region.recycle();
        HashMap<Integer, FloodFillArea> areaMap = new HashMap<>();
        for (int x = 0; x < size; x++) {
            for (int y = 0; y < size; y++) {
                int index = y * size + x;
                int i = pixels[index] & 0x00ffffff;
                FloodFillArea area = areaMap.get(i);
                if (area == null) {
                    area = new FloodFillArea();
                    area.init(x, y);
                    areaMap.put(i, area);
                } else {
                    area.record(x, y);
                }
            }
        }

        int count = 0;
        int countSmall = 0;
        int coloredCountSmall = 0;
        for (Map.Entry<Integer, FloodFillArea> entry : areaMap.entrySet()) {
            int i = entry.getKey();
            if (i <= 0) {
                continue;
            }
            FloodFillArea area = entry.getValue();
            if (area.getWidth() > 2 && area.getHeight() > 2) {
                count++;
            } else {
                countSmall++;
                if (coloredAreas.contains(i)) {
                    coloredCountSmall++;
                }
            }
        }

        return new int[]{count, countSmall, coloredCountSmall};

    }


}
