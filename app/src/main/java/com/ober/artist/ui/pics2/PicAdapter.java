package com.ober.artist.ui.pics2;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ober.artist.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ober on 2019-10-29.
 */
public final class PicAdapter extends RecyclerView.Adapter<PicHolder> {

    public interface OnItemClickListener {
        void onPicItemClicked(int pos, PicRecordItemBean item);
    }

    public interface OnItemLongClickListener {
        boolean onPicItemLongClicked(int pos, PicRecordItemBean item);
    }

    private final List<PicRecordItemBean> mData;

    private OnItemClickListener mOnItemClickListener;
    private OnItemLongClickListener mOnItemLongClickListener;

    public void setData(boolean reset, List<PicRecordItemBean> data) {
        if (reset) {
            mData.clear();
        }
        mData.addAll(data);
    }

    public List<PicRecordItemBean> getData() {
        return mData;
    }

    public PicAdapter() {
        mData = new ArrayList<>();
    }

    public void setOnItemClickListener(OnItemClickListener l) {
        this.mOnItemClickListener = l;
    }

    public void setOnItemLongClickListener(OnItemLongClickListener l) {
        this.mOnItemLongClickListener = l;
    }

    @NonNull
    @Override
    public PicHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View root = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_pics, viewGroup, false);
        return new PicHolder(root);
    }

    @Override
    public void onBindViewHolder(@NonNull PicHolder mainHolder, int i) {
        final PicRecordItemBean item = mData.get(i);
        mainHolder.cardView.setOnClickListener(view -> {
            if(mOnItemClickListener != null) {
                mOnItemClickListener.onPicItemClicked(i, item);
            }
        });
        mainHolder.cardView.setOnLongClickListener(v -> {
            if(mOnItemLongClickListener != null) {
                return mOnItemLongClickListener.onPicItemLongClicked(i, item);
            }
            return false;
        });
        mainHolder.bindData(item);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }
}