package com.ober.artist.ui.pics3;

import android.text.TextUtils;

import androidx.core.util.Consumer;

import com.google.gson.Gson;
import com.ober.artist.BuildConfig;
import com.ober.artist.color.localdata.LocalData;
import com.ober.artist.color.localdata.PicDownloader3;
import com.ober.artist.net.pbn.PbnApi;
import com.ober.artist.net.pbn.rest.ImgCouldListResp;
import com.ober.artist.ui.pics2.PicRecordItemBean;
import com.ober.artist.utils.TextUtil;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import okhttp3.Call;

public class ImgCouldLoader {

    private int totalCount = -1;
    private Call call;
    private PicDownloader3 downloader3;

    ImgCouldLoader() {

    }

    private String getThumbnailExtFromUrl(String url){
        int extIndex = url.lastIndexOf('.');
        if (extIndex > 0){
            String ext = url.substring(extIndex).toLowerCase();
            String[] supports = {".png", ".jpg", ".jpeg"};
            for (int i = 0; i < supports.length; ++i){
                if (ext.equals(supports[i])){
                    return ext;
                }
            }
        }
        return ".png";
    }

    public void load(String uid, int offset, int limit, Consumer<List<PicRecordItemBean>> callback) {
        cancel();

        if (totalCount >=0 && offset >= totalCount) {
            callback.accept(null);
            return;
        }

        if (offset <= 0) {
            PicDownloadRecordManager.getInstance().Clear();
        }

        boolean isDebug = BuildConfig.DEBUG;
        call = PbnApi.queryCloudImageList(offset, limit, isDebug, imgCouldListResp -> {

            if(imgCouldListResp == null ||
                    imgCouldListResp.getData() == null ||
                    imgCouldListResp.getData().getImages() == null) {
                callback.accept(null);
            } else {

                List<PicRecordItemBean> list = new LinkedList<>();
                ImgCouldListResp.Image[] images = imgCouldListResp.getData().getImages();
                List<PicDownloader3.DownloadParam> downloadParams = new ArrayList<>();

                for (ImgCouldListResp.Image image : images) {
                    PicDownloadRecordManager.getInstance().putImage(image);

                    File pngFile = LocalData.getTargetPngFile(uid, false, image.getId());
                    File thumbnailFile = LocalData.getTargetThumbnailFile(uid, false, image.getId()); //列表缩略图
                    File thumbFile = LocalData.getTargetThumbFile(uid, false, image.getId()); //涂色的缩略图
                    File regionFile = LocalData.getTargetRegionFile(uid, false, image.getId());
                    File regionOldFile = LocalData.getTargetRegionOldFile(uid, false, image.getId());
                    File pdfFile = LocalData.getTargetPdfFile(uid, false, image.getId());
                    File colorFile = LocalData.getTargetColorFile(uid, false, image.getId());

                    PicDownloader3.DownloadParam downloadParam = new PicDownloader3.DownloadParam(image.getId(), image.getPbnType());

                    if (!pngFile.exists() && !TextUtil.isTextEmpty(image.getPng())){
                        downloadParam.urls.add(image.getPng());
                        downloadParam.targetDirs.add(pngFile.getAbsolutePath());
                    }

                    if (!pdfFile.exists()){
                        downloadParam.urls.add(image.getPdf());
                        downloadParam.targetDirs.add(pdfFile.getAbsolutePath());
                    }

                    if (!regionFile.exists()){
                        downloadParam.urls.add(image.getRegion());
                        downloadParam.targetDirs.add(regionFile.getAbsolutePath());
                    }

                    if (!TextUtil.isTextEmpty(image.getColored())){
                        if (colorFile == null || !colorFile.exists()) {
                            if (colorFile == null) colorFile = new File(LocalData.getTargetFileRecordDir(uid, false, image.getId()), "colored.jpg");
                            downloadParam.urls.add(image.getColored());
                            downloadParam.targetDirs.add(colorFile.getAbsolutePath());
                        }
                    }

                    boolean isRegionUpdate = false;
                    if (!TextUtils.isEmpty(image.getRegion_old())){
                        downloadParam.urls.add(image.getRegion_old());
                        downloadParam.targetDirs.add(regionOldFile.getAbsolutePath());

                        isRegionUpdate = true;

                        if (regionFile.exists()) {
                            regionFile.delete();

                            downloadParam.urls.add(image.getRegion());
                            downloadParam.targetDirs.add(regionFile.getAbsolutePath());
                        }

                        if (pngFile.exists()) {
                            pngFile.delete();

                            downloadParam.urls.add(image.getPng());
                            downloadParam.targetDirs.add(pngFile.getAbsolutePath());
                        }

                        if (pdfFile.exists()) {
                            pdfFile.delete();

                            downloadParam.urls.add(image.getPdf());
                            downloadParam.targetDirs.add(pdfFile.getAbsolutePath());
                        }

                        if (!TextUtil.isTextEmpty(image.getColored())){
                            if (colorFile == null) {
                                colorFile = new File(LocalData.getTargetFileRecordDir(uid, false, image.getId()), "colored.jpg");
                            }

                            downloadParam.urls.add(image.getColored());
                            downloadParam.targetDirs.add(colorFile.getAbsolutePath());
                        }
                    }


                    String serverThumbnailUrl = image.getThumbnail();
                    if (thumbnailFile == null && !TextUtils.isEmpty(serverThumbnailUrl)){
                         String ext = getThumbnailExtFromUrl(serverThumbnailUrl);
                         thumbnailFile = new File(LocalData.getTargetFileRecordDir(uid, false, image.getId()), "thumbnail" + ext);

                         downloadParam.urls.add(serverThumbnailUrl);
                         downloadParam.targetDirs.add(thumbnailFile.getAbsolutePath());
                    }

                    if (downloadParam.urls != null && downloadParam.urls.size() > 0){
                        downloadParams.add(downloadParam);
                        ImgCouldRecord.getInstance().addImageRecord(downloadParam.id, downloadParam);
                    }

                    if (!TextUtil.isTextEmpty(image.getPlan())){
                        LocalData.saveDownloadedPlan(uid, false, image.getId(), image.getPlan());
                    }

                    boolean hasArchive = thumbFile.exists();
                    PicRecordItemBean bean = new PicRecordItemBean(image.getId(), false, isRegionUpdate,
                            image.getPbnType(), pngFile, hasArchive ? thumbFile : thumbnailFile, image.getPng(), serverThumbnailUrl);

                    bean.setHasArchive(hasArchive);
                    bean.setRegionURL(image.getRegion());
                    list.add(bean);
                }

                totalCount = imgCouldListResp.getData().getTotal();
                callback.accept(list);
//                downloader3 =  new PicDownloader3(uid, downloadParams, results -> callback.accept(list));
//                downloader3.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }
        });
    }

    public void cancel() {
        if(call != null) {
            call.cancel();
        }
    }
}
