package com.ober.artist.ui.packs;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.ober.artist.R;

/**
 * Created by ober on 19-1-10.
 */
public class PackHolder extends RecyclerView.ViewHolder {

    TextView tv;

    public PackHolder(@NonNull View itemView) {
        super(itemView);
        tv = itemView.findViewById(R.id.tv);
    }
}
