package com.ober.artist.ui.lookup;

import android.app.Activity;

import com.ober.artist.net.ApiCallback;
import com.ober.artist.net.bean.PbnImgEntity;
import com.ober.artist.net.pbn.PbnApi;
import com.ober.artist.net.pbn.rest.ImgDetailResp;
import com.ober.artist.ui.lookup.LookupDialog.Client;

import okhttp3.Call;

/**
 * Created by ober on 19-2-25.
 */
public class LookupManager extends Client {

    private Activity mActivity;

    private Call mImgReq;
    private LookupDialog mDialog;

    public LookupManager(Activity context) {
        super();
        mActivity = context;
    }

    public void showDialog() {
        if (mDialog == null) {
            mDialog = new LookupDialog(mActivity, this);
            mDialog.setOnDismissListener(dialog -> {
                if (mImgReq != null) {
                    mImgReq.cancel();
                    mImgReq = null;
                }
            });
        } else {
            if (mDialog.isShowing()) {
                mDialog.dismiss();
            }
            mDialog.setStateIdle();
        }

        mDialog.show();
    }

    @Override
    boolean search(String id) {
        if (mImgReq != null) {
            mImgReq.cancel();
            mImgReq = null;
        }

        id = id.trim();

        int idLen = id.length();

        if (idLen != 24) {
            return false;
        }

        mImgReq = PbnApi.requestImgDetail(id, new ApiCallback<ImgDetailResp>() {
            @Override
            public void onResp(ImgDetailResp imgDetailResp) {
                onResult(imgDetailResp);
            }
        }, false);

        return true;
    }

    public void onDestroy() {
        if (mImgReq != null) {
            mImgReq.cancel();
        }
        dispatchResult(false);
    }

    private void onResult(ImgDetailResp resp) {
        if (resp == null || resp.getData() == null) {
            dispatchResult(false);
        } else {
            dispatchResult(true);
            startDetailPage(resp.getData());
        }
    }

    private void startDetailPage(PbnImgEntity entity) {
        LookupDetailActivity.start(mActivity, entity);
    }

}
