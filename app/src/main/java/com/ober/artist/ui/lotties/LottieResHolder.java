package com.ober.artist.ui.lotties;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ober.artist.R;

/**
 * Created by ober on 19-9-3.
 */
public class LottieResHolder extends RecyclerView.ViewHolder {

    public final TextView tvName;
    public final TextView tvTime;
    public final ViewGroup container;

    public LottieResHolder(View itemView) {
        super(itemView);
        this.tvName = itemView.findViewById(R.id.tv_name);
        this.tvTime = itemView.findViewById(R.id.tv_time);
        this.container = itemView.findViewById(R.id.container);
    }

}
