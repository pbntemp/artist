package com.ober.artist.ui.pics2;

import java.io.File;
import java.io.Serializable;

/**
 * Created by ober on 2019-10-28.
 */
public class PicRecordItemBean implements Serializable {

    public final String id;
    public final File pngFile;
    public final File thumbFile;
    public final int type;

    public final boolean isLocal;
    public boolean isRegionUpdate;

    public final String pngURL;
    public final String thumbnailURL;

    private boolean hasArchive;
    private String regionURL;

    public PicRecordItemBean(String id, boolean isLocal, int type, File pngFile, File thumbFile) {
        this(id, isLocal, false, type, pngFile, thumbFile, null, null);
    }

    public PicRecordItemBean(String id, boolean isLocal, boolean isRegionUpdate, int type, File pngFile, File thumbFile,
                            String pngURL, String thumbnailURL) {
        this.id = id;
        this.isLocal = isLocal;
        this.isRegionUpdate = isRegionUpdate;
        this.pngFile = pngFile;
        this.thumbFile = thumbFile;
        this.type = type;
        this.hasArchive = false;
        this.pngURL = pngURL;
        this.thumbnailURL = thumbnailURL;
    }

    public boolean hasArchive() {
        return hasArchive;
    }

    public void setHasArchive(boolean has) {
        this.hasArchive = has;
    }

    public void setIsRegionUpdate(boolean isRegionUpdate) {
        this.isRegionUpdate = isRegionUpdate;
    }

    public void setRegionURL(String regionURL) {
        this.regionURL = regionURL;
    }

    public String getRegionURL() {
        return regionURL;
    }
}
