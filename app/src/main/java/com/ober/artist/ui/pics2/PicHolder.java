package com.ober.artist.ui.pics2;

import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.ober.artist.GlideApp;
import com.ober.artist.R;
import com.ober.artist.net.bean.Picture;

/**
 * Created by ober on 2019-10-29.
 */
public class PicHolder extends RecyclerView.ViewHolder {

    CardView cardView;
    ImageView imageView;
    ProgressBar progressBar;
    TextView tvType;
    TextView tvRegion;

    public PicHolder(@NonNull View itemView) {
        super(itemView);
        cardView = itemView.findViewById(R.id.cardView);
        imageView = itemView.findViewById(R.id.imageView);
        progressBar = itemView.findViewById(R.id.pb_loading);
        tvType = itemView.findViewById(R.id.tvType);
        tvRegion = itemView.findViewById(R.id.tvRegion);
    }

    public void bindData(PicRecordItemBean data) {
        final boolean useThumb = data.hasArchive();
        progressBar.setVisibility(View.VISIBLE);
        if (useThumb) {
            GlideApp.with(itemView)
                    .load((data.thumbFile == null  || !data.thumbFile.exists()) ? data.thumbnailURL : data.thumbFile)
                    .skipMemoryCache(true)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            progressBar.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            progressBar.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .into(imageView);
        } else {
            GlideApp.with(itemView)
                    .load((data.pngFile == null || !data.pngFile.exists()) ? data.pngURL : data.pngFile)
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            progressBar.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            progressBar.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .into(imageView);
        }

        setupTypeUI(data.type);
        setRegionUpdate(data.isRegionUpdate);
    }

    private void setupTypeUI(int type) {
        if (type == 0) {
            tvType.setText("");
            tvType.setBackground(null);
            tvType.setVisibility(View.INVISIBLE);
            return;
        }
        tvType.setVisibility(View.VISIBLE);
        tvType.setText(getStrType(type));
        tvType.setBackgroundResource(R.drawable.shape_pic_flag);
    }


    private void setRegionUpdate(boolean update) {
        if (update) {
            tvRegion.setVisibility(View.VISIBLE);
            tvRegion.setBackgroundResource(R.drawable.shape_pic_flag);
            return;
        }
        tvRegion.setBackground(null);
        tvRegion.setVisibility(View.INVISIBLE);
    }

    private static String getStrType(int type) {
        return Picture.getStrType(type);
    }
}