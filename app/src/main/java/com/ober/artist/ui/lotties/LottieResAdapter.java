package com.ober.artist.ui.lotties;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ober.artist.R;
import com.ober.artist.net.bean.LottieResBean;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by ober on 19-9-3.
 */
public class LottieResAdapter extends RecyclerView.Adapter<LottieResHolder> {


    public interface OnItemClickListener {
        void onItemClicked(LottieResBean bean, int pos);
        void onItemLongClicked(LottieResBean bean, int pos);
    }

    private List<LottieResBean> mData;

    private OnItemClickListener mItemClickListener;

    public LottieResAdapter() {
        mData = new ArrayList<>();
    }

    public void setData(List<LottieResBean> data) {
        mData.clear();
        mData.addAll(data);
    }

    public void setItemClickListener(OnItemClickListener onItemClickListener) {
        mItemClickListener = onItemClickListener;
    }

    public LottieResBean getItem(int pos) {
        return mData.get(pos);
    }

    public void removeByPosition(int pos) {
        mData.remove(pos);
    }

    @NonNull
    @Override
    public LottieResHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_lottie, parent, false);

        return new LottieResHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull LottieResHolder holder, final int position) {
        final LottieResBean bean = mData.get(position);
        holder.tvName.setText(bean.getName());
        holder.tvTime.setText(simpleFormatTime(bean.getCreateTime()));
        holder.container.setOnClickListener(v -> {
            if(mItemClickListener != null) {
                mItemClickListener.onItemClicked(bean, position);
            }
        });
        holder.container.setOnLongClickListener(v -> {
            if(mItemClickListener != null) {
                mItemClickListener.onItemLongClicked(bean, position);
            }
            return true;
        });
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd", Locale.US);

    private final Date tempdate = new Date();
    private String simpleFormatTime(long time) {
        tempdate.setTime(time);
        return sdf.format(tempdate);
    }
}
