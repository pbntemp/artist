package com.ober.artist.ui.pics2;

import android.os.AsyncTask;

import androidx.core.util.Consumer;

import com.ober.artist.color.localdata.LocalData;
import com.ober.artist.db.PbnDb;
import com.ober.artist.db.PicRecordDao;
import com.ober.artist.db.PictureRecord;
import com.ober.artist.utils.Files;

import java.io.File;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by ober on 2019-10-28.
 */
public class PicRecordLoader {

    public static PicRecordItemBean loadForIdSync(String uid, String id, int type) {
        File dir = LocalData.getTargetFileRecordDir(uid, true, id);
        if(!dir.exists()) {
            return null;
        }

        File[] files = dir.listFiles();
        if (files == null || files.length == 0) {
            Files.rm_rf(dir);
            return null;
        }
        final File pngFile = LocalData.getTargetPngFile(uid, true, id);
        final File thumbFile = LocalData.getTargetThumbFile(uid, true, id);
        final boolean hasThumb = thumbFile.exists();
        PicRecordItemBean itemBean = new PicRecordItemBean(id, true, type, pngFile, thumbFile);
        itemBean.setHasArchive(hasThumb);

        return itemBean;
    }

    public static List<PicRecordItemBean> loadAllSync(String uid) {
        PicRecordDao dao = PbnDb.get().getPicRecordDao();
        List<PictureRecord> pictureRecords = dao.getAllByUid(uid);
        List<PicRecordItemBean> list = new LinkedList<>();
        for(PictureRecord r : pictureRecords) {
            PicRecordItemBean bean = loadForIdSync(uid, r.getId(), r.getPbntype());
            if(bean != null) {
                list.add(bean);
            }
        }
        return list;
    }

    public static class LoadAllTask extends AsyncTask<Void, Void, List<PicRecordItemBean>> {

        Consumer<List<PicRecordItemBean>> mCallback;
        final String uid;

        public LoadAllTask(String uid, Consumer<List<PicRecordItemBean>> callback) {
            this.uid = uid;
            mCallback = callback;
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            mCallback = null;
        }

        @Override
        protected List<PicRecordItemBean> doInBackground(Void... voids) {
            return loadAllSync(uid);
        }

        @Override
        protected void onPostExecute(List<PicRecordItemBean> picRecordItemBeans) {
            super.onPostExecute(picRecordItemBeans);
            Consumer<List<PicRecordItemBean>> cb = mCallback;
            if(cb != null) {
                cb.accept(picRecordItemBeans);
            }
        }
    }

}
