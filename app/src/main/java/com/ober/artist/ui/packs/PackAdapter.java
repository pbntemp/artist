package com.ober.artist.ui.packs;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ober.artist.R;
import com.ober.artist.net.bean.Package;

import java.util.List;

/**
 * Created by ober on 19-1-10.
 */
public class PackAdapter extends RecyclerView.Adapter<PackHolder> {

    private List<Package> mData;

    public PackAdapter() {
    }

    public void setData(List<Package> mData) {
        this.mData = mData;
    }

    public List<Package> getData() {
        return mData;
    }

    protected void onItemClick(int i, Package pack) {
    }

    @NonNull
    @Override
    public PackHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_pack, viewGroup, false);
        return new PackHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull PackHolder packHolder, int i) {
        Package p = mData.get(i);
        packHolder.itemView.setOnClickListener(v -> onItemClick(i, p));
        packHolder.tv.setText(p.getName());
    }

    @Override
    public int getItemCount() {
        return mData == null ? 0 : mData.size();
    }
}
