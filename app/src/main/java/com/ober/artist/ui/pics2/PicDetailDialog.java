package com.ober.artist.ui.pics2;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatDialog;

import com.ober.artist.R;
import com.ober.artist.color.localdata.LocalData;
import com.ober.artist.db.PbnDb;
import com.ober.artist.db.PictureRecord;
import com.ober.artist.net.bean.Picture;
import com.ober.artist.net.pbn.PbnApi;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by ober on 2019-12-26.
 */
public class PicDetailDialog extends AppCompatDialog {

    public interface Delegate {
        void onPicItemDeleteAction(PicRecordItemBean bean, int pos);
        void onPicItemCheckAreaAction(PicRecordItemBean bean, int pos);
        void onPicItemUploadAction(PicRecordItemBean bean, int pos);
        void onPicItemThumbnailAction(PicRecordItemBean bean, int pos);
    }

    private static final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.US);

    private final String uid;
    private final PicRecordItemBean picRecordItemBean;
    private final Delegate delegate;
    private final int adapterPos;

    public PicDetailDialog(@NonNull Context context,
                           String uid,
                           PicRecordItemBean recordItemBean,
                           int adapterPos,
                           Delegate delegate) {
        super(context, android.R.style.Theme_Material_Light_Dialog_Alert);
        this.picRecordItemBean = recordItemBean;
        this.delegate = delegate;
        this.adapterPos = adapterPos;
        this.uid = uid;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_pic_detail);
        TextView tvMsg = findViewById(R.id.tv_msg);
        findViewById(R.id.btn_check_area).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                delegate.onPicItemCheckAreaAction(picRecordItemBean, adapterPos);
            }
        });
        findViewById(R.id.btn_upload).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PbnApi.setCmsApi(false);
                dismiss();
                delegate.onPicItemUploadAction(picRecordItemBean, adapterPos);
            }
        });
        findViewById(R.id.btn_upload_gp).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PbnApi.setCmsApi(true);
                dismiss();
                delegate.onPicItemUploadAction(picRecordItemBean, adapterPos);
            }
        });
        if (picRecordItemBean.isLocal) {
            findViewById(R.id.btn_delete).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                    delegate.onPicItemDeleteAction(picRecordItemBean, adapterPos);
                }
            });
        } else {
            findViewById(R.id.btn_delete).setVisibility(View.GONE);
        }

        findViewById(R.id.btn_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        final Button btnThumbnail = findViewById(R.id.btn_thumbnail);
        btnThumbnail.setVisibility(View.GONE);

        final Handler handler = new Handler();

        Runnable queryTask = () -> {
            List<PictureRecord> records =
                    PbnDb.get().getPicRecordDao().getById(uid, picRecordItemBean.id);
            if (!records.isEmpty()) {
                PictureRecord record = records.get(0);
                StringBuilder sb = new StringBuilder();
                sb.append("素材类型:").append(Picture.getStrType(picRecordItemBean.type))
                        .append("\n").append("创建时间:")
                        .append(SIMPLE_DATE_FORMAT.format(new Date(record.getDownloadTime())))
                        .append("\n").append("最后修改:");

                if (record.getUpdateTime() != 0) {
                    sb.append(SIMPLE_DATE_FORMAT.format(new Date(record.getUpdateTime())));
                } else {
                    sb.append("无");
                }

                sb.append("\n").append("最后上传:");
                if (record.getUploadedTime() != 0) {
                    sb.append(SIMPLE_DATE_FORMAT.format(new Date(record.getUploadedTime())));
                } else {
                    sb.append("无");
                }

                String msg = sb.toString();

                boolean hasThumbnail = LocalData.getTargetThumbnailFile(uid,picRecordItemBean.isLocal, picRecordItemBean.id) != null;

                handler.post(() -> {
                    tvMsg.setText(msg);
                });
            }
        };

        new Thread(queryTask).start();
    }
}
