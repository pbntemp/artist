package com.ober.artist.ui.pics3;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.util.Consumer;

import com.ober.artist.color.localdata.LocalData;
import com.ober.artist.color.localdata.PicDownloader3;
import com.ober.artist.ui.pics2.PicRecordItemBean;
import com.ober.artist.utils.ResourceSizeCheckUtil;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class CouldImgFragment extends AFragment {

    private final int PAGE_LIMIT_SIZE = 100;

    private ImgCouldLoader couldLoader;
    private PicDownloader3 downloader3;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        return view;
    }

    @Override
    protected void doLoadData(String uid, Consumer<List<PicRecordItemBean>> callback) {

        showProgressDialog(true);

        if(couldLoader != null) {
            couldLoader.cancel();
        } else {
            couldLoader = new ImgCouldLoader();
        }

        couldLoader.load(uid, isLoadingMore ? getItemCount() : 0, PAGE_LIMIT_SIZE, callback);
    }

    @Override
    protected boolean isLocal() {
        return false;
    }

    @Override
    protected boolean preItemClick(int pos, PicRecordItemBean item) {
        return checkResource(pos,item);
    }

    private boolean checkResource(int pos, PicRecordItemBean item) {
        File pngFile = LocalData.getTargetPngFile(uid, isLocal(), item.id);
        File regionFile = LocalData.getTargetRegionFile(uid, isLocal(), item.id);
        File pdfFile = LocalData.getTargetPdfFile(uid, isLocal(), item.id);
        File regionOldFile = LocalData.getTargetRegionOldFile(uid, isLocal(), item.id);
        File colorFile = LocalData.getTargetColorFile(uid, isLocal(), item.id);

//        File planFile = LocalData.getTargetPlanFile(uid, isLocal(), item.id);
//        File thumbnailFile = LocalData.getTargetThumbnailFile(uid, isLocal(), item.id);

        if(!pngFile.exists() || !pdfFile.exists() || !regionFile.exists()) {
            startDownloadFile(pos, item);
            return false;
        }

        if (item.isRegionUpdate && !regionOldFile.exists()) {
            startDownloadFile(pos, item);
            return false;
        }

        if ((colorFile == null || !colorFile.exists()) && !item.isLocal) {
            startDownloadFile(pos, item);
            return false;
        }
        ResourceSizeCheckUtil.CheckResult rs = ResourceSizeCheckUtil.checkResource(pngFile, pdfFile);
        return !rs.isTooLarge;
    }

    private void startDownloadFile(int pos, PicRecordItemBean item){

        showProgressDialog(true);
        //获取图片的具体信息，开始下载数据
        if (downloader3 != null) {
            downloader3.cancel(true);
        }
        PicDownloader3.DownloadParam downloadParam = ImgCouldRecord.getInstance().getImageById(item.id);

        if (downloadParam == null){
            showProgressDialog(false);
            hostListener.onPicItemClicked(isLocal(), pos, item);

            return;
        }

        List<PicDownloader3.DownloadParam> downloadParamList = new ArrayList<>();
        downloadParamList.add(downloadParam);

        downloader3 =  new PicDownloader3(uid, downloadParamList, results -> {

            showProgressDialog(false);
            if (results != null && results.size() == downloadParam.urls.size()) {
                ImgCouldRecord.getInstance().removeImageById(item.id);

                hostListener.onPicItemClicked(isLocal(), pos, item);
            } else {
                Toast.makeText(getContext(), "下载失败，请重试...", Toast.LENGTH_SHORT).show();
            }
        });
        downloader3.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

    }
}
