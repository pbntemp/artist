package com.ober.artist.ui.pics;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.core.content.FileProvider;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;

import com.google.gson.Gson;
import com.ober.artist.BaseRecyclerActivity;
import com.ober.artist.BuildConfig;
import com.ober.artist.R;
import com.ober.artist.color.ColorActivity;
import com.ober.artist.color.data.LocalStore;
import com.ober.artist.color.localdata.PicDownloader;
import com.ober.artist.net.ober.OberApi;
import com.ober.artist.net.bean.Package;
import com.ober.artist.net.bean.Picture;
import com.ober.artist.pack.ZipTask;
import com.ober.artist.utils.SToast;

import java.io.File;
import java.util.LinkedList;
import java.util.List;

import okhttp3.Call;

@Deprecated
public class PicActivity extends BaseRecyclerActivity {

    public static void start(Context c, Package p) {
        Intent intent = new Intent(c, PicActivity.class);
        intent.putExtra("data", new Gson().toJson(p));
        c.startActivity(intent);
    }

    private static final int REQUEST_COLOR_DRAW = 0x1;

    private PicAdapter adapter;

    private Package mPack;

    private Call mReq;

    private ZipTask mZipTask;

    private PicDownloader mDownloader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getIntent() == null) {
            finish();
            return;
        }

        String packJson = getIntent().getStringExtra("data");
        mPack = new Gson().fromJson(packJson, Package.class);
        if (mPack == null) {
            finish();
            return;
        }

        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        LinearLayoutManager lm = new LinearLayoutManager(this);
        lm.setOrientation(RecyclerView.VERTICAL);
        recyclerView.setLayoutManager(lm);
        adapter = new PicAdapter(mPack.getName()) {

            @Override
            protected void onItemClicked(int pos, Picture item, boolean longClick) {
                if(longClick) {
                    showPicOpDialog(item);
                } else {
                    startDownload(item);
                }
            }
        };

        recyclerView.setAdapter(adapter);

        swipeRefreshLayout.setOnRefreshListener(this::load);

        load();
    }

    private void load() {
        if (mReq != null) {
            mReq.cancel();
        }

        swipeRefreshLayout.setRefreshing(true);
        mReq = OberApi.requestPictures(mPack.getId(), pictureResp -> {
            swipeRefreshLayout.setRefreshing(false);
            if (pictureResp == null) {
                SToast.show("error");
                return;
            }
            adapter.setData(pictureResp.pictures);
            adapter.notifyDataSetChanged();
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_zip) {
            showPackOpDialog();
        }
        return true;
    }


    private void showPicOpDialog(Picture picture) {
        String[] items = new String[] {
            "Reset"
        };

        new AlertDialog.Builder(this)
                .setTitle("Picture Options")
                .setItems(items, (dialogInterface, i) -> {
                    dialogInterface.dismiss();
                    if (i == 0) {
                        showReconfirm("确定重置这张图片的着色方案？", () -> processReset(picture));
                    }
                })
                .show();
    }

    private void showReconfirm(String msg, Runnable next) {
        new AlertDialog.Builder(this)
                .setMessage(msg)
                .setNegativeButton("取消", (dialog, which) -> dialog.dismiss())
                .setPositiveButton("确定", (dialog, which) -> {
                    dialog.dismiss();
                    next.run();
                })
                .show();
    }

    private void showPackOpDialog() {
        String[] items = new String[]{
                "Pack Result",
                "Send Package"
        };

        new AlertDialog.Builder(this)
                .setItems(items, (dialogInterface, i) -> {
                    dialogInterface.dismiss();
                    if (i == 0) {
                        processPack();
                    } else if (i == 1) {
                        processSend();
                    }
                })
                .show();
    }

    private void processReset(Picture picture) {
        File thumb = LocalStore.getThumbFile(this, mPack.getName(), picture.getName());
        File edit = LocalStore.getEditBmpFile(this, mPack.getName(), picture.getName());
        File areaMap = LocalStore.getAreaMapFile(this, mPack.getName(), picture.getName());
        File sort = LocalStore.getSortFile(this, mPack.getName(), picture.getName());
        File plan = LocalStore.getPlanFile(this, mPack.getName(), picture.getName());
        File center = LocalStore.getCenterFile(this, mPack.getName(), picture.getName());
        thumb.delete();
        edit.delete();
        areaMap.delete();
        sort.delete();
        plan.delete();
        center.delete();
        adapter.notifyDataSetChanged();
    }

    private void processPack() {

        List<Picture> data = adapter.getData();
        if(data == null || data.size() == 0) {
            SToast.show("加载未完成");
            return;
        }

        List<Picture> cpData = new LinkedList<>(data);

        mDialog.show();
        mDialog.setMessage("Packaging...");
        mDialog.setCancelable(false);
        File dst = new File(LocalStore.appDir(), "pack2.zip");
        mZipTask = new ZipTask(mPack.getName(),
                cpData,
                dst,
                new ZipTask.Callback() {
            @Override
            public void onProgress(String s) {
                mDialog.setMessage(s);
            }

            @Override
            public void onResult(File dst) {
                mDialog.dismiss();
                if (dst != null && dst.exists()) {
                    SToast.show("packaged");
                    showPackResult(true);
                } else {
                    SToast.show("pack error!!");
                    showPackResult(false);
                }
            }
        });
        mZipTask.execute();
    }

    private void showPackResult(boolean success) {
        new AlertDialog.Builder(this)
                .setMessage(success ? "打包完成" : "打包失败")
                .setPositiveButton("ok", (dialog, which) -> dialog.dismiss())
                .show();
    }

    private void processSend() {
        File dst = new File(LocalStore.appDir(), "pack2.zip");
        boolean exists = dst.exists();
        if (!exists) {
            SToast.show("Pack it before sending..");
            return;
        }

        Uri uri = FileProvider.getUriForFile(this,
                BuildConfig.APPLICATION_ID + ".fileProvider", dst);

        Intent intent = new Intent();
        intent.setType("*/*");
        intent.setAction(Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_STREAM, uri);
        startActivity(Intent.createChooser(intent, "Send"));
    }

    private void startColorPage(Picture image) {
        ColorActivity.startForResult(this, mPack.getName(),
                image.getName(), image.getPbntype(), REQUEST_COLOR_DRAW);
    }

    @SuppressLint("StaticFieldLeak")
    private void startDownload(final Picture picture) {
        if (mDownloader != null) {
            mDownloader.cancel(true);
        }
        mDialog.show();
        mDialog.setCancelable(false);
        mDialog.setMessage("Download");
        mDownloader = new PicDownloader(mPack.getName(), picture.getName(), picture) {
            @Override
            protected void onProgressUpdate(Integer... values) {
                mDialog.setMessage("Download : " + values[0] + "/" + 3);
            }

            @Override
            protected void onPostExecute(Integer integer) {
                mDialog.dismiss();
                if (integer == null || integer < 0) {
                    SToast.show("err");
                    if(integer != null && integer == -99) {
                        showFatalAlert();
                    }

                    return;
                }
                startColorPage(picture);
            }
        };
        mDownloader.execute();
    }

    private void showFatalAlert() {
        new AlertDialog.Builder(this)
                .setMessage("这张图片上传时出错了，请重新上传它")
                .show();
    }

    private void showFatalAlert(String msg) {
        new AlertDialog.Builder(this)
                .setMessage(msg)
                .show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mDownloader != null) {
            mDownloader.cancel(true);
        }
        if (mZipTask != null) {
            mZipTask.cancel(true);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REQUEST_COLOR_DRAW) {
            if(resultCode == RESULT_OK) {
                adapter.notifyDataSetChanged();
            } else if(resultCode == ColorActivity.RESULT_INIT_FAILED) {
                if(data != null) {
                    String errMsg = data.getStringExtra("errMsg");
                    if(!TextUtils.isEmpty(errMsg)) {
                        showFatalAlert(errMsg);
                    }
                }
            }
        }

    }
}
