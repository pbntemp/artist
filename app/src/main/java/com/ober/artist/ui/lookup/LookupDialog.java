package com.ober.artist.ui.lookup;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.ober.artist.R;
import com.ober.artist.utils.SToast;

/**
 * Created by ober on 19-2-25.
 */
public class LookupDialog extends Dialog {

    abstract static class Client {
        LookupDialog dialog;

        Client() {
        }

        abstract boolean search(String id);

        void dispatchResult(boolean success) {
            if (success) {
                dialog.dismiss();
            } else {
                dialog.setStateErr();
            }
        }
    }

    private final Client mClient;

    public LookupDialog(@NonNull Context context, Client client) {
        super(context, R.style.ColorImgPrepareDialog);
        mClient = client;
        mClient.dialog = this;
    }

    private EditText etId;
    private ProgressBar pbLoading;
    private Button btnSearch;
    private TextView tvErr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_lookup);
        etId = findViewById(R.id.et_id);
        pbLoading = findViewById(R.id.pb_loading);
        btnSearch = findViewById(R.id.btn_search);
        tvErr = findViewById(R.id.tv_error);

        setStateIdle();

        btnSearch.setOnClickListener(v -> checkToSearch());

    }

    private void checkToSearch() {
        String id = etId.getText().toString();
        boolean start = mClient.search(id);
        if (start) {
            setStateLoading();
        } else {
            SToast.show("???");
        }
    }

    public void setStateIdle() {
        pbLoading.setVisibility(View.GONE);
        tvErr.setVisibility(View.GONE);
        etId.setVisibility(View.VISIBLE);
        btnSearch.setVisibility(View.VISIBLE);
        etId.setText(null);
    }

    private void setStateErr() {
        pbLoading.setVisibility(View.GONE);
        tvErr.setVisibility(View.VISIBLE);
        etId.setVisibility(View.GONE);
        btnSearch.setVisibility(View.GONE);
    }

    private void setStateLoading() {
        pbLoading.setVisibility(View.VISIBLE);
        tvErr.setVisibility(View.GONE);
        etId.setVisibility(View.GONE);
        btnSearch.setVisibility(View.GONE);
    }
}
