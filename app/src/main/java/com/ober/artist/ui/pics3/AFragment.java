package com.ober.artist.ui.pics3;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.util.Consumer;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.ober.artist.R;
import com.ober.artist.SUserManager2;
import com.ober.artist.color.AreaLookActivity;
import com.ober.artist.color.localdata.LocalData;
import com.ober.artist.db.PbnDb;
import com.ober.artist.ui.pics2.PicAdapter;
import com.ober.artist.ui.pics2.PicDetailDialog;
import com.ober.artist.ui.pics2.PicRecordItemBean;
import com.ober.artist.utils.Files;
import com.ober.artist.utils.SToast;
import com.ober.artist.xlog.XLogger;

import java.io.File;
import java.util.List;

public abstract class AFragment extends Fragment implements PicDetailDialog.Delegate {

    protected String uid;
    protected PicAdapter mAdapter;
    protected ProgressDialog mProgressDialog;

    protected RecyclerView recyclerView;
    protected SwipeRefreshLayout swipeRefreshLayout;

    protected HostListener hostListener;

    protected boolean isLoadingMore;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        hostListener = (HostListener) context;
        mProgressDialog = new ProgressDialog(context);
    }

    protected void showProgressDialog(boolean show){
        if (show){
            mProgressDialog.show();
            mProgressDialog.setMessage("下载中......");
            mProgressDialog.setCancelable(false);
        } else {
            mProgressDialog.dismiss();
        }

    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        uid = new SUserManager2().getUID();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_recycler, null);

        recyclerView = view.findViewById(R.id.recyclerView);
        swipeRefreshLayout = view.findViewById(R.id.swipe);

        mAdapter = new PicAdapter();
//        final LinearLayoutManager lm = new LinearLayoutManager(getActivity());
        final GridLayoutManager lm = new GridLayoutManager(getActivity(), 2);
//        lm.setOrientation(RecyclerView.VERTICAL);
        recyclerView.setLayoutManager(lm);
        recyclerView.setAdapter(mAdapter);

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int lastVisibleItemPosition = lm.findLastVisibleItemPosition();
                XLogger.d("onScrolled : dy:" + dy + " dx:" + dx + " last:" + lastVisibleItemPosition + " total:" + mAdapter.getItemCount());

                if (dy > 0 && lastVisibleItemPosition + 1 == mAdapter.getItemCount()) {
                    XLogger.d("loading executed");

                    boolean isRefreshing = swipeRefreshLayout.isRefreshing();
                    if (isRefreshing) {
//                        mAdapter.notifyItemRemoved(mAdapter.getItemCount());
                        return;
                    }
                    if (!isLoadingMore) {
                        isLoadingMore = true;
                        loadData();
                    }
                }

            }
        });

        swipeRefreshLayout.setOnRefreshListener(this::loadData);

        loadData();

        mAdapter.setOnItemClickListener((pos, item) -> {
            if (preItemClick(pos, item)) {
                hostListener.onPicItemClicked(isLocal(), pos, item);
            }
        });

        mAdapter.setOnItemLongClickListener((pos, item) -> {
            showPicDetail(item, pos);
            return true;
        });
        return view;
    }

    protected abstract boolean preItemClick(int pos, PicRecordItemBean item);

    private void loadData() {
        if (!isLoadingMore) {
            swipeRefreshLayout.setRefreshing(true);
        }

        doLoadData(uid, picRecordItemBeans -> {

            mProgressDialog.dismiss();

            if (picRecordItemBeans == null) {
                isLoadingMore = false;
                swipeRefreshLayout.setRefreshing(false);
                return;
            }
            mAdapter.setData(!isLoadingMore, picRecordItemBeans);
            mAdapter.notifyDataSetChanged();
            isLoadingMore = false;
            swipeRefreshLayout.setRefreshing(false);
        });
    }

    protected int getItemCount() {
        return mAdapter.getItemCount();
    }

    protected PicRecordItemBean getItem(int pos) {
        if (pos >= getItemCount()) return null;
        return mAdapter.getData().get(pos);
    }

    protected abstract void doLoadData(String uid, Consumer<List<PicRecordItemBean>> callback);

    protected abstract boolean isLocal();


    private void showPicDetail(PicRecordItemBean bean, int pos) {
        PicDetailDialog mPicDetailDialog = new PicDetailDialog(getActivity(), uid, bean, pos, this);
        mPicDetailDialog.show();
    }

    public interface HostListener {
        void onPicItemClicked(boolean isLocal, int pos, PicRecordItemBean item);
        void onPicItemUploadAction(int pos, PicRecordItemBean item);
    }


    @Override
    public void onPicItemDeleteAction(PicRecordItemBean bean, int pos) {
        alertDelete(bean, pos);
    }

    @Override
    public void onPicItemCheckAreaAction(PicRecordItemBean bean, int pos) {
        AreaLookActivity.startForLook(getContext(), bean.id, isLocal(), uid);
    }

    @Override
    public void onPicItemUploadAction(PicRecordItemBean bean, int pos) {
//        mPendingUploadItem = bean;
//        AreaLookActivity.startForUpload(this, bean.id, uid, REQUEST_PREVIEW_BEFORE_UPLOAD);
        hostListener.onPicItemUploadAction(pos, bean);
    }

    @Override
    public void onPicItemThumbnailAction(PicRecordItemBean bean, int pos) {
        //todo
    }

    private void performDelete(PicRecordItemBean bean, int pos) {
        if (mAdapter.getData().remove(pos) == bean) {
            mAdapter.notifyItemRemoved(pos);
        } else {
            mAdapter.notifyDataSetChanged();
        }
        String id = bean.id;

        new Thread(() -> {
            File imgDir = LocalData.getTargetFileRecordDir(uid, isLocal(), id);
            PbnDb.get().getPicRecordDao().deleteById(uid, id);
            Files.rm_rf(imgDir);
            getActivity().runOnUiThread(() -> SToast.show("removed"));
        }).start();
    }

    private void alertDelete(PicRecordItemBean bean, int pos) {
        new AlertDialog.Builder(getContext())
                .setMessage("确定在手机上删除该素材（不能找回）？")
                .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        performDelete(bean, pos);
                    }
                })
                .setNegativeButton("取消", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
    }
}
