package com.ober.artist.ui.packs;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.Menu;
import android.view.MenuItem;

import com.ober.artist.BaseRecyclerActivity;
import com.ober.artist.R;
import com.ober.artist.SUserManager;
import com.ober.artist.net.ober.OberApi;
import com.ober.artist.net.bean.Package;
import com.ober.artist.ui.lookup.LookupManager;
import com.ober.artist.ui.pics.PicActivity;
import com.ober.artist.utils.SToast;

import okhttp3.Call;

/**
 * Created by ober on 19-1-10.
 */

@Deprecated
public class PackActivity extends BaseRecyclerActivity {

    public static void start(Context c) {
        Intent intent = new Intent(c, PackActivity.class);
        c.startActivity(intent);
    }

    private PackAdapter mAdapter;

    private Call mReq;

    private LookupManager mLookupManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        LinearLayoutManager lm = new LinearLayoutManager(this);
        lm.setOrientation(RecyclerView.VERTICAL);
        DividerItemDecoration divider = new DividerItemDecoration(this,
                DividerItemDecoration.VERTICAL);
        Drawable d = getDrawable(R.drawable.divider);
        assert d != null;
        divider.setDrawable(d);

        mAdapter = new PackAdapter() {
            @Override
            protected void onItemClick(int i, Package pack) {
                startPics(pack);
            }
        };
        recyclerView.addItemDecoration(divider);
        recyclerView.setLayoutManager(lm);
        recyclerView.setAdapter(mAdapter);

        swipeRefreshLayout.setOnRefreshListener(this::load);

        load();
    }

    private void startPics(Package pack) {
        PicActivity.start(this, pack);
    }

    private void load() {
        if (mReq != null) {
            mReq.cancel();
        }
        swipeRefreshLayout.setRefreshing(true);
        String who = SUserManager.getUserName();
        mReq = OberApi.requestPacks(who, packageResp -> {
            swipeRefreshLayout.setRefreshing(false);
            if (packageResp == null) {
                SToast.show("error");
                return;
            }
            mAdapter.setData(packageResp.packages);
            mAdapter.notifyDataSetChanged();
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(mReq != null) {
            mReq.cancel();
        }
        if(mLookupManager != null) {
            mLookupManager.onDestroy();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_pack, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_lookup) {
            showLookup();
        }
        return true;
    }

    private void showLookup() {
        if(mLookupManager == null) {
            mLookupManager = new LookupManager(this);
        }
        mLookupManager.showDialog();
    }

}
