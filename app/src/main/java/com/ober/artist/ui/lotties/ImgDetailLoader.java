package com.ober.artist.ui.lotties;

import android.os.Handler;
import android.os.Looper;

import androidx.core.util.Consumer;

import com.ober.artist.lottie.data.LottieLocalStore;
import com.ober.artist.net.bean.LottieResBean;
import com.ober.artist.net.bean.PbnImgEntity;
import com.ober.artist.net.pbn.PbnApi;
import com.ober.artist.utils.GsonUtil;
import com.ober.artist.utils.SimpleFileEditor;

import java.io.File;

import okhttp3.Call;

/**
 * Created by ober on 19-9-5.
 */
public class ImgDetailLoader {


    private Call call;

    public ImgDetailLoader() {

    }

    public void load(LottieResBean bean, Consumer<PbnImgEntity> callback) {
        cancel();
        final long id = bean.getId();
        final File f = LottieLocalStore.imgBeanFile(id);
        if(f.exists()) {
            new Thread(() -> {
                final String json = SimpleFileEditor.readFromFile(f.getAbsolutePath());
                final PbnImgEntity entity = GsonUtil.fromJson(json, PbnImgEntity.class);
                new Handler(Looper.getMainLooper())
                        .post(() -> callback.accept(entity));
            }).start();
        } else {
            call = PbnApi.requestImgDetail(bean.getImgId(), imgDetailResp -> {
                if(imgDetailResp == null || imgDetailResp.getData() == null) {
                    callback.accept(null);
                } else {
                    callback.accept(imgDetailResp.getData());
                }
            }, true);
        }
    }

    public void cancel() {
        if(call != null) {
            call.cancel();
        }
    }
}
