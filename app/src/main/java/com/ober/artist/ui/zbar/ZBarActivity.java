package com.ober.artist.ui.zbar;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.annotations.SerializedName;
import com.ober.artist.App;
import com.ober.artist.R;
import com.ober.artist.db.PicType;
import com.ober.artist.utils.GsonUtil;
import com.ober.artist.utils.SToast;
import com.socks.library.KLog;

import cn.bingoogolapple.qrcode.core.QRCodeView;
import cn.bingoogolapple.qrcode.zbar.ZBarView;

/**
 * Created by ober on 2019-10-28.
 */
public class ZBarActivity extends AppCompatActivity implements QRCodeView.Delegate {

    private static final String TAG = "ZBarActivity";

    private ZBarView zBarView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zbar);
        zBarView = findViewById(R.id.zbarview);
        zBarView.setDelegate(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        zBarView.startCamera();
        zBarView.startSpotAndShowRect();
    }

    @Override
    protected void onStop() {
        zBarView.stopCamera();
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        zBarView.onDestroy();
        super.onDestroy();
    }

    private void vibrate() {
        Vibrator vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);
        if(vibrator != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                vibrator.vibrate(VibrationEffect.createOneShot(200, -1));
            } else {
                vibrator.vibrate(200);
            }
        }
    }

    @Override
    public void onScanQRCodeSuccess(String result) {
        KLog.i(TAG, "result", result);
        vibrate();

        ZCodeJsonBean bean;
        try {
            bean = GsonUtil.fromJson(result, ZCodeJsonBean.class);
        } catch (Exception ignore) {
            SToast.show("二维码错误");
            setResult(RESULT_CANCELED);
            finish();
            return;
        }

        final int pbnType = bean.getPbnType();
        if(pbnType < 0) {
            Toast.makeText(App.getInstance(),
                    "数据解析失败", Toast.LENGTH_SHORT).show();
            setResult(RESULT_CANCELED);
            finish();
            return;
        }

        PbnScanResult rs = new PbnScanResult();
        rs.setUrl(bean.zipurl);
        rs.setPbnType(pbnType);
        Intent intent = new Intent();
        intent.putExtra("data", rs);
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void onCameraAmbientBrightnessChanged(boolean isDark) {
        // 这里是通过修改提示文案来展示环境是否过暗的状态，接入方也可以根据 isDark 的值来实现其他交互效果
        String tipText = zBarView.getScanBoxView().getTipText();
        String ambientBrightnessTip = "\n环境过暗，请打开闪光灯";
        if (isDark) {
            if (!tipText.contains(ambientBrightnessTip)) {
                zBarView.getScanBoxView().setTipText(tipText + ambientBrightnessTip);
            }
        } else {
            if (tipText.contains(ambientBrightnessTip)) {
                tipText = tipText.substring(0, tipText.indexOf(ambientBrightnessTip));
                zBarView.getScanBoxView().setTipText(tipText);
            }
        }
    }

    @Override
    public void onScanQRCodeOpenCameraError() {
        KLog.e(TAG, "打开相机出错");
        SToast.show("打开相机出错");
    }

    private static final class ZCodeJsonBean {
        @SerializedName("size")
        private String size;

        @SerializedName("type")
        private String type;

        @SerializedName("zipurl")
        private String zipurl;

        int getPbnType() {
            if("normal".equals(type)) {
                if("normal".equals(size)) {
                    return PicType.PBN_TYPE_NORMAL;
                } else if("wallpaper".equals(size)) {
                    return PicType.PBN_TYPE_WALL_NORMAL;
                }
                return -2;
            } else if ("colored".equals(type)) {
                if("normal".equals(size)) {
                    return PicType.PBN_TYPE_COLORED;
                } else if("wallpaper".equals(size)) {
                    return PicType.PBN_TYPE_WALL_COLORED;
                }
                return -3;
            } else {
                return -1;
            }
        }

    }


}
