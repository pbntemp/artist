package com.ober.artist.ui.pics2;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ober.artist.BaseRecyclerActivity;
import com.ober.artist.R;
import com.ober.artist.SUserManager2;
import com.ober.artist.color.AreaLookActivity;
import com.ober.artist.color.ColorActivity2;
import com.ober.artist.color.localdata.LocalData;
import com.ober.artist.color.localdata.PicDownloader2;
import com.ober.artist.db.PbnDb;
import com.ober.artist.ui.zbar.PbnScanResult;
import com.ober.artist.ui.zbar.ZBarActivity;
import com.ober.artist.upload.UploadFlow;
import com.ober.artist.upload.UploadProgressDialog;
import com.ober.artist.utils.Files;
import com.ober.artist.utils.IOUtil;
import com.ober.artist.utils.SToast;

import java.io.File;
import java.util.List;

/**
 * Created by ober on 2019-10-28.
 */
public class PicRecordActivity extends BaseRecyclerActivity implements UploadFlow.Callback, PicDetailDialog.Delegate {

    public static void start(Context context) {
        Intent intent = new Intent(context, PicRecordActivity.class);
        context.startActivity(intent);
    }

    private static final int REQUEST_ZBAR = 11;

    private static final int REQUEST_COLOR = 12;

    private static final int REQUEST_PREVIEW_BEFORE_UPLOAD = 13;

    private PicDownloader2 mPicDownloader;

    private PicAdapter mAdapter;

    private PicRecordLoader.LoadAllTask mLoadTask;

    private ProgressDialog mProgressDialog;

    private String uid;

    private UploadFlow mUploadFlow;
    private UploadFlow.UploadSession mUploadSession;

    private UploadProgressDialog mUploadingDialog;

    private long mLastOnBackPressTimeStamp;

    private PicRecordItemBean mPendingUploadItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        uid = new SUserManager2().getUID();

        mAdapter = new PicAdapter();
        final LinearLayoutManager lm = new LinearLayoutManager(this);
        lm.setOrientation(RecyclerView.VERTICAL);
        recyclerView.setLayoutManager(lm);
        recyclerView.setAdapter(mAdapter);

        swipeRefreshLayout.setOnRefreshListener(this::loadData);

        mProgressDialog = new ProgressDialog(this);

        mUploadFlow = new UploadFlow(this, uid);
        mUploadFlow.setCallback(this);

        mUploadingDialog = new UploadProgressDialog(this, () -> {
            mUploadFlow.cancel();
            mUploadSession = null;
        }, () -> {
            if(mUploadSession != null) {
                doUploadRetry(mUploadSession);
            }
        });
        mUploadingDialog.setCancelable(false);

        loadData();

        mAdapter.setOnItemClickListener((pos, item)
                -> ColorActivity2.startForResultNew(
                        PicRecordActivity.this,
                uid, item.id,"", true, item.type, REQUEST_COLOR));

        mAdapter.setOnItemLongClickListener(new PicAdapter.OnItemLongClickListener() {
            @Override
            public boolean onPicItemLongClicked(int pos, PicRecordItemBean item) {
                showPicDetail(item, pos);
                return true;
            }
        });
    }

    @Override
    public void onBackPressed() {
        long now = System.currentTimeMillis();
        if(now - mLastOnBackPressTimeStamp < 2000) {
            super.onBackPressed();
        } else {
            mLastOnBackPressTimeStamp = now;
            SToast.show("再次点击返回退出");
        }
    }


    private void showPicDetail(PicRecordItemBean bean, int pos) {
        PicDetailDialog mPicDetailDialog = new PicDetailDialog(this, uid, bean, pos, this);
        mPicDetailDialog.show();
    }

    private void doUpload(PicRecordItemBean bean) {

        mUploadingDialog.show();
        mUploadingDialog.setUILoading();

        mUploadSession = mUploadFlow.start(bean.id, true);
    }

    private void doUploadRetry(UploadFlow.UploadSession uploadSession) {

        mUploadingDialog.show();
        mUploadingDialog.setUILoading();
        mUploadSession = mUploadFlow.retry(uploadSession);

    }

    private void loadData() {
        swipeRefreshLayout.setRefreshing(true);

        if(mLoadTask != null) {
            mLoadTask.cancel(true);
        }

        mLoadTask = new PicRecordLoader.LoadAllTask(uid, picRecordItemBeans -> {
            mAdapter.setData(false, picRecordItemBeans);
            mAdapter.notifyDataSetChanged();
            swipeRefreshLayout.setRefreshing(false);
            mLoadTask = null;
        });
        mLoadTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(mLoadTask != null) {
            mLoadTask.cancel(true);
        }

        if(mPicDownloader != null) {
            mPicDownloader.cancel(true);
        }

        if(mUploadFlow != null) {
            mUploadFlow.cancel();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_pic_record, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_add) {
            startZbarScan();
        }
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REQUEST_ZBAR) {
            if(resultCode == RESULT_OK) {
                assert data != null;
                PbnScanResult rs = data.getParcelableExtra("data");
                startDownload(rs);
            }
        } else if(requestCode == REQUEST_COLOR) {
            if(resultCode == RESULT_OK) {
                if(data != null) {
                    String id = data.getStringExtra("id");
                    if(id != null) {
                        notifyUpdateItem(id);
                    }
                }
            } else if(resultCode == ColorActivity2.RESULT_INIT_FAILED) {
                String errMsg = null;
                if(data != null) {
                    errMsg = data.getStringExtra("errMsg");
                }
                SToast.show("加载失败:" + errMsg);
            }
        } else if(requestCode == REQUEST_PREVIEW_BEFORE_UPLOAD) {
            PicRecordItemBean bean = mPendingUploadItem;
            if(bean == null) {
                return;
            }
            mPendingUploadItem = null;
            doUpload(bean);
        }
    }

    private void notifyUpdateItem(String id) {
        List<PicRecordItemBean> list = mAdapter.getData();
        final int size = list.size();
        for(int i = 0; i < size; i++) {
            PicRecordItemBean bean = list.get(i);
            if(bean.id.equals(id)) {
                bean.setHasArchive(true);
                mAdapter.notifyItemChanged(i);
                return;
            }
        }
    }

    private void startZbarScan() {
        SToast.show("正在打开相机");
        Intent intent = new Intent(this, ZBarActivity.class);
        startActivityForResult(intent, REQUEST_ZBAR);
    }

    private void startDownload(PbnScanResult rs) {
        mProgressDialog.show();
        mProgressDialog.setMessage("下载中");
        mProgressDialog.setCancelable(false);

        mPicDownloader = new PicDownloader2(uid, true, rs.getUrl(),
                rs.getPbnType(), result -> {
            mProgressDialog.dismiss();
            if(result.code == PicDownloader2.RESULT_OK) {
                PicRecordItemBean bean = PicRecordLoader.loadForIdSync(uid, result.generatedId, result.pbnType);
                insertToView(bean);
            } else {
                SToast.show(result.errMsg);
            }
        });
        mPicDownloader.executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
    }

    private void insertToView(PicRecordItemBean itemBean) {
        mAdapter.getData().add(0, itemBean);
        mAdapter.notifyItemInserted(0);
    }

    @Override
    public void onUploadSuccess(String id) {

        if(mUploadingDialog != null && mUploadingDialog.isShowing()) {
            mUploadingDialog.setUIResult(true, id);
        }

    }

    @Override
    public void onUploadFailed(int code, String msg) {
        if(mUploadingDialog != null && mUploadingDialog.isShowing()) {
            mUploadingDialog.setUIResult(false, msg);
        }
    }

    @Override
    public void onUploadProgress(String msg) {
        if(mUploadingDialog != null && mUploadingDialog.isShowing()) {
            mUploadingDialog.setProgressMsg(msg);
        }
    }

    @Override
    public void onUploadCanceled() {
        if(mUploadingDialog != null && mUploadingDialog.isShowing()) {
            mUploadingDialog.dismiss();
        }
    }

    @Override
    public void onPicItemDeleteAction(PicRecordItemBean bean, int pos) {
        alertDelete(bean, pos);
    }

    @Override
    public void onPicItemCheckAreaAction(PicRecordItemBean bean, int pos) {
        AreaLookActivity.startForLook(this, bean.id, true, uid);
    }

    @Override
    public void onPicItemUploadAction(PicRecordItemBean bean, int pos) {
        mPendingUploadItem = bean;
        AreaLookActivity.startForUpload(this, bean.id, uid, true, REQUEST_PREVIEW_BEFORE_UPLOAD);
    }

    @Override
    public void onPicItemThumbnailAction(PicRecordItemBean bean, int pos) {
        //todo
    }

    private void performDelete(PicRecordItemBean bean, int pos) {
        if(mAdapter.getData().remove(pos) == bean) {
            mAdapter.notifyItemRemoved(pos);
        } else {
            mAdapter.notifyDataSetChanged();
        }
        String id = bean.id;

        new Thread(() -> {
            File imgDir = LocalData.getTargetFileRecordDir(uid, true, id);
            PbnDb.get().getPicRecordDao().deleteById(uid, id);
            Files.rm_rf(imgDir);
            runOnUiThread(() -> SToast.show("removed"));
        }).start();
    }

    private void alertDelete(PicRecordItemBean bean, int pos) {
        new AlertDialog.Builder(this)
                .setMessage("确定在手机上删除该素材（不能找回）？")
                .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        performDelete(bean, pos);
                    }
                })
                .setNegativeButton("取消", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
    }
}
