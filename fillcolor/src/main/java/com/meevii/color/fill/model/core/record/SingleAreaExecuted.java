package com.meevii.color.fill.model.core.record;

import com.meevii.color.fill.model.core.action.FillColorAction;
import com.meevii.color.fill.model.core.PreparedFillColorTask;

/**
 * Created by ober on 2019/5/6.
 */
public class SingleAreaExecuted extends Executed {
    public FillColorAction fillColorAction;
    public int fillType;
    public Integer preColor;

    public static SingleAreaExecuted fromPreparedFillColorTask(PreparedFillColorTask task) {
        SingleAreaExecuted executed = new SingleAreaExecuted();
        executed.fillColorAction = task.fillColorAction;
        executed.fillType = task.fillType;
        executed.preColor = task.preColor;
        return executed;
    }

}
