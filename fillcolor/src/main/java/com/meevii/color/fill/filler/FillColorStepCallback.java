package com.meevii.color.fill.filler;

/**
 * Created by ober on 2018/11/23.
 */
public interface FillColorStepCallback {

    void callback(int l, int t, int r, int b);
}
