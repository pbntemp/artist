package com.meevii.color.fill.view;

import android.graphics.Path;

/**
 * Created by ober on 18-8-2.
 */
public class PathAnimUtil {

    public static Path genPath(int radius, int progressWidth) {
        int r = radius + progressWidth;
        Path path = new Path();
        path.moveTo((float) (r - radius * 0.35), (float) (r * 1.1));
        path.lineTo((float) (r - radius * 0.05), (float) (r + radius * 0.4));
        path.lineTo((float) (r + radius * 0.5), (float) (r - radius * 0.25));
        return path;
    }
}
