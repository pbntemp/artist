package com.meevii.color.fill.model.core.origin;

import android.graphics.Bitmap;

/**
 * Created by ober on 18-8-16.
 */
public class BitmapOriginImage extends ColorOriginImage {

    private final Bitmap bmp;

    private BitmapOriginImage(Bitmap b) {
        bmp = b;
    }

    public static ColorOriginImage create(Bitmap bmp) {
        return new BitmapOriginImage(bmp);
    }

    public Bitmap getBitmap() {
        return bmp;
    }
}
