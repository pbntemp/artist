package com.meevii.color.fill.draw.codec;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.pdf.PdfRenderer;
import android.net.Uri;
import android.os.Build;
import android.os.ParcelFileDescriptor;
import androidx.annotation.ColorInt;

import com.meevii.color.fill.view.gestures.decoder.ImageRegionDecoder;

import java.io.File;
import java.io.IOException;


/**
 * Created by ober on 18-8-15.
 */

@TargetApi(Build.VERSION_CODES.LOLLIPOP)
public class PDFRegionDecoder implements ImageRegionDecoder {
    /**
     * the page that will be rendered to a bitmap.
     */
    private PdfRenderer renderer;

    /**
     * defines the initial scale of the picture
     */
    private float scale;

    /**
     * the current page position in the pdf
     */
    private int position;

    /**
     * the pdf page
     */
    private PdfRenderer.Page page;

    /**
     * the file descriptor
     */
    private ParcelFileDescriptor descriptor;

    /**
     * the pdf file
     */
    private File file;

    /**
     * the background color of the pdf file. Default is white
     */
    private int backgroundColorPdf;

    /**
     * basic constructor for PDFDecoder.
     *
     * @param position:the current position in the pdf
     * @param file:        the pdf-file
     * @param scale:       the scale factor
     */
    public PDFRegionDecoder(int position, File file, float scale) {
        this(position, file, scale, Color.TRANSPARENT);
    }

    /**
     * extended constructor for PDFDecoder with a customizable background color.
     *
     * @param position:the        current position in the pdf
     * @param file:               the pdf-file
     * @param scale:              the scale factor
     * @param backgroundColorPdf: the background color of the pdf
     */
    public PDFRegionDecoder(int position, File file, float scale, @ColorInt int backgroundColorPdf) {
        this.file = file;
        this.scale = scale;
        this.position = position;
        this.backgroundColorPdf = backgroundColorPdf;
    }

    /**
     * Initializes the region decoder. This method initializes
     *
     * @param context not used here
     * @param uri     not used here (file is already loaded)
     * @return the rescaled point
     * @throws Exception
     */
    @Override
    public Point init(Context context, Uri uri) throws Exception {

        this.descriptor = ParcelFileDescriptor.open(file, ParcelFileDescriptor.MODE_READ_ONLY);
        this.renderer = new PdfRenderer(descriptor);
        page = renderer.openPage(position);

        return new Point((int) ((page.getWidth() * scale + 0.5f)),
                (int) ((page.getHeight() * scale + 0.5f)));
    }

    /**
     * Creates a {@link Bitmap} in the correct size and renders the region defined by rect of the
     * {@link PdfRenderer.Page} into it.
     *
     * @param rect       the rect of the {@link PdfRenderer.Page} to be rendered to the bitmap
     * @param sampleSize the sample size
     * @return a bitmap containing the rendered rect of the page
     */
    @Override
    public Bitmap decodeRegion(Rect rect, int sampleSize) {

        int bitmapWidth = rect.width() / sampleSize;
        int bitmapHeight = rect.height() / sampleSize;

        Bitmap bitmap = Bitmap.createBitmap(bitmapWidth, bitmapHeight,
                Bitmap.Config.ARGB_8888);

        Matrix matrix = new Matrix();
        matrix.setScale(scale / sampleSize, scale / sampleSize);
        matrix.postTranslate(-rect.left / sampleSize,  -rect.top / sampleSize);
//        Canvas canvas = new Canvas(bitmap);
//        //canvas.drawColor(backgroundColorPdf);
//        canvas.drawBitmap(bitmap, 0, 0, null);
        page.render(bitmap, null, matrix, PdfRenderer.Page.RENDER_MODE_FOR_DISPLAY);

        return bitmap;
    }

    @Override
    public boolean isReady() {
        return true;
    }

    /**
     * close everything
     */
    @Override
    public void recycle() {
        page.close();
        renderer.close();
        try {
            descriptor.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}