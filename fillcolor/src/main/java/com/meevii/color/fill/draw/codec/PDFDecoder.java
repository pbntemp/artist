package com.meevii.color.fill.draw.codec;


import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.pdf.PdfRenderer;
import android.net.Uri;
import android.os.Build;
import android.os.ParcelFileDescriptor;

import com.meevii.color.fill.view.gestures.decoder.ImageDecoder;

import java.io.File;


/**
 * Created by ober on 18-8-15.
 */

@TargetApi(Build.VERSION_CODES.LOLLIPOP)
public class PDFDecoder implements ImageDecoder {

    /**
     * defines the initial scale of the picture
     */
    private float scale;

    /**
     * the current pdf site
     */
    private int position;

    /**
     * the pdf file to render
     */
    private File file;

    /**
     * basic constructor for PDFDecoder.
     *
     * @param scale the scale to get from {@link Point} to Pixel
     */
    public PDFDecoder(int position, File file, float scale) {
        this.file = file;
        this.scale = scale;
        this.position = position;
    }

    /**
     * Creates a {@link Bitmap}in the correct size and renders the {@link PdfRenderer.Page} into it.
     *
     * @param context not used
     * @param uri     not used
     * @return a bitmap, containing the pdf page
     * @throws Exception, if rendering fails
     */
    @Override
    public Bitmap decode(Context context, Uri uri) throws Exception {
        ParcelFileDescriptor descriptor =
                ParcelFileDescriptor.open(file, ParcelFileDescriptor.MODE_READ_ONLY);

        PdfRenderer renderer = new PdfRenderer(descriptor);
        final PdfRenderer.Page page = renderer.openPage(position);

        Bitmap bitmap = Bitmap.createBitmap((int) (page.getWidth() * scale + 0.5),
                (int) (page.getHeight() * scale + 0.5f), Bitmap.Config.ARGB_8888);

        page.render(bitmap, null, null, PdfRenderer.Page.RENDER_MODE_FOR_DISPLAY);

        page.close();
        renderer.close();
        descriptor.close();

        return bitmap;
    }
}