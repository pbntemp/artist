package com.meevii.color.fill.model.core.resp;

/**
 * Created by ober on 19-4-28.
 */
public class RevokeSingleColorResp {

    public Integer targetColor;
    public Integer preColor;
    public int revokedFillType;

    public boolean isBack;
}
