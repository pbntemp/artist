package com.meevii.color.fill.filler;

import android.graphics.Bitmap;
import android.util.Log;

import com.meevii.color.fill.IFillColorFilter;
import com.meevii.color.fill.model.core.FloodFillArea;
import com.meevii.color.fill.model.core.action.OverwriteColorAction;
import com.meevii.color.fill.model.core.record.SingleAreaExecuted;
import com.meevii.color.fill.model.core.task.FillByClickColorTask;
import com.meevii.color.fill.model.core.action.FillColorAction;
import com.meevii.color.fill.model.core.PreparedFillColorTask;
import com.socks.library.KLog;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ober on 2018/11/8.
 */
public class FillColorFillerN implements IFillColorFilter {

    static {
        System.loadLibrary("pbn-lib");
    }

    private static native long nNewProgram();

    private static native int nInitRegion(long program, String filepath);

    private static native int nInitEditable(long program, Bitmap editableBitmap);

    private static native int nInitRegionAreas(long program);

    private static native int nPreFillAreas(long program, int[] data);

    @Deprecated
    private static native int nPreFillBadAreas(long program);

    private static native int nDestroy(long program);
    private static native int nProcessFillByColor(long program, int x, int y, boolean hasColor, int color,
                                             int[] outResult, int[] outArea, int[] preColor);
    private static native int nFillForSingleColor(long program, boolean hasColor, int color, int[] areaNum);

    private static native int[] nOverWriteColor(long program, int srcColor, boolean dstHasColor, int dstColor);

    private static native int nResetEditBmp(long program);

    private static native int nResetEditBmpAndInit(long program, int[] areas, int color);

    private static native int nResetEditBmpAndInitMap(long program, int[] map);

    private static native int nGetAreaMapSize(long program);
    private static native int nGetAreaMap(long program, int[] keys, int[] floodFillAreas);

    private static native int nFillForSingleColorStep(long program,
                                                      int x, int y,
                                                      boolean hasColor,
                                                      int color,
                                                      int areaNum,
                                                      FillColorStepCallback callback);

    public static native String nVersion();

    public static native String nGenCenterStr(Bitmap regionBmp, Bitmap originBmp);

    private Bitmap mEditBitmap;

    private boolean isChanged = false;

    private volatile boolean isAlive;

    private final long mProgram;

    private final boolean ripple;

    private FillColorStepCallback mInvalidateTask;

    private FillColorFillerN(boolean ripple, FillColorStepCallback runnable) {
        isAlive = true;
        mProgram = nNewProgram();
        mInvalidateTask = runnable;
        this.ripple = ripple;
    }

    public static IFillColorFilter newInstance(boolean hasRipple, FillColorStepCallback invalidateTask) {
        Log.i("NFillColorFilter", "init version = " + FillColorFillerN.nVersion());
        return new FillColorFillerN(hasRipple, invalidateTask);
    }

    public FillColorAction revokeBackFillTask(SingleAreaExecuted singleAreaExecuted) {
        if(!isAlive) {
            return null;
        }

        final int num = singleAreaExecuted.fillColorAction.number;

        final int type = singleAreaExecuted.fillType;
        switch (type) {
            case 0://same color
                return new FillColorAction(num, null);
            case 1://fill color
                return new FillColorAction(num, singleAreaExecuted.fillColorAction.color);
            case 2://override color
                return new FillColorAction(num, singleAreaExecuted.fillColorAction.color);
            case 3://clear color
                return new FillColorAction(num, null);
        }

        throw new RuntimeException("bad type revoke back " + type);
    }

    public FillColorAction revokeFillTask(SingleAreaExecuted singleAreaExecuted) {
        if(!isAlive) {
            return null;
        }

        final int num = singleAreaExecuted.fillColorAction.number;

        final int type = singleAreaExecuted.fillType;
        switch (type) {
            case 0://same color
                return new FillColorAction(num, singleAreaExecuted.preColor);
            case 1://fill color
                return new FillColorAction(num, null);
            case 2://override color
                return new FillColorAction(num, singleAreaExecuted.preColor);
            case 3://clear color
                return new FillColorAction(num, singleAreaExecuted.preColor);
        }

        throw new RuntimeException("bad type revoke " + type);
    }

    public PreparedFillColorTask processFillByColor(FillByClickColorTask t) {
        if(!isAlive) {
            return null;
        }

        PreparedFillColorTask preparedFillColorTask = new PreparedFillColorTask();

        Integer color = t.color;
        final int c;
        final boolean hasColor;
        if (color == null) {
            c = 0;
            hasColor = false;
        } else {
            c = color;
            hasColor = true;
        }
        int[] result = new int[1];
        int[] area = new int[1];
        int[] preColor = new int[1];

        nProcessFillByColor(mProgram, t.x, t.y, hasColor, c, result, area, preColor);

        //outResult
        //-1 : error xy
        //-2 : error data
        //-3 : bad area
        //0  : same color
        //1  : fill color
        //2  : over write color
        //3  : clear color
        //4  : none color

        if(result[0] < 0) {

            if(result[0] == -3) {
                KLog.w("FillColorFillerN", "bad area intercepted");
            }
            return null;
        }

        if(result[0] == 0) {
            preparedFillColorTask.fillColorAction = new FillColorAction(area[0], null);
            preparedFillColorTask.fillType = 0;
            preparedFillColorTask.preColor = preColor[0];
            return preparedFillColorTask;
        }

        if(result[0] == 1 || result[0] == 2) {
            FillColorAction task = new FillColorAction(area[0], color);
            task.xy = new int[] {t.x, t.y};
            preparedFillColorTask.fillType = result[0];
            preparedFillColorTask.fillColorAction = task;
            if(result[0] == 2) {
                preparedFillColorTask.preColor = preColor[0];
            }
            return preparedFillColorTask;
        }

        if(result[0] == 3) {
            FillColorAction task = new FillColorAction(area[0], null);
            task.xy = new int[] {t.x, t.y};
            preparedFillColorTask.preColor = preColor[0];
            preparedFillColorTask.fillType = 3;
            preparedFillColorTask.fillColorAction = task;
            return preparedFillColorTask;
        }

        return null;
    }

//    @Override
//    public void initRegion(Bitmap calculateBitmap) {
//        if(!isAlive) {
//            return;
//        }
//
//        nInitRegion(mProgram, calculateBitmap);
//    }

    @Override
    public int initRegion(File regionFile) {
        if(!isAlive) {
            return -1;
        }

        int r = nInitRegion(mProgram, regionFile.getAbsolutePath());
        if(r == 1) {
            return 1;
        }
        return -1;
    }

    @Deprecated //测试
    public static int test(String regionFile) {
        long p = nNewProgram();
        nInitRegion(p, regionFile);
        int r = nInitRegionAreas(p);
        nDestroy(p);
        return r;
    }

    @Override
    public void initEditable(Bitmap editableBitmap) {
        if(!isAlive) {
            return;
        }

        nInitEditable(mProgram, editableBitmap);
        mEditBitmap = editableBitmap;
    }

    @Override
    public void initRegionAreas() {
        if(!isAlive) {
            return;
        }
        nInitRegionAreas(mProgram);
    }

    @Override
    public void preFillAreas(HashMap<Integer, FloodFillArea> areaMap) {

        if(!isAlive) {
            return;
        }

        int size = areaMap.size();
        int[] data = new int[size * 3];
        int i = 0;
        for(Map.Entry<Integer, FloodFillArea> entry : areaMap.entrySet()) {
            int key = entry.getKey();
            data[3 * i] = key;
            Integer color = entry.getValue().color;
            if(color == null) {
                data[3 * i + 1] = 0;
                data[3 * i + 2] = 0;
            } else {
                data[3 * i + 1] = 1;
                data[3 * i + 2] = color;
            }
            i++;
        }
        nPreFillAreas(mProgram, data);
    }

    @Override
    public void destroy() {
        isAlive = false;
        mInvalidateTask = null;
        nDestroy(mProgram);
    }

    @Override
    public int getEditState() {
        if(isChanged) {
            return STATE_CHANGED;
        }
        return STATE_NO_CHANGED;
    }

    @Override
    public void resetToCalculateBitmap() {
        if(!isAlive) {
            return;
        }
        isChanged = nResetEditBmp(mProgram) >= 0;
    }

    @Override
    public void resetToCalculateBitmapAndInit(List<Integer> areas, int color) {
        if(!isAlive) {
            return;
        }

        final int count = areas.size();
        int[] areaArr = new int[count];
        for(int i = 0; i < count; i++) {
            areaArr[i] = areas.get(i);
        }
        int r = nResetEditBmpAndInit(mProgram, areaArr, color);
        if(r != count) {
            KLog.w("FillColorFillerN", "input and result count not match!! " + count + "--" + r);
        }

        isChanged = r >= 0;
    }

    @Override
    public void resetToCalculateBitmapAndInitMap(Map<Integer, Integer> areaColorMap) {
        if(!isAlive) {
            return;
        }

        if(areaColorMap == null) {
            return;
        }

        final int count = areaColorMap.size();
        if(count == 0) {
            return;
        }

        final int[] dataArr = new int[count * 2];
        int index = 0;
        for(Map.Entry<Integer, Integer> entry : areaColorMap.entrySet()) {
            dataArr[index * 2] = entry.getKey();
            dataArr[index * 2 + 1] = entry.getValue();
            index ++;
        }

        nResetEditBmpAndInitMap(mProgram, dataArr);

        isChanged = true;
    }

    @Override
    public Bitmap getEditedBitmap() {
        return mEditBitmap;
    }

    @Override
    public HashMap<Integer, FloodFillArea> getAreaMap() {

        if(!isAlive) {
            return null;
        }

        int size = nGetAreaMapSize(mProgram);
        int[] keys = new int[size];
        int[] areaData = new int[size * 6];

        nGetAreaMap(mProgram, keys, areaData);

        HashMap<Integer, FloodFillArea> map = new HashMap<>();
        for(int i = 0; i < size; i++) {
            final int key = keys[i];

            FloodFillArea area = new FloodFillArea();
            area.left = areaData[i * 6];
            area.top = areaData[i * 6 + 1];
            area.right = areaData[i * 6 + 2];
            area.bottom = areaData[i * 6 + 3];

            if(areaData[i * 6 + 4] == 0) {
                area.color = null;
            } else {
                area.color = areaData[i * 6 + 5];
            }

            map.put(key, area);
        }

        return map;
    }

    @Override
    public int[] overwriteColor(OverwriteColorAction action) {
        if(!isAlive) {
            return null;
        }

        final int srcColor = action.srcColor;
        final Integer dstColor = action.dstColor;
        final boolean hasColor = dstColor != null;
        KLog.d("FillColorFillerN", "overwriteColor " + srcColor + "--" + dstColor);

        return nOverWriteColor(mProgram, srcColor, hasColor, hasColor ? dstColor : 0);
    }

    @Override
    public boolean floodFill(FillColorAction action) {

        if(!isAlive) {
            return false;
        }

        final int number = action.number;
        final Integer color = action.color;

        KLog.d("FillColorFillerN", "floodFill " + number + "--" + color);


        boolean useRipple = ripple && mEditBitmap.getWidth() <= 1024;

        boolean hasColor = color != null;
        int r;
        if(hasColor) {
            if(action.xy != null && useRipple) {
                r = nFillForSingleColorStep(mProgram, action.xy[0], action.xy[1], true, color, number, new FillColorStepCallback() {
                    @Override
                    public void callback(int l, int t, int r, int b) {
                        if(mInvalidateTask != null) {
                            mInvalidateTask.callback(l, t, r, b);
                        }
                    }
                });
            } else {
                r = nFillForSingleColor(mProgram, true, color, new int[]{number});
            }
        } else {
            r = nFillForSingleColor(mProgram, false, 0, new int[]{number});
        }

        isChanged = r > 0;

        return r > 0;
    }

    @Override
    public void onActivityDestroy() {
        isAlive = false;
        mInvalidateTask = null;
    }


}
