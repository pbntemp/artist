package com.meevii.color.fill.model.core.resp;

/**
 * Created by ober on 19-5-6.
 */
public class OverwriteColorResp {

    public int srcColor;
    public Integer dstColor;
    public int[] areas;
}
