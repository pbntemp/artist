package com.meevii.color.fill.model.core;

import com.meevii.color.fill.model.core.action.FillColorAction;

/**
 * Created by ober on 19-2-26.
 */
public class PreparedFillColorTask {
    public FillColorAction fillColorAction;
    public int fillType;
    public Integer preColor;
}
