package com.meevii.color.fill.util;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.pdf.PdfRenderer;
import android.os.Build;
import android.os.ParcelFileDescriptor;

import com.meevii.color.fill.FillColorConfig;

import java.io.File;


public class FillColorHelper {


    public static Bitmap loadForegroundPng(File file, int size) {
        if (file.getName().endsWith("png")) {
            return decodeBitmapFromFile(file.getAbsolutePath(), size, size);
        }
        return null;
    }

    public static Bitmap loadForegroundBitmapForEffect(File file) {
        if (file.getName().endsWith("png")) {
            return decodeBitmapFromFile(file.getAbsolutePath(), 1024, 1024);
        }
        return loadPDF(file, 1024, 1024);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public static Bitmap loadPDF(File file, int width, int height) {
        try {
            ParcelFileDescriptor mFileDescriptor = ParcelFileDescriptor.open(file, ParcelFileDescriptor.MODE_READ_ONLY);
            PdfRenderer mPdfRenderer = new PdfRenderer(mFileDescriptor);
            // Use `openPage` to open a specific page in PDF.
            PdfRenderer.Page mCurrentPage = mPdfRenderer.openPage(0);
            // Important: the destination mDisplayBitmap must be ARGB (not RGB).
            Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            mCurrentPage.render(bitmap, null, null, PdfRenderer.Page.RENDER_MODE_FOR_DISPLAY);
            close(mCurrentPage);
            close(mPdfRenderer);
            close(mFileDescriptor);
            return bitmap;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 关闭流。
     */
    public static void close(AutoCloseable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (Exception e) {
            }
        }
    }

    /**
     * 如果bitmap的尺寸超过Calculate应有的尺寸(1024*1024)，则执行缩放。
     */
    public static Bitmap resizeCalculateBitmapIfNeed(Bitmap bitmap) {
        if (bitmap != null) {
            int width = bitmap.getWidth();
            int height = bitmap.getHeight();
            int mustWH = FillColorConfig.BASE_SIZE * FillColorConfig.BASE_SIZE;
            if (width > mustWH) {
                float scale = (float) (mustWH * 1.0 / width);
                Matrix matrix = new Matrix();
                matrix.postScale(scale, scale);
                bitmap = Bitmap.createBitmap(bitmap, 0, 0, width, height, matrix, true);
            }
        }
        return bitmap;
    }

    public static Bitmap decodeBitmapFromFile(String path, int reqWidth, int reqHeight) {
        Bitmap bitmap = BitmapFactory.decodeFile(path);
        if(bitmap == null) {
            return null;
        }
        if (reqWidth != bitmap.getWidth() || reqHeight != bitmap.getHeight()) {
            Matrix matrix = new Matrix();
            matrix.postScale(reqWidth * 1.0f / bitmap.getWidth(), reqHeight * 1.0f / bitmap.getHeight());
            bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
        }
        return bitmap;
    }

}
