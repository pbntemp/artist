package com.meevii.color.fill.model.core.action;

import androidx.annotation.Nullable;

/**
 * Created by ober on 19-5-6.
 */
public class OverwriteColorAction {

    public int srcColor;

    @Nullable
    public Integer dstColor;
}
