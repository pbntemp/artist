package com.meevii.color.fill.draw;


import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Message;
import android.view.View;

import com.meevii.color.fill.FillColorImageView;
import com.meevii.color.fill.IFillColorFilter;
import com.meevii.color.fill.filler.FillColorFillerN;
import com.meevii.color.fill.filler.FillColorStepCallback;
import com.meevii.color.fill.model.core.action.FillColorAction;
import com.meevii.color.fill.model.core.FloodFillArea;
import com.meevii.color.fill.model.core.action.OverwriteColorAction;
import com.meevii.color.fill.model.core.PreparedFillColorTask;
import com.meevii.color.fill.model.core.record.Executed;
import com.meevii.color.fill.model.core.record.OverwriteColorExecuted;
import com.meevii.color.fill.model.core.record.SingleAreaExecuted;
import com.meevii.color.fill.model.core.resp.FillColorResp;
import com.meevii.color.fill.model.core.resp.OverwriteColorResp;
import com.meevii.color.fill.model.core.resp.RevokeOverwriteResp;
import com.meevii.color.fill.model.core.resp.RevokeSingleColorResp;
import com.meevii.color.fill.model.core.task.AbsTask;
import com.meevii.color.fill.model.core.task.FillByClickColorTask;
import com.meevii.color.fill.model.core.task.OverWriteColorTask;
import com.meevii.color.fill.model.core.task.RefillTask;
import com.meevii.color.fill.model.core.task.ResetColorsTask;
import com.meevii.color.fill.model.core.task.ResetTask;
import com.meevii.color.fill.model.core.task.RevokeBackTask;
import com.meevii.color.fill.model.core.task.RevokeTask;
import com.meevii.color.fill.model.core.task.RunnableTask;
import com.meevii.color.fill.util.SizedStack;
import com.socks.library.KLog;

import java.io.File;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.concurrent.LinkedBlockingQueue;


/**
 * 颜色填充机器，主要用来维护若干的队列，维护Task。
 *
 * @author cutler
 */
public class FillColorMachine implements Runnable {

    private static final String TAG = "FillColorMachine";

    private volatile boolean isAlive;
    private Thread mThread;

    private Reference<View> mFillColorImageView;

    private IFillColorFilter mFloodFiller;

    private LinkedBlockingQueue<AbsTask> mTaskQueue;

    private Handler mHandler;

    public FillColorMachine(View imageView, boolean useRipple) {
        mFillColorImageView = new WeakReference<>(imageView);
        //mFloodFiller = FillColorFiller.newInstance();
        mFloodFiller = FillColorFillerN.newInstance(useRipple, new FillColorStepCallback() {
            @Override
            public void callback(int l, int t, int r, int b) {
                View v = mFillColorImageView.get();
                if (v != null) {
                    ((FillColorImageView) v).postInvalidateFloodFillArea(l, t, r, b);
                }
            }
        });
        mTaskQueue = new LinkedBlockingQueue<>();
    }

    /**
     * 初始化填色器
     */
    public void initAndPreFill(Bitmap editableBitmap, File calculateBitmapFile,
                               HashMap<Integer, FloodFillArea> areaMap) {

        int r = mFloodFiller.initRegion(calculateBitmapFile);
        if(r < 0) {
            calculateBitmapFile.delete();
            throw new RuntimeException("init region failed");
        }
        mFloodFiller.initEditable(editableBitmap);

        mFloodFiller.initRegionAreas();

        if (areaMap != null) {
            mFloodFiller.preFillAreas(areaMap);
        }
    }

    public void startIfNeed() {
        if (!isAlive) {

            if (mThread != null && mThread.isAlive() && !mThread.isInterrupted()) {
                mThread.interrupt();
            }

            isAlive = true;
            mThread = new Thread(this, "Drawing");
            mThread.start();
        }
    }

    public void setCallback(Handler handler) {
        mHandler = handler;
    }


    public boolean isIdle() {
        if (!isAlive) {
            return false;
        }

        return mTaskQueue.size() == 0;
    }

    public int getTaskQueueSize() {
        return mTaskQueue.size();
    }

    @Override
    public void run() {

        sendMsg(FillColorEvents.CB_THREAD_START);

        SizedStack<Executed> mExecuteCache = new SizedStack<>(30);
        SizedStack<Executed> mRevokeBackCache = new SizedStack<>(30);

        mExecuteCache.setCallback(revokeCacheStateChangeCallback);
        mRevokeBackCache.setCallback(revokeBackCacheStateChangeCallback);

        while (isAlive) {
            KLog.d(TAG, "POLL");
            try {
                AbsTask t = mTaskQueue.take();

                if (t instanceof RunnableTask) {
                    RunnableTask task = (RunnableTask) t;
                    task.callback.run();
                } else if (t instanceof FillByClickColorTask) {
                    FillByClickColorTask task = (FillByClickColorTask) t;
                    PreparedFillColorTask prepareTask = ((FillColorFillerN) mFloodFiller).processFillByColor(task);
                    if (prepareTask == null) {
                        continue;
                    }

                    boolean r = mFloodFiller.floodFill(prepareTask.fillColorAction);
                    if (r) {
                        FillColorImageView v = (FillColorImageView) mFillColorImageView.get();
                        if (v == null) {
                            continue;
                        }
                        v.postInvalidate();

                        mExecuteCache.push(SingleAreaExecuted.fromPreparedFillColorTask(prepareTask));
                        mRevokeBackCache.clear();

                        FillColorResp resp = new FillColorResp();
                        resp.color = prepareTask.fillColorAction.color;
                        resp.fillType = prepareTask.fillType;
                        resp.preColor = prepareTask.preColor;

                        sendMsg(FillColorEvents.CB_FILL_IS_RIGHT, resp,
                                prepareTask.fillColorAction.number, 0);

                    }
                } else if(t instanceof OverWriteColorTask) {

                    OverWriteColorTask task = (OverWriteColorTask) t;
                    OverwriteColorAction action = new OverwriteColorAction();
                    action.dstColor = task.dstColor;
                    action.srcColor = task.srcColor;

                    int[] filledAreas = mFloodFiller.overwriteColor(action);
                    if(filledAreas != null) {
                        FillColorImageView v = (FillColorImageView) mFillColorImageView.get();

                        if(task.cache) {
                            OverwriteColorExecuted executed = new OverwriteColorExecuted();
                            executed.preColor = task.srcColor;
                            executed.dstColor = task.dstColor;
                            executed.areas = filledAreas;
                            mExecuteCache.push(executed);
                            mRevokeBackCache.clear();
                        }

                        OverwriteColorResp resp = new OverwriteColorResp();
                        resp.areas = filledAreas;
                        resp.dstColor = task.dstColor;
                        resp.srcColor = task.srcColor;

                        sendMsg(FillColorEvents.CB_OVERWRITE_COLOR, resp);

                        if (v == null) {
                            continue;
                        }
                        v.postInvalidate();
                    }

                } else if (t instanceof RevokeTask) {
                    Executed executed = mExecuteCache.pop();

                    if (executed == null) {
                        return;
                    }

                    if (executed instanceof SingleAreaExecuted) {

                        SingleAreaExecuted singleAreaExecuted = (SingleAreaExecuted) executed;

                        RevokeSingleColorResp resp = new RevokeSingleColorResp();
                        resp.targetColor = singleAreaExecuted.fillColorAction.color;
                        resp.preColor = singleAreaExecuted.preColor;
                        resp.revokedFillType = singleAreaExecuted.fillType;

                        FillColorAction action = ((FillColorFillerN) mFloodFiller).revokeFillTask(singleAreaExecuted);
                        if (action == null) {
                            return;
                        }
                        boolean r = mFloodFiller.floodFill(action);
                        if (r) {

                            mRevokeBackCache.push(executed);

                            FillColorImageView v = (FillColorImageView) mFillColorImageView.get();

                            sendMsg(FillColorEvents.CB_FILL_REVOKE_SUC, resp);

                            if (v == null) {
                                continue;
                            }
                            v.postInvalidate();
                        } else {
                            sendMsg(FillColorEvents.CB_FILL_REVOKE_FAIL);
                        }
                    } else if(executed instanceof OverwriteColorExecuted) {
                        OverwriteColorExecuted overwriteColorExecuted = (OverwriteColorExecuted) executed;

                        final int[] areas = overwriteColorExecuted.areas;
                        final int count = areas.length;
                        final int preColor = overwriteColorExecuted.preColor;
                        for(int i = 0; i < count; i++) {
                            FillColorAction action = new FillColorAction(areas[i], preColor);
                            mFloodFiller.floodFill(action);
                        }

                        RevokeOverwriteResp resp = new RevokeOverwriteResp();
                        resp.areas = areas;
                        resp.dstColor = overwriteColorExecuted.dstColor;
                        resp.preColor = preColor;

                        mRevokeBackCache.push(executed);

                        FillColorImageView v = (FillColorImageView) mFillColorImageView.get();

                        sendMsg(FillColorEvents.CB_OVERWRITE_REVOKE_SUC, resp);

                        if(v != null) {
                            v.postInvalidate();
                        }
                    }
                } else if(t instanceof RevokeBackTask) {

                    Executed executed = mRevokeBackCache.pop();

                    if (executed == null) {
                        return;
                    }

                    if (executed instanceof SingleAreaExecuted) {

                        SingleAreaExecuted singleAreaExecuted = (SingleAreaExecuted) executed;

                        RevokeSingleColorResp resp = new RevokeSingleColorResp();

                        resp.targetColor = singleAreaExecuted.fillColorAction.color;
                        resp.preColor = singleAreaExecuted.preColor;
                        resp.revokedFillType = singleAreaExecuted.fillType;
                        resp.isBack = true;

                        FillColorAction action = ((FillColorFillerN) mFloodFiller).revokeBackFillTask(singleAreaExecuted);
                        if (action == null) {
                            return;
                        }
                        boolean r = mFloodFiller.floodFill(action);
                        if (r) {

                            mExecuteCache.push(executed);

                            FillColorImageView v = (FillColorImageView) mFillColorImageView.get();

                            sendMsg(FillColorEvents.CB_FILL_REVOKE_BACK_SUC, resp);

                            if (v == null) {
                                continue;
                            }
                            v.postInvalidate();
                        } else {
                            sendMsg(FillColorEvents.CB_FILL_REVOKE_BACK_FAIL);
                        }
                    } else if(executed instanceof OverwriteColorExecuted) {
                        OverwriteColorExecuted overwriteColorExecuted = (OverwriteColorExecuted) executed;

                        final int[] areas = overwriteColorExecuted.areas;
                        final int count = areas.length;
                        final int preColor = overwriteColorExecuted.preColor;
                        for(int i = 0; i < count; i++) {
                            FillColorAction action = new FillColorAction(areas[i], overwriteColorExecuted.dstColor);
                            mFloodFiller.floodFill(action);
                        }

                        RevokeOverwriteResp resp = new RevokeOverwriteResp();
                        resp.areas = areas;
                        resp.dstColor = overwriteColorExecuted.dstColor;
                        resp.preColor = preColor;
                        resp.isBack = true;

                        mExecuteCache.push(executed);

                        FillColorImageView v = (FillColorImageView) mFillColorImageView.get();

                        sendMsg(FillColorEvents.CB_OVERWRITE_REVOKE_BACK_SUC, resp);

                        if(v != null) {
                            v.postInvalidate();
                        }
                    }


                } else if(t instanceof ResetTask) {
                    mFloodFiller.resetToCalculateBitmap();

                    mExecuteCache.clear();

                    sendMsg(FillColorEvents.CB_RESET_SUC);

                    FillColorImageView v = (FillColorImageView) mFillColorImageView.get();

                    if(v != null) {
                        v.postInvalidate();
                    }

                } else if(t instanceof ResetColorsTask) {
                    ResetColorsTask task = (ResetColorsTask) t;

                    int size = task.areas.size();

                    mFloodFiller.resetToCalculateBitmapAndInit(task.areas, task.color);

                    sendMsg(FillColorEvents.CB_RESET_SUC, size);

                    FillColorImageView v = (FillColorImageView) mFillColorImageView.get();

                    if(v != null) {
                        v.postInvalidate();
                    }
                } else if(t instanceof RefillTask) {
                    RefillTask task = (RefillTask) t;

                    mFloodFiller.resetToCalculateBitmapAndInitMap(task.refillMap);

                    sendMsg(FillColorEvents.CB_REFILL_SUC);

                    FillColorImageView v = (FillColorImageView) mFillColorImageView.get();

                    if(v != null) {
                        v.postInvalidate();
                    }
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        mExecuteCache.setCallback(null);

        mFloodFiller.destroy();

        sendMsg(FillColorEvents.CB_THREAD_END);
    }


    private SizedStack.StateChangeCallback revokeCacheStateChangeCallback = empty
            -> sendMsg(FillColorEvents.CB_REVOKE_CACHE_STATE_CHANGE, empty ? 1 : 0);

    private SizedStack.StateChangeCallback revokeBackCacheStateChangeCallback = empty
            -> sendMsg(FillColorEvents.CB_REVOKE_BACK_CACHE_STATE_CHANGE, empty ? 1 : 0);

    /**
     * append a new task
     */
    public void schedule(AbsTask task) {
        try {
            mTaskQueue.put(task);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * 返回当前正在编辑的Bitmap
     */
    public Bitmap getEditedBitmap() {
        return mFloodFiller.getEditedBitmap();
    }

    /**
     * 返回当前正在编辑的Bitmap的填色区域
     */
    public HashMap<Integer, FloodFillArea> getAreaMap() {
        return mFloodFiller.getAreaMap();
    }

    /**
     * 返回图片的编辑状态
     */
    public int getEditState() {
        return mFloodFiller.getEditState();
    }

    /**
     * 将图片重置为未编辑状态
     */
    public void resetToCalculateBitmap() {
        mTaskQueue.clear();
        mFloodFiller.resetToCalculateBitmap();
        if (mFillColorImageView.get() != null) {
            mFillColorImageView.get().postInvalidate();
        }
    }

    /**
     * 停止子线程，并释放资源。
     */
    public void release() {
        isAlive = false;
        mTaskQueue.clear();

        mFloodFiller.onActivityDestroy();

        if (mHandler != null) {
            mHandler.removeCallbacksAndMessages(null);
        }

        if (mThread != null && mThread.isAlive() && !mThread.isInterrupted()) {
            mThread.interrupt();
        }
        if (mFillColorImageView != null) {
            FillColorImageView iv = (FillColorImageView) mFillColorImageView.get();
            if (iv != null) {
                iv.destroy();
                mFillColorImageView.clear();
            }
        }
    }

    private void sendMsg(int msgWhat) {
        if (mHandler != null) {
            mHandler.sendEmptyMessage(msgWhat);
        }
    }

    private void sendMsg(int msgWhat, int arg1) {
        if (mHandler != null) {
            Message message = mHandler.obtainMessage();
            message.what = msgWhat;
            message.arg1 = arg1;
            mHandler.sendMessage(message);
        }
    }

    private void sendMsg(int msgWhat, Object obj) {
        if (mHandler != null) {
            Message message = mHandler.obtainMessage();
            message.what = msgWhat;
            message.obj = obj;
            mHandler.sendMessage(message);
        }
    }

    private void sendMsg(int msgWhat, Object o, int arg1, int arg2) {
        if (mHandler != null) {
            Message message = mHandler.obtainMessage();
            message.what = msgWhat;
            message.arg1 = arg1;
            message.arg2 = arg2;
            message.obj = o;
            mHandler.sendMessage(message);
        }
    }
}
