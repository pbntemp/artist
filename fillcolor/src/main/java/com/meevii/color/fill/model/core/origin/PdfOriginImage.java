package com.meevii.color.fill.model.core.origin;

import java.io.File;

/**
 * Created by ober on 18-8-16.
 */
public class PdfOriginImage extends ColorOriginImage {
    private final File file;
    private final float pdfScale;

    private PdfOriginImage(File file, float pdfScale) {
        this.file = file;
        this.pdfScale = pdfScale;
    }

    public File getFile() {
        return file;
    }

    public float getPdfScale() {
        return pdfScale;
    }

    public static ColorOriginImage fromFile(File file, float scale) {
        return new PdfOriginImage(file, scale);
    }
}
