package com.meevii.color.fill.filler;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

// https://stackoverflow.com/questions/16968412/how-to-use-flood-fill-algorithm-in-android

public final class RegionGenerator {

    private int mWidth;
    private int mHeight;
    private int[] mCalculatePixels;
    private boolean[] mPixelsChecked;
    private Queue<FloodFillRange> mRanges;

    // prefill
    private int prefillNumber = 1;

    public int[] init(int mWidth, int mHeight, int[] mCalculatePixels) {
        this.mWidth = mWidth;
        this.mHeight = mHeight;
        this.mCalculatePixels = mCalculatePixels;
        mPixelsChecked = new boolean[mCalculatePixels.length];
        mRanges = new LinkedList<>();
        for (int x = 0; x < mHeight; x++) {
            for (int y = 0; y < mWidth; y++) {
                if (innerFloodFill(x, y)) {
                    prefillNumber++;
                }
            }
        }
        mPixelsChecked = null;
        mRanges = null;
        for (int i = 0; i < mCalculatePixels.length; i++) {
            mCalculatePixels[i] |= 0xFF000000;
        }
        return mCalculatePixels;
    }


    private boolean innerFloodFill(int x, int y) {
        int pxIdx = (mWidth * y) + x;
        if (mCalculatePixels[pxIdx] != 0) {
            return false;
        }
        Arrays.fill(mPixelsChecked, false);
        mRanges.clear();
        // ***Do first call to floodfill.
        doLinearFill(x, y);
        // ***Call floodfill routine while floodfill mRanges still exist on the
        // queue
        FloodFillRange range;
        while (mRanges.size() > 0) {
            // **Get Next Range Off the Queue
            range = mRanges.remove();
            // **Check Above and Below Each Pixel in the Floodfill Range
            int downPxIdx = (mWidth * (range.Y + 1)) + range.startX;
            int upPxIdx = (mWidth * (range.Y - 1)) + range.startX;
            int upY = range.Y - 1;// so we can pass the y coord by ref
            int downY = range.Y + 1;
            for (int i = range.startX; i <= range.endX; i++) {
                try {
                    // *Start Fill Upwards
                    // if we're not above the top of the mDisplayBitmap and the pixel above
                    // this one is within the color mTolerance
                    if (range.Y > 0 && !mPixelsChecked[upPxIdx]
                            && mCalculatePixels[upPxIdx] == 0)
                        doLinearFill(i, upY);
                    // *Start Fill Downwards
                    // if we're not below the bottom of the mDisplayBitmap and the pixel
                    // below this one is within the color mTolerance
                    if (range.Y < (mHeight - 1) && !mPixelsChecked[downPxIdx]
                            && mCalculatePixels[downPxIdx] == 0)
                        doLinearFill(i, downY);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                downPxIdx++;
                upPxIdx++;
            }
        }
        return true;
    }

    private void doLinearFill(int x, int y) {
        // ***Find Left Edge of Color Area
        int lFillLoc = x; // the location to check/fill on the left
        int pxIdx = (mWidth * y) + x;
        while (true) {
            // **fill with the color
            mCalculatePixels[pxIdx] = prefillNumber;
            // **indicate that this pixel has already been checked and filled
            mPixelsChecked[pxIdx] = true;
            // **de-increment
            lFillLoc--; // de-increment counter
            pxIdx--; // de-increment pixel index
            // **exit loop if we're at edge of mDisplayBitmap or color area
            if (lFillLoc < 0 || (mPixelsChecked[pxIdx]) || mCalculatePixels[pxIdx] != 0) {
                break;
            }
        }
        lFillLoc++;
        // ***Find Right Edge of Color Area
        int rFillLoc = x; // the location to check/fill on the left
        pxIdx = (mWidth * y) + x;
        while (true) {
            // **fill with the color
            mCalculatePixels[pxIdx] = prefillNumber;
            // **indicate that this pixel has already been checked and filled
            mPixelsChecked[pxIdx] = true;
            // **increment
            rFillLoc++; // increment counter
            pxIdx++; // increment pixel index
            // **exit loop if we're at edge of mDisplayBitmap or color area
            if (rFillLoc >= mWidth || mPixelsChecked[pxIdx] || mCalculatePixels[pxIdx] != 0) {
                break;
            }
        }
        rFillLoc--;
        mRanges.offer(new FloodFillRange(lFillLoc, rFillLoc, y));
    }

    private static final class FloodFillRange {
        public int startX;
        public int endX;
        public int Y;

        public FloodFillRange(int startX, int endX, int y) {
            this.startX = startX;
            this.endX = endX;
            this.Y = y;
        }
    }

}