package com.meevii.color.fill.model.core.task;

import java.util.List;

/**
 * Created by ober on 19-9-5.
 */
public class ResetColorsTask extends AbsTask {

    public List<Integer> areas;
    public int color;

}
