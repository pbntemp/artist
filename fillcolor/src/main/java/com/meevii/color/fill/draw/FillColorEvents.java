package com.meevii.color.fill.draw;

/**
 * Created by ober on 18-8-13.
 */
public class FillColorEvents {

    public static final int CB_THREAD_START = 0x01;
    public static final int CB_THREAD_END = 0x02;

    public static final int CB_FILL_IS_RIGHT = 0x03;

    public static final int CB_REVOKE_CACHE_STATE_CHANGE = 0x04;//arg1=empty  (0,1)
    public static final int CB_FILL_REVOKE_SUC = 0x05;
    public static final int CB_FILL_REVOKE_FAIL = 0x06;

    public static final int CB_OVERWRITE_COLOR = 0x07;

    public static final int CB_OVERWRITE_REVOKE_SUC = 0x08;

    public static final int CB_RESET_SUC = 0x09;
    public static final int CB_REFILL_SUC = 0x0a;

    public static final int CB_REVOKE_BACK_CACHE_STATE_CHANGE = 0x0b;
    public static final int CB_FILL_REVOKE_BACK_SUC = 0x0c;
    public static final int CB_FILL_REVOKE_BACK_FAIL = 0x0d;
    public static final int CB_OVERWRITE_REVOKE_BACK_SUC = 0x0e;
}
