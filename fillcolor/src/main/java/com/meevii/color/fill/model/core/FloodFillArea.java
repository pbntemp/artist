package com.meevii.color.fill.model.core;

import com.google.gson.annotations.SerializedName;

public final class FloodFillArea { //一个矩形
    @SerializedName("a")
    public int bottom;
    @SerializedName("b")
    public int left;
    @SerializedName("c")
    public int right;
    @SerializedName("d")
    public int top;
    @SerializedName("e")
    public Integer color;

    public void record(int x, int y) { //扩大区域，使他包含(x,y)
        if (x < this.left) {
            this.left = x;
        }
        if (x > this.right) {
            this.right = x;
        }
        if (y < this.top) {
            this.top = y;
        }
        if (y > this.bottom) {
            this.bottom = y;
        }
    }

    public void init(int x, int y) {
        this.left = x;
        this.right = x;
        this.top = y;
        this.bottom = y;
    }

    public int getHeight() {
        return this.bottom - this.top + 1;
    }

    public int getWidth() {
        return this.right - this.left + 1;
    }
}
