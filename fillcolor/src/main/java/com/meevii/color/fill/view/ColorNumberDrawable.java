package com.meevii.color.fill.view;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.graphics.PixelFormat;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * Created by ober on 18-8-2.
 */
public class ColorNumberDrawable extends Drawable {

    public static class Param {
        public int textSize;
        public int radius;
        public int progressColor;
        public int progressBgColor;
        public int progressWidth;
        public Path path;
        public int pathStrokeSize;
    }


    private final Paint mPaint;
    private final int radius;
    private final int progressColor;
    private final int progressBgColor;
    private final int progressWidth;
    private final int textSize;
    private final Path path;
    private final int pathStrokeSize;
    private final int progressRadius;

    private int color;
    private String number;

    private int textColor;
    private float mTextWidth;
    private float mTextHeightOffset;

    private float circleProgress;
    private boolean showCircleProgress;

    private float pathProgress;
    private boolean showPath;

    private boolean showText;

    private PathMeasure pathMeasure;

    private Path tempPath = new Path();
    private RectF tempRectF = new RectF();

    private boolean shouldDrawBorder;
    private static final int BORDER_COLOR = Color.parseColor("#e1e1e1");
    private static final int BORDER_WIDTH = 2;

    public ColorNumberDrawable(Param param) {
        mPaint = new Paint();

        textSize = param.textSize;
        radius = param.radius;
        progressColor = param.progressColor;
        progressBgColor = param.progressBgColor;
        progressWidth = param.progressWidth;
        path = param.path;
        pathStrokeSize = param.pathStrokeSize;

        progressRadius = radius + progressWidth / 2;


        pathMeasure = new PathMeasure(path, false);
    }

    public void setData(int number, int color) {
        this.number = String.valueOf(number);
        this.color = color;
        resetPaint();
        mPaint.setTextSize(textSize);
        mTextWidth = mPaint.measureText(this.number);
        mTextHeightOffset = mPaint.descent() + mPaint.ascent();

        textColor = getTextColor(color);
        shouldDrawBorder = calcShouldShowBorder(color);
    }

    public void setShowCircleProgress(boolean showCircleProgress) {
        this.showCircleProgress = showCircleProgress;
    }

    public void setCircleProgress(float circleProgress) {
        this.circleProgress = circleProgress;
    }

    public void setShowText(boolean showText) {
        this.showText = showText;
    }

    public void setShowPath(boolean showPath) {
        this.showPath = showPath;
    }

    public void setPathProgress(float pathProgress) {
        this.pathProgress = pathProgress;
    }

    private void resetPaint() {
        mPaint.reset();
        mPaint.setAntiAlias(true);
        mPaint.setDither(true);

        mPaint.setAlpha(alpha);
        mPaint.setColorFilter(colorFilter);
    }

    @Override
    public void draw(@NonNull Canvas canvas) {

        if (number == null) {
            return;
        }

        int w = canvas.getWidth();
        int h = canvas.getHeight();
        final int centerX = w / 2;
        final int centerY = h / 2;
        resetPaint();
        mPaint.setColor(color);
        canvas.drawCircle(centerX, centerY, radius, mPaint);

        if (shouldDrawBorder) {
            mPaint.setColor(BORDER_COLOR);
            mPaint.setStyle(Paint.Style.STROKE);
            mPaint.setStrokeWidth(BORDER_WIDTH);
            canvas.drawCircle(centerX, centerY, radius - BORDER_WIDTH / 2, mPaint);
        }

        if (showText) {
            float startX = centerX - mTextWidth / 2 - 1;
            float startY = centerY - mTextHeightOffset / 2;

            resetPaint();
            mPaint.setTextSize(textSize);
            mPaint.setColor(textColor);
            canvas.drawText(number, startX, startY, mPaint);
        }

        if (showCircleProgress) {
            resetPaint();
            mPaint.setStrokeWidth(progressWidth);
            mPaint.setStyle(Paint.Style.STROKE);
            mPaint.setColor(progressBgColor);
            canvas.drawCircle(centerX, centerY, progressRadius, mPaint);

            if (circleProgress > 0) {
                tempRectF.set(centerX - progressRadius,
                        centerY - progressRadius,
                        centerX + progressRadius,
                        centerY + progressRadius);
                mPaint.setColor(progressColor);
                mPaint.setStrokeCap(Paint.Cap.BUTT);
                canvas.drawArc(tempRectF, -91,
                        360 * circleProgress,
                        false, mPaint);

                mPaint.setColor(Color.RED);
                mPaint.setStrokeWidth(2);
            }
        }

        if (showPath) {
            tempPath.reset();
            pathMeasure.getSegment(0,
                    pathMeasure.getLength() * pathProgress,
                    tempPath, true);
            resetPaint();
            mPaint.setColor(textColor);
            mPaint.setStrokeWidth(pathStrokeSize);
            mPaint.setStyle(Paint.Style.STROKE);

            canvas.drawPath(tempPath, mPaint);
        }

    }

    private int alpha = 255;
    private ColorFilter colorFilter;

    @Override
    public void setAlpha(int i) {
        this.alpha = i;
    }

    @Override
    public void setColorFilter(@Nullable ColorFilter colorFilter) {
        this.colorFilter = colorFilter;
    }

    @Override
    public int getOpacity() {
        return PixelFormat.UNKNOWN;
    }

    private static int getTextColor(int color) {
        int r = Color.red(color);
        int g = Color.green(color);
        int b = Color.blue(color);

        int bright = (int) (0.3 * r + 0.6 * g + 0.1 * b);
        if (bright > 160) {
            return Color.parseColor("#232323");
        } else {
            return Color.WHITE;
        }
    }

    private static boolean calcShouldShowBorder(int color) {
        int r = Color.red(color);
        int g = Color.green(color);
        int b = Color.blue(color);
        int bright = (int) (0.3 * r + 0.6 * g + 0.1 * b);
        return bright > 210;
    }
}
