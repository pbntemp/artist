package com.meevii.color.fill.model.core.task;

/**
 * Created by ober on 2018/12/5.
 */
public class FillByClickColorTask extends AbsTask {
    public int x;
    public int y;
    public Integer color;
}
