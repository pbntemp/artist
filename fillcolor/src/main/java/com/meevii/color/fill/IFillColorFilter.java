package com.meevii.color.fill;

import android.graphics.Bitmap;

import com.meevii.color.fill.model.core.FloodFillArea;
import com.meevii.color.fill.model.core.action.FillColorAction;
import com.meevii.color.fill.model.core.action.OverwriteColorAction;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ober on 2018/11/7.
 */
public interface IFillColorFilter {

    // 图片被重置最初状态
    int STATE_EMPTY = 1;
    // 本次用户编辑了图片
    int STATE_CHANGED = 2;
    // 本次用户没有编辑图片
    int STATE_NO_CHANGED = 3;

    void initRegionAreas();

    void preFillAreas(HashMap<Integer, FloodFillArea> areaMap);

    void destroy();

    int initRegion(File regionFile);

    void initEditable(Bitmap editableBitmap);

    int getEditState();

    void resetToCalculateBitmap();

    void resetToCalculateBitmapAndInit(List<Integer> areas, int color);

    void resetToCalculateBitmapAndInitMap(Map<Integer, Integer> areaColorMap);

    Bitmap getEditedBitmap();

    HashMap<Integer, FloodFillArea> getAreaMap();

    int[] overwriteColor(OverwriteColorAction action);

    boolean floodFill(FillColorAction task);

    void onActivityDestroy();
}
