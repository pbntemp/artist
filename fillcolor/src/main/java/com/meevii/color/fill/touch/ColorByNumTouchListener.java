package com.meevii.color.fill.touch;

import android.view.MotionEvent;
import android.view.View;

import com.meevii.color.fill.FillColorImageView;

public class ColorByNumTouchListener implements View.OnTouchListener {

    private static final String TAG = "PBNTouch";

    private FillColorImageView mFillColorImageView;

    private Integer mColor;

    // 标识当前一轮Touch事件是否是多指操作，如果是则当手指抬起时，不做绘图操作
    private boolean isMultiDown;
    // 记录手指按下时的位置
    private float mDownX;
    private float mDownY;

    private float mLastX;
    private float mLastY;

    public ColorByNumTouchListener(FillColorImageView fillColorImageView) {
        this.mFillColorImageView = fillColorImageView;
    }

    private float[] tempXY = new float[2];

    private boolean enableLongClick = true;

    private boolean isPressed;

    public void setColor(Integer mColor) {
        this.mColor = mColor;
    }

    public Integer getColor() {
        return mColor;
    }

    @Override
    public boolean onTouch(View view, MotionEvent event) {

        switch (event.getAction()) {
            case MotionEvent.ACTION_POINTER_2_DOWN:
                isMultiDown = true;
                break;
            case MotionEvent.ACTION_DOWN:
                mDownX = event.getX();
                mDownY = event.getY();
                isPressed = true;
                mLastX = mDownX;
                mLastY = mDownY;
                if(enableLongClick) {
                    postCheckForLongClick();
                }
                break;

            case MotionEvent.ACTION_MOVE:

                if(mHasPerformedLongPress) {
                    return true;
                }
                mLastX = event.getX();
                mLastY = event.getY();
                break;
            case MotionEvent.ACTION_UP:
                isPressed = false;
                final boolean isMultiDown = this.isMultiDown;
                this.isMultiDown = false;

                if(!mHasPerformedLongPress) {
                    if (!isMultiDown) {
                        float upX = event.getX();
                        float upY = event.getY();
                        if (Math.abs(mDownX - upX) <= 30 && Math.abs(mDownY - upY) <= 30) { //在这个范围内算是点击
                            //pointF = mFillColorImageView.convertToSimple(mFillColorImageView.viewToSourceCoord(upX, upY));
                            tempXY[0] = upX;
                            tempXY[1] = upY;
                            mFillColorImageView.mapViewPoint(tempXY);
                            mFillColorImageView.scheduleClickFillColor((int) tempXY[0], (int) tempXY[1], mColor);
                        }
                    }
                } else {
                    return true;
                }

                break;
        }
        return false;
    }

    private boolean mHasPerformedLongPress;
    private Runnable mPendingCheckForLongPress;

    private void postCheckForLongClick() {
        mHasPerformedLongPress = false;

        if(mPendingCheckForLongPress == null) {
            mPendingCheckForLongPress = new CheckForLongPress();
        } else {
            mFillColorImageView.removeCallbacks(mPendingCheckForLongPress);
        }

        mFillColorImageView.postDelayed(mPendingCheckForLongPress, 900);
    }

    private boolean performLongClick(int x, int y) {
        return mFillColorImageView.onLongClicked(x, y, mColor);
    }

    private class CheckForLongPress implements Runnable {

        @Override
        public void run() {
            if(isPressed && mFillColorImageView != null && mFillColorImageView.getParent() != null) {
                if(!isMultiDown) {
                    if (Math.abs(mDownX - mLastX) <= 30 && Math.abs(mDownY - mLastY) <= 30) {
                        tempXY[0] = mDownX;
                        tempXY[1] = mDownY;
                        mFillColorImageView.mapViewPoint(tempXY);
                        if (performLongClick((int)tempXY[0], (int)tempXY[1])) {
                            mHasPerformedLongPress = true;
                        }
                    }
                }
            }
        }
    }
}
