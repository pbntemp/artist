package com.meevii.color.fill.util;


import android.graphics.Color;

public class ColorUtil {

    public static String color2String(int color, boolean hasAlpha) {
        String r, g, b;

        StringBuilder su = new StringBuilder();
        r = Integer.toHexString(Color.red(color));
        g = Integer.toHexString(Color.green(color));
        b = Integer.toHexString(Color.blue(color));
        r = r.length() == 1 ? "0" + r : r;
        g = g.length() == 1 ? "0" + g : g;
        b = b.length() == 1 ? "0" + b : b;
        r = r.toUpperCase();
        g = g.toUpperCase();
        b = b.toUpperCase();
        su.append("#");
        if (hasAlpha) {
            String a = Integer.toHexString(Color.alpha(color));
            a = a.length() == 1 ? "0" + a : a;
            su.append(a);
        } else {
            su.append("FF");
        }
        su.append(r);
        su.append(g);
        su.append(b);
        //0xFF0000FF
        return su.toString();

    }
}
