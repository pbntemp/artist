package com.meevii.color.fill;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Message;
import androidx.annotation.AnyThread;
import androidx.annotation.NonNull;
import androidx.annotation.UiThread;
import androidx.annotation.WorkerThread;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.Toast;

import com.airbnb.lottie.LottieDrawable;
import com.meevii.color.fill.draw.FillColorEvents;
import com.meevii.color.fill.draw.FillColorMachine;
import com.meevii.color.fill.draw.codec.PDFDecoder;
import com.meevii.color.fill.draw.codec.PDFRegionDecoder;
import com.meevii.color.fill.model.core.FloodFillArea;
import com.meevii.color.fill.model.core.origin.ColorOriginImage;
import com.meevii.color.fill.model.core.origin.PdfOriginImage;
import com.meevii.color.fill.model.core.resp.FillColorResp;
import com.meevii.color.fill.model.core.resp.OverwriteColorResp;
import com.meevii.color.fill.model.core.resp.RevokeOverwriteResp;
import com.meevii.color.fill.model.core.resp.RevokeSingleColorResp;
import com.meevii.color.fill.model.core.task.FillByClickColorTask;
import com.meevii.color.fill.model.core.task.OverWriteColorTask;
import com.meevii.color.fill.model.core.task.RefillTask;
import com.meevii.color.fill.model.core.task.ResetColorsTask;
import com.meevii.color.fill.model.core.task.ResetTask;
import com.meevii.color.fill.model.core.task.RevokeBackTask;
import com.meevii.color.fill.model.core.task.RevokeTask;
import com.meevii.color.fill.model.core.task.RunnableTask;
import com.meevii.color.fill.touch.ColorByNumTouchListener;
import com.meevii.color.fill.util.ColorUtil;
import com.meevii.color.fill.view.gestures.ImageSource;
import com.meevii.color.fill.view.gestures.SubsamplingScaleImageView;
import com.socks.library.KLog;

import java.io.File;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 填色ImageView，本类只负责手势、touch相关的操作，具体填色任务由FillColorMachine负责。
 * https://github.com/davemorrissey/subsampling-scale-image-view
 *
 * @author cutler
 */
public class FillColorImageView extends SubsamplingScaleImageView {
    // 填色器对象
    private FillColorMachine mMachine;
    private final Object machineLock = new Object();

    // FillColorImageView会绘制两张Bitmap，上面的用来显示，下面的用来编辑填色（就是mEditableBitmap）。
    private Bitmap mEditableBitmap;
    // 标识是否接收Touch事件。
    private boolean enableTouch = true;



    // 处理Touch事件的监听器
    private ColorByNumTouchListener mNormalTouchListener;

    private CallbackHandler mHandler;

    private LottieDrawable lottieDrawable;

    private static final String TAG = "FillColorImageView";

    public FillColorImageView(Context context) {
        super(context);
        init();
    }

    public FillColorImageView(Context context, AttributeSet attr) {
        super(context, attr);
        init();
    }

    private void init() {
        mNormalTouchListener = new ColorByNumTouchListener(this);
        setDoubleTapEnabled(false);
        mHandler = new CallbackHandler(this);
    }

    @UiThread
    public void setLottieDrawable(LottieDrawable lottieDrawable) {

        if(this.lottieDrawable != null) {
            this.lottieDrawable.setCallback(null);
            this.lottieDrawable.stop();
            unscheduleDrawable(this.lottieDrawable);
        }

        if(lottieDrawable == null) {
            if(this.lottieDrawable != null) {
                this.lottieDrawable = null;
                postInvalidate();
            }
            return;
        }

        this.lottieDrawable = lottieDrawable;
        this.lottieDrawable.setScale((float) mEditableBitmap.getWidth()/((float)lottieDrawable
                .getComposition().getBounds().width()));

        lottieDrawable.setCallback(this);
        lottieDrawable.start();

        this.postInvalidate();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (!enableTouch) {
            return false;
        }
        mNormalTouchListener.onTouch(this, event);

        return super.dispatchTouchEvent(event);
    }

    public void setColor(Integer color) {
        mNormalTouchListener.setColor(color);
    }



    public Integer getColor() {
        return mNormalTouchListener.getColor();
    }

    /**
     * 返回当前正在编辑的Bitmap
     *
     * @return
     */
    public Bitmap getEditedBitmap() {
        return mMachine.getEditedBitmap();
    }

    /**
     * 返回当前正在编辑的Bitmap的填色区域
     */
    @AnyThread
    public HashMap<Integer, FloodFillArea> getAreaMap() {
        if (mMachine == null) {
            return null;
        }
        return mMachine.getAreaMap();
    }

    /**
     * 将图片重置为未编辑状态，并保留task历史
     */
    public void prepareReplay() {
        if (mMachine != null) {
            mMachine.resetToCalculateBitmap();
        }
    }

    /**
     * 返回图片的编辑状态
     */
    public int getEditState() {
        return isInitialized() ? mMachine.getEditState() : -1;
    }

    /**
     * 查看当前对象是否初始化完毕
     */
    public boolean isInitialized() {
        return mMachine != null;
    }

    public void initColorOriginImage(ColorOriginImage image) {
        if (image == null) {
            setImage(null);
            return;
        }
        if (image instanceof PdfOriginImage) {
            PdfOriginImage pdf = (PdfOriginImage) image;
            final File file = pdf.getFile();
            float scale = pdf.getPdfScale();
            setBitmapDecoderFactory(() -> new PDFDecoder(0, file, scale));
            setRegionDecoderFactory(() -> new PDFRegionDecoder(0, file, scale));
            setImage(ImageSource.uri(file.getAbsolutePath()));
        } else {
            throw new RuntimeException("not support type");
        }

        setupMaxScale();
    }

    private void setupMaxScale() {
        setMaxScale(8);
    }


    @WorkerThread
    private void setupBaseParamBySize(int editWidth, int editHeight, int foreWidth, int foreHeight) {

        float[] ground2regionFactor = new float[2];
        float[] source2regionFactor = new float[2];
        int[] groudSize = new int[2];

        if (FillColorConfig.usePdf()) {

            float factor = 1.0f / FillColorConfig.getScale();

            source2regionFactor[0] = factor * (float) editWidth / (float) foreWidth;
            source2regionFactor[1] = factor * (float) editHeight / (float) foreHeight;

            ground2regionFactor[0] = (float) editWidth / (float) foreWidth;
            ground2regionFactor[1] = (float) editHeight / (float) foreHeight;

            groudSize[0] = foreWidth;
            groudSize[1] = foreHeight;


            setupBaseParam(source2regionFactor, ground2regionFactor, groudSize);
        } else {
            source2regionFactor[0] = (float) editWidth / (float) foreWidth;
            source2regionFactor[1] = (float) editHeight / (float) foreHeight;
            groudSize[0] = foreWidth;
            groudSize[1] = foreHeight;

            ground2regionFactor[0] = (float) editWidth / (float) foreWidth;
            ground2regionFactor[1] = (float) editHeight / (float) foreHeight;

            setupBaseParam(source2regionFactor, ground2regionFactor, groudSize);
        }
    }

    /**
     * 执行初始化和预填色
     * 此方法在子线程中调用
     */
    @WorkerThread
    public void initAndPreFill(Bitmap editableBitmap, File calculateBitmapFile,
                               HashMap<Integer, FloodFillArea> areaMap,
                               boolean hasRipple,
                               int[] pdfSize) {

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;

        BitmapFactory.decodeFile(calculateBitmapFile.getAbsolutePath(), options);

        if (editableBitmap.getWidth() != options.outWidth) {
            throw new RuntimeException("region--edit bmp not match");
        }

        setupBaseParamBySize(editableBitmap.getWidth(), editableBitmap.getHeight(), pdfSize[0], pdfSize[1]);

        synchronized (machineLock) {
            if (mMachine == null) {
                mEditableBitmap = editableBitmap;
                mMachine = new FillColorMachine(FillColorImageView.this, true);
                mMachine.startIfNeed();

                mMachine.initAndPreFill(editableBitmap, calculateBitmapFile, areaMap);

                mMachine.setCallback(mHandler);
            }
        }
    }

    public void animateToArea(int areaNum) {

        Map<Integer, FloodFillArea> areaMap = getAreaMap();
        if (areaMap == null) {
            return;
        }

        setEnableTouch(false);

        FloodFillArea area = areaMap.get(areaNum);
        int x = area.left + area.getWidth() / 2;
        int y = area.top + area.getHeight() / 2;

        x /= getSource2regionFactor()[0];
        y /= getSource2regionFactor()[1];

        AnimationBuilder ab = animateScaleAndCenter(Math.min(4, getMaxScale()), new PointF(x, y));
        if (ab == null) {
            KLog.e(TAG, "view not ready");
            return;
        }

        ab.withInterruptible(false)
                .withDuration(1000)
                .withOnAnimationEventListener(new OnAnimationEventListener() {
                    @Override
                    public void onComplete() {
                        setEnableTouch(true);
                    }

                    @Override
                    public void onInterruptedByUser() {
                        setEnableTouch(true);
                    }

                    @Override
                    public void onInterruptedByNewAnim() {
                        setEnableTouch(true);
                    }
                }).start();
    }

    public int getCurrentQueenSize() {
        if (mMachine == null) {
            return 0;
        }

        return mMachine.getTaskQueueSize();
    }


    @Override
    protected void onPreDraw(Canvas canvas, Matrix matrix, Paint bitmapPaint, boolean isTile) {

        if (mEditableBitmap == null || mEditableBitmap.isRecycled()) {
            return;
        }

        canvas.drawBitmap(mEditableBitmap, matrix, bitmapPaint);
    }

    @Override
    protected void onPostDraw(Canvas canvas, Matrix matrix, Paint bitmapPaint, boolean isTile) {
        drawLottie(canvas, matrix, isTile);
    }

    @Override
    protected boolean verifyDrawable(@NonNull Drawable who) {
        if(who instanceof LottieDrawable) {
            return true;
        }
        return super.verifyDrawable(who);
    }

    private void drawLottie(Canvas canvas, Matrix matrix, boolean isTile) {

        if(!isBaseParamSet()) {
            return;
        }

        if(lottieDrawable == null) {
            return;
        }

        final int i = canvas.save();

        canvas.setMatrix(matrix);

        lottieDrawable.draw(canvas);

        canvas.restoreToCount(i);
    }

    //释放填色线程，不可再填色
    public void release() {
        synchronized (machineLock) {
            if (mMachine != null) {
                mMachine.release();
                mMachine = null;
            }
        }
    }

    //销毁，不再可用
    public void destroy() {
        super.destroy();
    }

    /*
     * 转换坐标点
     */
    public PointF convertToSimple(PointF pointF) {
        if (pointF != null) {
            pointF.x /= FillColorConfig.getScale();
            pointF.y /= FillColorConfig.getScale();
        }
        return pointF;
    }

    public boolean isEnableTouch() {
        return enableTouch;
    }

    public void setEnableTouch(boolean enableTouch) {
        this.enableTouch = enableTouch;
    }

    public FillColorMachine getMachine() {
        return mMachine;
    }

    public boolean scheduleOverWriteColor(int srcColor, Integer dstColor) {
        if (mMachine == null) {
            return false;
        }
        OverWriteColorTask task = new OverWriteColorTask();
        task.srcColor = srcColor;
        task.dstColor = dstColor;
        task.cache = true;
        mMachine.schedule(task);
        return true;
    }

    public boolean scheduleRefillAll(Map<Integer, Integer> refillMap) {
        if(mMachine == null) {
            return false;
        }
        RefillTask task = new RefillTask();
        task.refillMap = refillMap;
        mMachine.schedule(task);
        return true;
    }

    public interface ClickFilter {
        boolean handleClick(int x, int y, Integer color);
    }

    public interface ImageOnLongClickListener {
        boolean onImageLongClicked(int positionColor);
    }

    private ClickFilter mClickFilter;
    private ImageOnLongClickListener mLongClickListener;

    public void setClickFilter(ClickFilter filter) {
        mClickFilter = filter;
    }

    public void setImageLongClickListener(ImageOnLongClickListener longClickListener) {
        this.mLongClickListener = longClickListener;
    }

    public boolean onLongClicked(int x, int y, Integer color) {
        if(mLongClickListener == null) {
            return false;
        }

        if(x < 0 || y < 0) {
            return false;
        }

        if(mEditableBitmap == null) {
            return false;
        }

        if(x >= mEditableBitmap.getWidth() || y >= mEditableBitmap.getHeight()) {
            return false;
        }

        int editColor = mEditableBitmap.getPixel(x, y);

        return mLongClickListener.onImageLongClicked(editColor);
    }

    public boolean scheduleClickFillColor(int x, int y, Integer color) {
        if (mMachine == null) {
            return false;
        }

        if(mClickFilter != null && mClickFilter.handleClick(x, y, color)) {
            return false;
        }

        FillByClickColorTask task = new FillByClickColorTask();
        task.color = color;
        task.x = x;
        task.y = y;
        mMachine.schedule(task);
        return true;
    }

    public boolean scheduleRevoke() {
        if (mMachine == null) {
            return false;
        }
        mMachine.schedule(new RevokeTask());
        return true;
    }

    public boolean scheduleRevokeBack() {
        if(mMachine == null) {
            return false;
        }

        mMachine.schedule(new RevokeBackTask());
        return true;
    }

    public boolean scheduleReset() {
        if(mMachine == null) {
            return false;
        }

        mMachine.schedule(new ResetTask());
        return true;
    }

    public boolean scheduleReset(List<Integer> areas, int color) {
        if(mMachine == null) {
            return false;
        }

        ResetColorsTask task = new ResetColorsTask();
        task.areas = areas;
        task.color = color;

        mMachine.schedule(task);
        return true;
    }

    private final RectF tempRectF = new RectF();

    public void postInvalidateFloodFillArea(int l, int t, int r, int b) {

        //tempRectF.set(l, t, r, b);

        //mapRegionRect(tempRectF);

        postInvalidate();

    }

    private void mapRegionRect(RectF rect) {

        float l = rect.left;
        float t = rect.top;
        float r = rect.right;
        float b = rect.bottom;
        if (FillColorConfig.usePdf()) {
            l *= 2; //pdf模式base1024 / pdf原图2048, bmp模式原图加载时已经1024
            t *= 2;
            r *= 2;
            b *= 2;
        }

        float scale = FillColorConfig.getScale();

        l *= scale;
        t *= scale;
        r *= scale;
        b *= scale;

        rect.left = sourceToViewX(l);
        rect.top = sourceToViewY(t);
        rect.right = sourceToViewX(r);
        rect.bottom = sourceToViewY(b);
    }

    public void scheduleRunnableTask(RunnableTask task) {
        if (mMachine != null) {
            mMachine.schedule(task);
        }
    }

    private FillColorListener fillColorListener;

    public void setFillColorListener(FillColorListener fillColorListener) {
        this.fillColorListener = fillColorListener;
    }

    private static class CallbackHandler extends Handler {

        Reference<FillColorImageView> ref;

        CallbackHandler(FillColorImageView self) {
            ref = new WeakReference<>(self);
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            FillColorImageView self = ref.get();
            if (self == null) {
                return;
            }
            switch (msg.what) {
                case FillColorEvents.CB_FILL_IS_RIGHT: {

                    FillColorResp resp = (FillColorResp) msg.obj;
                    int number = msg.arg1;
                    self.postInvalidate();
                    if (self.fillColorListener != null) {
                        self.fillColorListener.onAreaFilled(number, resp);
                    }
                }
                break;
                case FillColorEvents.CB_REVOKE_CACHE_STATE_CHANGE: {
                    int empty = msg.arg1;
                    if (self.fillColorListener != null) {
                        self.fillColorListener.onRevokeCacheStateChanged(empty == 1);
                    }
                }
                break;
                case FillColorEvents.CB_REVOKE_BACK_CACHE_STATE_CHANGE: {
                    int empty = msg.arg1;
                    if(self.fillColorListener != null) {
                        self.fillColorListener.onRevokeBackCacheStateChanged(empty == 1);
                    }
                }
                break;
                case FillColorEvents.CB_FILL_REVOKE_SUC:
                case FillColorEvents.CB_FILL_REVOKE_BACK_SUC: {
                    if (self.fillColorListener != null) {
                        RevokeSingleColorResp revokeColorResp = (RevokeSingleColorResp) msg.obj;
                        self.fillColorListener.onRecalledFillColor(true, revokeColorResp);
                    }
                }
                break;
                case FillColorEvents.CB_FILL_REVOKE_FAIL:
                case FillColorEvents.CB_FILL_REVOKE_BACK_FAIL: {
                    if (self.fillColorListener != null) {
                        self.fillColorListener.onRecalledFillColor(false, null);
                    }
                }
                break;
                case FillColorEvents.CB_OVERWRITE_COLOR: {
                    OverwriteColorResp resp = (OverwriteColorResp) msg.obj;
                    if(self.fillColorListener != null) {
                        self.fillColorListener.onOverwriteColor(resp);
                    }
                }
                break;
                case FillColorEvents.CB_OVERWRITE_REVOKE_SUC:
                case FillColorEvents.CB_OVERWRITE_REVOKE_BACK_SUC: {
                    RevokeOverwriteResp resp = (RevokeOverwriteResp) msg.obj;
                    if(self.fillColorListener != null) {
                        self.fillColorListener.onRecalledOverwrite(true, resp);
                    }
                }
                break;
                case FillColorEvents.CB_RESET_SUC:
                    if(self.fillColorListener != null) {
                        self.fillColorListener.onReset();
                    }
                break;
                case FillColorEvents.CB_REFILL_SUC:
                    if(self.fillColorListener != null) {
                        self.fillColorListener.onRefilled();
                    }
                break;
            }
        }
    }

    public interface FillColorListener {
        void onAreaFilled(int num, FillColorResp color);

        void onRevokeCacheStateChanged(boolean empty);

        void onRevokeBackCacheStateChanged(boolean empty);

        void onRecalledFillColor(boolean success, RevokeSingleColorResp resp);

        void onRecalledOverwrite(boolean success, RevokeOverwriteResp resp);

        void onOverwriteColor(OverwriteColorResp resp);

        void onReset();

        void onRefilled();
    }
}
