package com.meevii.color.fill.model.core.resp;

/**
 * Created by ober on 19-4-28.
 */
public class FillColorResp {

    //fillType
    //0  : same color
    //1  : fill color
    //2  : over write color
    //3  : clear color
    //4  : none color

    public static final int FILL_SAME_COLOR = 0;
    public static final int FILL_COLOR = 1;
    public static final int FILL_OVERWITE = 2;
    public static final int FILL_CLEAR = 3;
    public static final int FILL_NONE = 4;

    public Integer color;
    public Integer preColor;
    public int fillType;
}
