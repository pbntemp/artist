package com.meevii.color.fill.util;

import java.util.LinkedList;

/**
 * Created by ober on 19-2-26.
 */
public class SizedStack<T> {

    public interface StateChangeCallback {
        void onSizedStateChange(boolean empty);
    }

    private final LinkedList<T> data;
    private final int max;
    private StateChangeCallback mCallback;

    public SizedStack(int max) {
        data = new LinkedList<>();
        this.max = max;
    }

    public void setCallback(StateChangeCallback mCallback) {
        this.mCallback = mCallback;
    }

    public void push(T t) {
        int size = data.size();
        if(size == max) {
            data.remove(0);
        }
        data.add(t);
        if(size == 0) {
            if(mCallback != null) {
                mCallback.onSizedStateChange(false);
            }
        }
    }

    public T pop() {
        int size = data.size();
        if(size == 0) {
            return null;
        }

        T t = data.get(size - 1);
        data.remove(size - 1);

        if(size == 1) {
            if(mCallback != null) {
                mCallback.onSizedStateChange(true);
            }
        }

        return t;
    }

    public void clear() {
        int size = data.size();
        if(size == 0) {
            return;
        }

        data.clear();
        if(mCallback != null) {
            mCallback.onSizedStateChange(true);
        }
    }

    public T peek() {
        int size = data.size();
        if(size == 0) {
            return null;
        }
        return data.get(size - 1);
    }
}
