package com.meevii.color.fill.model.core.action;

import androidx.annotation.Nullable;

/**
 * Created by ober on 18-8-13.
 */
public class FillColorAction {

    public int number;

    public Integer color;

    @Nullable
    public int[] xy;

    public FillColorAction(int number, Integer color) {
        this.number = number;
        this.color = color;
    }

}
