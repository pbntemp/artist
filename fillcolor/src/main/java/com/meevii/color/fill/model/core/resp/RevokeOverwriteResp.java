package com.meevii.color.fill.model.core.resp;

import androidx.annotation.Nullable;

/**
 * Created by ober on 19-5-6.
 */
public class RevokeOverwriteResp {

    public int preColor;
    @Nullable
    public Integer dstColor;
    public int[] areas;

    public boolean isBack;
}
