package com.meevii.color.fill.model.core.record;

import androidx.annotation.Nullable;

/**
 * Created by ober on 19-5-6.
 */
public class OverwriteColorExecuted extends Executed {

    public int preColor;

    @Nullable
    public Integer dstColor;

    public int[] areas;
}
