package com.meevii.color.fill.model.core.task;

import androidx.annotation.Nullable;

/**
 * Created by ober on 2019/5/6.
 */
public class OverWriteColorTask extends AbsTask {
    public int srcColor;

    @Nullable
    public Integer dstColor;

    public boolean cache;

}
