package com.meevii.color.fill;

/**
 * Created by ober on 18-8-16.
 */
public class FillColorConfig {

    private static final int ORIGIN_PNG_SIZE = 2048; //png原图尺寸

    private static final int ORIGIN_PDF_SIZE = 2048;

    //基础尺寸，　region图的尺寸
    public static final int BASE_SIZE = 1024;

    //画面上的png缩放, 实际尺寸＝DISPLAY_PNG_SCALE * BASE_SIZE
    private static float DISPLAY_PNG_SCALE = 1;

    //pdf的scale
    private static int PDF_SCALE = 8;

    //初始化pdf　bitmap的尺寸
    private static int PDF_TILE_BMP_SIZE = 2048;

    //是否使用PDF
    private static boolean USE_PDF = false;

    public static void initPDFStyle(int scale, int bmpSize) {
        USE_PDF = true;
        PDF_SCALE = scale;
        PDF_TILE_BMP_SIZE = bmpSize;
    }

    public static void initPNGStyle(int scale) {
        USE_PDF = false;
        DISPLAY_PNG_SCALE = scale;
    }

    public static boolean usePdf() {
        return USE_PDF;
    }

    public static float getScale() {
        if(USE_PDF) {
            return PDF_SCALE;
        }
        return DISPLAY_PNG_SCALE;
    }

    public static int getPdfTileBmpSize() {
        return PDF_TILE_BMP_SIZE;
    }

    public static int getOriginSize() {
        return 2048;
    }
}
