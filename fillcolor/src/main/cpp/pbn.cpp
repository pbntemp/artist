
//
// Created by ober on 2018/11/7.
//

#include "pbn.h"

using namespace std;

#define VERSION_FLOOD_FILLER "v1.0.2"

#define castOPainter ((OPainter*)program)
#define checkProgram if(!program){return -99;}
#define checkProgram2 if(!program){return NULL;}

_jstring *Java_com_meevii_color_fill_filler_FillColorFillerN_nVersion(JNIEnv *env, jclass type) {
    return env -> NewStringUTF(VERSION_FLOOD_FILLER);
}

jlong Java_com_meevii_color_fill_filler_FillColorFillerN_nNewProgram(JNIEnv *env, jclass type) {
    LOGI("Java_com_meevii_color_fill_filler_FillColorFillerN_nNewProgram");
    OPainter* program = new OPainter();
    return (int64_t)program;
}

jint Java_com_meevii_color_fill_filler_FillColorFillerN_nInitRegion(JNIEnv *env, jclass type,
                                                                    jlong program,
                                                                    jstring regionPng) {
    checkProgram;

    return castOPainter -> initRegion(env, regionPng);
}

jint Java_com_meevii_color_fill_filler_FillColorFillerN_nInitEditable(JNIEnv *env, jclass type,
                                                                      jlong program,
                                                                      jobject editableBitmap) {
    checkProgram;

    return castOPainter -> initEditable(env, editableBitmap);
}

jint Java_com_meevii_color_fill_filler_FillColorFillerN_nInitRegionAreas(JNIEnv *env, jclass type,
                                                                    jlong program) {

    checkProgram;

    return castOPainter -> initRegionAreas(env);
}

jint Java_com_meevii_color_fill_filler_FillColorFillerN_nPreFillAreas(JNIEnv *env, jclass type,
                                                                      jlong program,
                                                                      jintArray data_) {
    checkProgram;

    return castOPainter -> preFillAreas(env, data_);
}

jint Java_com_meevii_color_fill_filler_FillColorFillerN_nPreFillBadAreas(JNIEnv *env, jclass type,
                                                                    jlong program) {

    checkProgram;

    return castOPainter -> preFillBadAreas(env);
}

//jint Java_com_meevii_color_fill_filler_FillColorFillerN_nInitCenterMap(JNIEnv *env, jclass type,
//                                                                  jlong program, jintArray nums_,
//                                                                  jintArray centers_) {
//
//    checkProgram;
//
//    return castOPainter -> initCenterMap(env, nums_, centers_);
//}
//
//jint Java_com_meevii_color_fill_filler_FillColorFillerN_nInitBlockAreaMap(JNIEnv *env, jclass type,
//                                                                     jlong program, jintArray keys_,
//                                                                     jintArray values_,
//                                                                     jint keyCount) {
//    checkProgram;
//
//    return castOPainter -> initBlockAreaMap(env, keys_, values_, keyCount);
//}

jint Java_com_meevii_color_fill_filler_FillColorFillerN_nFillForSingleColor(JNIEnv *env, jclass type,
                                                                            jlong program,
                                                                            jboolean hasColor,
                                                                            jint color,
                                                                            jintArray areaNum_) {

    checkProgram;

    return castOPainter -> fillForSingleColor(env, hasColor, color, areaNum_);
}

jintArray Java_com_meevii_color_fill_filler_FillColorFillerN_nOverWriteColor(JNIEnv *env, jclass type,
                                                                   jlong program, jint srcColor,
                                                                   jboolean dstHasColor,
                                                                   jint dstColor) {

    checkProgram2;

    return castOPainter -> overwriteColor(env, srcColor, dstHasColor, dstColor);
}


jint Java_com_meevii_color_fill_filler_FillColorFillerN_nFillForSingleColorStep(JNIEnv *env, jclass type,
                                                                           jlong program, jint x, jint y,
                                                                           jboolean hasColor,
                                                                           jint color,
                                                                           jint areaNum, jobject jcallback) {

    checkProgram;

    return castOPainter -> fillForSingleColorStep(env, x, y, hasColor, color, areaNum, jcallback);

}

jint Java_com_meevii_color_fill_filler_FillColorFillerN_nGetAreaMapSize(JNIEnv *env, jclass type,
                                                                   jlong program) {

    checkProgram;

    return castOPainter -> getAreaMapSize(env);
}

jint Java_com_meevii_color_fill_filler_FillColorFillerN_nGetAreaMap(JNIEnv *env, jclass type,
                                                                    jlong program, jintArray keys_,
                                                                    jintArray floodFillAreas_) {

    checkProgram;

    return castOPainter -> getAreaMap(env, keys_, floodFillAreas_);
}

jint Java_com_meevii_color_fill_filler_FillColorFillerN_nResetEditBmp(JNIEnv *env, jclass type,
                                                                      jlong program) {

    checkProgram;

    return castOPainter -> resetEditBmp(env);
}

jint Java_com_meevii_color_fill_filler_FillColorFillerN_nResetEditBmpAndInit(JNIEnv *env, jclass type,
                                                                        jlong program,
                                                                        jintArray areas_,
                                                                        jint color) {
    checkProgram;

    OPainter* p = castOPainter;

    jint *areas = env->GetIntArrayElements(areas_, NULL);
    int32_t count = env->GetArrayLength(areas_);

    int32_t result = p -> resetEditBmpAndInit(env, areas, count, color);

    env->ReleaseIntArrayElements(areas_, areas, 0);

    return result;
}

jint Java_com_meevii_color_fill_filler_FillColorFillerN_nResetEditBmpAndInitMap(JNIEnv *env,
                                                                           jclass type,
                                                                           jlong program,
                                                                           jintArray mapArr) {
    checkProgram;

    OPainter* p = castOPainter;
    jint *data = env->GetIntArrayElements(mapArr, NULL);
    int32_t count = env->GetArrayLength(mapArr);

    int32_t result = p -> resetEditBmpAndInitMap(env, data, count);

    env->ReleaseIntArrayElements(mapArr, data, 0);

    return result;
}

jint Java_com_meevii_color_fill_filler_FillColorFillerN_nDestroy(JNIEnv *env, jclass type,
                                                                 jlong program) {

    checkProgram;

    LOGI("Java_com_meevii_color_fill_filler_FillColorFillerN_nDestroy");

    OPainter* painter = castOPainter;
    painter -> destroy(env);
    delete (painter);
    return 0;
}

jint Java_com_meevii_color_fill_filler_FillColorFillerN_nProcessFillByColor(JNIEnv *env, jclass type,
                                                                       jlong program,
                                                                       jint x, jint y,
                                                                       jboolean hasColor,
                                                                       jint targetColor,
                                                                       jintArray outResult_,
                                                                       jintArray outArea_,
                                                                       jintArray preColor_) {
    checkProgram;

    OPainter* painter = castOPainter;
    return painter -> processFillByColor(env, x, y, hasColor, targetColor, outResult_, outArea_, preColor_);
}

#define initArea(area, x, y) \
    area -> left = x;\
    area -> right = x;\
    area -> top = y;\
    area -> bottom = y;\
    area -> colored = 0;\
    area -> color = 0x00ffffff;

#define recordArea(area, x, y) \
    if(x < area->left) {\
        area->left = x;\
    }\
    if(x > area->right) {\
        area->right = x;\
    }\
    if(y < area->top) {\
        area->top = y;\
    }\
    if(y > area->bottom) {\
        area->bottom = y;\
    }


JNIEXPORT jstring JNICALL
Java_com_meevii_color_fill_filler_FillColorFillerN_nGenCenterStr(JNIEnv *env, jclass type,
                                                                 jobject regionBmp,
                                                                 jobject originBmp) {

    LOGD("[OPainter] gen center str");


    AndroidBitmapInfo regionInfo;
    AndroidBitmapInfo originInfo;
    AndroidBitmap_getInfo(env, regionBmp, &regionInfo);
    AndroidBitmap_getInfo(env, originBmp, &originInfo);

    if(regionInfo.width != originInfo.width) {
        __android_log_assert("not match", "GenCenter", "region origin not match");
    }

    void* regionData;
    AndroidBitmap_lockPixels(env, regionBmp, &regionData);
    void* originData;
    AndroidBitmap_lockPixels(env, originBmp, &originData);

    int32_t* regionPixels = (int32_t*)regionData;
    int32_t* originPixels = (int32_t*)originData;

    map<int32_t, FloodFillArea*> areaMapInstance;

    map<int32_t, FloodFillArea*>* areaMap = &areaMapInstance;

    const int32_t w = regionInfo.width;
    const int32_t h = regionInfo.height;

    int32_t pxIndex = 0;
    FloodFillArea* lastArea = NULL;
    int32_t lastAreaNumber = 0;
    for(int32_t y = 0; y < h; y++) {
        for(int32_t x = 0; x < w; x++) {
            int32_t number = *(regionPixels + pxIndex) & 0x00ffffff;
            pxIndex++;
            if(number <= 0) {
                continue;
            }

            if(lastArea && number == lastAreaNumber) {
                recordArea(lastArea, x, y);
            } else {
                lastAreaNumber = number;
                map<int32_t, FloodFillArea *>::iterator it = areaMap->find(number);
                if (it == areaMap->end()) {
                    FloodFillArea *area = (FloodFillArea *) malloc(sizeof(*area));
                    initArea(area, x, y);
                    areaMap->insert(map<int32_t, FloodFillArea *>::value_type(number, area));
                    lastArea = area;
                } else {
                    FloodFillArea *area = (FloodFillArea *) (it->second);
                    recordArea(area, x, y);
                    lastArea = area;
                }
            }
        }
    }

    areaMapType::iterator areaMapIt = areaMap -> begin();

    int32_t count = 0;

    while (areaMapIt != areaMap -> end()) {
        const int32_t num = areaMapIt -> first;

        const FloodFillArea* area = areaMapIt -> second;
        int32_t w = area -> right - area -> left + 1;
        int32_t h = area -> bottom - area -> top + 1;
        if(w * h <= 2) {
            count++;
            areaMap -> erase(areaMapIt);
        }
        areaMapIt++;
    }

    LOGD("[OPainter] remove %d small areas", count);

    char* centerStr = CenterGenerator::generateCentermapString(originPixels, regionPixels,
        regionInfo.width, regionInfo.height, areaMap, NULL);

    AndroidBitmap_unlockPixels(env, regionBmp);
    AndroidBitmap_unlockPixels(env, originBmp);

    if (centerStr == NULL) {
        return env -> NewStringUTF("");
    } else {
        jstring result = env -> NewStringUTF(centerStr);
        delete[](centerStr);
        return result;
    }

}
