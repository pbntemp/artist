//
// Created by dxy on 2019/1/29.
//

#include "CenterGenerator.h"
#include "logdef.h"


bool _isPointNearbyOutofBoundary(int * datas, int datasWidth, int datasHeight, int number, int x, int y, int nearbyLen)
{
    int r = nearbyLen;
    int left = max(0, x - r);
    int top = max(0, y - r);

    int right = min(x + r, datasWidth - 1);
    int bottom = min(y + r, datasHeight - 1);

    for (int i = top; i <= bottom; ++i)
    {
        for (int j = left; j <= right; ++j)
        {
            int index = i * datasWidth + j;
            if (datas[index] != number)
                return true;
        }
    }
    return false;
}

bool isPointOutofBoundary(int * datas, int datasWidth, int datasHeight, int number, int x, int y)
{
    return _isPointNearbyOutofBoundary(datas, datasWidth, datasHeight, number, x, y, 0);
}

bool isPointNearbyOutofBoundary(int * datas, int datasWidth, int datasHeight, int number, int x, int y)
{
    return _isPointNearbyOutofBoundary(datas, datasWidth, datasHeight, number, x, y, 1);
}


bool isSquareNearbyOutofBoundary(int * datas, int datasWidth, int datasHeight, int number, int rightBottomIndex, int sideLen, int nearbyLen)
{
    int posEndY = rightBottomIndex / datasWidth;
    int posEndX = rightBottomIndex - posEndY * datasWidth;

    int x = posEndX - sideLen + 1;
    int y = posEndY - sideLen + 1;

    if (_isPointNearbyOutofBoundary(datas, datasWidth, datasHeight, number, x, y, nearbyLen))
        return true;

    if (_isPointNearbyOutofBoundary(datas, datasWidth, datasHeight, number, x + sideLen - 1, y, nearbyLen))
        return true;

    if (_isPointNearbyOutofBoundary(datas, datasWidth, datasHeight, number, x, y + sideLen - 1, nearbyLen))
        return true;

    if (_isPointNearbyOutofBoundary(datas, datasWidth, datasHeight, number, x + sideLen - 1, y + sideLen - 1, nearbyLen))
        return true;

    return false;
}

int32_t getAdjustLen(int32_t candidacySideLen)
{
    int32_t adjustLen = 0;
    if (candidacySideLen >= 80) adjustLen = 16;
    else if (candidacySideLen >= 60) adjustLen = 12;
    else if (candidacySideLen >= 40) adjustLen = 10;
    else if (candidacySideLen >= 20) adjustLen = 8;
    else if (candidacySideLen >= 10) adjustLen = 4;
    else if (candidacySideLen >= 6) adjustLen = 3;
    else if (candidacySideLen >= 3) adjustLen = 1;

    return adjustLen;
}

inline int rgb2bgr(int32_t bgr) {
    return ((bgr & 0x00ff0000)>>16) | ((bgr & 0x0000ff00)) | ((bgr & 0x000000ff)<<16);
}

inline bool isBlackColor(int32_t * ori_datas, int dataIndex)
{
    if (ori_datas == NULL)
        return false;

    int alpha = (ori_datas[dataIndex] >> 24) & 0xFF;
    if (alpha == 0)
        return false;

    return (ori_datas[dataIndex] & 0x00FFFFFF) <= 0x007FFFFF;
}


bool testSquare(int * datas, int datasWidth, int datasHeight, int number, int & dataIndex, int &sideLen, int zoomLen, int nearbyLen)
{
    int old_dataIndex = dataIndex;
    int old_sideLen = sideLen;

    if (sideLen <= zoomLen)
        return false;

    sideLen -= zoomLen;

    //左
    dataIndex -= zoomLen;
    if (!isSquareNearbyOutofBoundary(datas, datasWidth, datasHeight, number, dataIndex, sideLen, nearbyLen))
        return true;

    if (zoomLen != 0)
    {
        //左上
        dataIndex -= datasWidth * zoomLen;
        if (!isSquareNearbyOutofBoundary(datas, datasWidth, datasHeight, number, dataIndex, sideLen, nearbyLen))
            return true;

        //上
        dataIndex += zoomLen;
        if (!isSquareNearbyOutofBoundary(datas, datasWidth, datasHeight, number, dataIndex, sideLen, nearbyLen))
            return true;
    }

    dataIndex = old_dataIndex;
    sideLen = old_sideLen;
    return false;
}

bool CenterGenerator::setSquare( int32_t * ori_datas, int32_t * datas,
                                 int32_t datasWidth, int32_t datasHeight,
                                 int32_t number,
                                 int32_t startX, int32_t startY,
                                 int32_t width, int32_t height,
                                 CenterEx *pCenter)
{

    //int realNum = rgb2bgr(number);

    //LOGD("--------setquare for num : %d(%d), startX = %d %d (%d X %d)" , number, realNum, startX, startY, width, height);

    int32_t maxPosition = 0;

    int32_t sideLen = 0; //边长

    int32_t *dp = new int32_t[width * height];
    memset(dp, 0, width * height * sizeof(int32_t));

    for (int32_t x = 0; x < width; x++)
    {
        int32_t dataIndex = startX + x + startY * datasWidth;
        if ((datas[dataIndex] & 0x00FFFFFF) == (number & 0x00FFFFFF))
        {
            if (isBlackColor(ori_datas, dataIndex)) //region图是填色区域, 但原图是黑点, 要避开
            {
                continue;
            }
            dp[x] = 1;
            sideLen = 1;
            maxPosition = dataIndex;
        }
    }

    for (int32_t y = 0; y < height; y++)
    {
        int32_t dataIndex = startX + (startY + y) * datasWidth;
        if ((datas[dataIndex] & 0x00FFFFFF) == (number & 0x00FFFFFF))
        {
            if (isBlackColor(ori_datas, dataIndex))
            {
                continue;
            }

            dp[y * width] = 1;
            sideLen = 1;
            maxPosition = dataIndex;
        }
    }


    int candidacy = maxPosition; //候选
    int candidacySideLen = sideLen; //候选

    FixQueue squares(6);
    FixQueue sideLens(6);

    for (int y = 1; y < height; y++)
    {
        for (int x = 1; x< width; x++)
        {
            int cur = y * width + x;
            int dataIndex = (startY + y) * datasWidth + (startX + x);

            if ((datas[dataIndex] & 0x00FFFFFF) == (number & 0x00FFFFFF))
            {
                if (isBlackColor(ori_datas, dataIndex))
                {
                    continue;
                }

                int left = cur - 1;
                int up = cur - width;
                int leftUp = up - 1;
                dp[cur] = min(dp[left], min(dp[up], dp[leftUp])) + 1;

                if (dp[cur] == 0)
                    continue;


                if (sideLen < dp[cur])
                {
                    maxPosition = dataIndex;
                    sideLen = dp[cur];

                    candidacy = dataIndex;
                    candidacySideLen = sideLen;

                    squares.push(dataIndex);
                    sideLens.push(dp[cur]);
                }
                else if (sideLen == dp[cur])
                {
                    squares.push(dataIndex);
                    sideLens.push(dp[cur]);
                }
            }
        }

    }

    delete[] dp;

    bool bIgnoreOrigin = false;
    if (candidacySideLen == 0)
    {
        bIgnoreOrigin = true;
    }
    else if (candidacySideLen == 1)
    {
        if ((width > 20 || height > 20) || (width * height > 180)) bIgnoreOrigin = true;
    }

    if (bIgnoreOrigin)
    {
        if (ori_datas == NULL)
            return false;

        //源图数据可能有误， 不参考源图了
        return setSquare( NULL, datas,
                          datasWidth, datasHeight,
                          number,
                          startX, startY,
                          width, height,
                          pCenter);
    }

    bool bFindBest = false;
    int iSize = squares.size();
    //从大到小
    for (int i = iSize - 1; i >= 0; --i)
    {
        int dataIndex = squares.at(i);
        int sideLen = sideLens.at(i);

        if (testSquare(datas, datasWidth, datasHeight, number, dataIndex, sideLen, 0, 1))
        {
            bFindBest = true;
        }
        else if (testSquare(datas, datasWidth, datasHeight, number, dataIndex, sideLen, 1, 1))
        {
            bFindBest = true;
        }
        else if (testSquare(datas, datasWidth, datasHeight, number, dataIndex, sideLen, 2, 1))
        {
            bFindBest = true;
        }

        if (bFindBest)
        {
            candidacy = dataIndex;
            candidacySideLen = sideLen;
            break;
        }
    }

    if (!bFindBest)
    {
        for (int i = 0; i < iSize; ++i)
        {
            int dataIndex = squares.at(i);
            int sideLen = sideLens.at(i);

            if (sideLen < 2)
                continue;

            if (testSquare(datas, datasWidth, datasHeight, number, dataIndex, sideLen, 0, 0))
            {
                bFindBest = true;
            }

            if (bFindBest)
            {
                candidacy = dataIndex;
                candidacySideLen = sideLen;
                break;
            }
        }
    }

    if (candidacySideLen == 0)
        return false;


    int posEndY = candidacy / datasWidth;
    int posEndX = candidacy - posEndY * datasWidth;

    int startPosX = posEndX - candidacySideLen + 1 ;
    int startPosY = posEndY - candidacySideLen + 1;

    if (!bFindBest)
    {
        //尽量不要在边界
        int adjustLen = 0;

        if (isPointNearbyOutofBoundary(datas, datasWidth, datasHeight, number, startPosX, startPosY))
        {
            adjustLen = getAdjustLen(candidacySideLen);
        }

        else if (isPointNearbyOutofBoundary(datas, datasWidth, datasHeight, number, startPosX + candidacySideLen, startPosY))
        {
            adjustLen = getAdjustLen(candidacySideLen);
        }
        else if (isPointNearbyOutofBoundary(datas, datasWidth, datasHeight, number, startPosX, startPosY + candidacySideLen))
        {
            adjustLen = getAdjustLen(candidacySideLen);
        }
        else if (isPointNearbyOutofBoundary(datas, datasWidth, datasHeight, number, startPosX + candidacySideLen, startPosY + candidacySideLen))
        {
            adjustLen = getAdjustLen(candidacySideLen);
        }

        if (adjustLen > 0)
        {
            int k = adjustLen / 2;
            if (k == 0) k = 1;
            startPosX += k;
            startPosY += k;
            candidacySideLen -= adjustLen;
        }

    }

    if (candidacySideLen <= 1)//边长为1
    {
        if (width < 2 || height < 2)
        {
            return false;
        }
    }

    int32_t r = candidacySideLen / 2;

    int32_t centerPosX = startPosX + r;
    int32_t centerPosY = startPosY + r;

    if (pCenter != NULL)
    {
        pCenter->x = centerPosX;
        pCenter->y = centerPosY;
        pCenter->r = r == 0 ? 1 : r;

        if (candidacySideLen & 1)
            pCenter->bNeedAdjust = true;
        return true;
    }
    return false;
}

char * CenterGenerator::generateCentermapString(
        int32_t * ori_datas, int32_t * datas,
        int32_t nWidth, int32_t nHeight,
        map<int32_t, FloodFillArea*> *areaMap,
        /*out*/map<int32_t, Center*> * centerMap)
{
    int32_t find = 0;
    map<int32_t, FloodFillArea *>::iterator it = areaMap->begin();

    vector<CenterEx *> centers;
    CenterEx *pCenter = NULL;

    int total = areaMap->size();
    int failureCnt = 0;

    for (; it != areaMap->end(); ++it)
    {
        FloodFillArea *rc  = it->second;
        int w = rc->right - rc->left;
        if (w == 0)
            continue;

        int h = rc->bottom - rc->top;
        if (h == 0)
            continue;

        int32_t number = rgb2bgr(it->first) & 0x00FFFFFF;
        //if (number == 0xFFFFFF)
        //    continue;

        if (pCenter == NULL)
        {
            pCenter = new CenterEx;
            memset(pCenter, 0, sizeof(CenterEx));
        }

        bool bRet = setSquare(ori_datas, datas, nWidth, nHeight, it->first, rc->left, rc->top, w, h, pCenter);

        if(!bRet) {
            LOGW("setSquare FAIL");
        }

        bool bOK = false;
        if (bRet)
        {
            pCenter->color = number;
            centers.push_back(pCenter);
            if (centerMap != NULL)
            {
                Center * center = (Center*) malloc(sizeof(Center));
                center -> x = (int16_t)pCenter->x;
                center -> y = (int16_t)pCenter->y;
                center -> size = (int16_t)pCenter->r;

                centerMap -> insert(map<int32_t, Center*>::value_type(it->first, center));
                bOK = true;
            } else {
                bOK = true;
            }
            pCenter = NULL;
        }

        if (!bOK)
        {
            ++failureCnt;
            LOGD("--setSquare for color %d (%d) return false", number, it->first);
        }
    }
    if (pCenter != NULL) delete pCenter;

    LOGD("--setSquare for color total : %d, failure: %d", total, failureCnt);

    int32_t centerSize = centers.size();
    int32_t maxStringLen  = 20 * centerSize; //%s%d,%d,%d,%d = 1 + 10 + 1 + 3 + 1 + 3
    char * pRet = new char[maxStringLen];
    memset(pRet, 0, maxStringLen);

    //LOGD("--[OPainter]center size == %d----", centerSize);
    int32_t nCopy = 0;
    vector<CenterEx *>::iterator itCenter = centers.begin();
    for(; itCenter != centers.end(); ++itCenter)
    {
        CenterEx *pCenter = *itCenter;
        nCopy += sprintf(pRet + nCopy, "%s%d,%d,%d,%d", nCopy == 0 ? "" : "|", pCenter->color, pCenter->x, pCenter->y, pCenter->r);
        delete pCenter;
    }
    return pRet;
}
