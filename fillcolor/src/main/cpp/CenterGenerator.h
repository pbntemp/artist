//
// Created by dxy on 2019/1/29.
//

#ifndef COLORBYNUMBER_CENTERGENERATOR_H
#define COLORBYNUMBER_CENTERGENERATOR_H

#include "structdef.h"
#include <jni.h>
#include <stdlib.h>
#include <string.h>
#include <android/bitmap.h>
#include <map>
#include <set>
#include <vector>
#include <algorithm>
#include <queue>

using namespace std;

extern "C"
{

class FixQueue {
    int *datas;
    int iUse;
    int iSize;
    int iHead;
    int iEnd;

public:
    FixQueue(int size) {
        datas = new int[size];
        iUse = 0;
        iSize = size;
        iHead = -1;
        iEnd = -1;
    }

    ~FixQueue() {
        if (datas != NULL)
            delete[] datas;
    }

    void push(int x)
    {
        if (iHead == -1)
        {
            iEnd = iHead = 0;
            datas[iHead] = x;
            iUse = 1;
        }
        else if (iUse < iSize)
        {
            ++iEnd;
            datas[iEnd] = x;
            ++iUse;
        }
        else
        {
            ++iHead;
            iEnd = iHead - 1;
            if (iHead == iSize) iHead = 0;
            datas[iEnd] = x;
        }
    }

    int at(int index)
    {
        if (index >= iUse || index < 0)
            return 0;
        int _index = (iHead + index) ;
        if (_index >= iSize) _index -= iSize;

        return datas[_index];
    }

    int size()
    {
        return iUse;
    }

    void ReArrange()
    {
        if (iHead == 0 || iEnd >= iHead)
            return;

        int *temp = new int[iHead];
        for (int i = 0; i < iHead; ++i)
        {
            temp[i] = datas[i];
        }

        int nMove = iSize - iHead;
        for (int i = 0; i < nMove; ++i) {
            datas[i] = datas[iHead + i];
        }

        for (int i = 0; i < iHead; ++i) {
            datas[nMove + i] = temp[i];
        }

        delete[] temp;

        iHead = 0;
        iEnd = iSize - 1;
    }
};

class CenterGenerator {
public:
     //调用者需要delete[] 返回值
     static char * generateCentermapString(
        int32_t * ori_datas,
        int32_t * datas,
        int32_t nWidth,
        int32_t nHeight,
        map<int32_t, FloodFillArea*> *areaMap,
        /*out*/ map<int32_t, Center*> *pCenterMap
     );

    //ori_datas : 原bmp 或 pdf 数据(1024 * 1024)(0xargb)
    //datas: region图数据(0xargb)
    //datasWidth : 1024
    //datasHeight : 1024
    //number: region区域的颜色值
    //startX: 外接矩形左顶点
    //startY: 外接矩形右顶点
    //width: 外接矩形宽
    //height: 外接矩形高
    //Center: 计算出的最大矩形区域
     static bool setSquare(
            int32_t * ori_datas,
            int32_t * datas,
            int32_t datasWidth,
            int32_t datasHeight,
            int32_t number,
            int32_t startX,
            int32_t startY,
            int32_t width,
            int32_t height,
            /*out*/ CenterEx *pCenter
      );
};

}



#endif //COLORBYNUMBER_CENTERGENERATOR_H
