//
// Created by ober on 2018/11/16.
//

#ifndef COLORBYNUMBER_PBN_H
#define COLORBYNUMBER_PBN_H

#include "OPainter.h"
#include "CenterGenerator.h"

using namespace std;

extern "C" {
JNIEXPORT jstring JNICALL
Java_com_meevii_color_fill_filler_FillColorFillerN_nVersion(JNIEnv *env, jclass type);

JNIEXPORT jlong JNICALL
Java_com_meevii_color_fill_filler_FillColorFillerN_nNewProgram(JNIEnv *env, jclass type);


JNIEXPORT jint JNICALL
Java_com_meevii_color_fill_filler_FillColorFillerN_nInitRegion(JNIEnv *env, jclass type,
                                                               jlong program,
                                                               jstring regionPng);

JNIEXPORT jint JNICALL
Java_com_meevii_color_fill_filler_FillColorFillerN_nInitEditable(JNIEnv *env, jclass type,
                                                                 jlong program,
                                                                 jobject editableBitmap);

JNIEXPORT jint JNICALL
Java_com_meevii_color_fill_filler_FillColorFillerN_nInitRegionAreas(JNIEnv *env, jclass type,
                                                                    jlong program);

JNIEXPORT jint JNICALL
Java_com_meevii_color_fill_filler_FillColorFillerN_nPreFillAreas(JNIEnv *env, jclass type,
                                                                 jlong program,
                                                                 jintArray data_);

JNIEXPORT jint JNICALL
Java_com_meevii_color_fill_filler_FillColorFillerN_nPreFillBadAreas(JNIEnv *env, jclass type,
                                                                    jlong program);

//
//JNIEXPORT jint JNICALL
//Java_com_meevii_color_fill_filler_FillColorFillerN_nInitCenterMap(JNIEnv *env, jclass type,
//                                                                  jlong program,
//                                                                  jintArray nums_,
//                                                                  jintArray centers_);
//JNIEXPORT jint JNICALL
//Java_com_meevii_color_fill_filler_FillColorFillerN_nInitBlockAreaMap(JNIEnv *env, jclass type,
//                                                                     jlong program,
//                                                                     jintArray keys_,
//                                                                     jintArray values_,
//                                                                     jint keyCount);

JNIEXPORT jint JNICALL
Java_com_meevii_color_fill_filler_FillColorFillerN_nFillForSingleColor(JNIEnv *env, jclass type,
                                                                       jlong program,
                                                                       jboolean hasColor,
                                                                       jint color,
                                                                       jintArray areaNum_);

JNIEXPORT jint JNICALL
Java_com_meevii_color_fill_filler_FillColorFillerN_nFillForSingleColorStep(JNIEnv *env, jclass type,
                                                                           jlong program,
                                                                           jint x, jint y,
                                                                           jboolean hasColor,
                                                                           jint color,
                                                                           jint areaNum,
                                                                           jobject jcallback);

JNIEXPORT jintArray JNICALL
Java_com_meevii_color_fill_filler_FillColorFillerN_nOverWriteColor(JNIEnv *env, jclass type,
                                                                   jlong program, jint srcColor,
                                                                   jboolean dstHasColor,
                                                                   jint dstColor);

JNIEXPORT jint JNICALL
Java_com_meevii_color_fill_filler_FillColorFillerN_nGetAreaMapSize(JNIEnv *env, jclass type,
                                                                   jlong program);


JNIEXPORT jint JNICALL
Java_com_meevii_color_fill_filler_FillColorFillerN_nGetAreaMap(JNIEnv *env, jclass type,
                                                               jlong program,
                                                               jintArray keys_,
                                                               jintArray floodFillAreas_);
JNIEXPORT jint JNICALL
Java_com_meevii_color_fill_filler_FillColorFillerN_nProcessFillByColor(JNIEnv *env, jclass type,
                                                                         jlong program,
                                                                         jint x, jint y,
                                                                         jboolean hasColor,
                                                                         jint targetColor,
                                                                         jintArray outResult_,
                                                                         jintArray outArea_,
                                                                         jintArray preColor_);

JNIEXPORT jint JNICALL
Java_com_meevii_color_fill_filler_FillColorFillerN_nResetEditBmp(JNIEnv *env, jclass type,
                                                                 jlong program);

JNIEXPORT jint JNICALL
Java_com_meevii_color_fill_filler_FillColorFillerN_nDestroy(JNIEnv *env, jclass type,
                                                            jlong program);

JNIEXPORT jstring JNICALL
Java_com_meevii_color_fill_filler_FillColorFillerN_nGenCenterStr(JNIEnv *env, jclass type,
                                                            jobject regionBmp, jobject originBmp);

JNIEXPORT jint JNICALL
Java_com_meevii_color_fill_filler_FillColorFillerN_nResetEditBmpAndInit(JNIEnv *env, jclass type,
        jlong program,
        jintArray areas_,
        jint color);

JNIEXPORT jint JNICALL
Java_com_meevii_color_fill_filler_FillColorFillerN_nResetEditBmpAndInitMap(JNIEnv *env, jclass type,
                                                                        jlong program,
                                                                        jintArray mapArr);

}
#endif //COLORBYNUMBER_PBN_H