//
// Created by ober on 2018/11/16.
//

#ifndef COLORBYNUMBER_OPAINTER_H
#define COLORBYNUMBER_OPAINTER_H

#include "structdef.h"
#include <jni.h>
#include <stdlib.h>
#include <string.h>
#include <android/bitmap.h>
#include <map>
#include <set>
#include <vector>
#include <algorithm>
#include "logdef.h"
#include <thread>

#include "opng/PngBitmap.h"
using namespace std;


#define blockAreaMapType std::map<int32_t, set<int32_t>*>
#define areaMapType std::map<int32_t, FloodFillArea*>
#define centerMapType std::map<int32_t, Center*>

extern "C" {

class OPainter {
private:
    centerMapType *centerMap = NULL; //area -- center
    blockAreaMapType *blockAreaMap = NULL; //block -- set<area>
    areaMapType *areaMap = NULL;
    set<int32_t> *preFilledBadAreas = NULL;

    PngBitmap* regionPngBmp = NULL;

    jobject editBmp = NULL;

    int regionSize[2];
    int editSize[2];

    static void initArea(FloodFillArea *area, int x, int y);

    static void recordArea(FloodFillArea *area, int x, int y);

    static void dumpAreaMap(map<int32_t, FloodFillArea *> *areaMap);

    static void dumpBlockAreaMap(map<int32_t, set<int32_t> *> *m);

    static inline int32_t bgr2rgb(int32_t bgr);

    static inline int32_t rgb2bgr(int32_t bgr);

    static void destroyAreaMap(areaMapType *m);

    static void destroyBlockAreaMap(blockAreaMapType *m);

    static void destroyCenterMap(centerMapType *m);

public:

    int32_t initRegion(JNIEnv *env, jstring regionPng);

    int32_t initEditable(JNIEnv *env, jobject editableBitmap);

    int32_t initRegionAreas(JNIEnv *env);

    int32_t preFillAreas(JNIEnv *env, jintArray data_);

    int32_t preFillBadAreas(JNIEnv *env);
//
//    int32_t initCenterMap(JNIEnv *env, jintArray nums_, jintArray centers_);
//
//    int32_t initBlockAreaMap(JNIEnv *env,
//                             jintArray keys_, jintArray values_, jint keyCount);


    int32_t fillForSingleColor(JNIEnv *env,
                               jboolean hasColor, jint color,
                               jintArray areaNum_);

    jintArray overwriteColor(JNIEnv *env,
                             jint srcColor,
                             jboolean dstHasColor,
                             jint dstColor);

    int32_t getAreaMapSize(JNIEnv *env);


    int32_t getAreaMap(JNIEnv *env,
                       jintArray keys_, jintArray floodFillAreas_);

    int32_t fillForSingleColors(JNIEnv *env,
                                jboolean hasColor,
                                jint color,
                                int *areaNum, int32_t size);

    int32_t fillForSingleColorStep(JNIEnv* env,
                                   jint x, jint y,
                                   jboolean hasColor,
                                   jint color,
                                   int areaNum,
                                   jobject jcallback);

    int32_t processFillByColor(JNIEnv *env,
                               jint x, jint y,
                               jboolean hasColor,
                               jint targetColor,
                               jintArray outResult_,
                               jintArray outArea_,
                               jintArray preColor_);

    int32_t resetEditBmp(JNIEnv *env);

    int32_t resetEditBmpAndInit(JNIEnv *env, int *areas, int32_t size, int32_t color);

    int32_t resetEditBmpAndInitMap(JNIEnv *env, int* mapData, int size);

    int32_t destroy(JNIEnv *env);
};

};


#endif //COLORBYNUMBER_OPAINTER_H
