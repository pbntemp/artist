//
// Created by ober on 2018/11/7.
//

#ifndef COLORBYNUMBER_GLOBALDEF_H
#define COLORBYNUMBER_GLOBALDEF_H

#include <android/log.h>

#define TAG "OberJni"
#define LOGD(...) __android_log_print(ANDROID_LOG_DEBUG,TAG ,__VA_ARGS__)
#define LOGI(...) __android_log_print(ANDROID_LOG_INFO,TAG ,__VA_ARGS__)
#define LOGW(...) __android_log_print(ANDROID_LOG_WARN,TAG ,__VA_ARGS__)
#define LOGE(...) __android_log_print(ANDROID_LOG_ERROR,TAG ,__VA_ARGS__)
#define LOGF(...) __android_log_print(ANDROID_LOG_FATAL,TAG ,__VA_ARGS__)

#endif //COLORBYNUMBER_GLOBALDEF_H
