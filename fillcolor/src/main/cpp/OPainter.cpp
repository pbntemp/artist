//
// Created by ober on 2018/11/16.
//

#include "OPainter.h"

#define FILL_COLOR_STEP_COUNT 20
#define FILL_COLOR_UPDATE_SLEEP_TIME 10

using namespace std;

//-1 fopen null
//
int32_t OPainter::initRegion(JNIEnv *env, jstring regionPng) {
    LOGD("[OPainter] initRegion");

    const char* png_file_path = env -> GetStringUTFChars(regionPng, NULL);
    LOGD("fopen %s", png_file_path);
    FILE* fp = fopen(png_file_path, "r");
    env -> ReleaseStringUTFChars(regionPng, png_file_path);
    if(fp == NULL) {
        return -1;
    }

    int r;
    PngBitmap* o = PngBitmap::new_from_file(fp, OPNG_DATA_FORMAT_ABGR, r);
    if(r != OPNG_RESULT_SUCCESS) {
        LOGE("PngBitmap::new_from_file err %d", r);
        return -1;
    }

    regionPngBmp = o;
    o -> getSize(regionSize);

    return 1;
}

int32_t OPainter::initEditable(JNIEnv *env, jobject editableBitmap) {
    LOGD("[OPainter] initEditable");
    editBmp = env -> NewGlobalRef(editableBitmap);
    if(editBmp) {
        AndroidBitmapInfo info;
        AndroidBitmap_getInfo(env, editableBitmap, &info);
        editSize[0] = info.width;
        editSize[1] = info.height;
    }
    return editBmp ? 1 : -1;
}

int32_t OPainter::initRegionAreas(JNIEnv *env) {

    LOGD("[OPainter] initRegionAreas");

    areaMap = new map<int32_t, FloodFillArea*>();

    int info[2];
    int r;

    r = regionPngBmp -> getSize(info);

    if(r != OPNG_RESULT_SUCCESS) {
        LOGE("get region info err");
        return -1;
    }

    const int32_t w = info[0];
    const int32_t h = info[1];

    uint32_t * addr;
    r = regionPngBmp -> getData(&addr);
    if(r != OPNG_RESULT_SUCCESS) {
        LOGE("get region info err");
        return -1;
    }
    int32_t * regionPixels = (int32_t*)addr;

    int32_t pxIndex = 0;
    FloodFillArea* lastArea = NULL;
    int32_t lastAreaNumber = 0;
    for(int32_t y = 0; y < h; y++) {
        for(int32_t x = 0; x < w; x++) {
            int32_t number = *(regionPixels + pxIndex) & 0x00ffffff;
            regionPixels[pxIndex] = (uint32_t)number;
            pxIndex++;
            if(number <= 0 || number == 0xffffff) {
                continue;
            }

            if(lastArea && number == lastAreaNumber) {
                recordArea(lastArea, x, y);
            } else {
                lastAreaNumber = number;
                map<int32_t, FloodFillArea *>::iterator it = areaMap->find(number);
                if (it == areaMap->end()) {
                    FloodFillArea *area = (FloodFillArea *) malloc(sizeof(*area));
                    initArea(area, x, y);
                    areaMap->insert(map<int32_t, FloodFillArea *>::value_type(number, area));
                    lastArea = area;
                } else {
                    FloodFillArea *area = (FloodFillArea *) (it->second);
                    recordArea(area, x, y);
                    lastArea = area;
                }
            }
        }
    }

    //set areas bad-flag
    map<int32_t, FloodFillArea *>::iterator it = areaMap->begin();

    while (it != areaMap -> end()) {
        FloodFillArea* area = it -> second;
        const int left = area -> left;
        const int right = area -> right;
        const int top = area -> top;
        const int bottom = area -> bottom;
        const int _w = right - left + 1;
        const int _h = bottom - top + 1;
        if(_w * _h <= 2) {
            area -> bad = true;
        }
        it++;
    }

    dumpAreaMap(areaMap);

    return 0;
}


int32_t OPainter::preFillAreas(JNIEnv *env, jintArray data_) {

    LOGD("[OPainter] preFillAreas");

    int result = 0;
    jint *data = env->GetIntArrayElements(data_, NULL);
    int32_t size = env -> GetArrayLength(data_);
    for(int i = 0; i < size/3; i++) {
        const int32_t num = rgb2bgr(data[i * 3]);
        const int32_t hasColor = data[i * 3 + 1];
        const int32_t color = data[i * 3 + 2];
        areaMapType::iterator it = areaMap -> find(num);
        if(it == areaMap -> end()) {
            continue;
        } else {
            FloodFillArea* area = it -> second;
            area -> colored = (bool)hasColor;
            if(hasColor) {
                int areas[1];
                areas[0] = bgr2rgb(num);
                fillForSingleColors(env, 1, color, areas, 1);

                area -> color = rgb2bgr(color);
            } else {
                area -> color = 0x00ffffff;
            }

            result++;
        }
    }

    env->ReleaseIntArrayElements(data_, data, 0);
    return result;
}


int32_t OPainter::preFillBadAreas(JNIEnv *env) {
    LOGD("[OPainter] preFillBadAreas");

    areaMapType::iterator areaMapIt = areaMap -> begin();

    int32_t count = 0;
    preFilledBadAreas = new set<int32_t>();

    while (areaMapIt != areaMap -> end()) {
        const int32_t num = areaMapIt -> first;

        centerMapType::iterator centerIt = centerMap -> find(num);
        if(centerIt == centerMap -> end()) {
            const FloodFillArea* area = areaMapIt -> second;
            int32_t w = area -> right - area -> left + 1;
            int32_t h = area -> bottom - area -> top + 1;
            if(w < 10 || h < 10) {
                preFilledBadAreas -> insert(num);
                count++;
            }
        }
        areaMapIt++;
    }

    if(count > 0) {
        int32_t areas[count];
        int32_t index = 0;
        for(std::set<int32_t>::iterator it = preFilledBadAreas -> begin();
            it != preFilledBadAreas -> end(); it++) {
            areas[index++] = *it;
        }
        const int32_t color = 0xff000000;

        return fillForSingleColors(env, 1, color, areas, count);
    }

    return 0;
}

//int32_t OPainter::initCenterMap(JNIEnv *env,
//                                jintArray nums_,
//                                jintArray centers_) {
//    LOGD("[OPainter] initCenterMap");
//    jint *nums = env->GetIntArrayElements(nums_, NULL);
//    jint *centers = env->GetIntArrayElements(centers_, NULL);
//
//    int32_t count = env -> GetArrayLength(nums_);
//
//    centerMap = new centerMapType();
//
//    for(int i = 0; i < count; i++) {
//        const int32_t num = rgb2bgr(nums[i]);
//        Center * center = (Center*) malloc(sizeof(*center));
//        center -> x = (int16_t)centers[3 * i];
//        center -> y = (int16_t)centers[3 * i + 1];
//        center -> size = (int16_t)centers[3 * i + 2];
//
//        centerMap -> insert(centerMapType::value_type(num, center));
//    }
//
//    env->ReleaseIntArrayElements(nums_, nums, 0);
//    env->ReleaseIntArrayElements(centers_, centers, 0);
//
//    return 0;
//}


//int32_t OPainter::initBlockAreaMap(JNIEnv *env,
//                                   jintArray keys_,
//                                   jintArray values_,
//                                   jint keyCount) {
//
//    LOGD("[OPainter] intiBlockAreaMap");
//
//    blockAreaMap = new map<int32_t , set<int32_t>*>();
//
//    jint *blocks = env->GetIntArrayElements(keys_, NULL);
//    jint *areas = env->GetIntArrayElements(values_, NULL);
//
//    for(int i = 0; i < keyCount; i++) {
//        const int32_t block = blocks[i];
//        const int32_t area = rgb2bgr(areas[i]);
//        blockAreaMapType::iterator it = blockAreaMap->find(block);
//        if(it == blockAreaMap -> end()) {
//            set<int32_t> *areaSet = new set<int32_t>();
//            areaSet -> insert(area);
//            blockAreaMap -> insert(blockAreaMapType::value_type(block, areaSet));
//        } else {
//            set<int32_t> *areaSet = it -> second;
//            areaSet -> insert(area);
//        }
//    }
//
//    env->ReleaseIntArrayElements(keys_, blocks, 0);
//    env->ReleaseIntArrayElements(values_, areas, 0);
//
//    //dumpBlockAreaMap(blockAreaMap);
//
//    return 0;
//}



jintArray OPainter::overwriteColor(JNIEnv *env,
                         jint srcColor,
                         jboolean dstHasColor,
                         jint dstColor) {

    areaMapType::iterator it = areaMap -> begin();

    const int32_t tofind = rgb2bgr(srcColor);

    vector<int32_t> areas;
    while (it != areaMap -> end()) {
        FloodFillArea* area = it -> second;
        if(area -> colored && (area -> color == tofind)) {
            areas.push_back(bgr2rgb(it -> first));
        }
        it++;
    }

    int size = (int)areas.size();
    if(size == 0) {
        return NULL;
    }

    int* areas_arr = &areas[0];

    fillForSingleColors(env, dstHasColor, dstColor, areas_arr, size);

    jintArray jArray = env -> NewIntArray(size);
    jint *array = env -> GetIntArrayElements(jArray, NULL);
    for(int i = 0; i < size; i++) {
        *(array + i) = *(areas_arr + i);
    }
    env -> ReleaseIntArrayElements(jArray, array, 0);

    return jArray;
}


int32_t OPainter::fillForSingleColor(JNIEnv *env,
                                     jboolean hasColor,
                                     jint color,
                                     jintArray areaNum_) {
    //LOGI("nFillForSingleColor");

    jint *areaNum = env->GetIntArrayElements(areaNum_, NULL);
    int32_t size = env -> GetArrayLength(areaNum_);


    int32_t result = fillForSingleColors(env, hasColor, color, areaNum, size);

    env->ReleaseIntArrayElements(areaNum_, areaNum, 0);

    return result;
}


int32_t OPainter::getAreaMapSize(JNIEnv *env) {
    return (int32_t)areaMap -> size();
}


int32_t OPainter::getAreaMap(JNIEnv *env,
                             jintArray keys_,
                             jintArray floodFillAreas_) {
    jint *keys = env->GetIntArrayElements(keys_, NULL);
    jint *floodFillAreas = env->GetIntArrayElements(floodFillAreas_, NULL);

    areaMapType::iterator it = areaMap -> begin();
    int32_t index = 0;
    while (it != areaMap -> end()) {
        int32_t num = it -> first;
        FloodFillArea* area = it -> second;
        keys[index] = bgr2rgb(num);
        floodFillAreas[index * 6] = area -> left;
        floodFillAreas[index * 6 + 1] = area -> top;
        floodFillAreas[index * 6 + 2] = area -> right;
        floodFillAreas[index * 6 + 3] = area -> bottom;
        floodFillAreas[index * 6 + 4] = (area -> colored) ? 1 : 0;
        floodFillAreas[index * 6 + 5] = bgr2rgb(area -> color);
        it++;
        index++;
    }

    env->ReleaseIntArrayElements(keys_, keys, 0);
    env->ReleaseIntArrayElements(floodFillAreas_, floodFillAreas, 0);

    return 0;
}

int32_t OPainter::fillForSingleColors(JNIEnv *env,
                                  jboolean hasColor,
                                  jint color,
                                  int* areaNum, int32_t size) {
    int32_t result;
    if(size == 0) {
        result = 0;
    } else {

        const int SIZE_WIDTH = regionSize[0];
        const int SIZE_HEIGHT = regionSize[1];

        void *addr1;
        uint32_t *addr2;
        int r1 = AndroidBitmap_lockPixels(env, editBmp, &addr1);
        int r2 = regionPngBmp -> getData(&addr2);

        if (r1 != ANDROID_BITMAP_RESULT_SUCCESS) {
            result = -1;
        } else {
            result = 0;
            int32_t *editPixels = (int32_t *) addr1;
            int32_t *regionPixels = (int32_t *) addr2;
            for (int i = 0; i < size; i++) {
                const int32_t num = rgb2bgr(areaNum[i]);
                areaMapType::iterator it = areaMap->find(num);
                if (it == areaMap->end()) {
                    continue;
                }

                FloodFillArea *area = it->second;

                int32_t resultColor;
                bool resultColored;

                if(hasColor) {
                    resultColor = rgb2bgr(color);
                    resultColored = 1;
                    if(area -> colored) {
                        if(area -> color == resultColor) {
                            continue;
                        }
                    }

                } else {
                    if(area -> colored) {
                        resultColor = 0x00ffffff;
                        resultColored = 0;
                    } else {
                        continue;
                    }

                }

                area -> colored = resultColored;
                area -> color = resultColor;

                result++;

                const int32_t top = area->top;
                const int32_t bottom = area->bottom;
                const int32_t left = area->left;
                const int32_t right = area->right;
                for (int32_t y = top; y <= bottom; y++) {
                    for (int32_t x = left; x <= right; x++) {
                        const int32_t index = (SIZE_WIDTH * y) + x;
                        if (regionPixels[index] == num) {
                            editPixels[index] = (resultColor | 0xff000000);
                        }
                    }
                }
            }
        }

        if (r1 == 0) {
            AndroidBitmap_unlockPixels(env, editBmp);
        }
    }

    return result;
}

class SPointComp {
public:
    SPointComp() {

    }

    bool operator() (SPoint a, SPoint b) {

        return a.d - b.d < 0;
    }
};
int32_t OPainter::fillForSingleColorStep(JNIEnv *env,
                                         jint x, jint y,
                                         jboolean hasColor,
                                         jint color, int areaNum,
                                         jobject jcallback) {

    const int32_t SIZE_WIDTH = regionSize[0];
    const int32_t SIZE_HEIGHT = regionSize[1];

    void* addr1;
    uint32_t * addr2;

    int r1 = AndroidBitmap_lockPixels(env, editBmp, &addr1);
    int r2 = regionPngBmp -> getData(&addr2);
    if (r1 != ANDROID_BITMAP_RESULT_SUCCESS || r2 != OPNG_RESULT_SUCCESS) {
        LOGE("fillForSingleColorStep fail %d,%d", r1, r2);
        AndroidBitmap_unlockPixels(env, editBmp);
        return -1;
    }

    int32_t icolor = rgb2bgr(color);
    int32_t num = rgb2bgr(areaNum);

    areaMapType::iterator it = areaMap->find(num);
    if (it == areaMap->end()) {
        AndroidBitmap_unlockPixels(env, editBmp);
        return -2;
    }

    FloodFillArea *area = it->second;

    bool resultColored;
    int32_t resultColor;

    if(hasColor) {
        if(area -> colored) {
            if(area -> color == icolor) {
                AndroidBitmap_unlockPixels(env, editBmp);
                return -3;
            }
        }
        resultColored = 1;
        resultColor = icolor;
    } else {
        resultColored = 0;
        resultColor = 0x00ffffff;
    }

    area -> colored = resultColored;
    area -> color = resultColor;

    int32_t *editPixels = (int32_t *) addr1;
    int32_t *regionPixels = (int32_t *) addr2;

    int32_t pointNum = regionPixels[x + y * SIZE_WIDTH];
    if((pointNum & 0x00ffffff) != rgb2bgr(areaNum)) {
        LOGE("[OPainter] point not in right region area");
        AndroidBitmap_unlockPixels(env, editBmp);
        return -2;
    }

    jclass jrunableClass = env -> FindClass("com/meevii/color/fill/filler/FillColorStepCallback");
    const jmethodID jrunMethod = env -> GetMethodID(jrunableClass, "callback", "(IIII)V");

    vector<SPoint> vec;

    const int32_t top = area->top;
    const int32_t bottom = area->bottom;
    const int32_t left = area->left;
    const int32_t right = area->right;

    LOGD("[OPainter] push points to vect---");

    for (int32_t dy = top; dy <= bottom; dy++) {
        int32_t h = SIZE_WIDTH * dy;
        for (int32_t dx = left; dx <= right; dx++) {
            const int32_t index = h + dx;
            if (regionPixels[index] == num) {
                SPoint p;
                p.d = (x - dx) * (x - dx) + (y - dy) * (y - dy);
                p.index = index;
                vec.push_back(p);
            }
        }
    }

    LOGD("[OPainter] sort point start---");

    std::sort<SPoint>(vec.begin(), vec.end(), SPointComp());

    LOGD("[OPainter] sort point end---");

    int32_t size = (int32_t)vec.size();
    int32_t updatePeriod = size / FILL_COLOR_STEP_COUNT;

    int32_t targetColor = icolor | 0xff000000;

    for(int i = 0; i < size; i++) {
        SPoint p = vec[i];
        editPixels[p.index] = targetColor;
        if(i % updatePeriod == 0) {
            AndroidBitmap_unlockPixels(env, editBmp);
            env -> CallVoidMethod(jcallback, jrunMethod, left, top, right, bottom);
            std::this_thread::sleep_for(std::chrono::milliseconds(FILL_COLOR_UPDATE_SLEEP_TIME));
            AndroidBitmap_lockPixels(env, editBmp, &addr1);
        }
    }

    AndroidBitmap_unlockPixels(env, editBmp);

    return 1;
}

//outResult
//-1 : error xy
//-2 : error data
//-3 : bad area
//0  : same color
//1  : fill color
//2  : over write color
//3  : clear color
//4  : none color
int32_t OPainter::processFillByColor(JNIEnv *env, jint x, jint y, jboolean hasColor,
                                     jint targetColor, jintArray outResult_,
                                     jintArray outArea_,
                                     jintArray preColor_) {

    const int SIZE_WIDTH = regionSize[0];
    const int SIZE_HEIGHT = regionSize[1];

    jint *outResult = env->GetIntArrayElements(outResult_, NULL);
    jint *outArea = env->GetIntArrayElements(outArea_, NULL);
    jint *preColor = env->GetIntArrayElements(preColor_, NULL);
    int32_t r;
    int32_t areaNum;
    int32_t pre = 0;
    const int32_t index = x + y * SIZE_WIDTH;
    if(index < 0 || index >= SIZE_WIDTH * SIZE_HEIGHT) {
        r = -1;
        areaNum = 0;
    } else {
        uint32_t *addr;
        int flag = regionPngBmp -> getData(&addr);
        if(flag == ANDROID_BITMAP_RESULT_SUCCESS) {
            int32_t* pixels = (int32_t*) addr;
            int32_t clickedNum = pixels[index];

            areaMapType::iterator it = areaMap -> find(clickedNum);
            if(it == areaMap -> end()) {
                r = -2;
                areaNum = 0;
            } else {
                areaNum = bgr2rgb(clickedNum);
                FloodFillArea* floodFillArea = it -> second;

                if(floodFillArea -> bad) {
                    r = -3;
                    areaNum = 0;
                } else {

                    if (floodFillArea->colored) {
                        if (!hasColor) {
                            int32_t iColor = bgr2rgb(floodFillArea->color);
                            r = 3;
                            pre = iColor;
                        } else {
                            int32_t iColor = bgr2rgb(floodFillArea->color);
                            if (iColor == (targetColor & 0x00ffffff)) {
                                r = 0;
                            } else {
                                r = 2;
                            }
                            pre = iColor;
                        }
                    } else {
                        if (!hasColor) {
                            r = 4;
                        } else {
                            r = 1;
                        }
                    }
                }
            }


        } else {
            r = -1;
            areaNum = 0;
        }

    }

    outResult[0] = r;
    outArea[0] = areaNum;
    preColor[0] = pre;

    env->ReleaseIntArrayElements(outResult_, outResult, 0);
    env->ReleaseIntArrayElements(outArea_, outArea, 0);
    env->ReleaseIntArrayElements(preColor_, preColor, 0);

    return 1;
}

int32_t OPainter::resetEditBmp(JNIEnv *env) {

    void* addr;
    int32_t r = AndroidBitmap_lockPixels(env, editBmp, &addr);
    if(r != 0) {
        return r;
    }

    AndroidBitmapInfo info;
    AndroidBitmap_getInfo(env, editBmp, &info);
    const int w = info.width;
    const int h = info.height;

    int32_t *pixels = (int32_t*)addr;
    int32_t size = w * h;
    for(int i = 0; i < size; i++) {
        pixels[i] = 0x00ffffff;
    }
    areaMapType::iterator it = areaMap -> begin();
    if(preFilledBadAreas != NULL) {
        while (it != areaMap->end()) {
            int32_t num = it->first;
            std::set<int32_t>::iterator badAreaIt = preFilledBadAreas->find(num);
            if (badAreaIt == preFilledBadAreas->end()) {
                FloodFillArea *area = it->second;
                area->colored = 0;
                area->color = 0x00ffffff;
            }
            it++;
        }
    } else {
        while (it != areaMap->end()) {
            FloodFillArea *area = it->second;
            area->colored = 0;
            area->color = 0x00ffffff;
            it++;
        }
    }

    AndroidBitmap_unlockPixels(env, editBmp);

    return 0;
}

int32_t OPainter::resetEditBmpAndInit(JNIEnv *env, int *areas, int32_t areasize, int32_t color) {

    void* addr;
    int32_t r = AndroidBitmap_lockPixels(env, editBmp, &addr);
    if(r != 0) {
        return r;
    }

    AndroidBitmapInfo info;
    AndroidBitmap_getInfo(env, editBmp, &info);
    const int w = info.width;
    const int h = info.height;

    int32_t *pixels = (int32_t*)addr;
    int32_t size = w * h;
    for(int i = 0; i < size; i++) {
        pixels[i] = 0x00ffffff;
    }
    areaMapType::iterator it = areaMap -> begin();
    if(preFilledBadAreas != NULL) {
        while (it != areaMap->end()) {
            int32_t num = it->first;
            std::set<int32_t>::iterator badAreaIt = preFilledBadAreas->find(num);
            if (badAreaIt == preFilledBadAreas->end()) {
                FloodFillArea *area = it->second;
                area->colored = 0;
                area->color = 0x00ffffff;
            }
            it++;
        }
    } else {
        while (it != areaMap->end()) {
            FloodFillArea *area = it->second;
            area->colored = 0;
            area->color = 0x00ffffff;
            it++;
        }
    }

    int32_t result;
    if(areasize == 0) {
        result = 0;
    } else {

        const int SIZE_WIDTH = regionSize[0];
        const int SIZE_HEIGHT = regionSize[1];

        uint32_t *addr2;
        int r2 = regionPngBmp -> getData(&addr2);

       {
            result = 0;
            int32_t *editPixels = pixels;
            int32_t *regionPixels = (int32_t *) addr2;
            for (int i = 0; i < areasize; i++) {
                const int32_t num = rgb2bgr(areas[i]);
                areaMapType::iterator it = areaMap->find(num);
                if (it == areaMap->end()) {
                    continue;
                }

                FloodFillArea *area = it->second;

                int32_t resultColor = rgb2bgr(color);

                area -> colored = 1;
                area -> color = resultColor;

                result++;

                const int32_t top = area->top;
                const int32_t bottom = area->bottom;
                const int32_t left = area->left;
                const int32_t right = area->right;
                for (int32_t y = top; y <= bottom; y++) {
                    for (int32_t x = left; x <= right; x++) {
                        const int32_t index = (SIZE_WIDTH * y) + x;
                        if (regionPixels[index] == num) {
                            editPixels[index] = (resultColor | 0xff000000);
                        }
                    }
                }
            }
        }
    }

    AndroidBitmap_unlockPixels(env, editBmp);

    return result;
}


int32_t OPainter::resetEditBmpAndInitMap(JNIEnv *env, int *mapData, int dataSize) {

    void* addr;
    int32_t r = AndroidBitmap_lockPixels(env, editBmp, &addr);
    if(r != 0) {
        return r;
    }

    AndroidBitmapInfo info;
    AndroidBitmap_getInfo(env, editBmp, &info);
    const int w = info.width;
    const int h = info.height;
    int32_t *pixels = (int32_t *) addr;

    {
        int32_t size = w * h;
        for (int i = 0; i < size; i++) {
            pixels[i] = 0x00ffffff;
        }
        areaMapType::iterator it = areaMap->begin();
        if (preFilledBadAreas != NULL) {
            while (it != areaMap->end()) {
                int32_t num = it->first;
                std::set<int32_t>::iterator badAreaIt = preFilledBadAreas->find(num);
                if (badAreaIt == preFilledBadAreas->end()) {
                    FloodFillArea *area = it->second;
                    area->colored = 0;
                    area->color = 0x00ffffff;
                }
                it++;
            }
        } else {
            while (it != areaMap->end()) {
                FloodFillArea *area = it->second;
                area->colored = 0;
                area->color = 0x00ffffff;
                it++;
            }
        }
    }

    const int SIZE_WIDTH = regionSize[0];
    const int SIZE_HEIGHT = regionSize[1];

    uint32_t *addr2;
    int r2 = regionPngBmp -> getData(&addr2);
    int32_t result = 0;

    {
        int32_t *editPixels = pixels;
        int32_t *regionPixels = (int32_t *) addr2;
        int32_t areasize = dataSize / 2;

        for (int i = 0; i < areasize; i++) {

            const int32_t num = rgb2bgr(mapData[2 * i]);
            int32_t resultColor = rgb2bgr(mapData[2 * i + 1]);

            LOGI("fill for %d == %d", mapData[2*i], mapData[2 * i + 1]);

            areaMapType::iterator it = areaMap->find(num);
            if (it == areaMap->end()) {
                continue;
            }

            FloodFillArea *area = it->second;

            area -> colored = 1;
            area -> color = resultColor;

            result++;

            const int32_t top = area->top;
            const int32_t bottom = area->bottom;
            const int32_t left = area->left;
            const int32_t right = area->right;
            for (int32_t y = top; y <= bottom; y++) {
                for (int32_t x = left; x <= right; x++) {
                    const int32_t index = (SIZE_WIDTH * y) + x;
                    if (regionPixels[index] == num) {
                        editPixels[index] = (resultColor | 0xff000000);
                    }
                }
            }
        }
    }

    AndroidBitmap_unlockPixels(env, editBmp);

    return result;
}

int32_t OPainter::destroy(JNIEnv *env) {

    PngBitmap::recycle(regionPngBmp);
    regionPngBmp = NULL;

    env -> DeleteGlobalRef(editBmp);
    editBmp = NULL;

    destroyAreaMap(areaMap);
    areaMap = NULL;

    destroyCenterMap(centerMap);
    centerMap = NULL;

    destroyBlockAreaMap(blockAreaMap);
    blockAreaMap = NULL;

    delete(preFilledBadAreas);
    preFilledBadAreas = NULL;

    return 0;
}

void OPainter::initArea(FloodFillArea* area, int x, int y) {
    area -> left = x;
    area -> right = x;
    area -> top = y;
    area -> bottom = y;
    area -> colored = 0;
    area -> color = 0x00ffffff;
    area -> bad = false;
}

void OPainter::recordArea(FloodFillArea* area, int x, int y) {
    if(x < area->left) {
        area->left = x;
    }
    if(x > area->right) {
        area->right = x;
    }
    if(y < area->top) {
        area->top = y;
    }
    if(y > area->bottom) {
        area->bottom = y;
    }
}

void OPainter::dumpAreaMap(map<int32_t, FloodFillArea *> *areaMap) {
    LOGD("--dumpAreaMap--");
    std::map<int32_t, FloodFillArea*>::iterator it = areaMap -> begin();
    while(it != areaMap -> end()) {
        int32_t num = it -> first;
        FloodFillArea* area = it -> second;
        LOGD("[%d]--[%d,%d,%d,%d]", bgr2rgb(num), area->left, area->top, area->right, area->bottom);
        it++;
    }
    LOGD("--dumpAreaMap end--");
}

void OPainter::dumpBlockAreaMap(map<int32_t, set<int32_t>*> * m) {
    LOGD("---dumpBlockAreaMap---");
    std::map<int32_t, set<int32_t>*>::iterator it = m -> begin();
    while(it != m -> end()) {
        LOGD("block=%d---", it->first);
        set<int32_t>* s = it -> second;
        std::set<int32_t>::iterator setIt = s -> begin();
        while (setIt != s -> end()) {
            LOGD("[%d]", *setIt);
            setIt++;
        }
        it++;
    }
    LOGD(("---dumpBLockAreaMap end---"));
}

inline int32_t OPainter::bgr2rgb(int32_t bgr) {
    return ((bgr & 0x00ff0000)>>16) | ((bgr & 0x0000ff00)) | ((bgr & 0x000000ff)<<16);
}

inline int32_t OPainter::rgb2bgr(int32_t bgr) {
    return ((bgr & 0x00ff0000)>>16) | ((bgr & 0x0000ff00)) | ((bgr & 0x000000ff)<<16);
}

void OPainter::destroyAreaMap(areaMapType* m) {
    if(!m) {
        return;
    }

    areaMapType::iterator it = m -> begin();
    while (it != m -> end()) {
        FloodFillArea* area = it -> second;
        free(area);
        it++;
    }

    delete(m);
}

void OPainter::destroyBlockAreaMap(blockAreaMapType* m) {
    if(!m) {
        return;
    }

    blockAreaMapType::iterator it = m -> begin();

    while (it != m -> end()) {
        set<int32_t>* areas = it -> second;
        delete(areas);
        it++;
    }

    delete (m);
}

void OPainter::destroyCenterMap(centerMapType* m) {
    if(!m) {
        return;
    }

    centerMapType::iterator it = m -> begin();

    while (it != m -> end()) {
        Center* center = it -> second;
        free(center);
        it++;
    }

    delete(m);
}
