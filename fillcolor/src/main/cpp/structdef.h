//
// Created by ober on 2018/11/8.
//

#ifndef COLORBYNUMBER_STRUCTDEF_H
#define COLORBYNUMBER_STRUCTDEF_H

#include <stdint.h>

#include <jni.h>
#include <stdlib.h>
#include <string.h>
#include <android/bitmap.h>
#include <map>
#include <set>
#include <list>

typedef struct CenterStruct {
    int16_t x;
    int16_t y;
    int16_t size;
} Center;

typedef struct CenterStructEx {
    int32_t color;
    int16_t x;
    int16_t y;
    int16_t r;
    bool bNeedAdjust;
} CenterEx;

typedef struct FloodFillAreaStruct {
    int bottom;
    int left;
    int right;
    int top;
    bool colored;
    int32_t color;

    bool bad;//坏区域 (当前逻辑是w*h<=2)
} FloodFillArea;

typedef struct Int16Point {
    int32_t d;
    int32_t index;
} SPoint;

#endif //COLORBYNUMBER_STRUCTDEF_H
