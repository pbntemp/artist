//
// Created by ober on 2019/4/12.
//

#ifndef TESTN_PNGBITMAP_H
#define TESTN_PNGBITMAP_H


#define OPNG_RESULT_FAIL (-1)
#define OPNG_RESULT_SUCCESS 0
#define OPNG_RESULT_BAD_FILE 1
#define OPNG_RESULT_BAD_FILE_CONTENT 2
#define OPNG_RESULT_BAD_FILE_HEAD 3
#define OPNG_RESULT_BAD_ARGUMENT 5

#define OPNG_DATA_FORMAT_RGBA 1
#define OPNG_DATA_FORMAT_ARGB 2
#define OPNG_DATA_FORMAT_ABGR 3
#define OPNG_DATA_FORMAT_BGRA 4

#include <stdio.h>
#include <jni.h>

#define OPNG_DEBUG

class PngBitmap {

public:
    static PngBitmap* new_from_file(FILE *f, uint8_t useFormat, int &result);

    static void recycle(PngBitmap* pngBitmap);

    int getSize(int size[2]);
    int getDataFormat(uint8_t& dataFormat);
    int getColorType(int& colorType);
    int getData(uint32_t** data);
    ~PngBitmap();

private:
    int* size;
    uint8_t dataFormat;
    uint32_t* data;
    int colorType;
    PngBitmap(uint32_t* data, int w, int h, uint8_t data_format, int color_type);
};


#endif //TESTN_PNGBITMAP_H
