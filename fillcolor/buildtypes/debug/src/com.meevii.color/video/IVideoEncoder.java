package com.meevii.color.video;

/**
 * Created by ober on 18-11-19.
 */
public interface IVideoEncoder {
    int prepareEncoder();

    int encode();

    void release();
}
