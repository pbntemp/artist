package com.meevii.color.video;

import android.graphics.Bitmap;
import android.media.MediaCodecInfo;
import android.media.MediaCodecList;
import android.media.MediaFormat;
import android.os.Build;
import android.util.Log;

import com.meevii.color.fill.BuildConfig;
import com.socks.library.KLog;

import java.io.File;

/**
 * Created by ober on 18-11-19.
 */
public class NVideoEncoder implements IVideoEncoder {

    private static final String TAG = "NVideoEncoder";

    public static IVideoEncoder create(Bitmap bitmap, Bitmap foreground, String outpath) {
        return new NVideoEncoder(bitmap, foreground, outpath);
    }

    private final Bitmap mBitmap;
    private final Bitmap mForeBitmap;
    private final String mOutPath;

    private long mProgram;

    private NVideoEncoder(Bitmap bitmap, Bitmap fore, String outpath) {
        this.mBitmap = bitmap;
        this.mForeBitmap = fore;
        this.mOutPath = outpath;
    }

    private static native long nNewProgram();
    private static native int nPrepareEncoder(long program,
                                              String codecName, int colorFormat,
                                              Bitmap bmp, Bitmap fore, String outPath);
    private static native int nEncode(long program);
    private static native int nRelease(long program);
    private static native String nVersion();

    public static String version() {
        if(Build.VERSION.SDK_INT < 21) {
            return "NOT SUPPORT";
        } else {
            return nVersion();
        }
    }


    @Override
    public int prepareEncoder() {

        if(!VideoSupport.isSet) {
            KLog.e(TAG, "VideoSupport not setup successful");
            return -1;
        }

        mProgram = nNewProgram();

        MediaCodecInfo info = VideoSupport.mInfo;
        int colorFormat = VideoSupport.mColorFormat;

        int r = nPrepareEncoder(mProgram, info.getName(),
                colorFormat, mBitmap, mForeBitmap, mOutPath);
        if(r < 0) {
            KLog.e(TAG, "native prepare encoder failed:" + r);
        }
        return r;
    }

    @Override
    public int encode() {
        return nEncode(mProgram);
    }

    @Override
    public void release() {
        nRelease(mProgram);
    }
}
