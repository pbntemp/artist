package com.meevii.color.video;

import android.media.MediaCodecInfo;
import android.media.MediaCodecList;
import android.media.MediaFormat;
import android.os.Build;

import com.socks.library.KLog;

/**
 * Created by ober on 2018/11/20.
 */
public final class VideoSupport {

    private static final String TAG = "VideoSupport";
    public static final String MIME_TYPE = MediaFormat.MIMETYPE_VIDEO_AVC;

    static MediaCodecInfo mInfo;
    static int mColorFormat;
    static boolean isSet = false;

    public static boolean setup() {
        if(Build.VERSION.SDK_INT < 21) {
            return false;
        }
        MediaCodecInfo info = selectCodec(MIME_TYPE);
        if(info == null) {
            return false;
        }
        int colorFormat = selectColorFormat(info, MIME_TYPE);
        if(colorFormat == 0) {
            KLog.e(TAG, "colorFormat not found!");
            return false;
        }

        mInfo = info;
        mColorFormat = colorFormat;
        isSet = true;
        return true;
    }

    private static MediaCodecInfo selectCodec(String mimeType) {
        int numCodecs = MediaCodecList.getCodecCount();
        for (int i = 0; i < numCodecs; i++) {
            MediaCodecInfo codecInfo = MediaCodecList.getCodecInfoAt(i);
            if (!codecInfo.isEncoder()) {
                continue;
            }
            String[] types = codecInfo.getSupportedTypes();
            for (int j = 0; j < types.length; j++) {
                if (types[j].equalsIgnoreCase(mimeType)) {
                    return codecInfo;
                }
            }
        }
        return null;
    }

    private static int selectColorFormat(MediaCodecInfo codecInfo,
                                         String mimeType) {
        MediaCodecInfo.CodecCapabilities capabilities = codecInfo
                .getCapabilitiesForType(mimeType);
        for (int i = 0; i < capabilities.colorFormats.length; i++) {
            int colorFormat = capabilities.colorFormats[i];
            if (isRecognizedFormat(colorFormat)) {
                return colorFormat;
            }
        }
        return 0; // not reached
    }

    private static boolean isRecognizedFormat(int colorFormat) {
        switch (colorFormat) {
            // these are the formats we know how to handle for
            case MediaCodecInfo.CodecCapabilities.COLOR_FormatYUV420Planar:
            case MediaCodecInfo.CodecCapabilities.COLOR_FormatYUV420PackedPlanar:
            case MediaCodecInfo.CodecCapabilities.COLOR_FormatYUV420SemiPlanar:
            case MediaCodecInfo.CodecCapabilities.COLOR_FormatYUV420PackedSemiPlanar:
            case MediaCodecInfo.CodecCapabilities.COLOR_TI_FormatYUV420PackedSemiPlanar:
                return true;
            default:
                return false;
        }
    }

}
