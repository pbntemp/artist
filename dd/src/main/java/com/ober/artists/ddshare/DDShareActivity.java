package com.ober.artists.ddshare;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.Nullable;

import com.android.dingtalk.share.ddsharemodule.DDShareApiFactory;
import com.android.dingtalk.share.ddsharemodule.IDDAPIEventHandler;
import com.android.dingtalk.share.ddsharemodule.IDDShareApi;
import com.android.dingtalk.share.ddsharemodule.ShareConstant;
import com.android.dingtalk.share.ddsharemodule.message.BaseReq;
import com.android.dingtalk.share.ddsharemodule.message.BaseResp;
import com.android.dingtalk.share.ddsharemodule.message.SendAuth;

/**
 * Created by ober on 2019-10-28.
 */
public class DDShareActivity extends Activity implements IDDAPIEventHandler {

    private static final String TAG = "DDShareActivity";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG ,"onCreate==========>");
        try {
            //activity的export为true，try起来，防止第三方拒绝服务攻击
            IDDShareApi mIDDShareApi = DDShareApiFactory.createDDShareApi(this, DDClient.APP_ID, false);
            mIDDShareApi.handleIntent(getIntent(), this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onReq(BaseReq baseReq) {
        Log.d(TAG, "onReq " + baseReq.getClass().getSimpleName());
    }

    @Override
    public void onResp(BaseResp baseResp) {

        Log.d(TAG, "onResp " + baseResp.getClass().getSimpleName());

        int errCode = baseResp.mErrCode;
        Log.d(TAG , "errorCode==========>" + errCode);
        String errMsg = baseResp.mErrStr;
        Log.d(TAG , "errMsg==========>" + errMsg);

        if(baseResp.getType() == ShareConstant.COMMAND_SENDAUTH_V2 && (baseResp instanceof SendAuth.Resp)){
            SendAuth.Resp authResp = (SendAuth.Resp) baseResp;
            DDClient.handleAuthResult(this, authResp);
        }

        finish();

    }
}
