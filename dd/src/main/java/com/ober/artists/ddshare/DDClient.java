package com.ober.artists.ddshare;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.UiThread;

import com.android.dingtalk.share.ddsharemodule.DDShareApiFactory;
import com.android.dingtalk.share.ddsharemodule.IDDShareApi;
import com.android.dingtalk.share.ddsharemodule.message.BaseResp;
import com.android.dingtalk.share.ddsharemodule.message.SendAuth;

import java.util.UUID;

/**
 * Created by ober on 2019-10-28.
 */
public class DDClient {

    public static final int RESULT_OK = 0;
    public static final int RESULT_CANCEL = -2;
    public static final int RESULT_ERR = -3;

    public interface AuthListener {
        void onDDAuthResult(int code, String msg, String authRs);
    }

    private static final String TAG = "DDClient";

    public static final String APP_ID = "dingoag4oupdq1dtn6b9ln";

    private static final String ACTION_DD_LOGIN_RES = "action_dd_login_rs";

    private IDDShareApi mApi;

    private String mReqState;

    private final Context mActivity;

    private AuthListener mListener;

    private final LocalReceiver mReceiver;

    public DDClient(Context activity) {
        mActivity = activity;
        mReceiver = new LocalReceiver();
        init(activity);
    }

    public void destroy() {
        try {
            mActivity.unregisterReceiver(mReceiver);
        } catch (Exception ignore) {}
    }

    public void setAuthListener(AuthListener l) {
        mListener = l;
    }

    public boolean sendAuth() {
        if(mReqState != null) {
            return false;
        }
        mReqState = UUID.randomUUID().toString();
        SendAuth.Req req = new SendAuth.Req();
        req.scope = SendAuth.Req.SNS_LOGIN;
        req.state = mReqState;
        return mApi.sendReq(req);
    }

    public String checkSupport() {
        boolean isInstalled = mApi.isDDAppInstalled();
        if(!isInstalled) {
            return "未安装钉钉";
        }
        SendAuth.Req req = new SendAuth.Req();

        if(req.getSupportVersion() > mApi.getDDSupportAPI()) {
            return "钉钉版本过低，不支持登录授权";
        }

        return null;
    }

    private void init(Context activity) {
        Log.d(TAG, "init");
        createApi(activity);
        IntentFilter filter = new IntentFilter(ACTION_DD_LOGIN_RES);
        activity.registerReceiver(mReceiver, filter);
    }

    private void createApi(Context context) {
        mApi = DDShareApiFactory.createDDShareApi(context, APP_ID, true);
    }

    private void notifyAuthResult(int code, String msg, String authRs) {
        AuthListener l = mListener;
        if(l != null) {
            l.onDDAuthResult(code, msg, authRs);
        }
    }

    private void handleAuthSuccess(String code, String state) {
        if(state == null || !state.equals(mReqState)) {
            Toast.makeText(mActivity, "状态异常", Toast.LENGTH_SHORT).show();
            return;
        }
        notifyAuthResult(RESULT_OK, "OK", code);
    }

    private void handleAuthCanceled() {
        notifyAuthResult(RESULT_CANCEL, "授权取消", null);
    }

    private void handleAuthFailed(String errMsg) {
        notifyAuthResult(RESULT_ERR, errMsg, null);
    }

    private class LocalReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            int errCode = intent.getIntExtra("errCode", -99);
            String errMsg = intent.getStringExtra("errMsg");
            String code = intent.getStringExtra("code");
            String state = intent.getStringExtra("state");
            switch (errCode){
                case BaseResp.ErrCode.ERR_OK:
                    handleAuthSuccess(code, state);
                    break;
                case BaseResp.ErrCode.ERR_USER_CANCEL:
                    handleAuthCanceled();
                    break;
                default:
                    handleAuthFailed(errMsg);
                    break;
            }
            mReqState = null;
        }
    }


    private static void sendBroadCast(Context context,
                                      int errCode,
                                      String errMsg,
                                      @Nullable String code, @Nullable String state) {
        Intent intent = new Intent(ACTION_DD_LOGIN_RES);
        intent.putExtra("errCode", errCode);
        intent.putExtra("errMsg", errMsg);
        intent.putExtra("code", code);
        intent.putExtra("state", state);
        context.sendBroadcast(intent);
    }

    @UiThread
    static void handleAuthResult(Context context, SendAuth.Resp authResp) {

        int errCode = authResp.mErrCode;
        String errMsg = authResp.mErrStr;

        switch (errCode){
            case BaseResp.ErrCode.ERR_OK:
                sendBroadCast(context, errCode, errMsg, authResp.code, authResp.state);
                break;
            case BaseResp.ErrCode.ERR_USER_CANCEL:
                sendBroadCast(context, errCode, "授权取消", null, authResp.state);
                break;
            default:
                sendBroadCast(context, errCode, "授权异常" + errMsg, null, authResp.state);
                break;
        }

    }

}
