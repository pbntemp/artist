package com.ober.palette;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.view.View;

/**
 * Created by ober on 19-4-27.
 */
public class PaletteGalleryItemView extends View {

    private boolean mHighLight;
    private Integer mColor;

    private Rect mNormalRect;

    public PaletteGalleryItemView(Context context) {
        super(context);
    }

    public void setColor(Integer mColor) {
        this.mColor = mColor;
    }

    public void setHighLight(boolean highLight) {
        this.mHighLight = highLight;
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        mNormalRect = new Rect();
        final int topSmall = (int) (h * 0.2f);
        mNormalRect.set(0, topSmall, w, h);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if(mColor == null || mNormalRect == null) {
            return;
        }
        if(mHighLight) {
            canvas.drawColor(mColor);
        } else {
            canvas.clipRect(mNormalRect);
            canvas.drawColor(mColor);
        }

    }
}
