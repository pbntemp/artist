package com.ober.palette;

import android.view.View;
import android.view.ViewGroup;

/**
 * Created by ober on 19-4-27.
 */

final class ColorPanelManager {

    public interface OnItemClickListener {
        void onCircleColorItemClicked(int position);
    }

    private PaletteCircleView[] views;

    private final int hintRadius;

    private int mSelectPosition = -1;

    private OnItemClickListener onItemClickListener;

    public ColorPanelManager(ViewGroup parent) {
        hintRadius = parent.getResources()
                .getDimensionPixelSize(R.dimen.s9);
        initViews(parent);
    }

    public void setData(int[] data) {
        for(int i = 0; i < 12; i++) {
            views[i].setColor(data[i]);
            views[i].invalidate();
        }
    }

    public void setData(int[] data, int[] status, int[] selected) {
        for(int i = 0; i < 12; i++) {
            views[i].setColor(data[i]);
            views[i].setHintOn(status[i] > 0);
            views[i].setColorRecorded(selected[i] > 0);
            views[i].invalidate();
        }
    }

    public void notifyDataSetChanged() {
        if(views == null) {
            return;
        }
        for(int i = 0; i < 12; i++) {
            views[i].invalidate();
        }
    }

    public void setStatus(int[] status) {
        for(int i = 0; i < 12; i++) {
            PaletteCircleView view = views[i];

            if(status[i] > 0) {
                if(!view.isHintOn()) {
                    view.setHintOn(true);
                    view.invalidate();
                }
            } else {
                if(view.isHintOn()) {
                    view.setHintOn(false);
                    view.invalidate();
                }
            }
        }
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public void setSelectPosition(int selectPosition) {
        if(selectPosition == mSelectPosition) {
            return;
        }
        if(selectPosition == -1) {
            views[mSelectPosition].setSelectHint(false);
            views[mSelectPosition].invalidate();
            mSelectPosition = -1;
        } else {
            if(mSelectPosition != -1) {
                views[mSelectPosition].setSelectHint(false);
                views[mSelectPosition].invalidate();
            }
            views[selectPosition].setSelectHint(true);
            views[selectPosition].invalidate();
            mSelectPosition = selectPosition;
        }
    }

    private void initViews(ViewGroup parent) {
        views = new PaletteCircleView[12];
        ViewGroup lineTop = (ViewGroup) parent.getChildAt(0);
        ViewGroup lineBottom = (ViewGroup) parent.getChildAt(1);
        views[0] = (PaletteCircleView) lineTop.getChildAt(0);
        views[1] = (PaletteCircleView) lineTop.getChildAt(1);
        views[2] = (PaletteCircleView) lineTop.getChildAt(2);
        views[3] = (PaletteCircleView) lineTop.getChildAt(3);
        views[4] = (PaletteCircleView) lineTop.getChildAt(4);
        views[5] = (PaletteCircleView) lineTop.getChildAt(5);
        views[6] = (PaletteCircleView) lineBottom.getChildAt(0);
        views[7] = (PaletteCircleView) lineBottom.getChildAt(1);
        views[8] = (PaletteCircleView) lineBottom.getChildAt(2);
        views[9] = (PaletteCircleView) lineBottom.getChildAt(3);
        views[10] = (PaletteCircleView) lineBottom.getChildAt(4);
        views[11] = (PaletteCircleView) lineBottom.getChildAt(5);
        for(int i = 0; i < 12; i++) {
            final int k = i;
            views[i].setHintRadius(hintRadius);
            views[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(onItemClickListener != null) {
                        onItemClickListener.onCircleColorItemClicked(k);
                    }
                }
            });
        }
    }

}
