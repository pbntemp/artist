package com.ober.palette;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.Gallery;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ober on 19-4-27.
 */
public class PaletteGalleryAdapter extends BaseAdapter {

    public static final int REAL_COUNT = Integer.MAX_VALUE / 2;

    private final List<PaletteColorBean> mData;
    private final int mItemWidth;

    private int mSelectedPosition = -1;
    private final boolean isInfinity;

    public PaletteGalleryAdapter(List<PaletteColorBean> data, int itemWidth) {
        mData = new ArrayList<>(data);
        mItemWidth = itemWidth;
        this.isInfinity = data.size() > 10;
    }

    public void setSelectedPosition(int selectedPosition) {
        this.mSelectedPosition = getLogicPosition(selectedPosition);
    }

    public int getLogicCount() {
        return mData.size();
    }

    public int getLogicPosition(int realPosition) {
        return realPosition % getLogicCount();
    }

    @Override
    public int getCount() {
        if(isInfinity) {
            return REAL_COUNT;
        } else {
            return mData.size();
        }
    }

    @Override
    public PaletteColorBean getItem(int position) {
        return mData.get(getLogicPosition(position));
    }

    @Override
    public long getItemId(int position) {
        return getLogicPosition(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final PaletteGalleryItemView colorView;
        final PaletteColorBean bean = getItem(position);
        if(convertView == null) {

            FrameLayout f = new FrameLayout(parent.getContext());

            FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(mItemWidth, -1);

            colorView = new PaletteGalleryItemView(parent.getContext());
            f.addView(colorView, lp);
            f.setTag(colorView);

            Gallery.LayoutParams glp = new Gallery.LayoutParams(-2, -1);
            f.setLayoutParams(glp);

            convertView = f;

        } else {
            colorView = (PaletteGalleryItemView) convertView.getTag();
        }

        colorView.setColor(bean.color);
        colorView.setHighLight(mSelectedPosition == getLogicPosition(position));

        return convertView;
    }


}
