package com.ober.palette;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.Gallery;

import java.util.List;

/**
 * Created by ober on 19-4-27.
 */
public class Palette extends FrameLayout {

    public interface ColorSelectedListener {
        //selection {page, position, paletteId}
        void onColorSelected(int[] selection, PaletteColorBean colorBean);
    }

    public Palette(@NonNull Context context) {
        super(context);
        init();
    }

    public Palette(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public Palette(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private ColorSelectedListener mColorSelectedListener;

    private ColorPanelManager mColorPanelManager;
    private Gallery gallery;
    private PaletteGalleryAdapter galleryAdapter;
    private int rectItemWidth;

    private List<PaletteColorBean> mColorBeans;
    private int mPaleteId = -1;

    private int[] mSelection = new int[] {-1, -1};
    private int mCurrentPage = -1;

    private void init() {
        inflate(getContext(), R.layout.view_palette, this);

        ViewGroup colorPanel = findViewById(R.id.colorPanel);
        mColorPanelManager = new ColorPanelManager(colorPanel);
        gallery = findViewById(R.id.gallery);
        gallery.setSpacing(0);
        int screenW = getResources().getDisplayMetrics().widthPixels;
        rectItemWidth = screenW / 15;
        gallery.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                performGalleryItemSelected(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void setColorSelectedListener(ColorSelectedListener colorSelectedListener) {
        this.mColorSelectedListener = colorSelectedListener;
    }

    public List<PaletteColorBean> getData() {
        return mColorBeans;
    }

    public int getPaleteId() {
        return mPaleteId;
    }

    public void notifyColorPanelDataSetChanged() {
        if(mColorPanelManager != null && gallery != null && galleryAdapter != null && galleryAdapter.getCount() > 0) {
           //mColorPanelManager.notifyDataSetChanged();
            performGalleryItemSelected(gallery.getSelectedItemPosition());
        }
    }

    public void setData(List<PaletteColorBean> colorBeans, int paletteId) {
        if(mPaleteId == paletteId) {
            return;
        }
        mPaleteId = paletteId;
        mColorBeans = colorBeans;
        galleryAdapter = new PaletteGalleryAdapter(colorBeans, rectItemWidth);
        gallery.setAdapter(galleryAdapter);
        setGallerySelectionInternal(8);
        mSelection[0] = -1;
        mSelection[1] = -1;
        mCurrentPage = 8;

        mColorPanelManager.setOnItemClickListener(new ColorPanelManager.OnItemClickListener() {
            @Override
            public void onCircleColorItemClicked(int position) {
                mSelection[0] = mCurrentPage;
                mSelection[1] = position;
                mColorPanelManager.setSelectPosition(position);
                if(mColorSelectedListener != null) {
                    int[] selection = new int[3];
                    selection[0] = mCurrentPage;
                    selection[1] = position;
                    selection[2] = mPaleteId;
                    PaletteColorBean colorBean = mColorBeans.get(mCurrentPage);
                    mColorSelectedListener.onColorSelected(selection, colorBean);
                }
            }
        });
    }

    private void performGalleryItemSelected(int galleryPosition) {
        galleryAdapter.setSelectedPosition(galleryPosition);
        galleryAdapter.notifyDataSetChanged();

        int pos = galleryAdapter.getLogicPosition(galleryPosition);
        PaletteColorBean colorBean = mColorBeans.get(pos);
        int[] data = colorBean.data;
        int[] status = colorBean.state;
        int[] selected = colorBean.selected;
        mColorPanelManager.setData(data, status, selected);

        mCurrentPage = pos;

        if(mSelection[0] == pos) {
            mColorPanelManager.setSelectPosition(mSelection[1]);
        } else {
            mColorPanelManager.setSelectPosition(-1);
        }
    }

    private void setGallerySelectionInternal(int pos) {
        int count = mColorBeans.size();
        gallery.setSelection(count * (PaletteGalleryAdapter.REAL_COUNT / 2 / count) + pos, false);
    }

    public void setSelection(int[] selection) {
        if(selection == null) {
            throw new NullPointerException();
        }

        mSelection[0] = selection[0];
        mSelection[1] = selection[1];

        setGallerySelectionInternal(selection[0]);
        mColorPanelManager.setSelectPosition(selection[1]);
    }

    public void setSelectionByColor(int color) {
        int count = mColorBeans.size();
        for(int i = 0; i < count; i++) {
            PaletteColorBean group = mColorBeans.get(i);
            int[] colors = group.data;
            int len = colors.length;
            for(int j = 0; j < len; j++) {
                if(colors[j] == color) {
                    setSelection(new int[] {i, j});
                    return;
                }
            }
        }

        //throw new RuntimeException("data err " + color);
    }

}
