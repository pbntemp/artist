package com.ober.palette;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by ober on 19-4-27.
 */
public class PaletteCircleView extends View {

    private Integer mColor;
    private Paint mPaint;
    private int mRadius;
    private boolean mSelected;
    private boolean mHintOn;
    private int mHintRadius;
    private boolean mColorRecorded;

    private int mHintStrokeWidth;

    public PaletteCircleView(Context context) {
        super(context);
        init();
    }

    public PaletteCircleView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public void setHintRadius(int hintRadius) {
        this.mHintRadius = hintRadius;
    }

    public void setColor(Integer color) {
        mColor = color;
    }

    public void setSelectHint(boolean selected) {
        mSelected = selected;
    }

    public void setColorRecorded(boolean colorRecorded) {
        this.mColorRecorded = colorRecorded;
    }

    public void setHintOn(boolean on) {
        this.mHintOn = on;
    }

    public boolean isHintOn() {
        return mHintOn;
    }

    private void init() {
        mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mHintStrokeWidth = getResources().getDimensionPixelSize(R.dimen.s2);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        mRadius = w / 2;
    }

    private static final float sin45 = (float) Math.sin(45f / (2f * Math.PI));

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if(mColor == null) {
            return;
        }

        float halfWidth = getWidth() / 2f;
        float halfHeight = getHeight() / 2f;
        float redRadius = mRadius >> 2;

        mPaint.setStyle(Paint.Style.FILL);
        mPaint.setColor(mColor);
        canvas.drawCircle(halfWidth, halfHeight, mRadius, mPaint);

        if(mSelected) {
            mPaint.setColor(Color.WHITE);
            canvas.drawCircle(halfWidth, halfHeight, mHintRadius, mPaint);
        }

        if (mColorRecorded){
            mPaint.setColor(Color.RED);
            canvas.drawCircle(getWidth() - redRadius, redRadius, redRadius, mPaint);
        }

        if(mHintOn) {
            mPaint.setStyle(Paint.Style.STROKE);
            mPaint.setColor(Color.WHITE);
            mPaint.setStrokeWidth(3f);

            float r = mHintRadius * 1.5f;
            float offset = r * sin45;

            canvas.drawCircle(halfWidth, halfHeight, r, mPaint);
            if(!mSelected) {
                canvas.drawLine(halfWidth + offset - 1, halfHeight - offset + 1, halfWidth - offset + 1, halfHeight + offset - 1, mPaint);
            }
        }

    }
}
