package com.ober.palette;

/**
 * Created by ober on 19-4-27.
 */
public class PaletteColorBean {

    public final int color;
    public final int[] data; //这个调色盘的颜色 int[12]

    public final int[] state;
    public final int[] selected;

    public PaletteColorBean(int color, int[] data) {
        this.color = color;
        this.data = data;
        this.state = new int[data.length];
        this.selected = new int[data.length];
    }

    public void setState(int[] state) {
        int len = state.length;
        for(int i = 0; i < len; i ++) {
            this.state[i] = state[i];
        }
    }

}
